<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
$firstBit = new CFirstbitBeautyshop();

use Bitrix\Main\Loader,
	Bitrix\Main\ModuleManager;
?>
<div class="container_16">
	<? if ($isFilter) { ?>
		<div class="grid_4 grid_16_xs">
			<?
			if($arParams['SHOW_SUBSECTIONS'] == 'Y') {
				?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:catalog.section.list",
					"subsection_list",
					Array(
						"VIEW_MODE" => "LINE",
						"SHOW_PARENT_NAME" => "Y",
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
						"COUNT_ELEMENTS" => "Y",
						"TOP_DEPTH" => "1",
						"SECTION_FIELDS" => "",
						"SECTION_USER_FIELDS" => "",
						"ADD_SECTIONS_CHAIN" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_NOTES" => "",
						"CACHE_GROUPS" => "Y"
					),
					$component,
					array('HIDE_ICONS' => 'Y')
				);?>
				<?
			}
			?>
			<? $APPLICATION->IncludeComponent(
				"bitrix:catalog.smart.filter",
				"",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"SECTION_ID" => $arCurSection['ID'],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SAVE_IN_SESSION" => "N",
					"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
					"DISPLAY_ELEMENT_COUNT" => "Y",
					"XML_EXPORT" => "Y",
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"SECTION_TITLE" => "NAME",
					"SECTION_DESCRIPTION" => "DESCRIPTION",
					'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
					"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
					'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					"SEF_MODE" => $arParams["SEF_MODE"],
					"SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
					"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
					"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
				),
				$component,
				array('HIDE_ICONS' => 'Y')
			); ?>
		</div>
	<? } ?>

	<div class="<?=($isFilter ? "grid_12" : "grid_16")?> relative">
		<?
		\Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("catalog_section_area");
		if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y') {
			$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
		} else {
			$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
		}
		$intSectionID = 0;
		?>
		<?
		$catalogView['DEFAULT'] = array("TEMPLATE" => $firstBit->options['CATALOG_TEMPLATE_INITIAL']) +
			array("ELEMENTS" => $firstBit->options['CATALOG_ELEMENTS_INITIAL']) +
			array("SORT_FIELD" => 'SHOW') +
			array("SORT_ORDER" => 'desc');

		$catalogView['SORT_PROPERTIES'] = array(
			"SHOW" => array('NAME' => GetMessage('CT_BCS_CATALOG_SORT_FIELD_SHOW'), 'ORDER_1' => "desc"),
			"PROPERTY_MINIMUM_PRICE" => array('NAME' => GetMessage('CT_BCS_CATALOG_SORT_FIELD_PRICE'), 'ORDER_1' => 'asc', 'ORDER_2' => 'desc'),
			"NAME" => array('NAME' => GetMessage('CT_BCS_CATALOG_SORT_FIELD_NAME'), 'ORDER_1' => 'asc')
		);

		$catalogView['TEMPLATES'] = array('TILE', 'LIST_BIG', 'LIST');
		foreach($catalogView['TEMPLATES'] as $arTemplate) {
			if($firstBit->options['CATALOG_TEMPLATE_ORDER_'.$arTemplate.'_HIDE'] != 'Y') {
				$arTemplateOrder = $firstBit->options['CATALOG_TEMPLATE_ORDER_' . $arTemplate];
				$catalogView['SAVED'][$arTemplateOrder]['CODE'] = $arTemplate;
				if($firstBit->options['CATALOG_ELEMENTS_'.$arTemplate.'_HIDE'] != 'Y') {
					$catalogView['SAVED'][$arTemplateOrder]['ELEMENTS'] = explode(',',str_replace(" ","",$firstBit->options['CATALOG_ELEMENTS_'.$arTemplate]));
					if(
						!isset($catalogView['SAVED'][$arTemplateOrder]['ELEMENTS']) ||
						!is_array($catalogView['SAVED'][$arTemplateOrder]['ELEMENTS']) ||
						count($catalogView['SAVED'][$arTemplateOrder]['ELEMENTS'])<=0
					) {
						$catalogView['SAVED'][$arTemplateOrder]['ELEMENTS'][] = $catalogView['DEFAULT']['ELEMENTS'];
					}
				} else {
					$catalogView['SAVED'][$arTemplateOrder]['ELEMENTS'][] = $catalogView['DEFAULT']['ELEMENTS'];
				}
			}
		}
		unset($arTemplate);
		ksort($catalogView['SAVED']);
		foreach($catalogView['SAVED'] as $arTemplate) {
			$catalogView['SAVED_tmp'][] = $arTemplate;
		}
		$catalogView['SAVED'] = $catalogView['SAVED_tmp'];
		unset($catalogView['SAVED_tmp']);

		if(count($catalogView['SAVED'])>0) {

			if(!$catalogView['COOKIE'] = json_decode($_COOKIE["CATALOG_VIEW"])) {
				$defaultOptions = $catalogView['DEFAULT'];
			} else {
				$catalogView['USER']['TEMPLATE'] = $catalogView['COOKIE']->TEMPLATE;
				$catalogView['USER']['ELEMENTS'] = $catalogView['COOKIE']->ELEMENTS;
				$catalogView['USER']['SORT_FIELD'] = $catalogView['COOKIE']->SORT_FIELD;
				$catalogView['USER']['SORT_ORDER'] = $catalogView['COOKIE']->SORT_ORDER;
				$defaultOptions = $catalogView['USER'];
			}
			foreach ($catalogView['SAVED'] as $arTemplateIndex=>$arTemplate) {
				if($defaultOptions['TEMPLATE'] == $arTemplate['CODE']) {
					$catalogView['CURRENT']['INDEX'] = $arTemplateIndex;
					break;
				}
			}
			if(!isset($catalogView['CURRENT']['INDEX'])) {
				$catalogView['CURRENT']['INDEX'] = 0;
			}
			$catalogView['CURRENT']['TEMPLATE'] = $catalogView['SAVED'][$catalogView['CURRENT']['INDEX']]['CODE'];
			if(!$catalogView['COOKIE']) {
				$catalogView['CURRENT']['ELEMENTS'] = $catalogView['SAVED'][$catalogView['CURRENT']['INDEX']]['ELEMENTS'][0];
			} else {
				if($catalogView['USER']['ELEMENTS'] != 10000 && !in_array($catalogView['USER']['ELEMENTS'], $catalogView['SAVED'][$catalogView['CURRENT']['INDEX']]['ELEMENTS'])) {
					$catalogView['CURRENT']['ELEMENTS'] = $catalogView['SAVED'][$catalogView['CURRENT']['INDEX']]['ELEMENTS'][$firstBit->searchNearest($catalogView['USER']['ELEMENTS'], $catalogView['SAVED'][$catalogView['CURRENT']['INDEX']]['ELEMENTS'])];
				} else {
					$catalogView['CURRENT']['ELEMENTS'] = $catalogView['USER']['ELEMENTS'];
				}
			}
			$catalogView['CURRENT']['SORT_FIELD'] = $defaultOptions['SORT_FIELD'];
			$catalogView['CURRENT']['SORT_ORDER'] = $defaultOptions['SORT_ORDER'];
		} else {
			$catalogView['CURRENT'] = $catalogView['DEFAULT'];
		}
		setcookie("CATALOG_VIEW", json_encode($catalogView['CURRENT']),time()+60*60*24*30,'/');
		?>
		<?
		\Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("catalog_section_area", "");
		?>
		<? $intSectionID = $APPLICATION->IncludeComponent(
			"bitrix:catalog.section",
			"",
			array(
				"AJAX_MODE" => "Y",
				"AJAX_OPTION_JUMP" => "N",
				"CATALOG_SECTION_TEMPLATE" => $catalogView['CURRENT']['TEMPLATE'],
				"CATALOG_SORT_PARAMS" => $catalogView,
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ELEMENT_SORT_FIELD" => $catalogView['CURRENT']['SORT_FIELD'],
				"ELEMENT_SORT_ORDER" => $catalogView['CURRENT']['SORT_ORDER'],
				"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
				"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
				"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
				"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
				"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
				"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
				"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
				"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
				"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
				"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
				"USE_FILTER" => $arParams["USE_FILTER"],
				"FILTER_NAME" => $arParams["FILTER_NAME"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_FILTER" => $arParams["CACHE_FILTER"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"SET_TITLE" => $arParams["SET_TITLE"],
				"MESSAGE_404" => $arParams["MESSAGE_404"],
				"SET_STATUS_404" => $arParams["SET_STATUS_404"],
				"SHOW_404" => $arParams["SHOW_404"],
				"FILE_404" => $arParams["FILE_404"],
				"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
				"PAGE_ELEMENT_COUNT" => $_REQUEST['p'] ? $catalogView['CURRENT']['ELEMENTS'] * $_REQUEST['p']++ : $catalogView['CURRENT']['ELEMENTS'],
				"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
				"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
				"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
				"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
				"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
				"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
				"PAGER_TITLE" => $arParams["PAGER_TITLE"],
				"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
				"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
				"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
				"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
				"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
				"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
				"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
				"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
				"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
				"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
				"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
				"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
				"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
				"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
				"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
				"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
				"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
				"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
				"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
				"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
				"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
				'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
				'CURRENCY_ID' => $arParams['CURRENCY_ID'],
				'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
				'LABEL_PROP' => $arParams['LABEL_PROP'],
				'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
				'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
				'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
				'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
				'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
				'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
				'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
				'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
				'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
				'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
				'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
				'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
				'MESS_NULL_PRICE' => $arParams['MESS_NULL_PRICE'],
				"ADD_SECTIONS_CHAIN" => $arParams['ADD_SECTIONS_CHAIN'],
				'ADD_TO_BASKET_ACTION' => $basketAction,
				'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
				'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
				'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
				'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
				"USE_SALE" => $arParams['USE_SALE'],
				"SALE_IBLOCK_TYPE_ID" => $arParams['SALE_IBLOCK_TYPE_ID'],
				"SALE_IBLOCK_ID" => $arParams['SALE_IBLOCK_ID'],
				"DISPLAY_WISHLIST" => $arParams['DISPLAY_WISHLIST'],
				"MESS_BTN_WISHLIST" => $arParams['MESS_BTN_WISHLIST'],
				"USE_QUICKBUY" => $arParams['USE_QUICKBUY'],
				"MESS_BTN_QUICKBUY" => $arParams['MESS_BTN_QUICKBUY']
			),
			$component
		); ?>
		<?
		$GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;
		unset($basketAction);
		?>
	</div>
	<div class="clear"></div>
</div>