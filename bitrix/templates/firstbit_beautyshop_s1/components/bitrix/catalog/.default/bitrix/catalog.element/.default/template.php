<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$ComponentPath = $this->GetFolder();

$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	'QUICKBUY_BUTTON' => $strMainID.'_quickbuy',
	'PRICE_REQUEST_BUTTON' => $strMainID.'_price_request',
	'NOT_AVAILABLE_FLAG' => $strMainID.'_not_avail_flag',
	'AVAILABLE_FLAG' => $strMainID.'_avail_flag',
	'BUY_BTN_WRAPPER' => $strMainID.'_buy_btn_wrapper',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != '' ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] : $arResult['NAME']);
$strAlt = (isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != '' ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] : $arResult['NAME']);

if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
	$canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
} else {
	$canBuy = $arResult['CAN_BUY'];
}

reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']);

$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));
$wishlistBtnMessage = ($arParams['MESS_BTN_WISHLIST'] != '' ? $arParams['MESS_BTN_WISHLIST'] : GetMessage('CT_BCE_TPL_MESS_BTN_WISHLIST'));
?>

<div class="row product_card" id="<? echo $arItemIDs['ID']; ?>">
	<div class="container_16 margin_xs_0">
		<div class="grid_8 grid_9_sm grid_16_xs margin_xs_0">
			<div class="product_photo_wrapper" id="<? echo $arItemIDs['BIG_SLIDER_ID']; ?>">
				<div class="product_photo_big" id="<? echo $arItemIDs['BIG_IMG_CONT_ID']; ?>">
					<div class="product_photo_big_container">
						<span class="product_photo_big_aligner">
							<img id="<?=$arItemIDs['PICT']?>" src="<?=$arFirstPhoto['SRC']?>" alt="<?=$strAlt?>" title="<?=$strTitle?>">
						</span>
					</div>
				</div>
				<?
				if ($arResult['SHOW_SLIDER']) {
					if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS'])) {
						?>
						<div class="preview_list_slider_container" id="<? echo $arItemIDs['SLIDER_CONT_ID']; ?>">
							<div class="preview_list_slider_scroller_container">
								<div class="preview_list_slider">
									<ul id="<? echo $arItemIDs['SLIDER_LIST']; ?>" class="slider_initial">
										<?
										if(count($arResult['MORE_PHOTO'])>1) {
											$i = '0';
											foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto) {
												?>
												<li data-value="<? echo $arOnePhoto['ID']; ?>" <?=($i==0 ? 'class="bx_active"' : '')?>>
													<span class="image_container_wrapper">
														<span class="image_container" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span>
													</span>
												</li>
												<?
												$i++;
											}
											unset($i);
										}
										?>
									</ul>
									<script type="text/javascript">
										$(document).ready(function(){
											var owl_preview_slider_<? echo $strMainID;?> = $('#<? echo $arItemIDs['SLIDER_LIST']; ?>');
											owl_preview_slider_<? echo $strMainID;?>.owlCarousel({
												responsive: {
													0: {
														items: 3,
													},
													1280: {
														items: 5,
													}
												},
												loop: false,
												nav: true,
												dots: false,
												smartSpeed: 500,
												navContainer: '#owl_preview_slider_<? echo $strMainID;?>_nav',
												navText: ["<i class=\"fa fa-angle-left\"></i>","<i class=\"fa fa-angle-right\"></i>"],
												afterInit: initSlider(owl_preview_slider_<? echo $strMainID;?>),
											});
										});
									</script>
								</div>
							</div>
						</div>
						<?
					} else {
						foreach ($arResult['OFFERS'] as $key => $arOneOffer) {
								?>
								<div class="preview_list_slider_container" id="<? echo $arItemIDs['SLIDER_CONT_OF_ID'] . $arOneOffer['ID']; ?>">
									<div class="preview_list_slider_scroller_container">
										<div class="grid_16 relative owl_nav small" id="owl_preview_slider_<? echo $arItemIDs['SLIDER_LIST_OF_ID'].$arOneOffer['ID']; ?>_nav"></div>
										<div class="preview_list_slider">
											<ul id="<? echo $arItemIDs['SLIDER_LIST_OF_ID'] . $arOneOffer['ID']; ?>" class="slider_initial">
												<?
												if(count($arOneOffer['MORE_PHOTO'])>1) {
													foreach($arOneOffer['MORE_PHOTO'] as &$arOnePhoto) {
														?>
														<li data-value="<? echo $arOneOffer['ID'] . '_' . $arOnePhoto['ID']; ?>">
															<span class="image_container_wrapper">
																<span class="image_container" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span>
															</span>
														</li>
														<?
													}
												}
												?>
											</ul>
											<script type="text/javascript">
												$(document).ready(function () {
													var owl_preview_slider_<? echo $arItemIDs['SLIDER_LIST_OF_ID'] . $arOneOffer['ID']; ?> = $('#<? echo $arItemIDs['SLIDER_LIST_OF_ID'] . $arOneOffer['ID']; ?>');
													owl_preview_slider_<? echo $arItemIDs['SLIDER_LIST_OF_ID'].$arOneOffer['ID']; ?>.owlCarousel({
														responsive: {
															0: {
																items: 3,
															},
															1280: {
																items: 5,
															}
														},
														loop: false,
														nav: true,
														dots: false,
														smartSpeed: 500,
														navContainer: '#owl_preview_slider_<? echo $arItemIDs['SLIDER_LIST_OF_ID'].$arOneOffer['ID']; ?>_nav',
														navText: ["<i class=\"fa fa-angle-left\"></i>","<i class=\"fa fa-angle-right\"></i>"],
														afterInit: initSlider(owl_preview_slider_<? echo $arItemIDs['SLIDER_LIST_OF_ID'].$arOneOffer['ID']; ?>),
													});
												});
											</script>
										</div>
									</div>
								</div>
								<?
						}
					}
					unset($arOnePhoto);
				}
				?>
				<div class="product_photo_inner">
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="grid_8 grid_7_sm grid_16_xs margin_xs_0">
			<div class="product_short_descr_wrapper">
				<ul class="short_descr_list">
					<li class="short_descr_item">
						<div class="item_price">
							<div class="pull_left grid_11_sm grid_11_xs">
								<div class="price_wrapper">
									<div class="price_wrapper_inner">
										<div class="current_price" id="<? echo $arItemIDs['PRICE']; ?>">
										<?
										$minPrice = false;
										$minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
										if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS'])) {
											$canBuy = $arResult['CAN_BUY'];
										} else {
											$canBuy = $arResult['JS_OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
										}
										if (!empty($minPrice) && $minPrice['DISCOUNT_VALUE']>0) {
											echo $minPrice['PRINT_DISCOUNT_VALUE'];
										} else {
											$notAvailableMessage = ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessage('CT_BCE_CATALOG_NULL_PRICE'));
											echo $notAvailableMessage;
										}
										?>
										</div>
										<?
										if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) {
											?>
											<span class="old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>"><? echo $minPrice['PRINT_VALUE']; ?></span>
											<?
										}
										?>
									</div>
								</div>
								<?
								if($arParams['USE_SALE'] == 'Y') {
									if($arResult['SALE_ITEM']) {
										?>
										<h3 class="timer_inner_big_title"><?=GetMessage('CT_BCE_CATALOG_SALE_TO')?></h3>
										<a href="<? echo $arResult['SALE_ITEM']['DETAIL_PAGE_URL']; ?>" title="<? echo $arResult['SALE_ITEM']['NAME']; ?>">
											<span id="saleTimer" class="timer_inner_big" data-final-date="<? echo date("Y/m/d H:i:s", strtotime($arResult['SALE_ITEM']['PROPERTY_SALE_TO_VALUE'])); ?>"></span>
										</a>
										<script type="text/javascript">
											$('#saleTimer').countdown({
												until: new Date($('#saleTimer').attr('data-final-date')),
												layout: '<span class="timer_digit_group"><span class="timer_digit">{d10}</span><span class="timer_digit">{d1}</span><span class="timer_digit_label">{dl}</span></span>' +
												'<span class="timer_digit_separator">{sep}</span>' +
												'<span class="timer_digit_group"><span class="timer_digit">{h10}</span><span class="timer_digit">{h1}</span><span class="timer_digit_label">{hl}</span></span>' +
												'<span class="timer_digit_separator">{sep}</span>' +
												'<span class="timer_digit_group"><span class="timer_digit">{m10}</span><span class="timer_digit">{m1}</span><span class="timer_digit_label">{ml}</span></span>'
											});
										</script>
										<?
									}
								}
								?>
							</div>
						</div>
						<?
						$useBrands = ('Y' == $arParams['BRAND_USE']);
						if ($useBrands && $arResult['PROPERTIES']['BRAND_REF']['VALUE']) {
							?>
							<div class="pull_right grid_5_sm grid_5_xs">
								<div class="product_brand_wrapper">
									<img
										src="<?= $arResult['PROPERTIES']['BRAND_REF']['VALUE']['PREVIEW_PICTURE']['SRC'] ?>"/>
									<a href="<?= $arResult['PROPERTIES']['BRAND_REF']['VALUE']['DETAIL_PAGE_URL'] ?>" class="more_product"><?= GetMessage("BRAND_PRODUCTS") ?></a>
								</div>
							</div>
							<?
						}
						unset($useBrands);
						?>
					</li>
					<li class="short_descr_item">
						<div class="availability_inner">
							<span id="<? echo $arItemIDs['AVAILABLE_FLAG']; ?>" class="available_flag" style="<? echo ($canBuy ? 'display: inline-block;' : 'display: none;'); ?>"><i class="fa fa-check"></i> <?=GetMessage('CT_BCE_CATALOG_AVAILABLE')?></span>
							<span id="<? echo $arItemIDs['NOT_AVAILABLE_FLAG']; ?>" class="not_available_flag" style="<? echo ($canBuy ? 'display: none;' : 'display: inline-block;'); ?>"><i class="fa fa-ban"></i> <?=GetMessage('CT_BCE_CATALOG_NOT_AVAILABLE')?></span>
							<?
							if ($arParams['DISPLAY_COMPARE']) {
								if (is_array($_SESSION['CATALOG_COMPARE_LIST'][$arParams["IBLOCK_ID"]]['ITEMS']) && array_key_exists($arItem['ID'], $_SESSION['CATALOG_COMPARE_LIST'][$arParams["IBLOCK_ID"]]['ITEMS'])) {
									$compareClass = "active";
								} else {
									$compareClass = "";
								}
								?>
								<a id="<? echo $arItemIDs['COMPARE_LINK']; ?>" data-compare-id="<? echo $arResult['ID']; ?>" class="fa fa-bar-chart <? echo $compareClass; ?>" title="<? echo $compareBtnMessage; ?>" href="" onclick="compareHandler(this);return false;"></a>
								<script type="text/javascript">
									compareCheck(<?=$arResult['ID']?>);
								</script>
								<?
							}
							?>
							<?
							if ($arParams['DISPLAY_WISHLIST']) {
								?>
								<a data-wishlist-id="item_<? echo $arResult['ID']; ?>" class="fa fa-heart" title="<? echo $wishlistBtnMessage; ?>" onclick="wishlistHandler(this);return false;"></a>
								<script type="text/javascript">
									wishlistSetItem('item_<?=$arResult['ID']?>');
								</script>
								<?
							}
							?>
						</div>
					</li>
					<?
					$APPLICATION->IncludeComponent("bitrix:catalog.brandblock", "advantages", array(
						"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
						"IBLOCK_ID" => $arParams['IBLOCK_ID'],
						"ELEMENT_ID" => $arResult['ID'],
						"ELEMENT_CODE" => "",
						"PROP_CODE" => 'ADVANTAGE_REF',
						"CACHE_TYPE" => $arParams['CACHE_TYPE'],
						"CACHE_TIME" => $arParams['CACHE_TIME'],
						"CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
						"WIDTH" => "",
						"HEIGHT" => ""
					),
						$component,
						array("HIDE_ICONS" => "Y")
					);
					?>
					<?
					if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) && !empty($arResult['OFFERS_PROP'])) {
						$arSkuProps = array();
						?>
						<li class="short_descr_item">
							<div class="product_options_wrapper" id="<? echo $arItemIDs['PROP_DIV']; ?>">
								<div class="mobile_product_param show_xs"><?=GetMessage('CT_BCE_CATALOG_PRODUCT_PROPS')?> <i class="fa fa-chevron-circle-up"></i></div>
								<ul class="product_options_list" style="">
									<?
									foreach ($arResult['SKU_PROPS'] as &$arProp) {
										if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']])) {
											continue;
										}
										$arSkuProps[] = array(
											'ID' => $arProp['ID'],
											'SHOW_MODE' => $arProp['SHOW_MODE'],
											'VALUES_COUNT' => $arProp['VALUES_COUNT']
										);
										if ('TEXT' == $arProp['SHOW_MODE']) {
											if (5 < $arProp['VALUES_COUNT']) {
												$strSlideStyle = '';
											} else {
												$strSlideStyle = 'display: none;';
											}
											?>
											<li class="product_options_item" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
												<span class="options_title"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
												<div class="bx_size_scroller_container">
													<div class="product_params product_params_size">
														<ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" class="product_params_list">
															<?
															foreach ($arProp['VALUES'] as $arOneValue){
																$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
																?>
																<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID']; ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" class="product_params_item" style="display: none;">
																	<i title="<? echo $arOneValue['NAME']; ?>"></i><span class="cnt" title="<? echo $arOneValue['NAME']; ?>"><? echo $arOneValue['NAME']; ?></span></li>
																<?
															}
															?>
														</ul>
													</div>
													<div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
													<div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
												</div>
											</li>
										<?
										} elseif ('PICT' == $arProp['SHOW_MODE']) {
											if (5 < $arProp['VALUES_COUNT']) {
												$strSlideStyle = '';
											} else {
												$strSlideStyle = 'display: none;';
											}
											?>
											<li class="product_options_item" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
												<span class="options_title"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
												<div class="bx_scu_scroller_container">
													<div class="product_params product_params_sku">
														<ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" class="product_params_list">
															<?
															foreach ($arProp['VALUES'] as $arOneValue) {
																$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
																?>
																<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID'] ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" class="product_params_item" style="display: none;" >
																	<i title="<? echo $arOneValue['NAME']; ?>"></i>
																	<span class="cnt" style="background-image:url('<? echo $arOneValue['PICT']['SRC']; ?>');" title="<? echo $arOneValue['NAME']; ?>"><span class="cnt_item"></span></span></li>
																<?
															}
															?>
														</ul>
													</div>
													<div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
													<div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
												</div>
											</li>
										<?
										}
									}
									unset($arProp);
									?>
								</ul>
								<div class="clear"></div>
							</div>
						</li>
					<?
					}
					?>
					<li class="short_descr_item buy_btn_wrapper <? if(!empty($arResult['OFFERS'])) { ?>initial<? } ?> <? if(empty($arResult['OFFERS']) && !$canBuy) { ?>not_available<? } ?> <? if(empty($arResult['OFFERS']) && empty($minPrice) || empty($arResult['OFFERS']) && $minPrice['DISCOUNT_VALUE']<=0 ) { ?>null_price<? } ?>" id="<? echo $arItemIDs['BUY_BTN_WRAPPER']; ?>">
						<div class="product_buy_wrapper">
							<?
							$buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
							$addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
							$notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
							$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
							$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

							$showSubscribeBtn = false;
							$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));

							if ($arParams['USE_PRODUCT_QUANTITY'] == 'Y') {
								?>
								<div class="l20 product_count_wrapper">
									<span class="product_count">
										<a href="javascript:void(0)" class="btn_minus" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">-</a>
										<input id="<? echo $arItemIDs['QUANTITY']; ?>" type="text" class="count_value" value="<? echo (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) ? 1 : $arResult['CATALOG_MEASURE_RATIO'] ); ?>">
										<a href="javascript:void(0)" class="btn_plus" id="<? echo $arItemIDs['QUANTITY_UP']; ?>">+</a>
									</span>
								</div>
								<div class="l40 product_basket_wrapper">
									<span id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>">
										<?
										if ($showBuyBtn) {
											?>
											<button class="btn_round btn_color btn_wide" id="<? echo $arItemIDs['BUY_LINK']; ?>"><? echo $buyBtnMessage; ?></button>
											<?
										}
										if ($showAddBtn) {
											?>
											<button class="btn_round btn_color btn_wide" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><? echo $addToBasketBtnMessage; ?></button>
											<?
										}
										?>
									</span>
								</div>
								<span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable"><? echo $notAvailableMessage; ?></span>
								<?
								if ('Y' == $arParams['SHOW_MAX_QUANTITY']) {
									if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
										?>
										<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>" style="display: none;"><? echo GetMessage('OSTATOK'); ?>: <span></span></p>
										<?
									} else {
										if ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO']) {
											?>
											<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"><? echo GetMessage('OSTATOK'); ?>: <span><? echo $arResult['CATALOG_QUANTITY']; ?></span></p>
											<?
										}
									}
								}
							} else {
								?>
								<div class="item_buttons vam">
									<span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
										<?
										if ($showBuyBtn) {
											?>
											<a href="javascript:void(0);" class="btn_round btn_color btn_wide" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
											<?
										}
										if ($showAddBtn) {
											?>
											<a href="javascript:void(0);" class="btn_round btn_color btn_wide" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
											<?
										}
									?>
									</span>
									<span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable" style="display: <? echo (!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
								</div>
								<?
							}
							unset($showAddBtn, $showBuyBtn);
							if ($canBuy && $arParams['USE_QUICKBUY']) {
								$quickBuyMessage = ('' != $arParams['MESS_BTN_QUICKBUY'] ? $arParams['MESS_BTN_QUICKBUY'] : GetMessage('CT_BCE_TPL_MESS_BTN_QUICKBUY'));
								?>
								<div class="l40 product_quickbuy_wrapper">
									<span class="buy_btn" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
										<button class="btn_round btn_hover_color btn_border btn_wide" id="<? echo $arItemIDs['QUICKBUY_BUTTON']; ?>" data-quickbuy="" data-product-id="<? echo $arResult['ID']; ?>" title="<? echo $quickBuyMessage; ?>" onclick="modalQuickBuy(this);return false;"><? echo $quickBuyMessage; ?></button>
									</span>
								</div>
								<?
							}
							$priceRequestMessage = ('' != $arParams['MESS_BTN_PRICE_REQUEST'] ? $arParams['MESS_BTN_PRICE_REQUEST'] : GetMessage('CT_BCE_TPL_MESS_BTN_PRICE_REQUEST'));
							?>
							<div class="l40 price_request_wrapper">
								<button class="btn_round btn_color btn_wide" id="<? echo $arItemIDs['PRICE_REQUEST_BUTTON']; ?>" data-price-request="" data-product-id="<? echo $arResult['ID']; ?>" title="<? echo $priceRequestMessage; ?>" href="" onclick="modalPriceRequest(this);return false;"><? echo $priceRequestMessage; ?></button>
							</div>
						</div>
					</li>
					<li class="short_descr_item">
						<div class="social_share_wrapper">
							<span class="social_share_title hidden_sm hidden_xs"><? echo GetMessage('CT_BCE_CATALOG_SHARE'); ?></span>
							<div class="social_widget">
								<script type="text/javascript" src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
								<script type="text/javascript" src="https://yastatic.net/share2/share.js" charset="utf-8"></script>
								<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir" data-counter="" data-image-id="<?=$arItemIDs['PICT']?>" data-image=""></div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<section class="product_tab_sections">
	<div class="product_tabs">
		<div class="container_16 hidden_xs">
			<div class="grid_16">
				<ul class="btn-panel">
					<?
					if ('' != $arResult['DETAIL_TEXT']) {
						?>
						<li class="btn-panel-item"><a href="#item_tab_description"><? echo GetMessage('CATALOG_TAB_TITLE_DESCRIPTION'); ?></a></li>
						<?
					}
					?>
					<?
					if (!empty($arResult['DISPLAY_PROPERTIES'])) {
						?>
						<li class="btn-panel-item"><a href="#item_tab_properties"><? echo GetMessage('CATALOG_TAB_TITLE_PARAMETERS'); ?></a></li>
						<?
					}
					?>
					<?
					if (is_array($arResult['PROPERTIES']['VIDEO']['VALUE']) && !empty($arResult['PROPERTIES']['VIDEO']['VALUE'])) {
						?>
						<li class="btn-panel-item"><a href="#item_tab_video"><? echo GetMessage('CATALOG_TAB_TITLE_VIDEO'); ?></a></li>
						<?
					}
					?>
					<?
					if ('Y' == $arParams['USE_COMMENTS']) {
						?>
						<li class="btn-panel-item"><a href="#item_tab_comments"><? echo GetMessage('CATALOG_TAB_TITLE_COMMENTS'); ?></a></li>
						<?
					}
					?>
					<?
					if($arParams["USE_STORE"] == "Y") {
						?>
						<li class="btn-panel-item"><a href="#item_tab_stores"><? echo GetMessage('CATALOG_TAB_TITLE_STORE'); ?></a></li>
						<?
					}
					?>
					<?
					if(is_array($arResult['PROPERTIES']['DOCUMENTS']['VALUE']) && !empty($arResult['PROPERTIES']['DOCUMENTS']['VALUE'])) {
						?>
						<li class="btn-panel-item"><a href="#item_tab_files"><? echo GetMessage('CATALOG_TAB_TITLE_FILES'); ?></a></li>
						<?
					}
					?>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
		<div class="border_tabs hidden_xs"></div>
		<div class="container_16">
			<div class="grid_16">
				<?
				if ('' != $arResult['DETAIL_TEXT']) {
					?>
					<div class="mobile_tabs_title show_xs"><? echo GetMessage('CATALOG_TAB_TITLE_DESCRIPTION'); ?><i class="fa fa-chevron-circle-down"></i></div>
					<div id="item_tab_description" class="tabs_inner">
						<?
						if ('html' == $arResult['DETAIL_TEXT_TYPE']) {
							echo $arResult['DETAIL_TEXT'];
						} else {
							?>
							<p><? echo $arResult['DETAIL_TEXT']; ?></p>
							<?
						}
						?>
					</div>
					<?
				}
				if (!empty($arResult['DISPLAY_PROPERTIES'])) {
					?>
					<div class="mobile_tabs_title show_xs"><? echo GetMessage('CATALOG_TAB_TITLE_PARAMETERS'); ?><i class="fa fa-chevron-circle-down"></i></div>
					<div id="item_tab_properties" class="tabs_inner">
						<ul class="product_parametrs_wraper">
							<li class="product_parametrs_item">
								<div class="parametrs_body">
									<ul>
										<?
										foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp) {
											if($arOneProp['CODE'] == "VIDEO") continue;
											?>
												<li>
													<span><?=$arOneProp['NAME']?></span>
													<span><?=(is_array($arOneProp['DISPLAY_VALUE']) ? implode(' / ', $arOneProp['DISPLAY_VALUE']) : $arOneProp['DISPLAY_VALUE'])?></span>
												</li>
											<?
										}
										unset($arOneProp);
										?>
									</ul>
								</div>
							</li>
						</ul>
					</div>
					<?
				}
				if (is_array($arResult['PROPERTIES']['VIDEO']['VALUE']) && !empty($arResult['PROPERTIES']['VIDEO']['VALUE'])) {
					?>
					<div class="mobile_tabs_title show_xs"><? echo GetMessage('CATALOG_TAB_TITLE_VIDEO'); ?><i class="fa fa-chevron-circle-down"></i></div>
					<div id="item_tab_video" class="tabs_inner">
						<?
						foreach($arResult['PROPERTIES']['VIDEO']['VALUE'] as $arVideo) {
							?>
							<div class="video_frame grid_8 grid_8_sm grid_16_xs">
								<iframe width="100%" height="auto" src="https://www.youtube.com/embed/<?=$arVideo?>" frameborder="0" allowfullscreen></iframe>
							</div>
							<?
						}
						?>
						<div class="clear"></div>
					</div>
					<?
				}
				if ('Y' == $arParams['USE_COMMENTS']) {
					?>
					<div class="mobile_tabs_title show_xs"><? echo GetMessage('CATALOG_TAB_TITLE_COMMENTS'); ?><i class="fa fa-chevron-circle-down"></i></div>
					<div id="item_tab_comments" class="tabs_inner">
						<a href="#" id="comment_add" data-product-id="<?=$arResult["ID"]?>" onclick="modalCommentAdd(this);return false;" class="btn_round btn_border btn_hover_color comment_add" title="<?=GetMessage("CT_BCE_TPL_MESS_ADD_COMMENT")?>"><?=GetMessage("CT_BCE_TPL_MESS_ADD_COMMENT")?></a>
						<div class="reviews_wrapper">
							<?
							$GLOBALS['product_comments'] = array("PROPERTY_PRODUCT_ID" => $arResult["ID"]);
							?>
							<?$APPLICATION->IncludeComponent("bitrix:news.list","product_comments",Array(
									"DISPLAY_DATE" => "Y",
									"DISPLAY_NAME" => "Y",
									"DISPLAY_PICTURE" => "Y",
									"DISPLAY_PREVIEW_TEXT" => "Y",
									"AJAX_MODE" => "N",
									"IBLOCK_TYPE" => "firstbit_beautyshop_catalog",
									"IBLOCK_ID" => $arParams['DETAIL_COMMENTS_IBLOCK_ID'],
									"NEWS_COUNT" => "4",
									"SORT_BY1" => "ACTIVE_FROM",
									"SORT_ORDER1" => "DESC",
									"SORT_BY2" => "SORT",
									"SORT_ORDER2" => "ASC",
									"FILTER_NAME" => "product_comments",
									"FIELD_CODE" => Array("ID","DATE_CREATE"),
									"PROPERTY_CODE" => Array("PRODUCT_ID","USER_ID","USER_IP","USER_NAME","USER_EMAIL","USER_CITY","PRODUCT_RATING","REVIEW"),
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"PREVIEW_TRUNCATE_LEN" => "",
									"ACTIVE_DATE_FORMAT" => "d.m.Y",
									"SET_TITLE" => "N",
									"SET_BROWSER_TITLE" => "N",
									"SET_META_KEYWORDS" => "N",
									"SET_META_DESCRIPTION" => "N",
									"SET_LAST_MODIFIED" => "N",
									"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
									"ADD_SECTIONS_CHAIN" => "N",
									"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
									"PARENT_SECTION" => "",
									"PARENT_SECTION_CODE" => "",
									"INCLUDE_SUBSECTIONS" => "Y",
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "3600",
									"CACHE_FILTER" => "Y",
									"CACHE_GROUPS" => "Y",
									"DISPLAY_TOP_PAGER" => "Y",
									"DISPLAY_BOTTOM_PAGER" => "Y",
									"PAGER_TITLE" => "",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_TEMPLATE" => "",
									"PAGER_DESC_NUMBERING" => "Y",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL" => "Y",
									"PAGER_BASE_LINK_ENABLE" => "Y",
									"SET_STATUS_404" => "N",
									"SHOW_404" => "N",
									"MESSAGE_404" => "",
									"PAGER_BASE_LINK" => $APPLICATION->GetCurPage(true),
									"PAGER_PARAMS_NAME" => "arrPager",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"AJAX_OPTION_HISTORY" => "N",
									"AJAX_OPTION_ADDITIONAL" => ""
								)
							);?>
						</div>
					</div>
					<?
				}
				if($arParams["USE_STORE"] == "Y") {
					?>
					<div class="mobile_tabs_title show_xs"><? echo GetMessage('CATALOG_TAB_TITLE_STORE'); ?><i class="fa fa-chevron-circle-down"></i></div>
					<?
					$APPLICATION->IncludeComponent("bitrix:catalog.store.amount", "", array(
						"ELEMENT_ID" => $arResult["ID"],
						"STORE_PATH" => $arParams['STORE_PATH'],
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000",
						"MAIN_TITLE" => $arParams['MAIN_TITLE'],
						"USE_MIN_AMOUNT" => $arParams['USE_MIN_AMOUNT'],
						"MIN_AMOUNT" => $arParams['MIN_AMOUNT'],
						"STORES" => $arParams['STORES'],
						"SHOW_EMPTY_STORE" => $arParams['SHOW_EMPTY_STORE'],
						"SHOW_GENERAL_STORE_INFORMATION" => $arParams['SHOW_GENERAL_STORE_INFORMATION'],
						"USER_FIELDS" => $arParams['USER_FIELDS'],
						"FIELDS" => $arParams['FIELDS']
					),
					$component,
					array("HIDE_ICONS" => "Y")
					);?>
					<?
				}
				if(is_array($arResult['PROPERTIES']['DOCUMENTS']['VALUE']) && !empty($arResult['PROPERTIES']['DOCUMENTS']['VALUE'])) {
					?>
					<div class="mobile_tabs_title show_xs"><? echo GetMessage('CATALOG_TAB_TITLE_FILES'); ?><i class="fa fa-chevron-circle-down"></i></div>
					<div id="item_tab_files" class="tabs_inner">
						<div class="doc_wrapper">
							<ul class="doc_list">
								<?
								foreach($arResult['PROPERTIES']['DOCUMENTS']['VALUE'] as $arDocument) {
									$arDocumentSize = ($arDocument['FILE_SIZE'] < 1048576 ? round($arDocument['FILE_SIZE']/1024, 1).' '.GetMessage('CT_BCE_CATALOG_KB') : round($arDocument['FILE_SIZE']/1024/1024, 1).' '.GetMessage('CT_BCE_CATALOG_MB'));
									?>
									<li class="doc_item">
										<div class="i-container">
											<i class="fa fa-file-o" aria-hidden="true"></i>
											<div class="group">
												<a href="<?=$arDocument['SRC']?>"><?=$arDocument['DESCRIPTION']?></a>
												<div><?=$arDocumentSize?></div>
											</div>
										</div>
									</li>
									<?
								}
								?>
							</ul>
						</div>
					</div>
					<?
				}
				?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>

<div class="bx_item_detail <? echo $templateData['TEMPLATE_CLASS']; ?>">
	<div class="bx_item_container">
		<div class="bx_md">
			<div class="item_info_section">
				<?
				if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
					if ($arResult['OFFER_GROUP']) {
						foreach ($arResult['OFFER_GROUP_VALUES'] as $offerID) {
							?>
							<span id="<? echo $arItemIDs['OFFER_GROUP'].$offerID; ?>" style="display: none;">
								<?
								$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
									"",
									array(
										"IBLOCK_ID" => $arResult["OFFERS_IBLOCK"],
										"ELEMENT_ID" => $offerID,
										"PRICE_CODE" => $arParams["PRICE_CODE"],
										"BASKET_URL" => $arParams["BASKET_URL"],
										"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
										"CACHE_TYPE" => $arParams["CACHE_TYPE"],
										"CACHE_TIME" => $arParams["CACHE_TIME"],
										"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
										"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
										"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
										"CURRENCY_ID" => $arParams["CURRENCY_ID"]
									),
									$component,
									array("HIDE_ICONS" => "Y")
								);
								?>
							</span>
							<?
						}
					}
				} else {
					if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP']) {
						$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
							"",
							array(
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"ELEMENT_ID" => $arResult["ID"],
								"PRICE_CODE" => $arParams["PRICE_CODE"],
								"BASKET_URL" => $arParams["BASKET_URL"],
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
								"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
								"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
								"CURRENCY_ID" => $arParams["CURRENCY_ID"]
							),
							$component,
							array("HIDE_ICONS" => "Y")
						);
					}
				}
				?>
			</div>
		</div>
		<div style="clear: both;"></div>
	</div>
	<div class="clb"></div>
</div>
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
	foreach ($arResult['JS_OFFERS'] as &$arOneJS) {
		if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE']) {
			$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
			$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
		}
		$strProps = '';
		if ($arResult['SHOW_OFFERS_PROPS']) {
			if (!empty($arOneJS['DISPLAY_PROPERTIES'])) {
				foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp) {
					$strProps .= '<dt>'.$arOneProp['NAME'].'</dt><dd>'.(
						is_array($arOneProp['VALUE'])
						? implode(' / ', $arOneProp['VALUE'])
						: $arOneProp['VALUE']
					).'</dd>';
				}
			}
		}
		$arOneJS['DISPLAY_PROPERTIES'] = $strProps;
	}
	if (isset($arOneJS))
		unset($arOneJS);
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => true,
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
			'USE_STICKERS' => true,
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'DEFAULT_PICTURE' => array(
			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
		),
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'NAME' => $arResult['~NAME']
		),
		'BASKET' => array(
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'BASKET_URL' => $arParams['BASKET_URL'],
			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		),
		'OFFERS' => $arResult['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $arSkuProps
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
} else {
	$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties) {
		?>
		<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
			<?
			if (!empty($arResult['PRODUCT_PROPERTIES_FILL'])) {
				foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
					?>
					<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
					<?
					if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
						unset($arResult['PRODUCT_PROPERTIES'][$propID]);
				}
			}
			$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
			if (!$emptyProductProperties) {
				?>
				<table>
					<?
					foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
						?>
						<tr>
							<td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
							<td>
								<?
								if('L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE'] && 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']) {
									foreach($propInfo['VALUES'] as $valueID => $value) {
										?>
										<label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br>
										<?
									}
								} else {
									?>
									<select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]">
										<?
										foreach($propInfo['VALUES'] as $valueID => $value) {
											?>
											<option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option>
											<?
										}
										?>
									</select>
									<?
								}
								?>
							</td>
						</tr>
						<?
					}
					?>
				</table>
				<?
			}
			?>
		</div>
		<?
	}
	if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE']) {
		$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
		$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
	}
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
			'USE_STICKERS' => true,
		),
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'PICT' => $arFirstPhoto,
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'PRICE' => $arResult['MIN_PRICE'],
			'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
		),
		'BASKET' => array(
			'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	if ($arParams['DISPLAY_COMPARE']) {
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
	unset($emptyProductProperties);
}
?>

<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
BX.message({
	ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
	PRODUCT_NULL_PRICE: '<? echo ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessage('CT_BCE_CATALOG_NULL_PRICE')); ?>',
	PRODUCT_NOT_AVAILABLE: '<? echo ('' != $arParams['MESS_NOT_AVAILABLE'] ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE')) ?>',
	BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	PRODUCT_GIFT_LABEL: '<? echo GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL') ?>',
	SITE_ID: '<? echo SITE_ID; ?>',
	PRODUCT_NULL_PRICE: '<? echo ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessageJS('CT_BCE_CATALOG_NULL_PRICE')) ?>',
});
</script>