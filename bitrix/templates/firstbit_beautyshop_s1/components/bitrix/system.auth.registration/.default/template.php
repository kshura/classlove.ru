<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

ShowMessage($arParams["~AUTH_RESULT"]);

$APPLICATION->SetPageProperty("showLeftMenu", "N");
$APPLICATION->SetPageProperty("title", GetMessage('AUTH_REGISTER'));
$APPLICATION->SetTitle(GetMessage('AUTH_REGISTER'));

$APPLICATION->IncludeComponent(
	"bitrix:main.register",
	"",
	Array(
		"USER_PROPERTY_NAME" => "",
		"SEF_MODE" => "N",
		"SHOW_FIELDS" => Array(),
		"REQUIRED_FIELDS" => Array(),
		"AUTH" => "Y",
		"USE_BACKURL" => "Y",
		"SUCCESS_PAGE" => $APPLICATION->GetCurPageParam('',array('backurl')),
		"SET_TITLE" => "N",
		"USER_PROPERTY" => Array(),
		"PROPS_GROUP_USE" => "Y",
		"USE_AJAX_LOCATIONS" => "Y",
	)
);
?>
