<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
IncludeTemplateLangFile(__FILE__);
$this->setFrameMode(true);
?>
<?$this->SetViewTarget('pageTitle_subTitle');?>
	<div class="article_title_detail">
	<?if($arResult['DISPLAY_ACTIVE_FROM']):?>
		<div class="article_time"><?=$arResult['DISPLAY_ACTIVE_FROM']?></div>
	<?endif?>
	<?if($arResult['PROPERTIES']["ISSALE"]["VALUE"]):?>
		<div class="article_icon"><i class="i-sale-txt"><?=GetMessage("SALE")?></i></div>
	<?endif?>
	</div>
<?$this->EndViewTarget();?>
<div>
	<a class="link_back" href="<?=$arResult["LIST_PAGE_URL"]?>"><i class="fa fa-angle-left "></i><?=GetMessage("TOBACK")?></a>
</div>
<div class="article_detail_inner">
	<? if($arResult["DETAIL_PICTURE"]["SRC"]) {?>
		<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="" height="" title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>">
	<? } ?>
	<? if(strtotime($arResult['PROPERTIES']['SALE_FROM']['VALUE']) < time() && strtotime($arResult['PROPERTIES']['SALE_TO']['VALUE']) > time()) {
		?>
		<div class="sale_timer_container grey">
			<div class="grid_8 grid_8_sm grid_16_xs"><h2><?=GetMessage('NEWS_TPL_SALE_TO')?></h2>
			</div>
			<div class="grid_8 grid_8_sm grid_16_xs">
				<span id="saleTimer" class="timer_inner_big" data-final-date="<? echo date("Y/m/d H:i:s", strtotime($arResult['PROPERTIES']['SALE_TO']['VALUE'])); ?>"></span>
				<script type="text/javascript">
					$('#saleTimer').countdown({
						until: new Date($('#saleTimer').attr('data-final-date')),
						layout: '<span class="timer_digit_group"><span class="timer_digit">{d10}</span><span class="timer_digit">{d1}</span><span class="timer_digit_label">{dl}</span></span>' +
						'<span class="timer_digit_separator">{sep}</span>' +
						'<span class="timer_digit_group"><span class="timer_digit">{h10}</span><span class="timer_digit">{h1}</span><span class="timer_digit_label">{hl}</span></span>' +
						'<span class="timer_digit_separator">{sep}</span>' +
						'<span class="timer_digit_group"><span class="timer_digit">{m10}</span><span class="timer_digit">{m1}</span><span class="timer_digit_label">{ml}</span></span>' +
						'<span class="timer_digit_separator">{sep}</span>' +
						'<span class="timer_digit_group"><span class="timer_digit">{s10}</span><span class="timer_digit">{s1}</span><span class="timer_digit_label">{sl}</span></span>'
					});
				</script>
			</div>
			<div class="clear"></div>
		</div>
		<?
	}
	?>
	<div class="article_detail">
		<?=$arResult["DETAIL_TEXT"]?>
	</div>
	<div class="share_block">
		<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
		<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
		<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir" data-counter=""></div>
	</div>
</div>
