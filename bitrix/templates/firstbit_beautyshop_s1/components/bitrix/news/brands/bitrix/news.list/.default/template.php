<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if(!$brandsView = $_COOKIE["brands_view"]) {
	$brandsView = 'brands_img';
	setcookie("brands_view", $brandsView, time()+60*60*24*30,'/');
}
?>
<?$this->SetViewTarget('pageTitle_subTitle');?>
<div class="sort_wrapper">
	<div class="position_wrapper hidden_xs">
		<div class="position-container">
			<a class="position-item <? if($brandsView == 'brands_img') echo 'select'; ?>" href="#" data-brands-view="brands_img"><i class="fa fa-tile"></i></a>
			<a class="position-item <? if($brandsView == 'brands_list') echo 'select'; ?>" href="#" data-brands-view="brands_list"><i class="fa fa-list"></i></a>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?$this->EndViewTarget();?>

<div class="row brands">
	<div class="row grey">
		<div class="container_16">
			<div class="grid_16 margin_sm_10">
				<ul class="brands_letter_list hidden_xs">
					<?
					foreach($arResult['ITEMS'] as $brandGroup) {
						?>
						<li>
							<a href="#<?=$brandGroup['LETTER']?>" data-letter="<?=$brandGroup['LETTER']?>"><?=($brandGroup['LETTER'] == 'NUMERIC' || $brandGroup['LETTER'] == 'NON_LATIN' ? GetMessage('CT_BRANDS_LIST_'.$brandGroup['LETTER']) : $brandGroup['LETTER'])?></a>
						</li>
						<?
					}
					?>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="row">
		<div class="container_12">
			<div class="grid_12 grid_16_sm grid_16_xs margin_sm_10 margin_xs_10">
				<ul class="brands_item_list <?=$brandsView?>">
					<?foreach($arResult['ITEMS'] as $brandGroup){?>
						<li class="brands_title grid_12" rel="<?=$brandGroup['LETTER']?>"><a name="<?=$brandGroup['LETTER']?>"></a><span><?=($brandGroup['LETTER'] == 'NUMERIC' || $brandGroup['LETTER'] == 'NON_LATIN' ? GetMessage('CT_BRANDS_LIST_'.$brandGroup['LETTER']) : $brandGroup['LETTER'])?></span></li>
						<?foreach($brandGroup['ITEMS'] as $arBrand){?>
							<li class="grid_3 grid_3_sm grid_6_xs">
								<a href="<?=$arBrand['DETAIL_PAGE_URL']?>"><img src="<?=$arBrand['PREVIEW_PICTURE']?>" alt="<?=$arBrand['NAME']?>" class="img-responsive"></a>
								<a href="<?=$arBrand['DETAIL_PAGE_URL']?>"><?=$arBrand['NAME']?></a>
							</li>
						<?}?>
					<?}?>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
</div>
