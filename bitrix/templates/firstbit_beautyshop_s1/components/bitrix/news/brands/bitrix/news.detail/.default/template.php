<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$this->SetViewTarget('pageTitle_subTitle');?>
<div class="brand_link">
	<a href="<?=$arResult['PROPERTIES']['LINK']['VALUE']?>"><?=GetMessage('OFFICIAL_SITE')?></a>
</div>
<?$this->EndViewTarget();?>

<div class="row brand_item">
	<div class="container_16">
		<div class="grid_16 grid_16_sm grid_16_xs brand_description">
			<? if($arParams["PREVIEW_PICTURE"]!="N" && is_array($arResult["PREVIEW_PICTURE"])) { ?>
				<div class="brand_picture">
					<img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arResult["PREVIEW_PICTURE"]["ALT"]?>" title="<?=$arResult["PREVIEW_PICTURE"]["TITLE"]?>" />
				</div>
			<? } ?>
			<?=$arResult["DETAIL_TEXT"];unset($arResult["DETAIL_TEXT"]);?>
		</div>
		<div class="clear"></div>
	</div>
</div>