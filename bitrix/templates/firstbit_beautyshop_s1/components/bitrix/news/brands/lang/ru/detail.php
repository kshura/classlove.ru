<?
$MESS ['T_NEWS_DETAIL_BACK'] = "Возврат к списку";
$MESS ['CATEGORIES'] = "Материалы по теме:";
$MESS["CT_BCS_CATALOG_SORT_FIELD_NAME"] = "По алфавиту";
$MESS["CT_BCS_CATALOG_SORT_FIELD_PRICE"] = "По цене";
$MESS["CT_BCS_CATALOG_SORT_FIELD_SHOW"] = "По популярности";
$MESS["CT_BCS_CATALOG_SHOW_ALL_ELEMENTS"] = "Все";
$MESS["CT_BCS_CATALOG_SHOW_MORE_ELEMENTS"] = "Показать ещё";
$MESS["CT_BCS_TPL_MESS_BTN_WISHLIST"] = "Избранное";
$MESS["CT_BCS_TPL_MESS_BTN_QUICKBUY"] = "Купить в 1 клик";
$MESS["CT_BCS_CATALOG_SHOW_PER_QUANTITY"] = "Показывать: ";
$MESS["CT_BCS_TPL_BRAND_ITEMS"] = "Товары бренда в каталоге";
$MESS["CT_BCS_TPL_ALL_SECTION"] = "Все разделы";