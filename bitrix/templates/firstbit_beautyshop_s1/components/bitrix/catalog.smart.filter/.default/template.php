<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="hidden_lg">
		<a href="javascript:void(0);" class="btn_round btn_color filter_call_btn" onclick="mobileFilterOpen();"><? echo GetMessage('CT_BCSF_FILTER_SHOW_BUTTON'); ?></a>
	</div>
<div id="filter_wrapper" class="filter_wrapper vertical white">
		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			<div class="filter_inner">
				<div class="filter_mobile_title hidden_lg">
					<span>&nbsp;</span>
					<div class="close_filter" onclick="mobileFilterClose();"><i class="fa fa-times"></i></div>
				</div>
				<?
				foreach($arResult["HIDDEN"] as $arItem) {
					?>
					<input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>" value="<? echo $arItem["HTML_VALUE"] ?>"/>
					<?
				}
				?>
				<div class="row">
					<?
					foreach($arResult["ITEMS"] as $key=>$arItem) {
						$key = $arItem["ENCODED_ID"];
						if($arItem["CODE"] == "MINIMUM_PRICE") {
							if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0) {
								continue;
							}
							$precision = 2;
							if (Bitrix\Main\Loader::includeModule("currency")) {
								$res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
								$precision = $res['DECIMALS'];
							}
							?>
							<div class="filter_block">
								<div class="filter_header">
									<div class="filter_title"><? echo GetMessage('CT_BCSF_PRICE_TITLE'); ?></div>
									<div class="pull_right"><i class="fa fa-chevron-circle-down"></i></div>
								</div>
								<div class="filter_body" data-role="bx_filter_block">
									<div class="filter_price">
										<input class="min_price min2" type="text" name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>" id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>" value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>" size="5" onkeyup="smartFilter.keyup(this)" />
										&nbsp;&mdash;&nbsp;
										<input class="max_price max2" type="text" name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>" id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>" value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>" size="5" onkeyup="smartFilter.keyup(this)" />
										<div class="price-range">
											<div class="bx-ui-slider-track" id="drag_track_<?= $key ?>">
												<?
												$precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
												$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
												?>
												<div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?= $key ?>"></div>
												<div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?= $key ?>"></div>
												<div class="bx-ui-slider-pricebar-v" style="left: 0;right: 0;" id="colorAvailableActive_<?= $key ?>"></div>
												<div class="prices-range" id="drag_tracker_<?= $key ?>" style="left: 0%; right: 0%;">
													<a class="bx-ui-slider-handle ui-state-default left" style="left:0;" href="javascript:void(0)" id="left_slider_<?= $key ?>"></a>
													<a class="bx-ui-slider-handle ui-state-default right" style="right:0;" href="javascript:void(0)" id="right_slider_<?= $key ?>"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<span class="bx-filter-container-modef"></span>
							</div>
						<?
						$arJsParams = array(
							"leftSlider" => 'left_slider_' . $key,
							"rightSlider" => 'right_slider_' . $key,
							"tracker" => "drag_tracker_" . $key,
							"trackerWrap" => "drag_track_" . $key,
							"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
							"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
							"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
							"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
							"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
							"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
							"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
							"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
							"precision" => $precision,
							"colorUnavailableActive" => 'colorUnavailableActive_' . $key,
							"colorAvailableActive" => 'colorAvailableActive_' . $key,
							"colorAvailableInactive" => 'colorAvailableInactive_' . $key,
						);
						?>
							<script type="text/javascript">
								BX.ready(function () {
									window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
								});
							</script>
							<?
						}
					}

					foreach($arResult["ITEMS"] as $key=>$arItem) {
						if(empty($arItem["VALUES"]) || isset($arItem["PRICE"]) || $arItem["CODE"] == "MINIMUM_PRICE") {
							continue;
						}
						if ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)) {
							continue;
						}
						?>
						<div class="filter_block">
							<div class="filter_header">
								<div class="filter_title"><?=$arItem["NAME"]?></div>
								<div class="pull_right"><i class="fa fa-chevron-circle-up filter_toggle"></i></div>
							</div>
							<div class="filter_body" data-role="bx_filter_block" style="display:<?=($arItem['DISPLAY_EXPANDED'] != 'Y' ? 'none' : '')?>;">
								<div class="filter_parameters_container">
										<?
										$arCur = current($arItem["VALUES"]);
										switch ($arItem["DISPLAY_TYPE"]) {
											case "A":
												?>
											<div class="filter_price">
												<input class="min_price min2" type="text" name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>" id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>" value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>" size="5" onkeyup="smartFilter.keyup(this)" />
												&nbsp;&mdash;&nbsp;
												<input class="max_price max2" type="text" name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>" id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>" value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>" size="5" onkeyup="smartFilter.keyup(this)" />
												<div class="price-range">
													<div class="bx-ui-slider-track" id="drag_track_<?= $key ?>">
														<?
														$precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
														$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
														?>
														<div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?= $key ?>"></div>
														<div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?= $key ?>"></div>
														<div class="bx-ui-slider-pricebar-v" style="left: 0;right: 0;" id="colorAvailableActive_<?= $key ?>"></div>
														<div class="prices-range" id="drag_tracker_<?= $key ?>" style="left: 0%; right: 0%;">
															<a class="bx-ui-slider-handle ui-state-default left" style="left:0;" href="javascript:void(0)" id="left_slider_<?= $key ?>"></a>
															<a class="bx-ui-slider-handle ui-state-default right" style="right:0;" href="javascript:void(0)" id="right_slider_<?= $key ?>"></a>
														</div>
													</div>
												</div>
											</div>
												<?
												$arJsParams = array(
													"leftSlider" => 'left_slider_'.$key,
													"rightSlider" => 'right_slider_'.$key,
													"tracker" => "drag_tracker_".$key,
													"trackerWrap" => "drag_track_".$key,
													"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
													"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
													"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
													"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
													"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
													"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
													"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
													"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
													"precision" => $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0,
													"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
													"colorAvailableActive" => 'colorAvailableActive_'.$key,
													"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
												);
												?>
												<script type="text/javascript">
													BX.ready(function(){
														window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
													});
												</script>
												<?
												break;
											case "A_OLD":
												?>
												<div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
													<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
													<div class="bx-filter-input-container">
														<input class="min-price" type="text" name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>" id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>" value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>" size="5" onkeyup="smartFilter.keyup(this)" />
													</div>
												</div>
												<div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
													<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
													<div class="bx-filter-input-container">
														<input class="max-price" type="text" name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>" id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>" value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>" size="5" onkeyup="smartFilter.keyup(this)" />
													</div>
												</div>
												<div class="col-xs-10 col-xs-offset-1 bx-ui-slider-track-container">
													<div class="bx-ui-slider-track" id="drag_track_<?=$key?>">
														<?
														$precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
														$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
														$value1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
														$value2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
														$value3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
														$value4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
														$value5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
														?>
														<div class="bx-ui-slider-part p1"><span><?=$value1?></span></div>
														<div class="bx-ui-slider-part p2"><span><?=$value2?></span></div>
														<div class="bx-ui-slider-part p3"><span><?=$value3?></span></div>
														<div class="bx-ui-slider-part p4"><span><?=$value4?></span></div>
														<div class="bx-ui-slider-part p5"><span><?=$value5?></span></div>
														<div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
														<div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
														<div class="bx-ui-slider-pricebar-v" style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
														<div class="bx-ui-slider-range" id="drag_tracker_<?=$key?>" style="left: 0;right: 0;">
															<a class="bx-ui-slider-handle left" style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
															<a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
														</div>
													</div>
												</div>
												<?
												$arJsParams = array(
													"leftSlider" => 'left_slider_'.$key,
													"rightSlider" => 'right_slider_'.$key,
													"tracker" => "drag_tracker_".$key,
													"trackerWrap" => "drag_track_".$key,
													"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
													"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
													"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
													"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
													"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
													"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
													"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
													"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
													"precision" => $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0,
													"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
													"colorAvailableActive" => 'colorAvailableActive_'.$key,
													"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
												);
												?>
												<script type="text/javascript">
													BX.ready(function(){
														window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
													});
												</script>
												<?
												break;
											case "B":
												?>
												<div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
													<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
													<div class="bx-filter-input-container">
														<input class="min-price" type="text" name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>" id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>" value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>" size="5" onkeyup="smartFilter.keyup(this)" />
													</div>
												</div>
												<div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
													<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
													<div class="bx-filter-input-container">
														<input class="max-price" type="text" name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>" id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>" value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>" size="5" onkeyup="smartFilter.keyup(this)" />
													</div>
												</div>
												<?
												break;
											case "G":
												?>
												<div class="bx-filter-param-btn-inline">
													<?
													foreach ($arItem["VALUES"] as $val => $ar) {
														?>
														<input style="display: none" type="checkbox" name="<?= $ar["CONTROL_NAME"] ?>" id="<?= $ar["CONTROL_ID"] ?>" value="<?= $ar["HTML_VALUE"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> />
														<?
														$class = "";
														if ($ar["CHECKED"]) {
															$class .= " bx-active";
														}
														if ($ar["DISABLED"]) {
															$class .= " disabled";
														}
														?>
														<label for="<?= $ar["CONTROL_ID"] ?>" title="<?= $ar['VALUE']?>" data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-label bx-filter-param-color <?= $class ?>" onclick="smartFilter.keyup(BX('<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>')); BX.toggleClass(this, 'bx-active');">
															<span class="bx-filter-param-btn bx-color-sl">
																<?
																if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])) {
																	?>
																	<span class="bx-filter-btn-color-icon" style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
																	<?
																	}
																?>
															</span>
														</label>
														<?
													}
													?>
												</div>
												<?
												break;
											case "H":
												?>
												<div class="bx-filter-param-btn-block">
													<?
													foreach ($arItem["VALUES"] as $val => $ar) {
														?>
														<input style="display: none" type="checkbox" name="<?= $ar["CONTROL_NAME"] ?>" id="<?= $ar["CONTROL_ID"] ?>" value="<?= $ar["HTML_VALUE"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> />
														<?
														$class = "";
														if ($ar["CHECKED"]) {
															$class .= " bx-active";
														}
														if ($ar["DISABLED"]) {
															$class .= " disabled";
														}
														?>
														<label for="<?= $ar["CONTROL_ID"] ?>" data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-label<?= $class ?>" onclick="smartFilter.keyup(BX('<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>')); BX.toggleClass(this, 'bx-active');">
															<span class="bx-filter-param-btn bx-color-sl">
																<?
																if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])) {
																	?>
																	<span class="bx-filter-btn-color-icon" style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
																	<?
																}
																?>
															</span>
															<span class="bx-filter-param-text" title="<?= $ar["VALUE"]; ?>"><?= $ar["VALUE"]; ?>
																<?
																if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])) {
																	?>(<span data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
																}
																?>
															</span>
														</label>
														<?
													}
													?>
												</div>
												<?
												break;
											case "P":
												$checkedItemExist = false;
												?>
												<div class="bx-filter-select-container">
													<div class="bx-filter-select-block">
														<div class="bx-filter-select-text" data-role="currentOption" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
															<?
															foreach ($arItem["VALUES"] as $val => $ar) {
																if ($ar["CHECKED"]) {
																	echo $ar["VALUE"];
																	$checkedItemExist = true;
																}
															}
															if (!$checkedItemExist) {
																echo GetMessage("CT_BCSF_FILTER_ALL");
															}
															?>
															<div class="bx-filter-select-arrow"></div>
														</div>
														<input style="display: none" type="radio" name="<?=$arCur["CONTROL_NAME_ALT"]?>" id="<? echo "all_".$arCur["CONTROL_ID"] ?>" value="" />
														<?
														foreach ($arItem["VALUES"] as $val => $ar) {
															?>
															<input style="display: none" type="radio" name="<?= $ar["CONTROL_NAME_ALT"] ?>" id="<?= $ar["CONTROL_ID"] ?>" value="<? echo $ar["HTML_VALUE_ALT"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> />
															<?
														}
														?>
														<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
															<ul>
																<li>
																	<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
																		<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
																	</label>
																</li>
																<?
																foreach ($arItem["VALUES"] as $val => $ar) {
																	$class = "";
																	if ($ar["CHECKED"]) {
																		$class .= " selected";
																	}
																	if ($ar["DISABLED"]) {
																		$class .= " disabled";
																	}
																	?>
																	<li>
																		<label for="<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-label<?= $class ?>" data-role="label_<?= $ar["CONTROL_ID"] ?>" onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>')"><?= $ar["VALUE"] ?></label>
																	</li>
																	<?
																}
																?>
															</ul>
														</div>
													</div>
												</div>
												<?
												break;
											case "R":
												?>
												<div class="bx-filter-select-container">
													<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
														<div class="bx-filter-select-text fix" data-role="currentOption">
															<?
															$checkedItemExist = false;
															foreach ($arItem["VALUES"] as $val => $ar) {
																if ($ar["CHECKED"]) {
																	?>
																	<? if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
																		<span class="bx-filter-btn-color-icon" style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
																	<?endif ?>
																	<span class="bx-filter-param-text"><?= $ar["VALUE"] ?></span>
																	<?
																	$checkedItemExist = true;
																}
															}
															if (!$checkedItemExist) {
																?>
																<span class="bx-filter-btn-color-icon all"></span>
																<?
																echo GetMessage("CT_BCSF_FILTER_ALL");
															}
															?>
														</div>
														<div class="bx-filter-select-arrow"></div>
														<input style="display: none" type="radio" name="<?=$arCur["CONTROL_NAME_ALT"]?>" id="<? echo "all_".$arCur["CONTROL_ID"] ?>" value="" />
														<?
														foreach ($arItem["VALUES"] as $val => $ar) {
															?>
															<input style="display: none" type="radio" name="<?= $ar["CONTROL_NAME_ALT"] ?>" id="<?= $ar["CONTROL_ID"] ?>" value="<?= $ar["HTML_VALUE_ALT"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> />
															<?
														}
														?>
														<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none">
															<ul>
																<li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
																	<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
																		<span class="bx-filter-btn-color-icon all"></span>
																		<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
																	</label>
																</li>
																<?
																foreach ($arItem["VALUES"] as $val => $ar) {
																	$class = "";
																	if ($ar["CHECKED"]) {
																		$class .= " selected";
																	}
																	if ($ar["DISABLED"]) {
																		$class .= " disabled";
																	}
																	?>
																	<li>
																		<label for="<?= $ar["CONTROL_ID"] ?>" data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-label<?= $class ?>" onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>')">
																			<?
																			if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])) {
																				?>
																				<span class="bx-filter-btn-color-icon" style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
																				<?
																			}
																			?>
																			<span class="bx-filter-param-text">
																				<?= $ar["VALUE"] ?>
																			</span>
																		</label>
																	</li>
																	<?
																}
																?>
															</ul>
														</div>
													</div>
												</div>
												<?
												break;
											case "K":
												?>
												<ul class="options-list">
													<?
													foreach($arItem["VALUES"] as $val => $ar) {
														?>
														<li class="options-list-item">
															<label data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled' : '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
																<input type="radio" value="<? echo $ar["HTML_VALUE_ALT"] ?>" name="<? echo $ar["CONTROL_NAME_ALT"] ?>" id="<? echo $ar["CONTROL_ID"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> onclick="smartFilter.click(this)" /><?= $ar["VALUE"]; ?>
																<?
																if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])) {
																	?>
																	&nbsp;( <span data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)
																	<?
																}
																?>
															</label>
														</li>
														<?
														}
													?>
												</ul>
												<?
												break;
											case "U":
												?>
												<div class="bx-filter-parameters-box-container-block">
													<div class="bx-filter-input-container bx-filter-calendar-container">
														<?$APPLICATION->IncludeComponent(
															'bitrix:main.calendar',
															'',
															array(
																'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
																'SHOW_INPUT' => 'Y',
																'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
																'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
																'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
																'SHOW_TIME' => 'N',
																'HIDE_TIMEBAR' => 'Y',
															),
															null,
															array('HIDE_ICONS' => 'Y')
														);?>
													</div>
												</div>
												<div class="bx-filter-parameters-box-container-block">
													<div class="bx-filter-input-container bx-filter-calendar-container">
														<?$APPLICATION->IncludeComponent(
															'bitrix:main.calendar',
															'',
															array(
																'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
																'SHOW_INPUT' => 'Y',
																'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
																'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
																'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
																'SHOW_TIME' => 'N',
																'HIDE_TIMEBAR' => 'Y',
															),
															null,
															array('HIDE_ICONS' => 'Y')
														);?>
													</div>
												</div>
												<?
												break;
											default:
												?>
												<ul class="options-list">
													<?
													foreach($arItem["VALUES"] as $val => $ar) {
														?>
														<li class="options-list-item">
															<label data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled' : '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
																<input type="checkbox" value="<? echo $ar["HTML_VALUE"] ?>" name="<? echo $ar["CONTROL_NAME"] ?>" id="<? echo $ar["CONTROL_ID"] ?>" <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?> onclick="smartFilter.click(this)" /><?= $ar["VALUE"]; ?>
																<?
																if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])) {
																	?>
																	&nbsp;( <span data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)
																	<?
																}
																?>
															</label>
														</li>
														<?
													}
													?>
												</ul>
												<?
										}
										?>
								</div>
								<div style="clear: both"></div>
							</div>
							<span class="bx-filter-container-modef"></span>
						</div>
						<?
					}
					?>
					<div class="filter_btn">
						<input class="btn_round btn_border btn_hover_color" type="submit" id="del_filter" name="del_filter" value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>" />
						<input class="btn_round btn_color" type="submit" id="set_filter" name="set_filter" value="<?=GetMessage("CT_BCSF_SET_FILTER")?>" />
						<div class="bx-filter-popup-result" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
							<label><?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?></label><a href="<?echo $arResult["FILTER_URL"]?>" target=""><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
						</div>
					</div>
				</div>
				<div class="clb"></div>
			</div>
		</form>
	</div>
<div class="clear"></div>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>