<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	use Bitrix\Main\Localization\Loc;

	Loc::loadMessages(__FILE__);
	$legalColors = array(
		'green' => true,
		'yellow' => true,
		'red' => true,
		'gray' => true
	);
	$defaultColors = array(
		'N' => 'green',
		'P' => 'yellow',
		'F' => 'gray',
	);

	foreach ($arParams as $key => $val)
		if(strpos($key, "STATUS_COLOR_") !== false && !$legalColors[$val])
			unset($arParams[$key]);
	if(is_array($arResult['INFO']) && !empty($arResult['INFO']))
	{
		foreach($arResult['INFO']['STATUS'] as $id => $stat)
		{
			$arResult['INFO']['STATUS'][$id]["COLOR"] = $arParams['STATUS_COLOR_'.$id] ? $arParams['STATUS_COLOR_'.$id] : (isset($defaultColors[$id]) ? $defaultColors[$id] : 'gray');
			$arResult["ORDER_BY_STATUS"][$id] = array();
		}
	}
	if(is_array($arResult["ORDERS"]) && !empty($arResult["ORDERS"])) {
		foreach ($arResult["ORDERS"] as $orderIndex=>$order) {
			$tmpProps = array();
			$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"), array("PERSON_TYPE_ID" => $order['ORDER']["PERSON_TYPE_ID"], "ACTIVE" => "Y", "IS_PAYER" => "Y"));
			while ($arProps = $db_props->Fetch()) {
				$db_vals = CSaleOrderPropsValue::GetList(array("SORT" => "ASC"), array("ORDER_ID" => $order['ORDER']['ID'], "ORDER_PROPS_ID" => $arProps["ID"]));
				while ($arVals = $db_vals->Fetch()) {
					$arResult["ORDERS"][$orderIndex]['ORDER']['PAYER'] = $arVals['VALUE'];
				}
			}

			$order['HAS_DELIVERY'] = intval($order["ORDER"]["DELIVERY_ID"]) || strpos($order["ORDER"]["DELIVERY_ID"], ":") !== false;
			$color = $arParams['STATUS_COLOR_'.$stat['ID']];
			$order['STATUS_COLOR_CLASS'] = empty($color) ? 'gray' : $color;

			$arResult["ORDERS"][$orderIndex]['ORDER']['DATE_INSERT_FORMATED'] = date('d.m.y',strtotime($order['ORDER']['DATE_INSERT_FORMATED']));
			$arResult["ORDER_BY_STATUS"][$stat['ID']][] = $order;

		}
	}
?>