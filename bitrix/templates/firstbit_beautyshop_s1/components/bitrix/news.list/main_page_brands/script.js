$(document).ready(function () {
	var owl_brands = $('#owl_brands');
	owl_brands.owlCarousel({
		responsive: {
			0: {
				items: 1,
			},
			560: {
				items: 2,
			},
			760: {
				items: 3,
			},
			959: {
				items: 6,
			}
		},
		loop: false,
		nav: true,
		dots: false,
		smartSpeed: 500,
		navContainer: '#owl_brands_nav',
		navText: ["<i class=\"fa fa-angle-left\"></i>","<i class=\"fa fa-angle-right\"></i>"],
		afterInit: initSlider(owl_brands),
	});
	$('.mainpage_brands_letter a').click(function() {
		$(this).parent().addClass('active').siblings().removeClass('active');
		$('.mainpage_brands_list[rel=' + $(this).attr('data-letter') + ']').show().siblings().hide();
	});
});