<?php
/**
 * Date: 028 28.03
 * Time: 13:43
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
if(count($arResult["ITEMS"]) > 0) {
	?>
	<ul class="reviews_list">
		<?
		foreach ($arResult["ITEMS"] as $arItem) {
			?>
			<li class="reviews_item">
				<div class="reviews_header">
					<div class="reviews_info">
						<span class="user_name"><?= $arItem['PROPERTIES']['USER_NAME']['VALUE'] ?></span>
						<?
						if ($arItem['PROPERTIES']['USER_CITY']['VALUE']) {
							?>
							,&nbsp;
							<span class="user_city"><?= $arItem['PROPERTIES']['USER_CITY']['VALUE'] ?></span>
							<?
						}
						?>
						<span class="reviews_time"><?= $arItem['DATE_CREATE'] ?></span>
					</div>
					<div class="reviews_raiting">
						<?
						$star = 5;
						if ($arItem['PROPERTIES']['PRODUCT_RATING']['VALUE']) {
							$star = $star - $arItem['PROPERTIES']['PRODUCT_RATING']['VALUE'];
						}
						for ($i = 1; $i <= $arItem['PROPERTIES']['PRODUCT_RATING']['VALUE']; $i++) {
							?>
							<i class="fa fa-star"></i>
							<?
						}
						for ($i = 1; $i <= $star; $i++) {
							?>
							<i class="fa fa-star-o"></i>
							<?
						}
						?>
					</div>
				</div>
				<div class="reviews_body">
					<p><?= $arItem['PROPERTIES']['REVIEW']['VALUE']['TEXT'] ?></p>
				</div>
				<?
				if($arItem['PROPERTIES']['ANSWER']['VALUE']['TEXT']) {
					?>
					<div class="reviews_answer">
						<span class="reviews_answer_title"><?= GetMessage('TPL_REVIEW_ANSWER_TITLE') ?></span>
						<p><?= $arItem['PROPERTIES']['ANSWER']['VALUE']['TEXT'] ?></p>
					</div>
					<?
				}
				?>
			</li>
			<?
		}
		?>
	</ul>
	<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
		?>
		<?= $arResult["NAV_STRING"] ?>
		<?
	}
} else {
	?>
	<div class="page_empty_message active">
		<h3 class="page_empty_message_title"><?=GetMessage('TPL_REVIEW_LIST_EMPTY_TITLE')?></h3>
		<p class="page_empty_message_text"><?=GetMessage('TPL_REVIEW_LIST_EMPTY_TEXT')?></p>
	</div>
	<?
}
?>
