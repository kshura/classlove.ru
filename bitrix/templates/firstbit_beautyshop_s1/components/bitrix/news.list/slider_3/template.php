<?php
/**
* Date: 028 28.03
* Time: 13:43
*/
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
?>
<div class="container_16 slider_wrapper hidden_xs">
	<? foreach ($arResult['ITEMS'] as $arSlide) { ?>
		<div class="grid_8 grid_8_sm alpha omega extend">
			<? if ($arSlide['PROPERTIES']['URL']['VALUE']) { ?>
				<a href="<?=$arSlide['PROPERTIES']['URL']['VALUE']?>">
			<? } ?>
			<img src="<?=$arSlide['PROPERTIES']['BANNER_PICTURE_MEDIUM']['VALUE']['SRC']?>" />
			<? if ($arSlide['PROPERTIES']['URL']['VALUE']) { ?>
				</a>
			<? } ?>
		</div>
	<? } ?>
	<div class="clear"></div>
</div>
<div class="slider_wrapper show_xs slider_initial" id="owl_slider3">
	<? foreach ($arResult['ITEMS'] as $arSlide) { ?>
		<div class="slide container_16">
			<div class="grid_8 grid_8_sm extend">
				<? if ($arSlide['PROPERTIES']['URL']['VALUE']) { ?>
					<a href="<?=$arSlide['PROPERTIES']['URL']['VALUE']?>">
				<? } ?>
				<img src="<?=$arSlide['PROPERTIES']['BANNER_PICTURE_MEDIUM']['VALUE']['SRC']?>" />
				<? if ($arSlide['PROPERTIES']['URL']['VALUE']) { ?>
					</a>
				<? } ?>
			</div>
		</div>
	<? } ?>
</div>
<div class="container_16 show_xs">
	<div class="grid_16 grid_16_sm relative owl_nav light" id="owl_slider3_nav"></div>
	<div class="clear"></div>
</div>

