$(document).ready(function () {
	var owl_slider3 = $('#owl_slider3');
	owl_slider3.owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		dots: false,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		smartSpeed: 500,
		navContainer: '#owl_slider3_nav',
		navText: ["<i class=\"fa fa-chevron-left\"></i>","<i class=\"fa fa-chevron-right\"></i>"],
		afterInit: initSlider(owl_slider3),
	});
});