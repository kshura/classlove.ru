<?php
/**
* Date: 028 28.03
* Time: 13:43
*/
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
?>

<div class="slider_wrapper slider_initial" id="owl_slider2">
	<? foreach ($arResult['ITEMS'] as $arSlide) { ?>
		<? if ($arSlide['PROPERTIES']['URL']['VALUE'] && !$arSlide['PROPERTIES']['SLIDER_BUTTON_MESSAGE']['VALUE']) { ?>
			<a href="<?=$arSlide['PROPERTIES']['URL']['VALUE']?>">
		<? } ?>
		<div class="slide container_16" style="background-image: url('<?=$arSlide['PROPERTIES']['SLIDER_PICTURE_BIG']['VALUE']['SRC']?>');">
			<div class="grid_8 grid_8_sm text_container">
				<?
				if(is_array($arSlide['PROPERTIES']['SLIDER_TITLE']['VALUE']) && strlen($arSlide['PROPERTIES']['SLIDER_TITLE']['VALUE']['TEXT'])>0) {
					?>
					<h1><?= $arSlide['PROPERTIES']['SLIDER_TITLE']['~VALUE']['TEXT'] ?></h1>
					<?
				}
				?>
				<?
				if(is_array($arSlide['PROPERTIES']['SLIDER_PREVIEW_TEXT']['VALUE']) && strlen($arSlide['PROPERTIES']['SLIDER_PREVIEW_TEXT']['VALUE']['TEXT'])>0) {
					?>
					<p><?= $arSlide['PROPERTIES']['SLIDER_PREVIEW_TEXT']['~VALUE']['TEXT'] ?></p>
					<?
				}
				?>
				<? if ($arSlide['PROPERTIES']['URL']['VALUE'] && $arSlide['PROPERTIES']['SLIDER_BUTTON_MESSAGE']['VALUE']) { ?>
					<a href="<?=$arSlide['PROPERTIES']['URL']['VALUE']?>" class="btn_round btn_color"><?=$arSlide['PROPERTIES']['SLIDER_BUTTON_MESSAGE']['VALUE']?></a>
				<? } ?>
			</div>
			<div class="clear"></div>
		</div>
		<? if ($arSlide['PROPERTIES']['URL']['VALUE'] && !$arSlide['PROPERTIES']['SLIDER_BUTTON_MESSAGE']['VALUE']) { ?>
			</a>
		<? } ?>
	<? } ?>
</div>
<div class="container_16 hidden_xs">
	<div class="grid_16 grid_16_sm relative owl_nav light" id="owl_slider2_nav"></div>
	<div class="clear"></div>
</div>

