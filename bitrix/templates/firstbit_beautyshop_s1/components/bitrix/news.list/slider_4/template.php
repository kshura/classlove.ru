<?php
/**
* Date: 028 28.03
* Time: 13:43
*/
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
?>
<div class="container_16">
	<div class="slider_wrapper slider_initial" id="owl_slider4">
		<? foreach ($arResult['ITEMS'] as $itemIndex=>$arSlide) { ?>
			<? if($itemIndex < (count($arResult['ITEMS'])-2)) { ?>
				<? if ($arSlide['PROPERTIES']['URL']['VALUE'] && !$arSlide['PROPERTIES']['SLIDER_BUTTON_MESSAGE']['VALUE']) { ?>
					<a href="<?=$arSlide['PROPERTIES']['URL']['VALUE']?>">
				<? } ?>
					<div class="slide" style="background-image: url('<?=$arSlide['PROPERTIES']['SLIDER_PICTURE_SMALL']['VALUE']['SRC']?>');">
						<div class="text_container">
							<?
							if(is_array($arSlide['PROPERTIES']['SLIDER_TITLE']['VALUE']) && strlen($arSlide['PROPERTIES']['SLIDER_TITLE']['VALUE']['TEXT'])>0) {
								?>
								<h1><?= $arSlide['PROPERTIES']['SLIDER_TITLE']['~VALUE']['TEXT'] ?></h1>
								<?
							}
							?>
							<? if ($arSlide['PROPERTIES']['URL']['VALUE'] && $arSlide['PROPERTIES']['SLIDER_BUTTON_MESSAGE']['VALUE']) { ?>
								<a href="<?=$arSlide['PROPERTIES']['URL']['VALUE']?>" class="btn_round btn_color"><?=$arSlide['PROPERTIES']['SLIDER_BUTTON_MESSAGE']['VALUE']?></a>
							<? } ?>
						</div>
					</div>
				<? if ($arSlide['PROPERTIES']['URL']['VALUE'] && !$arSlide['PROPERTIES']['SLIDER_BUTTON_MESSAGE']['VALUE']) { ?>
					</a>
				<? } ?>
			<? } ?>
		<? } ?>
	</div>
	<div class="banner_wrapper hidden_xs">
		<? foreach ($arResult['ITEMS'] as $itemIndex=>$arSlide) { ?>
			<? if($itemIndex == (count($arResult['ITEMS'])-2)) { ?>
				<? if ($arSlide['PROPERTIES']['URL']['VALUE']) { ?>
					<a href="<?=$arSlide['PROPERTIES']['URL']['VALUE']?>">
				<? } ?>
					<div class="top_banner" style="background-image: url('<?=$arSlide['PROPERTIES']['BANNER_PICTURE_SMALL']['VALUE']['SRC']?>');"></div>
				<? if ($arSlide['PROPERTIES']['URL']['VALUE']) { ?>
					</a>
				<? } ?>
			<? } ?>
			<? if($itemIndex == (count($arResult['ITEMS'])-1)) { ?>
				<? if ($arSlide['PROPERTIES']['URL']['VALUE']) { ?>
					<a href="<?=$arSlide['PROPERTIES']['URL']['VALUE']?>">
				<? } ?>
					<div class="bottom_banner" style="background-image: url('<?=$arSlide['PROPERTIES']['BANNER_PICTURE_SMALL']['VALUE']['SRC']?>');"></div>
				<? if ($arSlide['PROPERTIES']['URL']['VALUE']) { ?>
					</a>
				<? } ?>
			<? } ?>
		<? } ?>
	</div>
	<div class="clear"></div>
</div>
