$(document).ready(function () {
	var owl_slider4 = $('#owl_slider4');
	owl_slider4.owlCarousel({
		items: 1,
		loop: true,
		nav: false,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		smartSpeed: 500,
		navContainer: 'false',
		afterInit: initSlider(owl_slider4),
	});
});