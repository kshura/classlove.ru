<?php
/**
* Date: 028 28.03
* Time: 13:43
*/
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$this->SetFrameMode(true);
?>
<section class="row banners6">
	<div class="banners_inner">
		<div class="banners_carousel show_xs">
			<div id="banners_slider" class="slider_initial">
				<? foreach($arResult['ITEMS'] as $arItem) { ?>
					<a href="<?= $arItem['PROPERTIES']['URL']['VALUE'] ?>">
						<div class="banner_wrapper banner_small" style="background-image:url('<?= $arItem['PROPERTIES']['HORIZONTAL_PICTURE_WIDE']['VALUE']['SRC']?>');">
							<?
							if($arParams['DISPLAY_NAME'] == 'Y') {
								?>
								<div class="banner_title_substrate">
									<span class="banner_title"><?= $arItem['~NAME'] ?></span>
								</div>
								<?
							}
							?>
						</div>
					</a>
				<? } ?>
				<? unset($arItem); ?>
			</div>
			<div class="container_16">
				<div class="grid_16 relative owl_nav light" id="owl_banners_nav"></div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="container_16 hidden_xs">
			<div class="grid_8 grid_8_sm">
				<?
				$arItem = array_shift($arResult['ITEMS']);
				?>
				<? if ($arItem['PROPERTIES']['URL']['VALUE']){ ?>
				<a href="<?= $arItem['PROPERTIES']['URL']['VALUE'] ?>">
					<? } ?>
					<div class="banner_wrapper banner_small" style="background-image:url('<?= $arItem['PROPERTIES']['HORIZONTAL_PICTURE_WIDE']['VALUE']['SRC'] ?>');">
						<?
						if($arParams['DISPLAY_NAME'] == 'Y') {
							?>
							<div class="banner_title_substrate">
								<span class="banner_title"><?= $arItem['~NAME'] ?></span>
							</div>
							<?
						}
						?>
						<div class="opacity_dark_panel"></div>
					</div>
					<? if ($arItem['PROPERTIES']['URL']['VALUE']){ ?>
				</a>
			<? } ?>
			</div>
			<div class="grid_8 grid_8_sm">
				<?
				$arItem = array_shift($arResult['ITEMS']);
				?>
				<? if ($arItem['PROPERTIES']['URL']['VALUE']){ ?>
				<a href="<?= $arItem['PROPERTIES']['URL']['VALUE'] ?>">
					<? } ?>
					<div class="banner_wrapper banner_small" style="background-image:url('<?= $arItem['PROPERTIES']['HORIZONTAL_PICTURE_WIDE']['VALUE']['SRC'] ?>');">
						<?
						if($arParams['DISPLAY_NAME'] == 'Y') {
							?>
							<div class="banner_title_substrate">
								<span class="banner_title"><?= $arItem['~NAME'] ?></span>
							</div>
							<?
						}
						?>
						<div class="opacity_dark_panel"></div>
					</div>
					<? if ($arItem['PROPERTIES']['URL']['VALUE']){ ?>
				</a>
			<? } ?>
			</div>
			<div class="clear"></div>
		</div>
		<div class="container_12 hidden_xs">
			<div class="grid_4 grid_4_sm">
				<?
				$arItem = array_shift($arResult['ITEMS']);
				?>
				<? if ($arItem['PROPERTIES']['URL']['VALUE']){ ?>
				<a href="<?= $arItem['PROPERTIES']['URL']['VALUE'] ?>">
					<? } ?>
					<div class="banner_wrapper banner_small" style="background-image:url('<?= $arItem['PROPERTIES']['HORIZONTAL_PICTURE_NARROW']['VALUE']['SRC'] ?>');">
						<?
						if($arParams['DISPLAY_NAME'] == 'Y') {
							?>
							<div class="banner_title_substrate">
								<span class="banner_title"><?= $arItem['~NAME'] ?></span>
							</div>
							<?
						}
						?>
						<div class="opacity_dark_panel"></div>
					</div>
					<? if ($arItem['PROPERTIES']['URL']['VALUE']){ ?>
				</a>
			<? } ?>
			</div>
			<div class="grid_4 grid_4_sm">
				<?
				$arItem = array_shift($arResult['ITEMS']);
				?>
				<? if ($arItem['PROPERTIES']['URL']['VALUE']){ ?>
				<a href="<?= $arItem['PROPERTIES']['URL']['VALUE'] ?>">
					<? } ?>
					<div class="banner_wrapper banner_small" style="background-image:url('<?= $arItem['PROPERTIES']['HORIZONTAL_PICTURE_NARROW']['VALUE']['SRC'] ?>');">
						<?
						if($arParams['DISPLAY_NAME'] == 'Y') {
							?>
							<div class="banner_title_substrate">
								<span class="banner_title"><?= $arItem['~NAME'] ?></span>
							</div>
							<?
						}
						?>
						<div class="opacity_dark_panel"></div>
					</div>
					<? if ($arItem['PROPERTIES']['URL']['VALUE']){ ?>
				</a>
			<? } ?>
			</div>
			<div class="grid_4 grid_4_sm">
				<?
				$arItem = array_shift($arResult['ITEMS']);
				?>
				<? if ($arItem['PROPERTIES']['URL']['VALUE']){ ?>
				<a href="<?= $arItem['PROPERTIES']['URL']['VALUE'] ?>">
					<? } ?>
					<div class="banner_wrapper banner_small" style="background-image:url('<?= $arItem['PROPERTIES']['HORIZONTAL_PICTURE_NARROW']['VALUE']['SRC'] ?>');">
						<?
						if($arParams['DISPLAY_NAME'] == 'Y') {
							?>
							<div class="banner_title_substrate">
								<span class="banner_title"><?= $arItem['~NAME'] ?></span>
							</div>
							<?
						}
						?>
						<div class="opacity_dark_panel"></div>
					</div>
					<? if ($arItem['PROPERTIES']['URL']['VALUE']){ ?>
				</a>
			<? } ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>