<?php
/**
* Date: 028 28.03
* Time: 13:43
*/
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
?>
<section class="row news">
	<div class="container_12 section_inner">
		<div class="grid_12 section_line_head">
			<h1 class="section_title"><? echo GetMessage('TPL_NEWS_TITLE');?></h1>
			<a href="<? echo $arResult['ITEMS'][0]['LIST_PAGE_URL']; ?>" class="section_more"><? echo GetMessage('TPL_NEWS_SHOW_ALL');?></a>
			<div class="owl_nav small dark" id="owl_news_nav"></div>
		</div>
		<div class="clear"></div>
		<div id="owl_news" class="slider_initial">
			<?
			foreach($arResult['ITEMS'] as $arItem) {
				?>
				<div class="news_item">
					<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>">
						<img src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" class="img_responsive">
					</a>
					<div class="description_wrapper">
						<span class="date"><? echo $arItem['ACTIVE_FROM']; ?></span>
						<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="">
							<h3 class="title"><? echo mb_substr($arItem['NAME'], 0, 58); ?><? if(strlen($arItem['NAME'])>59) echo '...'; ?></h3>
						</a>
						<p class="short_description"><? echo mb_substr($arItem['PREVIEW_TEXT'], 0, 115); ?><? if(strlen($arItem['PREVIEW_TEXT'])>116) echo '...'; ?></p>
					</div>
				</div>
				<?
			}
			?>
		</div>
	</div>
</section>
