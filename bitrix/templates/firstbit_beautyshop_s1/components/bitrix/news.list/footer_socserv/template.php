<?php
/**
 * Date: 028 28.03
 * Time: 13:43
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$this->SetFrameMode(true);
?>
<div class="social_inner">
	<?
	foreach($arResult['ITEMS'] as $arItem) {
		?>
		<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" title="<?=GetMessage('TPL_NEWS_SOCSERV_PAGE').' &mdash; '.$arItem['NAME']?>" class="i-social"><i class="fa fa-<?=$arItem['CODE']?>"></i></a>
		<?
	}
	?>
</div>