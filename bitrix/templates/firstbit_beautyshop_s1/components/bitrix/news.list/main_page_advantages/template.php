<?php
/**
 * Date: 028 28.03
 * Time: 13:43
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$this->SetFrameMode(true);
?>
<section class="row dark advantage">
	<div class="advantage_inner">
		<div class="container_16">
			<? foreach($arResult['ITEMS'] as $arItem) { ?>
				<div class="grid_4 grid_4_sm grid_8_xs">
					<div class="advantage_item">
						<?
						if($arItem['PREVIEW_PICTURE']['SRC']) {
							?>
							<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" />
							<?
						}
						?>
						<span><?=$arItem['PREVIEW_TEXT'] ?></span>
						<div class="v_separator"></div>
					</div>
				</div>
			<? } ?>
			<span class="clear"></span>
		</div>
	</div>
</section>