<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var string $strElementEdit */
/** @var string $strElementDelete */
/** @var array $arElementDeleteParams */
/** @var array $skuTemplate */
/** @var array $templateData */
global $APPLICATION;
$this->setFrameMode(true);
?>
<script><? require_once('script.js'); ?></script>
<section class="row newproduct">
	<div class="container_16 section_inner">
		<div class="grid_16 section_line_head">
			<h1 class="section_title"><? echo GetMessage('CT_NEW_TPL_TITLE');?></h1>
			<a href="<?=$arParams['PATH_TO_CATALOG']?>" class="section_more"><? echo GetMessage('CT_NEW_TPL_MESS_SHOW_ALL');?></a>
			<div class="owl_nav small dark" id="owl_newproduct_nav"></div>
		</div>
		<div class="clear"></div>
		<div id="owl_newproduct" class="slider_initial">
<?
foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$strMainID = $this->GetEditAreaId($arItem['ID']);

	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'MAIN_PROPS' => $strMainID.'_main_props',

		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'BASKET_ACTIONS' => $strMainID.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
		'COMPARE_LINK' => $strMainID.'_compare_link',

		'PRICE' => $strMainID.'_price',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',

		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
		'BASKET_PROP_DIV' => $strMainID.'_basket_prop'
	);

	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
	$productTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $arItem['NAME']
	);
	$imgTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $arItem['NAME']
	);

	$minPrice = false;
	if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE'])) {
		$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
	}

	$canBuy = $arItem['CAN_BUY'];
	$wishlistBtnMessage = ($arParams['MESS_BTN_WISHLIST'] != '' ? $arParams['MESS_BTN_WISHLIST'] : GetMessage('CATALOG_RECOMMENDED_PRODUCTS_TPL_MESS_BTN_WISHLIST'));
	?>
			<div class="product_item slider_product_item" id="<? echo $strMainID; ?>">
				<div class="product_item_inner">
					<div class="product_header">
						<?
						if ($arParams['DISPLAY_WISHLIST']) {
							?>
							<a data-wishlist-id="item_<? echo $arItem['ID']; ?>" class="fa fa-heart" title="<? echo $wishlistBtnMessage; ?>" onclick="wishlistHandler(this);return false;"></a>
							<script type="text/javascript">
								wishlistSetItem('item_<?=$arItem['ID']?>');
							</script>
							<?
						}
						?>
						<?
						if($arParams['USE_SALE'] == 'Y') {
							if($arItem['SALE_ITEM']) {
								?>
								<a href="<? echo $arItem['SALE_ITEM']['DETAIL_PAGE_URL']; ?>" title="<? echo $arItem['SALE_ITEM']['NAME']; ?>" class="i-timer"><span class="timer-inner" data-final-date="<? echo $arItem['SALE_ITEM']['PROPERTY_SALE_TO_VALUE']; ?>"><span>00</span><?=GetMessage('COUNTER_SECTION_DAYS')?> <span>01</span><?=GetMessage('COUNTER_SECTION_HOURS')?> <span>00</span><?=GetMessage('COUNTER_SECTION_MINUTES')?> <span>00</span><?=GetMessage('COUNTER_SECTION_SECONDS')?></span></a><br/>
								<?
							}
						}
						?>
						<?
						if ($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']) {
							?>
							<div class="i-new"><? echo $arItem['PROPERTIES']['NEWPRODUCT']['NAME']; ?></div><br/>
							<?
						}
						?>
						<?
						if ($arItem['PROPERTIES']['SALELEADER']['VALUE']) {
							?>
							<label class="i-hit"><? echo $arItem['PROPERTIES']['SALELEADER']['NAME'];?></label><br/>
							<?
						}
						?>
					</div>
					<div class="product_image">
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="bx_catalog_item_images"><img id="<?=$arItemIDs['PICT']?>" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" title="<?=$imgTitle?>" alt="<?=$imgTitle?>"/></a>
					</div>
					<div class="product_title">
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<? echo $arItem['NAME']; ?>"><h3><?=$arItem['NAME']?></h3></a>
					</div>
					<div class="buy_wrap">
							<div class="price_wrap">
								<span id="<? echo $arItemIDs['PRICE']; ?>" class="current_price">
									<?
									if (!empty($minPrice) && $minPrice['DISCOUNT_VALUE']>0) {
										if(!empty($arItem['OFFERS'])) {
											echo GetMessage('CT_NEW_TPL_MESS_PRICE_SIMPLE_MODE', array(
												'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE']
											));
										} else {
											echo $minPrice['PRINT_DISCOUNT_VALUE'];
										}
										if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) {
											?> <span class="old_price"><? echo $minPrice['PRINT_VALUE']; ?></span><?
										}
									} else {
										$notAvailableMessage = ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessage('CT_NEW_TPL_MESS_NULL_PRICE'));
										echo $notAvailableMessage;
									}
									?>
								</span>
							</div>
						<?
						if ($canBuy) {
						if(empty($arItem['OFFERS']) && !empty($minPrice) && $minPrice['DISCOUNT_VALUE']>0) {
						?>
							<div class="buy_btn_group">
								<?
								if ('Y' == $arParams['USE_PRODUCT_QUANTITY']) {
									?>
									<div class="buy_count_wrap">
										<div class="product_count">
											<a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0)" class="btn_minus" rel="nofollow">-</a>
											<input type="text" class="count_value" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
											<a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)" class="btn_plus" rel="nofollow">+</a>
										</div>
									</div>
									<?
								}
								?>
								<div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="bx_catalog_item_controls_blocktwo">
									<?
									if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY') {
										$basketTitle = ('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_NEW_TPL_MESS_BTN_BUY'));
									} else {
										$basketTitle = ('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_NEW_TPL_MESS_BTN_ADD_TO_BASKET'));
									}
									?>
									<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="cart_icon add_to_cart btn_round btn_color btn_wide" href="javascript:void(0)" rel="nofollow" title="<? echo $basketTitle; ?>"><span class="show_xs"><? echo $basketTitle; ?></span></a>
									<a class="cart_icon product_detail btn_round btn_disabled btn_hover_color" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" rel="nofollow" title="<? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_NEW_TPL_MESS_BTN_DETAIL')); ?>"><span class="show_xs"><? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_NEW_TPL_MESS_BTN_DETAIL')); ?></span></a>
								</div>
							</div>
						<?
						$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
						if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties) {
						?>
							<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
								<?
								if (!empty($arItem['PRODUCT_PROPERTIES_FILL'])) {
									foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
										?>
										<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
										<?
										if (isset($arItem['PRODUCT_PROPERTIES'][$propID])) {
											unset($arItem['PRODUCT_PROPERTIES'][$propID]);
										}
									}
								}
								$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
								if (!$emptyProductProperties) {
									?>
									<table>
										<?
										foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
											?>
											<tr>
												<td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
												<td>
													<?
													if ('L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE'] && 'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE']) {
														foreach ($propInfo['VALUES'] as $valueID => $value) {
															?>
															<label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br>
															<?
														}
													} else {
														?>
														<select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]">
															<?
															foreach ($propInfo['VALUES'] as $valueID => $value) {
																?>
																<option value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? 'selected' : ''); ?>><? echo $value; ?></option>
																<?
															}
															?>
														</select>
														<?
													}
													?>
												</td>
											</tr>
											<?
										}
										?>
									</table>
									<?
								}
								?>
							</div>
						<?
						}
						$arJSParams = array(
							'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
							'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
							'SHOW_ADD_BASKET_BTN' => false,
							'SHOW_BUY_BTN' => true,
							'SHOW_ABSENT' => true,
							'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
							'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
							'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
							'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
							'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
							'PRODUCT' => array(
								'ID' => $arItem['ID'],
								'NAME' => $productTitle,
								'PICT' => $arItem['PREVIEW_PICTURE'],
								'CAN_BUY' => $arItem["CAN_BUY"],
								'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
								'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
								'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
								'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
								'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
								'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
								'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
							),
							'VISUAL' => array(
								'ID' => $arItemIDs['ID'],
								'PICT_ID' => $arItemIDs['PICT'],
								'QUANTITY_ID' => $arItemIDs['QUANTITY'],
								'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
								'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
								'PRICE_ID' => $arItemIDs['PRICE'],
								'BUY_ID' => $arItemIDs['BUY_LINK'],
								'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
								'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
								'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
								'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
							),
							'BASKET' => array(
								'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
								'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
								'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
								'EMPTY_PROPS' => $emptyProductProperties,
								'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
								'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
							),
							'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
						);
						unset($emptyProductProperties);
						?>
							<script type="text/javascript">
								var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
							</script>
						<?
						} else {
						?>
							<div class="buy_btn_group not_available">
								<input type="hidden" class="count_value" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
								<div class="bx_catalog_item_controls_blocktwo">
									<a class="cart_icon product_detail btn_round btn_disabled btn_hover_color" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" rel="nofollow" title="<? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_NEW_TPL_MESS_BTN_DETAIL')); ?>"><span class="show_xs"><? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_NEW_TPL_MESS_BTN_DETAIL')); ?></span></a>
								</div>
							</div>
						<?
						}
						} else {
							?>
							<div class="buy_btn_group not_available">
								<input type="hidden" class="count_value" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
								<div class="bx_catalog_item_controls_blocktwo">
									<a class="cart_icon product_detail btn_round btn_disabled btn_hover_color" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" rel="nofollow" title="<? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_NEW_TPL_MESS_BTN_DETAIL')); ?>"><span class="show_xs"><? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_NEW_TPL_MESS_BTN_DETAIL')); ?></span></a>
								</div>
							</div>
							<?
						}
						?>
					</div>
				</div>
			</div>
	<?
}
?>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$.each($('#owl_newproduct .timer-inner'), function(){
					$(this).countdown({until: new Date($(this).attr('data-final-date')), layout: '{dn}{dl} {hnn}{hl} {mnn}{ml} {snn}{sl}', compact: true});
				});
			});
		</script>
	</div>
</section>
