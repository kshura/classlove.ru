<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$isFilter = ($arParams['USE_FILTER'] == 'Y');

if (!empty($arResult['ITEMS'])) {
	$skuTemplate = array();
	if (!empty($arResult['SKU_PROPS'])) {
		foreach ($arResult['SKU_PROPS'] as $arProp) {
			$propId = $arProp['ID'];
			$skuTemplate[$propId] = array(
				'SCROLL' => array(
					'START' => '',
					'FINISH' => '',
				),
				'FULL' => array(
					'START' => '',
					'FINISH' => '',
				),
				'ITEMS' => array()
			);
			$templateRow = '';
			if ('TEXT' == $arProp['SHOW_MODE']) {
				$skuTemplate[$propId]['SCROLL']['START'] = '<div class="bx_item_detail_size full" id="#ITEM#_prop_' . $propId . '_cont">' . '<span class="bx_item_section_name_gray">' . htmlspecialcharsbx($arProp['NAME']) . '</span>' . '<div class="bx_size"><ul id="#ITEM#_prop_' . $propId . '_list">';;
				$skuTemplate[$propId]['SCROLL']['FINISH'] = '</ul></div>' . '<div class="bx_slide_left" id="#ITEM#_prop_' . $propId . '_left" data-treevalue="' . $propId . '" style=""></div>' . '<div class="bx_slide_right" id="#ITEM#_prop_' . $propId . '_right" data-treevalue="' . $propId . '" style=""></div>' . '</div>';
				$skuTemplate[$propId]['FULL']['START'] = '<div class="product_params_wrapper" id="#ITEM#_prop_' . $propId . '_cont">' . '<span class="product_params_title">' . htmlspecialcharsbx($arProp['NAME']) . '</span>' . '<div class="product_params product_params_size"><ul id="#ITEM#_prop_' . $propId . '_list" class="product_params_list">';;
				$skuTemplate[$propId]['FULL']['FINISH'] = '</ul></div>' . '<div class="bx_slide_left" id="#ITEM#_prop_' . $propId . '_left" data-treevalue="' . $propId . '" style="display: none;"></div>' . '<div class="bx_slide_right" id="#ITEM#_prop_' . $propId . '_right" data-treevalue="' . $propId . '" style="display: none;"></div>' . '</div>';
				foreach ($arProp['VALUES'] as $value) {
					$value['NAME'] = htmlspecialcharsbx($value['NAME']);
					$skuTemplate[$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="' . $propId . '_' . $value['ID'] . '" data-onevalue="' . $value['ID'] . '" title="' . $value['NAME'] . '" class="product_params_item "><i></i><span class="cnt">' . $value['NAME'] . '</span></li>';
				}
				unset($value);
			} elseif ('PICT' == $arProp['SHOW_MODE']) {
				$skuTemplate[$propId]['SCROLL']['START'] = '<div class="bx_item_detail_sku full" id="#ITEM#_prop_' . $propId . '_cont">' . '<span class="bx_item_section_name_gray">' . htmlspecialcharsbx($arProp['NAME']) . '</span>' . '<div class="bx_sku"><ul id="#ITEM#_prop_' . $propId . '_list">';
				$skuTemplate[$propId]['SCROLL']['FINISH'] = '</ul></div>' . '<div class="bx_slide_left" id="#ITEM#_prop_' . $propId . '_left" data-treevalue="' . $propId . '" style=""></div>' . '<div class="bx_slide_right" id="#ITEM#_prop_' . $propId . '_right" data-treevalue="' . $propId . '" style=""></div>' . '</div>';
				$skuTemplate[$propId]['FULL']['START'] = '<div class="product_params_wrapper" id="#ITEM#_prop_' . $propId . '_cont">' . '<span class="product_params_title">' . htmlspecialcharsbx($arProp['NAME']) . '</span>' . '<div class="product_params product_params_sku"><ul id="#ITEM#_prop_' . $propId . '_list" class="product_params_list">';
				$skuTemplate[$propId]['FULL']['FINISH'] = '</ul></div>' . '<div class="bx_slide_left" id="#ITEM#_prop_' . $propId . '_left" data-treevalue="' . $propId . '" style="display: none;"></div>' . '<div class="bx_slide_right" id="#ITEM#_prop_' . $propId . '_right" data-treevalue="' . $propId . '" style="display: none;"></div>' . '</div>';
				foreach ($arProp['VALUES'] as $value) {
					$value['NAME'] = htmlspecialcharsbx($value['NAME']);
					$skuTemplate[$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="' . $propId . '_' . $value['ID'] . '" data-onevalue="' . $value['ID'] . '" class="product_params_item"><i title="' . $value['NAME'] . '"></i>' . '<span class="cnt" style="background-image:url(\'' . $value['PICT']['SRC'] . '\');" title="' . $value['NAME'] . '"><span class="cnt_item"></span></span></li>';
				}
				unset($value);
			}
		}
		unset($templateRow, $arProp);
	}

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

	?>
	<div class="list_view">
		<ul class="product_item_list">
			<?
			foreach ($arResult['ITEMS'] as $key => $arItem) {
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
				$strMainID = $this->GetEditAreaId($arItem['ID']);
				$arItemIDs = array(
					'ID' => $strMainID,
					'PICT' => $strMainID . '_pict',
					'STICKER_ID' => $strMainID . '_sticker',
					'SECOND_STICKER_ID' => $strMainID . '_secondsticker',
					'QUANTITY' => $strMainID . '_quantity',
					'QUANTITY_DOWN' => $strMainID . '_quant_down',
					'QUANTITY_UP' => $strMainID . '_quant_up',
					'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
					'BUY_LINK' => $strMainID . '_buy_link',
					'BASKET_ACTIONS' => $strMainID . '_basket_actions',
					'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
					'SUBSCRIBE_LINK' => $strMainID . '_subscribe',
					'COMPARE_LINK' => $strMainID . '_compare_link',
					'PRICE' => $strMainID . '_price',
					'DSC_PERC' => $strMainID . '_dsc_perc',
					'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',
					'PROP_DIV' => $strMainID . '_sku_tree',
					'PROP' => $strMainID . '_prop_',
					'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
					'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
				);
				$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
				$productTitle = (isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != '' ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arItem['NAME']);
				$imgTitle = (isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != '' ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] : $arItem['NAME']);
				$minPrice = false;
				if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE'])) {
					$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
				}

				$canBuyItem = $arItem['CAN_BUY'];

				$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCS_TPL_MESS_BTN_COMPARE'));
				$wishlistBtnMessage = ($arParams['MESS_BTN_WISHLIST'] != '' ? $arParams['MESS_BTN_WISHLIST'] : GetMessage('CT_BCS_TPL_MESS_BTN_WISHLIST'));
				?>
				<li class="product_item">
					<div class="product_item_inner" id="<? echo $strMainID; ?>">
						<div class="product_header">
							<div class="product_col1">
								<div class="product_image">
									<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="list_product_img_link"><img id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" title="<? echo $imgTitle; ?>" alt="<? echo $imgTitle; ?>"/></a>
								</div>
								<div class="product_title"><a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $productTitle; ?>"><? echo $productTitle; ?></a></div>
								<div class="product_info">
									<?
									if ($canBuyItem) {
										?>
										<i class="fa fa-check green"></i>
										<span><?= GetMessage('CT_BCS_TPL_MESS_PRODUCT_AVAILABLE') ?></span>
										<?
									} else {
										?>
										<i class="fa fa-ban red"></i>
										<span><?= GetMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE') ?></span>
										<?
									}
									if ($arParams['DISPLAY_WISHLIST']) {
										?>
										<a data-wishlist-id="item_<? echo $arItem['ID']; ?>" class="fa fa-heart" title="<? echo $wishlistBtnMessage; ?>" onclick="wishlistHandler(this);return false;"></a>
										<script type="text/javascript">
											wishlistSetItem('item_<?=$arItem['ID']?>');
										</script>
										<?
									}
									if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS'])) {
										if ($arParams['DISPLAY_COMPARE']) {
											if (is_array($_SESSION['CATALOG_COMPARE_LIST'][$arParams["IBLOCK_ID"]]['ITEMS']) && array_key_exists($arItem['ID'], $_SESSION['CATALOG_COMPARE_LIST'][$arParams["IBLOCK_ID"]]['ITEMS'])) {
												$compareClass = "active";
											} else {
												$compareClass = "";
											}
											?>
											<a id="<? echo $arItemIDs['COMPARE_LINK']; ?>" data-compare-id="<? echo $arItem['ID']; ?>" class="fa fa-bar-chart <? echo $compareClass; ?>" title="<? echo $compareBtnMessage; ?>" href="" onclick="compareHandler(this);return false;"></a>
											<script type="text/javascript">
												compareCheck(<?=$arItem['ID']?>);
											</script>
											<?
										}
									}
									?>
								</div>
							</div>
							<div class="product_col2">
								<div class="buy_wrap">
									<?
									if ($canBuyItem) {
										?>
										<div class="price_wrap">
								<span id="<? echo $arItemIDs['PRICE']; ?>" class="current_price">
									<?
									if (!empty($minPrice) && $minPrice['DISCOUNT_VALUE']>0) {
										if (!empty($arItem['OFFERS'])) {
											echo GetMessage('CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE_LIST', array(
												'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE'],
												'#MEASURE#' => GetMessage('CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE', array(
													'#VALUE#' => $minPrice['CATALOG_MEASURE_RATIO'],
													'#UNIT#' => $minPrice['CATALOG_MEASURE_NAME']
												))
											));
										} else {
											echo $minPrice['PRINT_DISCOUNT_VALUE'];
										}
										if (empty($arItem['OFFERS'])) {
											if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) {
												?><span class="old_price"><? echo $minPrice['PRINT_VALUE']; ?></span><?
											}
										}
									} else {
										$notAvailableMessage = ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NULL_PRICE'));
										?>
										<span class="not_available">
										<? echo $notAvailableMessage; ?>
										</span>
										<?
									}
									?>
								</span>
										</div>
										<div class="buy_btn_group">
											<?
											if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS'])) {
											if($canBuyItem && !empty($arItem['MIN_PRICE']) && $arItem['MIN_PRICE']['DISCOUNT_VALUE']>0) {
												?>
												<div class="buy_count_wrap">
													<div class="product_count">
														<a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>"
														 href="javascript:void(0)" class="btn_minus" rel="nofollow">-</a>
														<input type="text" class="count_value"
														 id="<? echo $arItemIDs['QUANTITY']; ?>"
														 name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
														 value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
														<a id="<? echo $arItemIDs['QUANTITY_UP']; ?>"
														 href="javascript:void(0)" class="btn_plus" rel="nofollow">+</a>
													</div>
												</div>
												<div class="buy_btn_wrap" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>">
													<?
													if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY') {
														$basketTitle = ('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCS_TPL_MESS_BTN_BUY'));
													} else {
														$basketTitle = ('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET'));
													}
													?>
													<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="btn_round btn_color btn_wide"
													 href="javascript:void(0)" rel="nofollow"
													 title="<?= $basketTitle ?>"><?= $basketTitle ?></a>
												</div>
												<?
											} else {
												?>
												<?
												$detailTitle = ('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL'));
												?>
												<a class="btn_round btn_disabled btn_hover_color" href="<?=$arItem['DETAIL_PAGE_URL']?>" rel="nofollow" rel="nofollow" title="<?=$detailTitle?>"><?=$detailTitle?></a>
											<?
											}
											?>

											<?
											$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
											if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties) {
											?>
												<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
													<?
													if (!empty($arItem['PRODUCT_PROPERTIES_FILL'])) {
														foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
															?>
															<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
															<?
															if (isset($arItem['PRODUCT_PROPERTIES'][$propID])) {
																unset($arItem['PRODUCT_PROPERTIES'][$propID]);
															}
														}
													}
													$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
													if (!$emptyProductProperties) {
														?>
														<table>
															<?
															foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
																?>
																<tr>
																	<td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
																	<td>
																		<?
																		if ('L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE'] && 'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE']) {
																			foreach ($propInfo['VALUES'] as $valueID => $value) {
																				?>
																				<label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br>
																				<?
																			}
																		} else {
																			?>
																			<select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]">
																				<?
																				foreach ($propInfo['VALUES'] as $valueID => $value) {
																					?>
																					<option value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? 'selected' : ''); ?>><? echo $value; ?></option>
																					<?
																				}
																				?>
																			</select>
																			<?
																		}
																		?>
																	</td>
																</tr>
																<?
															}
															?>
														</table>
														<?
													}
													?>
												</div>
											<?
											}
											$arJSParams = array(
												'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
												'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
												'SHOW_ADD_BASKET_BTN' => false,
												'SHOW_BUY_BTN' => true,
												'SHOW_ABSENT' => true,
												'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
												'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
												'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
												'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
												'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
												'PRODUCT' => array(
													'ID' => $arItem['ID'],
													'NAME' => $productTitle,
													'PICT' => $arItem['PREVIEW_PICTURE'],
													'CAN_BUY' => $arItem["CAN_BUY"],
													'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
													'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
													'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
													'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
													'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
													'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
													'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
												),
												'BASKET' => array(
													'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
													'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
													'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
													'EMPTY_PROPS' => $emptyProductProperties,
													'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
													'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
												),
												'VISUAL' => array(
													'ID' => $arItemIDs['ID'],
													'PICT_ID' => $arItemIDs['PICT'],
													'QUANTITY_ID' => $arItemIDs['QUANTITY'],
													'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
													'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
													'PRICE_ID' => $arItemIDs['PRICE'],
													'BUY_ID' => $arItemIDs['BUY_LINK'],
													'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
													'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
													'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
													'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
												),
												'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
											);
											if ($arParams['DISPLAY_COMPARE']) {
												$arJSParams['COMPARE'] = array(
													'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
													'COMPARE_PATH' => $arParams['COMPARE_PATH']
												);
											}
											unset($emptyProductProperties);
											?>
												<script type="text/javascript">
													var <? echo $strObName; ?> = new JCCatalogSectionList(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
												</script>
											<?
											} else {
											?>
												<div class="btn_round btn_border btn_hover_color btn_wide btn_more_offers" onclick="openProduct(this);"><?=GetMessage('CT_BCS_CATALOG_OFFERS_LIST')?><i class="fa fa-chevron-circle-up"></i></div>
												<?
											}
											?>
										</div>
										<?
									} else {
										$notAvailableMessage = ('' != $arParams['MESS_NOT_AVAILABLE'] ? $arParams['MESS_NOT_AVAILABLE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE'));
										?>
										<div id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="price_wrap">
											<span class="current_price not_available"><? echo $notAvailableMessage; ?></span>
										</div>
										<div class="buy_btn_group">
											<?
											if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS'])) {
												?>

												<div class="buy_btn_wrap">
													<?
													$detailTitle = ('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL'));
													?>
													<a class="btn_round btn_disabled btn_hover_color" href="<?=$arItem['DETAIL_PAGE_URL']?>" rel="nofollow" rel="nofollow" title="<?=$detailTitle?>"><?=$detailTitle?></a>
												</div>
												<?
											} else {
												?>
												<div class="btn_round btn_border btn_hover_color btn_wide btn_more_offers" onclick="openProduct(this);"><?=GetMessage('CT_BCS_CATALOG_OFFERS_LIST')?><i class="fa fa-chevron-circle-up"></i></div>
												<?
											}
											?>
										</div>
										<?
									}
									?>
								</div>
							</div>
						</div>
						<?
						if (isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
							?>
							<ul class="product_offers_list">
								<?
								foreach ($arItem['JS_OFFERS'] as $itemOffer) {
									$strSkuID = $this->GetEditAreaId($itemOffer['ID']);
									$arSkuIDs = array(
										'ID' => $strSkuID,
										'STICKER_ID' => $strSkuID . '_sticker',
										'SECOND_STICKER_ID' => $strSkuID . '_secondsticker',
										'QUANTITY' => $strSkuID . '_quantity',
										'QUANTITY_DOWN' => $strSkuID . '_quant_down',
										'QUANTITY_UP' => $strSkuID . '_quant_up',
										'QUANTITY_MEASURE' => $strSkuID . '_quant_measure',
										'BUY_LINK' => $strSkuID . '_buy_link',
										'BASKET_ACTIONS' => $strSkuID . '_basket_actions',
										'NOT_AVAILABLE_MESS' => $strSkuID . '_not_avail',
										'SUBSCRIBE_LINK' => $strSkuID . '_subscribe',
										'COMPARE_LINK' => $strSkuID . '_compare_link',
										'PRICE' => $strSkuID . '_price',
										'DSC_PERC' => $strSkuID . '_dsc_perc',
										'SECOND_DSC_PERC' => $strSkuID . '_second_dsc_perc',
										'PROP_DIV' => $strSkuID . '_sku_tree',
										'PROP' => $strSkuID . '_prop_',
										'DISPLAY_PROP_DIV' => $strSkuID . '_sku_prop',
										'BASKET_PROP_DIV' => $strSkuID . '_basket_prop',
									);
									$strSkuObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strSkuID);

									$canBuyOffer = $itemOffer['CAN_BUY'];
									?>
									<li class="product_offer" id="<? echo $strSkuID; ?>">
										<div class="product_col1">
											<div class="product_title"><?=$itemOffer['NAME']?></div>
											<div class="product_info">
												<?
												if ($canBuyOffer) {
													?>
													<i class="fa fa-check green"></i>
													<span><?= GetMessage('CT_BCS_TPL_MESS_PRODUCT_AVAILABLE') ?></span>
													<?
												}
												else {
													?>
													<i class="fa fa-ban red"></i>
													<span><?= GetMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE') ?></span>
													<?
												}
												?>
												<?
												if ($arParams['DISPLAY_COMPARE']) {
													if (is_array($_SESSION['CATALOG_COMPARE_LIST'][$arParams["IBLOCK_ID"]]['ITEMS']) && array_key_exists($itemOffer['ID'], $_SESSION['CATALOG_COMPARE_LIST'][$arParams["IBLOCK_ID"]]['ITEMS'])) {
														$compareClass = "active";
													} else {
														$compareClass = "";
													}
													?>
													<a id="<? echo $arItemIDs['COMPARE_LINK']; ?>" data-compare-id="<? echo $itemOffer['ID']; ?>" class="fa fa-bar-chart <? echo $compareClass; ?>" title="<? echo $compareBtnMessage; ?>" href="" onclick="compareHandler(this);return false;"></a>
													<script type="text/javascript">
														compareCheck(<?=$itemOffer['ID']?>);
													</script>
													<?
												}
												?>
											</div>
										</div>
										<div class="product_col2" id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>">
											<div class="buy_wrap">
												<?
												if ($canBuyOffer) {
													?>
													<div class="price_wrap">
										<span id="<? echo $arSkuIDs['PRICE']; ?>" class="current_price">
											<?
											if (!empty($itemOffer['PRICE']) && $itemOffer['PRICE']['DISCOUNT_VALUE']>0) {
												echo $itemOffer['PRICE']['PRINT_DISCOUNT_VALUE'];
												if ('Y' == $arParams['SHOW_OLD_PRICE'] && $itemOffer['PRICE']['DISCOUNT_VALUE'] < $itemOffer['PRICE']['VALUE']) {
													?> <span class="old_price"><? echo $itemOffer['PRICE']['PRINT_VALUE']; ?></span><?
												}
											} else {
												?>
												<span class="not_available">
												<?
												$notAvailableMessage = ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NULL_PRICE'));
												echo $notAvailableMessage;
												?>
												</span>
												<?
											}
											?>
										</span>
													</div>
													<?
												} else {
													$notAvailableMessage = ('' != $arParams['MESS_NOT_AVAILABLE'] ? $arParams['MESS_NOT_AVAILABLE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE'))
													?>
													<div id="<? echo $arSkuIDs['NOT_AVAILABLE_MESS']; ?>" class="price_wrap">
														<div class="current_price not_available"><?=$notAvailableMessage?></div>
													</div>
													<?
												}
												?>
												<div class="buy_btn_group">
													<?

													if($canBuyOffer && !empty($itemOffer['PRICE']) && $itemOffer['PRICE']['DISCOUNT_VALUE']>0) {
														?>
														<div class="buy_count_wrap">
															<div class="product_count">
																<a id="<? echo $arSkuIDs['QUANTITY_DOWN']; ?>"
																 href="javascript:void(0)" class="btn_minus" rel="nofollow">-</a>
																<input type="text" class="count_value"
																 id="<? echo $arSkuIDs['QUANTITY']; ?>"
																 name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
																 value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
																<a id="<? echo $arSkuIDs['QUANTITY_UP']; ?>"
																 href="javascript:void(0)" class="btn_plus" rel="nofollow">+</a>
															</div>
														</div>
														<div class="buy_btn_wrap" id="<? echo $arSkuIDs['BASKET_ACTIONS']; ?>">
															<?
															if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY') {
																$basketTitle = ('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCS_TPL_MESS_BTN_BUY'));
															} else {
																$basketTitle = ('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET'));
															}
															?>
															<a id="<? echo $arSkuIDs['BUY_LINK']; ?>" class="btn_round btn_color btn_wide"
															 href="javascript:void(0)" rel="nofollow"
															 title="<?= $basketTitle ?>"><?= $basketTitle ?></a>
														</div>
														<?
													} else {
														?>
														<div class="buy_btn_wrap">
															<?
															$detailTitle = ('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL'));
															?>
															<a class="btn_round btn_disabled btn_hover_color" href="<?=$arItem['DETAIL_PAGE_URL']?>" rel="nofollow" rel="nofollow" title="<?=$detailTitle?>"><?=$detailTitle?></a>
														</div>
														<?
													}
													?>
												</div>
											</div>
										</div>
									</li>
								<?

								$boolShowOfferProps = ('Y' == $arParams['PRODUCT_DISPLAY_MODE'] && $arItem['OFFERS_PROPS_DISPLAY']);
								$boolShowProductProps = (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES']));
								if (!empty($arItem['OFFERS_PROP'])) {
								$arSkuProps = array();
								?>

								<?

								$arJSParams = array(
									'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
									'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
									'SHOW_ADD_BASKET_BTN' => false,
									'SHOW_BUY_BTN' => true,
									'SHOW_ABSENT' => true,
									'SHOW_SKU_PROPS' => $arItem['OFFERS_PROPS_DISPLAY'],
									'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
									'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
									'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
									'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
									'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
									'DEFAULT_PICTURE' => array(
										'PICTURE' => $arItem['PRODUCT_PREVIEW'],
										'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
									),
									'VISUAL' => array(
										'ID' => $arSkuIDs['ID'],
										'PICT_ID' => $arSkuIDs['PICT'],
										'QUANTITY_ID' => $arSkuIDs['QUANTITY'],
										'QUANTITY_UP_ID' => $arSkuIDs['QUANTITY_UP'],
										'QUANTITY_DOWN_ID' => $arSkuIDs['QUANTITY_DOWN'],
										'QUANTITY_MEASURE' => $arSkuIDs['QUANTITY_MEASURE'],
										'PRICE_ID' => $arSkuIDs['PRICE'],
										'TREE_ID' => $arSkuIDs['PROP_DIV'],
										'TREE_ITEM_ID' => $arSkuIDs['PROP'],
										'BUY_ID' => $arSkuIDs['BUY_LINK'],
										'ADD_BASKET_ID' => $arSkuIDs['ADD_BASKET_ID'],
										'DSC_PERC' => $arSkuIDs['DSC_PERC'],
										'SECOND_DSC_PERC' => $arSkuIDs['SECOND_DSC_PERC'],
										'DISPLAY_PROP_DIV' => $arSkuIDs['DISPLAY_PROP_DIV'],
										'BASKET_ACTIONS_ID' => $arSkuIDs['BASKET_ACTIONS'],
										'NOT_AVAILABLE_MESS' => $arSkuIDs['NOT_AVAILABLE_MESS'],
										'COMPARE_LINK_ID' => $arSkuIDs['COMPARE_LINK']
									),
									'BASKET' => array(
										'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
										'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
										'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
										'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
										'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
									),
									'PRODUCT' => array(
										'ID' => $itemOffer['ID'],
										'NAME' => $itemOffer['NAME']
									),
									'OFFERS' => $arItem['JS_OFFERS'],
									'OFFER_SELECTED' => $arItem['OFFERS_SELECTED'],
									'TREE_PROPS' => $arSkuProps,
									'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
								);
								if ($arParams['DISPLAY_COMPARE']) {
									$arJSParams['COMPARE'] = array(
										'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
										'COMPARE_PATH' => $arParams['COMPARE_PATH']
									);
								}
								?>
									<script type="text/javascript">
										var <? echo $strSkuObName; ?> = new JCCatalogSectionList(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
									</script>
									<?
								}

									?>
									<?
								}
								?>
							</ul>
							<?
						}
						?>
					</div>
				</li>
				<?
			}
			?>
		</ul>
	</div>
	<script type="text/javascript">
		BX.message({
			BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
			BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
			ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
			TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
			TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
			TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
			BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
			BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
			BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
			BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
			COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
			COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
			COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
			BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
			SITE_ID: '<? echo SITE_ID; ?>'
		});
	</script>
	<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
}