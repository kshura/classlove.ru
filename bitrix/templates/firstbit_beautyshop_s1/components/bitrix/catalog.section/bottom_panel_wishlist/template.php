<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$isFilter = ($arParams['USE_FILTER'] == 'Y');
?>
	<script><? require_once('script.js'); ?></script>
<?
if (!empty($arResult['ITEMS'])) {
	$URI = dirname(preg_replace("/^[^\:\/]+[\:\/]+[^\:\/]+(.+)/","$1",$_SERVER['HTTP_REFERER'])).'/';
?>
	<div class="bottom_panel_first_line" data-wishlist="">
		<div class="grid_16 relative owl_nav dark" id="owl_wishlist_bottom_nav"></div>
		<div class="carousel_wrap">
			<ul id="owl_wishlist_bottom_panel" class="slider_initial">
				<?
				foreach ($arResult['ITEMS'] as $key => $arItem) {
					$strMainID = $this->GetEditAreaId($arItem['ID']);
					$arItemIDs = array(
						'ID' => $strMainID,
						'PICT' => $strMainID . '_pict',
						'STICKER_ID' => $strMainID . '_sticker',
						'SECOND_STICKER_ID' => $strMainID . '_secondsticker',
						'QUANTITY' => $strMainID . '_quantity',
						'QUANTITY_DOWN' => $strMainID . '_quant_down',
						'QUANTITY_UP' => $strMainID . '_quant_up',
						'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
						'BUY_LINK' => $strMainID . '_buy_link',
						'BASKET_ACTIONS' => $strMainID . '_basket_actions',
						'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
						'SUBSCRIBE_LINK' => $strMainID . '_subscribe',
						'COMPARE_LINK' => $strMainID . '_compare_link',
						'PRICE' => $strMainID . '_price',
						'DSC_PERC' => $strMainID . '_dsc_perc',
						'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',
						'PROP_DIV' => $strMainID . '_sku_tree',
						'PROP' => $strMainID . '_prop_',
						'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
						'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
					);
					$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
					$productTitle = (isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != '' ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arItem['NAME']);
					$imgTitle = (isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != '' ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] : $arItem['NAME']);
					$minPrice = false;
					if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE'])) {
						$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
					}
					$canBuy = $arItem['CAN_BUY'];
					?>
					<li class="panel_carousel product_item" id="<? echo $strMainID; ?>" data-wishlist-item="item_<? echo $arItem['ID']; ?>">
						<div class="close_btn">
							<a href="<? echo $APPLICATION->GetCurPage(); ?>" data-wishlist-id="item_<? echo $arItem['ID']; ?>" title="<? echo GetMessage('CT_BCS_TPL_MESS_BTN_DELETE'); ?>" onmouseup="wishlistHandler(this)"><i class="fa fa-times delete_panel_item"></i></a>
						</div>
						<div class="product_image">
						<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="bx_catalog_item_images"><img id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" title="<? echo $imgTitle; ?>" alt="<? echo $imgTitle; ?>"/></a>
						</div>
						<div class="product_description">
							<div class="product_title">
								<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $productTitle; ?>"><h3><? echo $productTitle; ?></h3></a>
							</div>
								<div class="price_wrap">
									<span id="<? echo $arItemIDs['PRICE']; ?>" class="current_price">
										<?
										if (!empty($minPrice) && $minPrice['DISCOUNT_VALUE']>0) {
											if (!empty($arItem['OFFERS'])) {
												echo GetMessage('CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE', array(
													'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE']
												));
											} else {
												echo $minPrice['PRINT_DISCOUNT_VALUE'];
											}
											if (empty($arItem['OFFERS'])) {
												if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) {
													?>
													<span class="old_price"><? echo $minPrice['PRINT_VALUE']; ?></span>
													<?
												}
											}
										} else {
											$notAvailableMessage = ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NULL_PRICE'));
											echo $notAvailableMessage;
										}
										?>
									</span>
								</div>
						</div>
					</li>
					<?
					unset($minPrice);
				}
				?>
			</ul>
		</div>
		<div style="clear: both;"></div>
		<div class="empty_message">
			<div class=""><? echo GetMessage('CT_BCS_TPL_EMPTY_MESSAGE');?></div>
		</div>
	</div>
	<div class="bottom_panel_second_line">
		<a href="<?=$arParams['PATH_TO_WISHLIST']?>" class="btn_round btn_color"><? echo GetMessage('CT_BCS_TPL_MESS_GOTO_WISHLIST'); ?></a>
		<a href="<?=$arParams['PATH_TO_CATALOG']?>" class="btn_round btn_color hidden"><? echo GetMessage('CT_BCS_TPL_MESS_GOTO_CATALOG'); ?></a>
	</div>
	<?
} else {
	?>
	<div class="bottom_panel_first_line">
		<div class="empty_message active">
			<div class=""><? echo GetMessage('CT_BCS_TPL_EMPTY_MESSAGE');?></div>
		</div>
	</div>
	<div class="bottom_panel_second_line empty_line">
		<a href="<?=$arParams['PATH_TO_CATALOG']?>" class="btn_round btn_color hidden"><? echo GetMessage('CT_BCS_TPL_MESS_GOTO_CATALOG'); ?></a>
	</div>
	<?
}
?>