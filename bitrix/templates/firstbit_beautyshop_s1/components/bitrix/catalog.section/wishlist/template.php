<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$isFilter = ($arParams['USE_FILTER'] == 'Y');

if (!empty($arResult['ITEMS'])) {
	$skuTemplate = array();
	if (!empty($arResult['SKU_PROPS'])) {
		foreach ($arResult['SKU_PROPS'] as $arProp) {
			$propId = $arProp['ID'];
			$skuTemplate[$propId] = array(
				'START' => '',
				'FINISH' => '',
				'ITEMS' => array()
			);
			$templateRow = '';
			if ('TEXT' == $arProp['SHOW_MODE']) {
				$skuTemplate[$propId]['START'] = '<div class="product_params_wrapper" id="#ITEM#_prop_' . $propId . '_cont">' . '<span class="product_params_title">' . htmlspecialcharsbx($arProp['NAME']) . '</span>' . '<div class="product_params product_params_size"><ul id="#ITEM#_prop_' . $propId . '_list" class="product_params_list">';
				$skuTemplate[$propId]['FINISH'] = '</ul></div>' . '</div>';
				foreach ($arProp['VALUES'] as $value) {
					$value['NAME'] = htmlspecialcharsbx($value['NAME']);
					$skuTemplate[$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="' . $propId . '_' . $value['ID'] . '" data-onevalue="' . $value['ID'] . '" title="' . $value['NAME'] . '" class="product_params_item "><i></i><span class="cnt">' . $value['NAME'] . '</span></li>';
				}
				unset($value);
			} elseif ('PICT' == $arProp['SHOW_MODE']) {
				$skuTemplate[$propId]['START'] = '<div class="product_params_wrapper" id="#ITEM#_prop_' . $propId . '_cont">' . '<span class="product_params_title">' . htmlspecialcharsbx($arProp['NAME']) . '</span>' . '<div class="product_params product_params_sku"><ul id="#ITEM#_prop_' . $propId . '_list" class="product_params_list">';
				$skuTemplate[$propId]['FINISH'] = '</ul></div>' . '</div>';
				foreach ($arProp['VALUES'] as $value) {
					$value['NAME'] = htmlspecialcharsbx($value['NAME']);
					$skuTemplate[$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="' . $propId . '_' . $value['ID'] . '" data-onevalue="' . $value['ID'] . '" class="product_params_item"><i title="' . $value['NAME'] . '"></i>' . '<span class="cnt" style="background-image:url(\'' . $value['PICT']['SRC'] . '\');" title="' . $value['NAME'] . '"><span class="cnt_item"></span></span></li>';
				}
				unset($value);
			}
		}
		unset($templateRow, $arProp);
	}

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>

	<div class="container_12">
	<div class="grid_view margin_xs_10 wishlist" data-wishlist="">
		<?
		foreach ($arResult['ITEMS'] as $key => $arItem) {
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
			$strMainID = $this->GetEditAreaId($arItem['ID']);
			$arItemIDs = array(
				'ID' => $strMainID,
				'PICT' => $strMainID . '_pict',
				'STICKER_ID' => $strMainID . '_sticker',
				'SECOND_STICKER_ID' => $strMainID . '_secondsticker',
				'QUANTITY' => $strMainID . '_quantity',
				'QUANTITY_DOWN' => $strMainID . '_quant_down',
				'QUANTITY_UP' => $strMainID . '_quant_up',
				'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
				'BUY_LINK' => $strMainID . '_buy_link',
				'BASKET_ACTIONS' => $strMainID . '_basket_actions',
				'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
				'SUBSCRIBE_LINK' => $strMainID . '_subscribe',
				'COMPARE_LINK' => $strMainID . '_compare_link',
				'PRICE' => $strMainID . '_price',
				'DSC_PERC' => $strMainID . '_dsc_perc',
				'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',
				'PROP_DIV' => $strMainID . '_sku_tree',
				'PROP' => $strMainID . '_prop_',
				'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
				'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
			);
			$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
			$productTitle = (isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != '' ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arItem['NAME']);
			$imgTitle = (isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != '' ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] : $arItem['NAME']);

			$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCS_TPL_MESS_BTN_COMPARE'));
			$wishlistBtnMessage = ($arParams['MESS_BTN_WISHLIST'] != '' ? $arParams['MESS_BTN_WISHLIST'] : GetMessage('CT_BCS_TPL_MESS_BTN_WISHLIST'));
			?>
			<div class="grid_3 grid_4_sm product_item" data-wishlist-item="item_<? echo $arItem['ID']; ?>">
				<div class="product_item_inner" id="<? echo $strMainID; ?>">
					<div class="close_btn">
						<a href="<?=$arParams['PATH_TO_WISHLIST']?>" data-wishlist-id="item_<? echo $arItem['ID']; ?>" title="<? echo GetMessage('CT_BCS_TPL_MESS_BTN_DELETE'); ?>" onmouseup="wishlistHandler(this);"><i class="fa fa-times"></i></a>
					</div>
					<div class="product_header">
						<?
						if ($arParams['DISPLAY_COMPARE']) {
							if (is_array($_SESSION['CATALOG_COMPARE_LIST'][$arParams["IBLOCK_ID"]]['ITEMS']) && array_key_exists($arItem['ID'], $_SESSION['CATALOG_COMPARE_LIST'][$arParams["IBLOCK_ID"]]['ITEMS'])) {
								$compareClass = "active";
							} else {
								$compareClass = "";
							}
							?>
							<a id="<? echo $arItemIDs['COMPARE_LINK']; ?>" data-compare-id="<? echo $arItem['ID']; ?>" class="fa grey_raiting <? echo $compareClass; ?>" title="<? echo $compareBtnMessage; ?>" href="" onclick="compareHandler(this);return false;"></a>
							<script type="text/javascript">
								compareCheck(<?=$arItem['ID']?>);
							</script>
							<?
						}
						?>
						<?
						if($arParams['USE_SALE'] == 'Y') {
							if($arItem['SALE_ITEM']) {
								?>
								<a href="<? echo $arItem['SALE_ITEM']['DETAIL_PAGE_URL']; ?>" title="<? echo $arItem['SALE_ITEM']['NAME']; ?>" class="i-timer"><span class="timer-inner" data-final-date="<? echo $arItem['SALE_ITEM']['PROPERTY_SALE_TO_VALUE']; ?>"><span>00</span><?=GetMessage('COUNTER_SECTION_DAYS')?> <span>01</span><?=GetMessage('COUNTER_SECTION_HOURS')?> <span>00</span><?=GetMessage('COUNTER_SECTION_MINUTES')?> <span>00</span><?=GetMessage('COUNTER_SECTION_SECONDS')?></span></a><br/>
								<?
							}
						}
						?>
						<?
						if ($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']) {
							?>
							<div class="i-new"><? echo $arItem['PROPERTIES']['NEWPRODUCT']['NAME']; ?></div><br/>
							<?
						}
						?>
						<?
						if ($arItem['PROPERTIES']['SALELEADER']['VALUE']) {
							?>
							<div class="i-hit"><? echo $arItem['PROPERTIES']['SALELEADER']['NAME'];?></div><br/>
							<?
						}
						?>
					</div>
					<div class="product_image">
 		 			<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="bx_catalog_item_images"><img id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" title="<? echo $imgTitle; ?>" alt="<? echo $imgTitle; ?>"/></a>
					</div>
					<div class="product_title">
						<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $productTitle; ?>"><h3><? echo $productTitle; ?></h3></a>
					</div>
					<div class="buy_wrap">
							<div class="price_wrap">
								<span class="current_price" id="<? echo $arItemIDs['PRICE']; ?>">
									<?
									$minPrice = false;
									$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
									if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS'])) {
										$canBuy = $arItem['CAN_BUY'];
									} else {
										$canBuy = $arItem['JS_OFFERS'][$arItem['OFFERS_SELECTED']]['CAN_BUY'];
									}
									if (!empty($minPrice) && $minPrice['DISCOUNT_VALUE']>0) {
										echo $minPrice['PRINT_DISCOUNT_VALUE'];
										if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) {
											?>
											<span class="old_price"><? echo $minPrice['PRINT_VALUE']; ?></span>
											<?
										}
									} else {
										$notAvailableMessage = ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NULL_PRICE'));
										echo $notAvailableMessage;
									}
									?>
								</span>
							</div>
							<?
							$buy_btn_group_class = '';
							if(!empty($arItem['OFFERS']))
								$buy_btn_group_class .= ' initial';
							if(empty($arResult['OFFERS']) && !$canBuy)
								$buy_btn_group_class .= ' not_available';
							if(empty($arResult['OFFERS']) && empty($minPrice) || empty($arResult['OFFERS']) && $minPrice['DISCOUNT_VALUE']<=0)
								$buy_btn_group_class .= ' null_price';
							?>
							<div class="buy_btn_group<? echo $buy_btn_group_class;?>" id="<? echo $arItemIDs['BUY_BTN_WRAPPER']; ?>">
								<?
								if ('Y' == $arParams['USE_PRODUCT_QUANTITY']) {
									?>
									<div class="buy_count_wrap">
										<div class="product_count">
											<a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>"
											 href="javascript:void(0)" class="btn_minus" rel="nofollow">-</a>
											<input type="text" class="count_value"
												 id="<? echo $arItemIDs['QUANTITY']; ?>"
												 name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
												 value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
											<a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)"
											 class="btn_plus" rel="nofollow">+</a>
										</div>
									</div>
									<?
								}
								?>
								<div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="bx_catalog_item_controls_blocktwo">
									<?
									if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY') {
										$basketTitle = ('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCS_TPL_MESS_BTN_BUY'));
									} else {
										$basketTitle = ('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET'));
									}
									?>
									<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="cart_icon add_to_cart btn_round btn_color btn_wide"
									 href="javascript:void(0)" rel="nofollow"
									 title="<? echo $basketTitle; ?>"><span class="show_xs"><? echo $basketTitle; ?></span></a>
									<a class="cart_icon product_detail btn_round btn_disabled btn_hover_color" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" rel="nofollow" title="<? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL')); ?>"><span class="show_xs"><? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL')); ?></span></a>
								</div>
							</div>
							<div style="clear: both;"></div>
						</div>
						<?
						if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS'])) {
						if ($canBuy && $minPrice['VALUE']>0 && $arParams['USE_QUICKBUY']) {
							$quickBuyMessage = ('' != $arParams['MESS_BTN_QUICKBUY'] ? $arParams['MESS_BTN_QUICKBUY'] : GetMessage('CT_BCS_TPL_MESS_BTN_QUICKBUY'));
							?>
							<div class="product_bottom">
								<div class="btn_round btn_wide btn_border btn_hover_color btn_quickbuy" data-quickbuy="" data-product-id="<? echo $arItem['ID']; ?>" title="<? echo $quickBuyMessage; ?>" onclick="modalQuickBuy(this);return false;"><? echo $quickBuyMessage; ?></div>
							</div>
						<?
						}
						$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
						if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties) {
						?>
							<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
								<?
								if (!empty($arItem['PRODUCT_PROPERTIES_FILL'])) {
									foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
										?>
										<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
										<?
										if (isset($arItem['PRODUCT_PROPERTIES'][$propID])) {
											unset($arItem['PRODUCT_PROPERTIES'][$propID]);
										}
									}
								}
								$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
								if (!$emptyProductProperties) {
									?>
									<table>
										<?
										foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
											?>
											<tr>
												<td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
												<td>
													<?
													if ('L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE'] && 'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE']) {
														foreach ($propInfo['VALUES'] as $valueID => $value) {
															?>
															<label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br>
															<?
														}
													} else {
														?>
														<select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]">
															<?
															foreach ($propInfo['VALUES'] as $valueID => $value) {
																?>
																<option value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? 'selected' : ''); ?>><? echo $value; ?></option>
																<?
															}
															?>
														</select>
														<?
													}
													?>
												</td>
											</tr>
											<?
										}
										?>
									</table>
									<?
								}
								?>
							</div>
						<?
						}
						$arJSParams = array(
							'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
							'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
							'SHOW_ADD_BASKET_BTN' => false,
							'SHOW_BUY_BTN' => true,
							'SHOW_ABSENT' => true,
							'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
							'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
							'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
							'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
							'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
							'PRODUCT' => array(
								'ID' => $arItem['ID'],
								'NAME' => $productTitle,
								'PICT' => $arItem['PREVIEW_PICTURE'],
								'CAN_BUY' => $arItem["CAN_BUY"],
								'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
								'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
								'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
								'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
								'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
								'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
								'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
							),
							'BASKET' => array(
								'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
								'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
								'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
								'EMPTY_PROPS' => $emptyProductProperties,
								'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
								'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
							),
							'VISUAL' => array(
								'ID' => $arItemIDs['ID'],
								'PICT_ID' => $arItemIDs['PICT'],
								'QUANTITY_ID' => $arItemIDs['QUANTITY'],
								'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
								'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
								'PRICE_ID' => $arItemIDs['PRICE'],
								'BUY_ID' => $arItemIDs['BUY_LINK'],
								'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
								'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
								'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
								'BUY_BTN_WRAPPER' => $arItemIDs['BUY_BTN_WRAPPER'],
								'NOT_AVAILABLE_FLAG' => $arItemIDs['NOT_AVAILABLE_FLAG'],
								'AVAILABLE_FLAG' => $arItemIDs['AVAILABLE_FLAG'],
								'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
							),
							'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
						);
						if ($arParams['DISPLAY_COMPARE']) {
							$arJSParams['COMPARE'] = array(
								'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
								'COMPARE_PATH' => $arParams['COMPARE_PATH']
							);
						}
						unset($emptyProductProperties);
						unset($minPrice);
						?>
							<script type="text/javascript">
								var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
							</script><?
						} else {
						?>
							<div class="product_bottom">
								<?
								$boolShowOfferProps = ('Y' == $arParams['PRODUCT_DISPLAY_MODE'] && $arItem['OFFERS_PROPS_DISPLAY']);
								$boolShowProductProps = (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES']));
								if (!empty($arItem['OFFERS_PROP'])) {
									$arSkuProps = array();
									?>
									<div class="product_bottom_inner" id="<? echo $arItemIDs['PROP_DIV']; ?>">
										<?
										foreach ($skuTemplate as $propId => $propTemplate) {
											if (!isset($arItem['SKU_TREE_VALUES'][$propId])) {
												continue;
											}
											echo str_replace('#ITEM#_prop_', $arItemIDs['PROP'], $propTemplate['START']);
											foreach ($propTemplate['ITEMS'] as $value => $valueItem) {
												if (!isset($arItem['SKU_TREE_VALUES'][$propId][$value])) {
													continue;
												}
												echo str_replace('#ITEM#_prop_', $arItemIDs['PROP'], $valueItem);
											}
											unset($value, $valueItem);
											echo str_replace('#ITEM#_prop_', $arItemIDs['PROP'], $propTemplate['FINISH']);
										}
										unset($propId, $propTemplate);
										foreach ($arResult['SKU_PROPS'] as $arOneProp) {
											if (!isset($arItem['OFFERS_PROP'][$arOneProp['CODE']])) {
												continue;
											}
											$arSkuProps[] = array(
												'ID' => $arOneProp['ID'],
												'SHOW_MODE' => $arOneProp['SHOW_MODE'],
												'VALUES_COUNT' => $arOneProp['VALUES_COUNT']
											);
										}
										foreach ($arItem['JS_OFFERS'] as &$arOneJs) {
											if (0 < $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT']) {
												$arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'] = '-' . $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'] . '%';
												$arOneJs['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = '-' . $arOneJs['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] . '%';
											}
										}
										unset($arOneJs);
										?>
									</div>
								<?
								if ($arItem['OFFERS_PROPS_DISPLAY']) {
									foreach ($arItem['JS_OFFERS'] as $keyOffer => $arJSOffer) {
										$strProps = '';
										if (!empty($arJSOffer['DISPLAY_PROPERTIES'])) {
											foreach ($arJSOffer['DISPLAY_PROPERTIES'] as $arOneProp) {
												$strProps .= '<br>' . $arOneProp['NAME'] . ' <strong>' . (is_array($arOneProp['VALUE']) ? implode(' / ', $arOneProp['VALUE']) : $arOneProp['VALUE']) . '</strong>';
											}
										}
										$arItem['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
									}
								}
								$arJSParams = array(
									'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
									'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
									'SHOW_ADD_BASKET_BTN' => false,
									'SHOW_BUY_BTN' => true,
									'SHOW_ABSENT' => true,
									'SHOW_SKU_PROPS' => $arItem['OFFERS_PROPS_DISPLAY'],
									'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
									'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
									'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
									'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
									'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
									'DEFAULT_PICTURE' => array(
										'PICTURE' => $arItem['PRODUCT_PREVIEW'],
										'PICTURE_SECOND' => $arItem['PRODUCT_PREVIEW_SECOND']
									),
									'VISUAL' => array(
										'ID' => $arItemIDs['ID'],
										'PICT_ID' => $arItemIDs['PICT'],
										'QUANTITY_ID' => $arItemIDs['QUANTITY'],
										'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
										'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
										'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
										'PRICE_ID' => $arItemIDs['PRICE'],
										'TREE_ID' => $arItemIDs['PROP_DIV'],
										'TREE_ITEM_ID' => $arItemIDs['PROP'],
										'BUY_ID' => $arItemIDs['BUY_LINK'],
										'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
										'DSC_PERC' => $arItemIDs['DSC_PERC'],
										'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
										'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
										'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
										'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
										'BUY_BTN_WRAPPER' => $arItemIDs['BUY_BTN_WRAPPER'],
										'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
									),
									'BASKET' => array(
										'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
										'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
										'SKU_PROPS' => $arItem['OFFERS_PROP_CODES'],
										'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
										'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
									),
									'PRODUCT' => array(
										'ID' => $arItem['ID'],
										'NAME' => $productTitle
									),
									'OFFERS' => $arItem['JS_OFFERS'],
									'OFFER_SELECTED' => $arItem['OFFERS_SELECTED'],
									'TREE_PROPS' => $arSkuProps,
									'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
								);
								if ($arParams['DISPLAY_COMPARE']) {
									$arJSParams['COMPARE'] = array(
										'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
										'COMPARE_PATH' => $arParams['COMPARE_PATH']
									);
								}
								?>
									<script type="text/javascript">
										var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
									</script>
									<?
								}

								?>
								<a class="product_params_more" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL')); ?></a>
							</div>
							<?
						}
						?>
					</div>
				</div>
				<?
			}
			?>
			<div style="clear: both;"></div>
			<div class="page_empty_message">
				<div class=""><? echo GetMessage('CT_BCS_TPL_EMPTY_MESSAGE');?></div>
			</div>
		</div>
	</div>
	<?
	if($arResult['NAV_RESULT']->NavPageCount > 1) {
		if($_REQUEST['p']) {
			$catalogNavpage = $_REQUEST['p']+1;
		} else {
			$catalogNavpage = '2';
		}
		?>
		<div class="grid_16">
			<div class="btn_wrapper catalog">
				<a href="<?=$APPLICATION->GetCurPage(false).'?p='.$catalogNavpage?>" class="btn_round btn_border btn_hover_color btn_big"><? echo GetMessage('CT_BCS_CATALOG_SHOW_MORE_ELEMENTS');?></a>
			</div>
		</div>
		<?
	}
	?>
	<script type="text/javascript">
		BX.message({
			BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
			BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
			ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
			TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
			TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
			TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
			BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
			BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
			BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
			BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
			COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
			COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
			COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
			BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
			PRODUCT_NULL_PRICE: '<? echo ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessageJS('CT_BCS_TPL_MESS_PRODUCT_NULL_PRICE')) ?>',
			PRODUCT_NOT_AVAILABLE: '<? echo ('' != $arParams['MESS_NOT_AVAILABLE'] ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE')) ?>',
			SITE_ID: '<? echo SITE_ID; ?>'
		});
		$.each($('.grid_view.wishlist .timer-inner'), function(){
			$(this).countdown({until: new Date($(this).attr('data-final-date')), layout: '{dn}{dl} {hnn}{hl} {mnn}{ml} {snn}{sl}', compact: true});
		});
	</script>
	<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"]) {
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
} else {
	?>
	<div class="page_empty_message active">
		<div class=""><? echo GetMessage('CT_BCS_TPL_EMPTY_MESSAGE');?></div>
	</div>
	<?
}
?>