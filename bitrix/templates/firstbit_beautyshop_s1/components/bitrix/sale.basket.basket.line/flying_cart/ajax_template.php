<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)).'/top_template.php');
?>
<div class="flying_cart_container" id="flying_cart">
	<div id="flying_cart_label" class="flying_cart_label grey <? if($_COOKIE['flying_cart'] && $_COOKIE['flying_cart']=='1') { ?>open<? } ?>" onclick="flyingCartToggle();">
		<i class="fa fa-shopping-cart"></i>
	</div>
	<div id="flying_cart_body" class="flying_cart_body grey <? if($_COOKIE['flying_cart'] && $_COOKIE['flying_cart']=='1') { ?>open<? } ?>">
		<div class="flying_cart_header show_lg">
			<h2><? echo GetMessage('TSB1_CART'); ?></h2>
			<? if ($arResult['NUM_PRODUCTS'] > 0) { ?>
				<div class="summ_right">
					<span><? echo GetMessage('TSB1_TOTAL_PRICE'); ?>: <strong><?=$arResult['TOTAL_PRICE']?></strong></span>
				</div>
			<? } ?>
		</div>
		<div class="flying_cart_first_line">
			<? if ($arResult['NUM_PRODUCTS'] > 0) { ?>
				<div class="relative owl_nav dark" id="owl_flying_cart_nav"></div>
				<div class="carousel_wrap">
					<ul id="<?=$cartId?>products" data-role="flying_cart_item_list" class="flying_cart_item_list">
						<?
						foreach ($arResult["CATEGORIES"] as $category => $items) {
							if (empty($items))
								continue;
							?>
							<? foreach ($items as $v) { ?>
								<li class="product_item">
									<div class="close_btn">
										<a href="#" title="<?=GetMessage('TSB1_DELETE')?>" onclick="<?= $cartId ?>.removeItemFromCart(<?= $v['ID'] ?>);return false;" title="<?= GetMessage("TSB1_DELETE") ?>"><i class="fa fa-times delete_panel_item"></i></a>
									</div>
									<div class="product_image">
										<a href="<?= $v["DETAIL_PAGE_URL"] ?>" class="bx_catalog_item_images">
											<img src="<?= $v["PICTURE_SRC"] ?>" title="<?= $v["NAME"] ?>" alt="<?= $v["NAME"] ?>">
										</a>
									</div>
									<div class="product_description">
										<div class="product_title">
											<a href="<?= $v["DETAIL_PAGE_URL"] ?>" title="<?= $v["NAME"] ?>"><h3><?= $v["NAME"] ?></h3></a>
										</div>
									</div>
									<div class="price_wrap">
										<span class="current_price">
											<? if($v["FULL_PRICE"] != $v["PRICE_FMT"]) { ?>
												<div class="old_price"><?= $v["FULL_PRICE"] ?></div>
											<? } ?>
											<?= $v["PRICE_FMT"] ?>&nbsp;<span>(<?=$v['QUANTITY']?>&nbsp;<?=GetMessage('TSB1_CART_PCS')?>)</span>
										</span>
									</div>
								</li>
								<?
							}
						}
						?>
					</ul>
				</div>
			<? } else { ?>
				<div class="empty_message">
					<div><? echo GetMessage('TSB1_YOUR_CART_EMPTY'); ?></div>
				</div>
			<? } ?>
		</div>
		<div class="flying_cart_second_line">
			<? if ($arResult['NUM_PRODUCTS'] > 0) { ?>
				<a href="<?= $arParams['PATH_TO_BASKET'] ?>" class="btn_round btn_border btn_hover_color"><?= GetMessage('TSB1_GOTO_CART') ?></a>
				<a href="<?= $arParams['PATH_TO_ORDER'] ?>" class="btn_round btn_color"><?= GetMessage('TSB1_GOTO_ORDER') ?>: <strong><?=$arResult['TOTAL_PRICE']?></strong></a>
			<? } else { ?>
				<a href="<?= $arParams['PATH_TO_CATALOG'] ?>" class="btn_round btn_color"><?= GetMessage('TSB1_GOTO_CATALOG') ?></a>
			<? } ?>
		</div>
	</div>
</div>
<script>
var owl_flying_cart = $('.flying_cart_item_list');
owl_flying_cart.owlCarousel({
	items: 3,
	loop: false,
	nav: true,
	dots: false,
	animateOut: 'fadeOut',
	animateIn: 'fadeIn',
	smartSpeed: 500,
	navContainer: '#owl_flying_cart_nav',
	navText: ["<i class=\"fa fa-chevron-left\"></i>","<i class=\"fa fa-chevron-right\"></i>"],
	afterInit: initSlider(owl_flying_cart),
});

BX.ready(function(){
	<?=$cartId?>.fixCart();
});
</script>