<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
	<div class="cart_wrapper">
		<a href="<?=$arParams['PATH_TO_BASKET']?>" class="cart_link">
			<i class="fa fa-shopping-cart"></i>
		</a>
		<div class="cart_info">
			<div class="cart_title"><?=GetMessage('TSB1_CART')?>:</div>
			<span class="cart_item_count"><?=$arResult['NUM_PRODUCTS']?> <span class="hidden_xs"><?=GetMessage('TSB1_MEASURE')?></span></span>
			<i class="fa vertical_split"></i>
			<span class="cart_price_total"><?=$arResult['TOTAL_PRICE']?></span>
		</div>
	</div>
