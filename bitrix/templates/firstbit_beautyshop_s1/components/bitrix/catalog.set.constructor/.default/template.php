<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
$this->setFrameMode(true);

$curJsId = $this->randString();
$arEmptyPreview = CFile::GetFileArray(CFirstbitBeautyshop::queryOption('NO_PHOTO', SITE_ID));
?>
<div id="bx-set-const-<?=$curJsId?>" class="product_set bx-modal-container container-fluid">
	<div class="container_16">
		<div class="grid_16 line_head">
			<h1><?=GetMessage("CATALOG_SET_BUY_SET")?></h1>
		</div>
		<div class="clear"></div>

		<div class="set_container">
			<div class="grid_4 grid_5_sm">
				<div class="bx-original-item-container">
					<div class="bx-original-image-container">
					<?if ($arResult["ELEMENT"]["DETAIL_PICTURE"]["SRC"]):?>
						<img src="<?=$arResult["ELEMENT"]["DETAIL_PICTURE"]["SRC"]?>">
					<?else:?>
						<img src="<?=$arEmptyPreview['SRC']?>">
					<?endif?>
					</div>

					<div>
						<div class="product_description_title"><?=$arResult["ELEMENT"]["NAME"]?></div>
						<div class="bx-added-item-new-price"><span class="bx-added-item-current-price"><?=$arResult["ELEMENT"]["PRICE_PRINT_DISCOUNT_VALUE"]?>
							<?if (!($arResult["ELEMENT"]["PRICE_VALUE"] == $arResult["ELEMENT"]["PRICE_DISCOUNT_VALUE"])):?><span class="bx-catalog-set-item-price-old"><?=$arResult["ELEMENT"]["PRICE_PRINT_VALUE"]?></span><?endif?>
								</span>
						</div>
					</div>
				</div>
			</div>

			<div class="grid_12 grid_11_sm">
				<div class="bx-added-item-table-container">
					<table class="bx-added-item-table">
						<tbody data-role="set-items">
						<?foreach($arResult["SET_ITEMS"]["DEFAULT"] as $key => $arItem):?>
							<tr
								data-id="<?=$arItem["ID"]?>"
								data-img="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"
								data-url="<?=$arItem["DETAIL_PAGE_URL"]?>"
								data-name="<?=$arItem["NAME"]?>"
								data-price="<?=$arItem["PRICE_DISCOUNT_VALUE"]?>"
								data-print-price="<?=$arItem["PRICE_PRINT_DISCOUNT_VALUE"]?>"
								data-old-price="<?=$arItem["PRICE_VALUE"]?>"
								data-print-old-price="<?=$arItem["PRICE_PRINT_VALUE"]?>"
								data-diff-price="<?=$arItem["PRICE_DISCOUNT_DIFFERENCE_VALUE"]?>"
								data-measure="<?=$arItem["MEASURE"]["SYMBOL_RUS"]; ?>"
								data-quantity="<?=$arItem["BASKET_QUANTITY"]; ?>"
							>
								<td class="bx-added-item-table-cell-img">
									<?if ($arItem["DETAIL_PICTURE"]["SRC"]):?>
										<img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" class="img-responsive" alt="">
									<?else:?>
										<img src="<?=$arEmptyPreview['SRC']?>" class="img-responsive" alt="">
									<?endif?>
								</td>
								<td class="bx-added-item-table-cell-itemname">
									<a class="tdn" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
								</td>
								<td class="bx-added-item-table-cell-price">
									<span class="bx-added-item-current-price"><?=$arItem["PRICE_PRINT_DISCOUNT_VALUE"]?>
									<?if ($arItem["PRICE_VALUE"] != $arItem["PRICE_DISCOUNT_VALUE"]):?>
										<span class="bx-added-item-old-price"><?=$arItem["PRICE_PRINT_VALUE"]?></span>
									<?endif?>
										</span>
								</td>
								<td class="bx-added-item-table-cell-del"><div class="bx-added-item-delete" data-role="set-delete-btn"><i class="fa fa-times delete_panel_item"></i></div></td>
							</tr>
							<? unset($arItem); ?>
						<?endforeach?>
						</tbody>
					</table><div style="display: none;" data-set-message="empty-set"></div>
				</div>
			</div>
			<div class="clear"></div>

			<div class="grid_16 set_result">
				<div class="set_result_cell">
					<div class="btn_round btn_border btn_hover_color new_set_btn"><? echo GetMessage('CATALOG_SET_CONSTRUCT');?></div>
				</div>
				<div class="set_result_cell">
					<div class="bx-constructor-container-result">
						<span class="bx-item-set-current-price set_price" data-role="set-price"><? echo GetMessage('CATALOG_SET_SUM');?>: <?=$arResult["SET_ITEMS"]["PRICE"]?></span>
						<span class="set_discount"><span class="bx-added-item-old-price" data-role="set-old-price"><?
							if ($arResult["SET_ITEMS"]["OLD_PRICE"])
							{
								?><? echo GetMessage('CATALOG_SET_OLD_PRICE');?> <?= $arResult["SET_ITEMS"]["OLD_PRICE"] ?> <?
							}
						?></span><span class="bx-item-set-economy-price" data-role="set-diff-price"><?
							if ($arResult["SET_ITEMS"]["PRICE_DISCOUNT_DIFFERENCE"])
							{
								?><?= GetMessage("CATALOG_SET_DISCOUNT_DIFF", array("#PRICE#" => $arResult["SET_ITEMS"]["PRICE_DISCOUNT_DIFFERENCE"])) ?><?
							}
						?></span></span>
					</div>
				</div>
				<div class="set_result_cell">
					<a href="javascript:void(0)" data-role="set-buy-btn" class="btn_round btn_big btn_color"><?=GetMessage("CATALOG_SET_BUY")?></a>
				</div>
			</div>
			<div class="clear"></div>
		</div>

	<div class="grid_16">
	<div class="bx-catalog-set-topsale-slider-box">
		<div class="bx-catalog-set-topsale-slider-container">
			<div class="bx-catalog-set-topsale-slids bx-catalog-set-topsale-slids-<?=$curJsId?>" data-role="set-other-items">
				<?
				$first = true;
				foreach($arResult["SET_ITEMS"]["OTHER"] as $key => $arItem):?>
				<div class="bx-catalog-set-item-container bx-catalog-set-item-container-<?=$curJsId?>"
					data-id="<?=$arItem["ID"]?>"
					data-img="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"
					data-url="<?=$arItem["DETAIL_PAGE_URL"]?>"
					data-name="<?=$arItem["NAME"]?>"
					data-price="<?=$arItem["PRICE_DISCOUNT_VALUE"]?>"
					data-print-price="<?=$arItem["PRICE_PRINT_DISCOUNT_VALUE"]?>"
					data-old-price="<?=$arItem["PRICE_VALUE"]?>"
					data-print-old-price="<?=$arItem["PRICE_PRINT_VALUE"]?>"
					data-diff-price="<?=$arItem["PRICE_DISCOUNT_DIFFERENCE_VALUE"]?>"
					data-measure="<?=$arItem["MEASURE"]["SYMBOL_RUS"]; ?>"
					data-quantity="<?=$arItem["BASKET_QUANTITY"]; ?>"<?
				if (!$arItem['CAN_BUY'] && $first)
				{
					echo 'data-not-avail="yes"';
					$first = false;
				}
				?>
				>
					<div class="bx-catalog-set-item">
						<div class="bx-catalog-set-item-img-container">
							<?if ($arItem["DETAIL_PICTURE"]["SRC"]):?>
								<img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" class="img-responsive" alt=""/>
							<?else:?>
								<img src="<?=$arEmptyPreview['SRC']?>" class="img-responsive"/>
							<?endif?>
						</div>
						<div class="bx-catalog-set-item-title">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
						</div>
						<div class="bx-added-item-new-price">
							<span class="bx-added-item-current-price">
								<?
								if ($arItem['CAN_BUY']) {
									?>
									<?=$arItem["PRICE_PRINT_DISCOUNT_VALUE"] ?>
									<? if (!($arItem["PRICE_VALUE"] == $arItem["PRICE_DISCOUNT_VALUE"])): ?><span
										class="bx-catalog-set-item-price-old"><?=$arItem["PRICE_PRINT_VALUE"]?></span><?endif ?>
									<?
								} else {
									?>
									<? echo GetMessage('CATALOG_SET_MESS_NOT_AVAILABLE'); ?>
									<?
								}
								?>
							</span>
						</div>
						<div class="bx-catalog-set-item-add-btn">
							<?
							if ($arItem['CAN_BUY'])
							{
								?><a href="javascript:void(0)" data-role="set-add-btn" class="btn_round btn_border btn_hover_color btn_wide"><?= GetMessage("CATALOG_SET_BUTTON_ADD") ?></a><?
							}
							?>
						</div>
					</div>
				</div>
				<?endforeach?>
			</div>
		</div>
	</div>
	</div>
	<div class="clear"></div>
</div>
</div>
<?
$arJsParams = array(
	"numSliderItems" => count($arResult["SET_ITEMS"]["OTHER"]),
	"numSetItems" => count($arResult["SET_ITEMS"]["DEFAULT"]),
	"jsId" => $curJsId,
	"parentContId" => "bx-set-const-".$curJsId,
	"ajaxPath" => $this->GetFolder().'/ajax.php',
	"currency" => $arResult["ELEMENT"]["PRICE_CURRENCY"],
	"mainElementPrice" => $arResult["ELEMENT"]["PRICE_DISCOUNT_VALUE"],
	"mainElementOldPrice" => $arResult["ELEMENT"]["PRICE_VALUE"],
	"mainElementDiffPrice" => $arResult["ELEMENT"]["PRICE_DISCOUNT_DIFFERENCE_VALUE"],
	"mainElementBasketQuantity" => $arResult["ELEMENT"]["BASKET_QUANTITY"],
	"lid" => SITE_ID,
	"iblockId" => $arParams["IBLOCK_ID"],
	"basketUrl" => $arParams["BASKET_URL"],
	"setIds" => $arResult["DEFAULT_SET_IDS"],
	"offersCartProps" => $arParams["OFFERS_CART_PROPERTIES"],
	"itemsRatio" => $arResult["BASKET_QUANTITY"],
	"noFotoSrc" => $arEmptyPreview['SRC'],
	"messages" => array(
		"EMPTY_SET" => GetMessage('CT_BCE_CATALOG_MESS_EMPTY_SET'),
		"ADD_BUTTON" => GetMessage("CATALOG_SET_BUTTON_ADD")
	)
);
?>
<script type="text/javascript">
	BX.ready(function(){
		new BX.Catalog.SetConstructor(<?=CUtil::PhpToJSObject($arJsParams, false, true, true)?>);
	});
	$(document).on('click', '.new_set_btn', function(event) {
		event.preventDefault();
		$('.bx-modal-container .bx-catalog-set-topsale-slider-container').slideToggle();
	});
</script>