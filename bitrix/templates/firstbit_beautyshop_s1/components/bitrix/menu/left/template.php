<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->SetFrameMode(true);

if (!empty($arResult)) {
	?>
	<div class="grid_4 grid_16_sm grid_16_xs margin_sm_10 margin_xs_10 menu_left">
		<div class="menu-container">
			<ul id="vertical-multilevel-menu" class="menu-list-level1">
				<?
				$previousLevel = 0;
				foreach ($arResult as $arItem) {
					if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
						echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
					}
					if ($arItem["IS_PARENT"]) {
						if ($arItem["DEPTH_LEVEL"] == 1) {
							?>
							<li class="menu-list-item dropdown <? if ($arItem["SELECTED"]): ?>active<? endif ?>">
								<a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
								<ul class="menu-list-level2">
									<?
						}
						else {
									?>
							<li class="menu-list-item dropdown <? if ($arItem["SELECTED"]): ?>active<? endif ?>">
								<a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
								<ul class="menu-list-level2">
									<?
						}
					}
					else {
						if ($arItem["DEPTH_LEVEL"] == 1) {
							?>
							<li class="menu-list-item <? if ($arItem["SELECTED"]): ?>active<? endif ?>"><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
							<?
						}
						else {
							?>
							<li class="menu-list-item <? if ($arItem["SELECTED"]): ?>active<? endif ?>"><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
							<?
						}
					}
					$previousLevel = $arItem["DEPTH_LEVEL"];
				}
				if ($previousLevel > 1) {
					echo str_repeat("</ul></li>", ($previousLevel - 1));
				}
				?>
			</ul>
		</div>
	</div>
	<?
}
?>