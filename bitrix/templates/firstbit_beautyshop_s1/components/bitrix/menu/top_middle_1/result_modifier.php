<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$previousLevel = 0;

$levels = array();

$last_level_parent = array();

foreach($arResult as $arItemIndex=>$arItem) {
	if($arItem['PARAMS']['IS_CAT'] == 'Y') {
		if (array_key_exists($arItem["DEPTH_LEVEL"], $levels)) {
			if ($arItem["DEPTH_LEVEL"] > $previousLevel) {
				$levels[$arItem["DEPTH_LEVEL"]] = 0;
			}
			else {
				$levels[$arItem["DEPTH_LEVEL"]] = $levels[$arItem["DEPTH_LEVEL"]] + 1;
			}
		}
		else {
			$levels[$arItem["DEPTH_LEVEL"]] = 0;
		}

		if($arItem['IS_PARENT'] == 1) {
			$last_level_parent[$arItem["DEPTH_LEVEL"]] = $arItem["LINK"];
		}

		if($arItem["DEPTH_LEVEL"] < $previousLevel) $remove = false;

		if($levels[$arItem["DEPTH_LEVEL"]] >= $arParams['MENU_LIMIT'] && $arItem["DEPTH_LEVEL"] > 2) {
			$remove = true;
		}

		if($remove == true) {
			if($levels[$arItem["DEPTH_LEVEL"]] == $arParams['MENU_LIMIT'] && array_key_exists(($arItemIndex-1), $arResult) && is_array($arResult[($arItemIndex-1)])) {
				$arResult[$arItemIndex] = array(
					'TEXT' => GetMessage('MORE_SECTIONS'),
					'LINK' => $last_level_parent[($arItem["DEPTH_LEVEL"]-1)],
					'PERMISSION' => 'R',
					'PARAMS' => Array(
						'IS_CAT' => 'Y',
						'MORE_SECTIONS' => 'Y'
					),
					'DEPTH_LEVEL' => $arItem["DEPTH_LEVEL"],
					'IS_PARENT' => ''
				);
			} else {
				unset($arResult[$arItemIndex]);
			}
		}
		$previousLevel = $arItem["DEPTH_LEVEL"];
	}
}