<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$this->SetFrameMode(true);
?>

<menu class="menu1">
	<ul class="menu_flex <?=$arParams['ITEMS_SHOW']?>">
		<?
		$previousLevel = 0;
		foreach($arResult as $arItem) {

			if($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
				echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
			}

			if($arItem["IS_PARENT"]) {
				if($arItem["DEPTH_LEVEL"] == 1) {
					?>
					<li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
						<a class="not_underline" href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a>
						<ul class="level<?=($arItem["DEPTH_LEVEL"]+1)?> <? if($arItem["PARAMS"]["IS_CAT"] != 'Y'): ?>common<? else: ?>grey catalog<? endif; ?>">
					<?
				}
				else {
					?>
					<li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
						<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
						<ul class="level<?=($arItem["DEPTH_LEVEL"]+1)?>">
					<?
				}
			}
			else {
				if($arItem["DEPTH_LEVEL"] == 1) {
					?>
					<li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
						<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
					</li>
					<?
				}
				else {
					?>
					<li class="<? if ($arItem["SELECTED"]): ?>active<?endif ?> <? if($arItem["PARAMS"]["MORE_SECTIONS"] == 'Y'): ?>more_sections<?endif ?>">
						<a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?><? if($arItem["PARAMS"]["MORE_SECTIONS"] == 'Y'): ?> <i class="fa fa-angle-right" aria-hidden="true"></i><?endif ?></a>
					</li>
					<?
				}
			}
			$previousLevel = $arItem["DEPTH_LEVEL"];
		}

		if($previousLevel > 1) { // closing list after last element
			echo str_repeat("</ul></li>", ($previousLevel-1) );
		}
	?>
	</ul>
</menu>