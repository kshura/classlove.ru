<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$this->SetFrameMode(true);
?>

<menu class="menu3">
	<ul class="menu_flex <?=$arParams['ITEMS_SHOW']?>">
		<?
		$previousLevel = 0;
		foreach($arResult as $arItem) {

			if($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
				echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
			}

			if($arItem["IS_PARENT"]) {
				if($arItem["DEPTH_LEVEL"] == 1) {
					?>
					<li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
						<a class="not_underline" href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a>
						<ul class="level<?=($arItem["DEPTH_LEVEL"]+1)?> <? if($arItem["PARAMS"]["IS_CAT"] != 'Y'): ?>common<? else: ?>grey catalog<? endif; ?>">
					<?
				}
				else {
					?>
					<li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
						<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?><i class="fa fa-caret-right" aria-hidden="true"></i></a>
						<ul class="level<?=($arItem["DEPTH_LEVEL"]+1)?>">
					<?
				}
			}
			else {
				if($arItem["DEPTH_LEVEL"] == 1) {
					?>
					<li class="<?if ($arItem["SELECTED"]):?>active<?endif?>">
						<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
					</li>
					<?
				}
				else {
					?>
					<li class="<? if ($arItem["SELECTED"]): ?>active<?endif ?> <? if($arItem["PARAMS"]["MORE_SECTIONS"] == 'Y'): ?>more_sections<?endif ?>">
						<a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?><? if($arItem["PARAMS"]["MORE_SECTIONS"] == 'Y'): ?> <i class="fa fa-angle-right" aria-hidden="true"></i><?endif ?></a>
						<?
						if($arItem["PARAMS"]["IS_CAT"] == 'Y') {
							if(is_array($arParams['HEADER_RESULT']['CATALOG_SECTION']['SECTIONS'][$arParams['HEADER_RESULT']['CATALOG_SECTION']['URL_TO_ID'][$arItem["LINK"]]]['PICTURE'])) {
								?>
								<ul class="append image">
									<li>
										<a class="" href="<?=$arItem["LINK"]?>" title='<?=$arItem['TEXT']?>' style="background-image: url(<?=$arParams['HEADER_RESULT']['CATALOG_SECTION']['SECTIONS'][$arParams['HEADER_RESULT']['CATALOG_SECTION']['URL_TO_ID'][$arItem["LINK"]]]['PICTURE']['SRC']?>);"></a>
									</li>
								</ul>
								<?
							}
						}
						?>
					</li>
					<?
				}
			}
			$previousLevel = $arItem["DEPTH_LEVEL"];
		}

		if($previousLevel > 1) { // closing list after last element
			echo str_repeat("</ul></li>", ($previousLevel-1) );
		}
	?>
	</ul>
</menu>