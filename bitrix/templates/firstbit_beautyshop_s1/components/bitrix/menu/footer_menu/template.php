<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
?>
<menu>
	<ul>
		<?foreach($arResult as $arMenuLink){?>
			<li>
				<a href="<?=$arMenuLink['LINK']?>" class="not_underline"><?=$arMenuLink['TEXT']?></a>
			</li>
		<?}?>
	</ul>
</menu>