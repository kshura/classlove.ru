<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->SetFrameMode(true);
?>
<? if (!empty($arResult)) { ?>
	<div class="personal relative">
		<div class="container_12 margin_xs_0">
			<ul class="personal_list">
				<?
				$previousLevel = 0;
				$index = 1;
				foreach($arResult as $arItem) {
					if($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
						echo str_repeat('</ul></li>', ($previousLevel - $arItem["DEPTH_LEVEL"]));
					}
					if($arItem["DEPTH_LEVEL"] == 1) {
						?>
						<li class="grid_3 grid_4_sm grid_12_xs personal_item margin_xs_0">
							<h2><?=$arItem["TEXT"]?></h2>
							<? if($arItem['PARAMS']['ICON']) { ?>
								<i class="fa fa-<?=$arItem['PARAMS']['ICON'];?> personal_icon"></i>
							<? } ?>
							<? if($arItem["IS_PARENT"]) { ?>
								<i class="fa fa-chevron-circle-down hidden_lg hidden_sm personal_toggle"></i>
								<ul class="personal_menu">
							<? } ?>
						<?
						$index++;
					} else {
						?>
						<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
						<?
					}
					$previousLevel = $arItem["DEPTH_LEVEL"];
				}
				if($previousLevel > 1) {
					echo str_repeat("</ul></li>", ($previousLevel-1) );
				}
				?>
			</ul>
				<div class="clear"></div>
		</div>
	</div>
<? } ?>