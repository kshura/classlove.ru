<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if (empty($arResult["BRAND_BLOCKS"]))
	return;
?>
<li class="short_descr_item">
	<div class="advantage_wrapper">
	<?
	foreach ($arResult["BRAND_BLOCKS"] as $arAdvantage){
	?>
		<div class="l33">
			<div class="l33_inner">
				<img src="<?=$arAdvantage['PICT']['SRC']?>">
				<span><?=$arAdvantage['NAME']?></span>
			</div>
		</div>
	<? } ?>
	</div>
</li>
