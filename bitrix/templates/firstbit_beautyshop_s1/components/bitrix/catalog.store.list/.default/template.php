<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->SetFrameMode(true);
if(strlen($arResult["ERROR_MESSAGE"])>0)
	ShowError($arResult["ERROR_MESSAGE"]);
$arPlacemarks = array();
$gpsN = '';
$gpsS = '';

?>
<div class="store_list_wrapper">
	<div class="store_list_header">
		<div class="btn_wrapper pull_right hidden_xs">
			<span class="store_list_table active"><i class="fa fa-bars"></i><span><? echo GetMessage('S_SHOW_LIST'); ?></span></span>
			<span class="store_list_maps"><i class="fa fa-map-marker"></i><span><? echo GetMessage('S_SHOW_MAP'); ?></span></span>
		</div>
	</div>
	<div class="store_list_body">
		<div class="store_list_tab1 hidden_xs">
			<table class="store_list_tab">
				<thead>
					<tr>
						<td class="column-1"><? echo GetMessage('S_ADDRESS'); ?></td>
						<td class="column-2"><? echo GetMessage('S_PHONE'); ?></td>
						<td class="column-3"><? echo GetMessage('S_SCHEDULE'); ?></td>
						<td class="column-4"></td>
					</tr>
				</thead>
				<tbody>
					<?
					foreach($arResult['STORES'] as $arStore) {
						?>
						<tr>
							<td class="column-1"><b><? echo $arStore['TITLE'];?></b><span><? echo $arStore['ADDRESS'];?></span></td>
							<td class="column-2">
								<?
								foreach($arStore['PHONE'] as $arPhoneIndex=>$arPhone) {
									?>
									<b><a href="tel:<? echo $arPhone['FORMAT'];?>"><? echo $arPhone['DISPLAY'];?></a></b>
									<?
									if(($arPhoneIndex+1)!=count($arStore['PHONE'])) {
										?>
										<br/>
										<?
									}
								}
								?>
							</td>
							<td class="column-3"><? echo $arStore['SCHEDULE'];?></td>
							<td class="column-4"><a href="<? echo $arStore['URL'];?>" class="btn_round btn_color"><? echo GetMessage('S_DETAIL'); ?></a></td>
						</tr>
						<?
					}
					?>
				</tbody>
			</table>
		</div>
		<div class="store_list_tab3">
			<div class="store_map_tab">
				<div class="container_16">
					<div class="map">
						<div class="grid_4 grid_5_sm alpha omega menu_container">
							<ul class="store_list">
								<?
								$JSplacemarks = array();
								$i = 0;
								?>
								<? foreach($arResult["STORES"] as $pid => $arProperty) { ?>
									<li class="store_item store1" data-yamaps-placemark="<?=$i?>">
										<h3 class="city"><?=$arProperty["TITLE"]?></h3>
										<span class="street"><?=$arProperty["ADDRESS"]?></span>
										<?
										foreach($arStore['PHONE'] as $arPhoneIndex=>$arPhone) {
											?>
											<span class="phone"><? echo $arPhone['DISPLAY'];?></span>
											<?
										}
										?>
										<div class="btn_container show_xs"><a href="<? echo $arProperty['URL'] ?>" class="btn_round btn_color"><? echo GetMessage("S_DETAIL");?></a></div>
									</li>
									<?
									$JSplacemark = "
										myPlacemark".$i." = new ymaps.Placemark([".$arProperty["GPS_N"].",".$arProperty["GPS_S"]."], {
											hintContent: '".$arStore["TITLE"]."',
											balloonContent: '<div style=\"width:200px;\">' +
												'<h3 class=\"city\">".$arProperty["TITLE"]."</h3>' +
												'<span class=\"street\">".$arProperty["ADDRESS"]."</span>' +";
									foreach($arStore['PHONE'] as $arPhoneIndex=>$arPhone) {
										$JSplacemark .= "
												'<span class=\"phone\">" . $arPhone["DISPLAY"] . "</span>' +
										";
									}
									$JSplacemark .= "
												'<br/>' +
												'<a href=\"".$arProperty['URL']."\" class=\"btn_round btn_color btn_wide\">".GetMessage("S_DETAIL")."</a>' +
												'</div>'},
											{
												iconLayout: 'default#image',
												iconImageHref: '".SITE_TEMPLATE_PATH."/img/YaIcon.png',
												iconImageSize: [39, 35],
												iconImageOffset: [-15, -32],
											});
									";
									$JSplacemarks[] = $JSplacemark;
									unset($JSplacemark);
									$i++;
									?>
								<? } ?>
							</ul>
						</div>
						<div class="grid_12 grid_11_sm alpha omega map_container hidden_xs">
							<div id="map" style="width: 100%; height: 330px"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var myMap;
	function init() {
		var myMap = new ymaps.Map("map", {
			center: [55.76, 37.64],
			behaviors: ['default', 'scrollZoom'],
			zoom: 10
		});
		<?
		foreach ($JSplacemarks as $placemarkIndex => $placemark) {
			echo $placemark;
			echo 'myMap.geoObjects.add(myPlacemark' . $placemarkIndex . ');';
		}
		?>
		$("[data-yamaps-placemark]").click(function(){
			var currentPlacemaker = eval('myPlacemark'+$(this).attr('data-yamaps-placemark'));
			myMap.setCenter(currentPlacemaker.geometry.getCoordinates(),15);
			currentPlacemaker.balloon.open();
		});
	};
	var obStoreAmount = new JCCatalogStoreSKU(<? echo CUtil::PhpToJSObject($arResult['JS'], false, true, true); ?>);
</script>
<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU&onload=init"></script>