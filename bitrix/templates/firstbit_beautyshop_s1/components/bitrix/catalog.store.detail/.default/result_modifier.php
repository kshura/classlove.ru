<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();

$arResult['USER_FIELDS'] = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ("CAT_STORE", $arResult['ID']);
$arResult['PHONE'] = CFirstbitBeautyshop::parsePhone($arResult['PHONE']);
if(!empty($arResult['USER_FIELDS']['UF_MORE_PHOTO']['VALUE'])) {
	foreach ($arResult['USER_FIELDS']['UF_MORE_PHOTO']['VALUE'] as $photoIndex=>$arPhoto) {
		$arResult['USER_FIELDS']['UF_MORE_PHOTO']['VALUE'][$photoIndex] = CFile::GetFileArray($arPhoto);
		$image_resize = CFile::ResizeImageGet($arResult['USER_FIELDS']['UF_MORE_PHOTO']['VALUE'][$photoIndex], array("width" => 146, "height" => 104));
		$arResult['USER_FIELDS']['UF_MORE_PHOTO']['VALUE'][$photoIndex]['PREVIEW_PICTURE'] = $image_resize;
	}
}