<?php
$APPLICATION->IncludeComponent(
	"bitrix:sale.location.selector.steps",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"ID" => $arResult["VALUES"][$arProperty["CODE"]],
		"CODE" => "",
		"INPUT_NAME" => "REGISTER[".$arProperty["CODE"]."]",
		"PROVIDE_LINK_BY" => "id",
		"JSCONTROL_GLOBAL_ID" => "",
		"JS_CALLBACK" => "",
		"SEARCH_BY_PRIMARY" => "N",
		"FILTER_BY_SITE" => "Y",
		"SHOW_DEFAULT_LOCATIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"FILTER_SITE_ID" => "s1"
	)
);