<?
/**
* Bitrix Framework
* @package bitrix
* @subpackage main
* @copyright 2001-2014 Bitrix
*/

/**
* Bitrix vars
* @global CMain $APPLICATION
* @global CUser $USER
* @param array $arParams
* @param array $arResult
* @param CBitrixComponentTemplate $this
*/

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$firstBit = new CFirstbitBeautyshop(SITE_ID);

if ($USER->IsAuthorized()) {
	?>
	<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
	<?
}
else {
	if (count($arResult["ERRORS"]) > 0) {
		foreach ($arResult["ERRORS"] as $key => $error) {
			if (intval($key) == 0 && $key !== 0) {
				$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
			}
		}
		ShowError(implode("<br />", $arResult["ERRORS"]));
	}
	else if ($arResult["USE_EMAIL_CONFIRMATION"] === "Y") {
		?>
		<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
		<?
	}

	if ($arResult["BACKURL"] <> '') {
		?>
		<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
		<?
	}
	?>
	<div class="profile_tabs registration">
		<?
		if (count($arResult["PERSON_TYPES"])>1) {
			?>
			<ul class="btn-panel">
				<?
				foreach($arResult["PERSON_TYPES"] as $arPersonTypeIndex=>$arPersonType) {
					?>
					<li class="btn-panel-item"><a href="#tabs-<?=$arPersonTypeIndex?>"><?=$arPersonType["NAME"]?></a></li>
					<?
				}
				?>
			</ul>
			<?
		}
		?>
		<div>
			<?
			if ($arParams['SEPARATE_REGISTRATION']) {
				foreach ($arResult["PERSON_TYPES"] as $arPersonTypeIndex=>$arPersonType) {
					?>
					<div id="tabs-<?=$arPersonTypeIndex?>" class="tabs_inner">
						<form method="post" action="<?=POST_FORM_ACTION_URI?>" class="form form_general form_registration" name="regform" enctype="multipart/form-data">
							<input type="hidden" name="REGISTER[PERSON_TYPE]" value="<?=$arPersonType['ID']?>" />
							<?
							foreach ($arPersonType["PROPERTIES_GROUPS"] as $arPropertiesGroupIndex=>$arPropertiesGroup) {
								if ($arParams['PROPS_GROUP_USE'] == 'Y') {
									?>
									<div class="form_fields_section">
										<h2><?=$arPropertiesGroup["NAME"]?></h2>
									</div>
									<?
								}
								if ($arParams['PROPS_GROUP_USE'] == 'Y' || $arParams['PROPS_GROUP_USE'] == 'N' && $arPropertiesGroupIndex == 0) {
									?>
									<ul class="form_fields">
									<?
								}
								foreach ($arPropertiesGroup["PROPERTIES"] as $arProperty) {
									switch ($arProperty["TYPE"]) {
										case 'LOCATION':
											$locationBlock = rand(100000,999999);
											?>
											<li data-location-target="<?=$locationBlock?>">
												<label><?=$arProperty["NAME"]?>:</label>

												<?
												$APPLICATION->IncludeComponent(
													"bitrix:sale.location.selector.steps",
													"",
													Array(
														"COMPONENT_TEMPLATE" => ".default",
														"ID" => $arResult["VALUES"][$arProperty["CODE"]],
														"CODE" => "",
														"INPUT_NAME" => "REGISTER[".$arProperty["CODE"]."]",
														"PROVIDE_LINK_BY" => "id",
														"JSCONTROL_GLOBAL_ID" => "",
														"JS_CALLBACK" => "",
														"SEARCH_BY_PRIMARY" => "N",
														"FILTER_BY_SITE" => "Y",
														"SHOW_DEFAULT_LOCATIONS" => "Y",
														"CACHE_TYPE" => "A",
														"CACHE_TIME" => "36000000",
														"FILTER_SITE_ID" => SITE_ID
													)
												);
												?>
												<?
												if ($arProperty['REQUIED'] == 'Y') {
													?>
													<i class="fa fa-check-circle not-active" title="<?=GetMessage("AUTH_REQUIRED")?>"></i>
													<?
												}
												?>
											</li>
											<?
											break;
										case 'SELECT':
											?>
											<li>
												<label><?=$arProperty["NAME"]?>:</label>
												<?
												foreach ($arProperty['VARIANTS'] as $arVariant) {
													?>
													<label><input type="radio" name="REGISTER[<?=$arProperty["CODE"]?>]" value="<?=$arVariant['VALUE']?>" <? if($arResult["VALUES"][$arProperty["CODE"]] == $arVariant['VALUE'] || $arProperty["DEFAULT_VALUE"] == $arVariant['VALUE']) : ?>checked="checked"<? endif; ?>><?=$arVariant['NAME']?></label>
													<?
												}
												?>
											</li>
											<?
											break;
										case 'TEXTAREA':
											?>
											<li>
												<label class="textarea-label"><?=$arProperty["NAME"]?>:</label>
												<textarea name="REGISTER[<?=$arProperty["CODE"]?>]" <? if($arProperty['REQUIED'] == 'Y'){ ?>required="required"<? } ?>><?=$arResult["VALUES"][$arProperty["CODE"]]?></textarea>
												<?
												if ($arProperty['REQUIED'] == 'Y') {
													?>
													<i class="fa fa-check-circle not-active" title="<?=GetMessage("AUTH_REQUIRED")?>"></i>
													<?
												}
												?>
											</li>
											<?
											break;
										default:
											if ($arProperty['CODE'] == 'PHONE') {
												$dataType = 'data-mask="phone"';
											}
											?>
											<li>
												<label><?=$arProperty["NAME"]?>:</label>
												<input type="text" <?=($dataType ? $dataType : false)?> value="<?=$arResult["VALUES"][$arProperty["CODE"]]?>" <? if($arParams['USE_AJAX_LOCATIONS'] == 'Y' && ($arProperty["CODE"] == "POSTAL_CODE" || $arProperty["CODE"] == "COMPANY_POSTAL_CODE")) : ?>data-location-handler="<?=$locationBlock?>"<? endif; ?> name="REGISTER[<?=$arProperty["CODE"]?>]" <? if($arProperty['REQUIED'] == 'Y'){ ?>required="required"<? } ?>>
												<?
												if ($arProperty['REQUIED'] == 'Y') {
													?>
													<i class="fa fa-check-circle not-active" title="<?=GetMessage("AUTH_REQUIRED")?>"></i>
													<?
												}
												?>
											</li>
											<?
											break;
									}
									unset($dataType);
								}
								if ($arParams['PROPS_GROUP_USE'] == 'Y' || $arParams['PROPS_GROUP_USE'] == 'N' && $arPropertiesGroupIndex == count($arPersonType["PROPERTIES_GROUPS"])-1) {
									?>
									</ul>
									<?
								}
							}
							?>
							<div class="form_required_wrapper">
								<ul class="form_fields">
									<?
									foreach ($arResult["SHOW_FIELDS"] as $arField) {
										$passwordClass = "";
										switch ($arField) {
											case "LOGIN":
												if ($arParams['EMAIL_LOGIN'] == 'Y') {
													$passwordClass = "hidden";
													$fieldType = "hidden";
													$fieldValue = "temp_login";
												} else {
													$fieldType = "text";
													$fieldValue = $arResult["VALUES"][$arField];
												}
												break;
											case "EMAIL":
												$fieldType = "text";
												$fieldValue = $arResult["VALUES"][$arField];
												$dataType = 'data-mask="email"';
												break;
											case "PASSWORD":
												$fieldType = "password";
												$fieldValue = $arResult["VALUES"][$arField];
												break;
											case "CONFIRM_PASSWORD":
												$fieldType = "password";
												$fieldValue = $arResult["VALUES"][$arField];
												break;
										}
										?>
										<li class="<?=$passwordClass?>">
											<label><?=GetMessage("REGISTER_FIELD_".$arField)?>:</label>
											<input type="<?=$fieldType?>" <?=($dataType ? $dataType : false)?> value="<?=$fieldValue?>" class="pass1" name="REGISTER[<?=$arField?>]">
											<?
											if($arResult["REQUIRED_FIELDS_FLAGS"][$arField] == 'Y') {
												?>
												<i class="fa fa-check-circle not-active" title="<?=GetMessage("AUTH_REQUIRED")?>"></i>
												<?
											}
											?>
										</li>
										<?
										unset($dataType);
									}

									/* CAPTCHA */
									if ($arResult["USE_CAPTCHA"] == "Y") {
										?>
										<li>
											<label><?=GetMessage("REGISTER_CAPTCHA_TITLE")?>:</label>
											<div class="captha_wrapper">
												<?#=GetMessage("REGISTER_CAPTCHA_TITLE")?>
												<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
												<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
												<?#=GetMessage("REGISTER_CAPTCHA_PROMT")?>
												<input type="text" name="captcha_word" maxlength="50" value="" />
											</div>
										</li>
										<?
										}
									/* !CAPTCHA */

									if ($firstBit->options['CONSENT_FORM'] === 'Y' && $firstBit->options['AGREEMENT_ID'] > 0) {
										?>
										<li>
											<input type="hidden" name="agreement_id" value="<?= $firstBit->options['AGREEMENT_ID']?>" />
											<input type="checkbox" required name="agreement_consent" id="legal_<?= $arPersonTypeIndex ?>" class="legal" value="Y"><label for="legal_<?= $arPersonTypeIndex ?>"><a href="<?= $firstBit->options['AGREEMENT_PAGE_LINK'] ?>" target="_blank" rel="nofollow"><?= $firstBit->options['AGREEMENT_LABEL_FORM_TEXT']?></a></label>
										</li>
										<?
									}
									?>
									<div class="form_btn_wrapper btn_wrapper">
										<input type="submit" class="btn_round btn_color btn_big" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
									</div>
								</ul>
							</div>
						</form>
					</div>
					<?
				}
			}
			else {
				?>
				<div id="tabs-<?=$arPersonTypeIndex?>" class="tabs_inner">
					<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
						<div class="registration_password_wrapper">
							<div class="password_inner">
								<?
								foreach ($arResult["SHOW_FIELDS"] as $arField) {
									switch ($arField) {
										case "LOGIN":
											if ($arParams['EMAIL_LOGIN'] == 'Y') {
												$passwordClass = "password_line_hidden";
												$fieldType = "hidden";
												$fieldValue = "temp_login";
											}
											else {
												$passwordClass = "password_line";
												$fieldType = "text";
												$fieldValue = $arResult["VALUES"][$arField];
											}
											break;
										case "EMAIL":
											$passwordClass = "password_line";
											$fieldType = "email";
											$fieldValue = $arResult["VALUES"][$arField];
											$dataType = 'data-mask="email"';
											break;
										case "PASSWORD":
											$passwordClass = "password_line";
											$fieldType = "password";
											$fieldValue = $arResult["VALUES"][$arField];
											break;
										case "CONFIRM_PASSWORD":
											$passwordClass = "password_line";
											$fieldType = "password";
											$fieldValue = $arResult["VALUES"][$arField];
											break;
									}
									?>
									<div class="<?=$passwordClass?>">
										<label><?=GetMessage("REGISTER_FIELD_".$arField)?>:</label>
										<input type="<?=$fieldType?>" <?=($dataType ? $dataType : false)?> value="<?=$fieldValue?>" class="pass1" name="REGISTER[<?=$arField?>]">
										<?
										if ($arResult["REQUIRED_FIELDS_FLAGS"][$arField] == 'Y') {
											?>
											<i class="fa fa-check-circle not-active" title="<?=GetMessage("AUTH_REQUIRED")?>"></i>
											<?
										}
										?>
									</div>
									<?
									unset($dataType);
								}
								?>
								<div class="password_last_line">
									<?
									/* CAPTCHA */
									if ($arResult["USE_CAPTCHA"] == "Y") {
										?>
										<label><?=GetMessage("REGISTER_CAPTCHA_TITLE")?>:</label>
										<div class="captha_wrapper">
											<?#=GetMessage("REGISTER_CAPTCHA_TITLE")?>
											<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
											<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
											<?#=GetMessage("REGISTER_CAPTCHA_PROMT")?>
											<input type="text" name="captcha_word" maxlength="50" value="" />
										</div>
										<?
									}
									/* !CAPTCHA */

									if ($firstBit->options['CONSENT_FORM'] === 'Y' && $firstBit->options['AGREEMENT_ID'] > 0) {
										?>
										<li>
											<input type="hidden" name="agreement_id" value="<?= $firstBit->options['AGREEMENT_ID'] ?>" />
											<input type="checkbox" required name="agreement_consent" id="legal_<?= $arPersonTypeIndex ?>" class="legal" value="Y"><label for="legal_<?= $arPersonTypeIndex ?>"><a href="<?= $firstBit->options['AGREEMENT_PAGE_LINK'] ?>" target="_blank" rel="nofollow"><?= $firstBit->options['AGREEMENT_LABEL_FORM_TEXT'] ?></a></label>
										</li>
										<?
									}
									?>
									<div class="registration_btn_wrapper">
										<input type="submit" class="btn green-bg" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			<? } ?>
		</div>
	</div>
	<?
}

if (count($arResult["PERSON_TYPES"]>1)) {
	if (isset($arResult["VALUES"]["PERSON_TYPE"]) && strlen($arResult["VALUES"]["PERSON_TYPE"])) {
		foreach ($arResult["PERSON_TYPES"] as $arPersonTypeIndex=>$arPersonType) {
			if ($arPersonType['ID'] == $arResult["VALUES"]["PERSON_TYPE"]) {
				$personTypeTab = $arPersonTypeIndex;
				break;
			}
			else {
				$personTypeTab = 0;
			}
		}
	}
	else {
		$personTypeTab = 0;
	}
	?>
	<script type="text/javascript">
		$('.registration_tabs').tabs({
			active: <?=$personTypeTab?>
		});
	</script>
	<?
}
?>
