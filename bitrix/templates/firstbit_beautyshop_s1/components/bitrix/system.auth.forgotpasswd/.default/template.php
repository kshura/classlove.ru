<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
$APPLICATION->SetTitle(GetMessage("AUTH_FORGOT_PASS_TITLE"));
$APPLICATION->SetPageProperty('title', GetMessage("AUTH_FORGOT_PASS_TITLE"));
$APPLICATION->SetPageProperty("showLeftMenu", "N");

ShowMessage($arParams["~AUTH_RESULT"]);

?>
<form name="bform" class="form form_general form_registration" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="SEND_PWD">
	<p>
	<?=GetMessage("AUTH_FORGOT_PASSWORD_1")?>
	</p>

<div class="form_required_wrapper">
	<ul class="form_fields">
		<li>
			<label><?=GetMessage("AUTH_LOGIN")?></label>
			<input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" />
		</li>
		<li class="centered">
			<?=GetMessage("AUTH_OR")?>
		</li>
		<li>
			<label><?=GetMessage("AUTH_EMAIL")?></label>
			<input type="text" name="USER_EMAIL" maxlength="255" />
		</li>
	</ul>
	<div class="form_btn_wrapper btn_wrapper">
		<input type="submit" class="btn_round btn_color" name="send_account_info" value="<?=GetMessage("AUTH_GET_CHECK_STRING")?>" />
	</div>
</div>

	<?$this->SetViewTarget('pageTitle_subTitle');?>
	<noindex>
		<a href="<?=$arResult["AUTH_AUTH_URL"]?>" class="btn_round btn_border btn_hover_color pull_right" rel="nofollow"><?=GetMessage("AUTH_AUTH")?></a>
	</noindex>
	<?$this->EndViewTarget();?>
</form>
<script type="text/javascript">
document.bform.USER_LOGIN.focus();
</script>
