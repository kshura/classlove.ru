<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div id="item_tab_stores" class="tabs_inner">
	<div class="store_list_wrapper amount">
		<div class="store_list_header hidden_xs ">
			<div class="btn_wrapper">
				<span class="btn_tab btn_left active" data-store-tab="store_list_tab1"><?=GetMessage('S_PICKUP')?></span>
				<span class="btn_tab btn_right" data-store-tab="store_list_tab2"><?=GetMessage('S_DELIVERY')?></span>
			</div>
			<div class="maps_btn pull_right" data-store-tab="store_list_tab3">
				<i class="fa fa-map-marker" aria-hidden="true"></i><span><?=GetMessage('S_SHOW_MAP')?></span>
			</div>
		</div>
		<div class="store_list_body">
			<div class="store_list_tab1 hidden_xs">
				<table class="store_list_tab">
					<thead>
						<tr>
							<td class="column-1"><?=GetMessage('S_ADDRESS')?></td>
							<td class="column-2"><?=GetMessage('S_AMOUNT')?></td>
							<td class="column-3"><?=GetMessage('S_SHIPPING_TIME')?></td>
							<td class="column-4"><?=GetMessage('S_SCHEDULE')?></td>
							<td class="column-5"></td>
						</tr>
					</thead>
					<tbody>
						<?foreach($arResult["STORES"] as $pid => $arStore):?>
							<tr>
								<td class="column-1"><b><?=$arStore["TITLE"]?></b><span><?=$arStore["ADDRESS"]?></span></td>
								<?
								switch ($arStore["AMOUNT"]) {
									case 'ABSENT':
										$amountClass = "fa-ban red";
										break;
									case 'FEW':
										$amountClass = "fa-check red";
										break;
									case 'NOT_MUCH':
										$amountClass = "fa-check yellow";
										break;
									case 'MUCH':
										$amountClass = "fa-check green";
										break;
								}
								?>
								<td class="column-2" id="<?=$arResult['JS']['ID']?>_<?=$arStore['ID']?>"><i class="fa <?=$amountClass?>"></i> <?=GetMessage($arStore["AMOUNT"])?></td>
								<td class="column-3"><?=$arStore["USER_FIELDS"]["UF_SHIPPING_TIME"]["CONTENT"]?></td>
								<td class="column-4"><?=$arStore["SCHEDULE"]?></td>
								<td class="column-5"><a href="<? echo $arStore['URL']; ?>" class="btn_round btn_border btn_hover_color"><?=GetMessage('S_ABOUT')?></a></td>
							</tr>
						<?endforeach;?>
					</tbody>
				</table>
			</div>
			<div class="store_list_tab2 hidden_xs">
				<div class="pickup_tab">
					<?
					$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR."include/delivery.php"
					)
					);
					?>
				</div>
			</div>
			<div class="store_list_tab3">
				<div class="store_map_tab">
					<div class="container_16">
						<div class="map">
							<div class="grid_4 grid_5_sm alpha omega menu_container">
								<ul class="store_list">
									<?
									$JSplacemarks = array();
									$i = 0;
									?>
									<?foreach($arResult["STORES"] as $pid => $arStore):?>
										<li class="store_item store1" data-yamaps-placemark="<?=$i?>">
											<h3 class="city"><?=$arStore["TITLE"]?></h3>
											<span class="street"><?=$arStore["ADDRESS"]?></span>
											<?
											foreach($arStore['PHONE'] as $arPhoneIndex=>$arPhone) {
												?>
												<span class="phone"><? echo $arPhone['DISPLAY'];?></span>
												<?
											}
											?>
											<a href="mailto:<?=$arStore["EMAIL"]?>" class="email"><?=$arStore["EMAIL"]?></a>
										</li>
										<?
										$JSplacemark = "
														myPlacemark".$i." = new ymaps.Placemark([".$arStore['COORDINATES']["GPS_N"].",".$arStore['COORDINATES']["GPS_S"]."], {
															hintContent: '".$arStore["TITLE"]."',
															balloonContent: '<div style=\"width:200px;\">' +
																'<h3 class=\"city\">".$arStore["TITLE"]."</h3>' +
																'<span class=\"street\">".$arStore["ADDRESS"]."</span>' +";
										foreach($arStore['PHONE'] as $arPhoneIndex=>$arPhone) {
											$JSplacemark .= "
																'<span class=\"phone\">" . $arPhone["DISPLAY"] . "</span>' +
														";
										}
										$JSplacemark .= "
																'<br/>' +
																'<a href=\"".$arStore['URL']."\" class=\"btn_round btn_color btn_wide\">".GetMessage("S_DETAIL")."</a>' +
																'</div>'},
															{
																iconLayout: 'default#image',
															});
													";
										$JSplacemarks[] = $JSplacemark;
										unset($JSplacemark);
										$i++;
										?>
									<?endforeach;?>
								</ul>
							</div>
							<div class="grid_12 grid_11_sm alpha omega map_container hidden_xs">
								<div id="map" style="width: 100%; height: 330px"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?if (isset($arResult["IS_SKU"]) && $arResult["IS_SKU"] == 1):?>
	<script type="text/javascript">

		var myMap;
		function init() {
			var myMap = new ymaps.Map("map", {
				center: [55.76, 37.64],
				behaviors: ['default', 'scrollZoom'],
				zoom: 10,
				controls: ['zoomControl']
			});
	<?
			foreach ($JSplacemarks as $placemarkIndex => $placemark) {
				echo $placemark;
				echo 'myMap.geoObjects.add(myPlacemark' . $placemarkIndex . ');';
			}
			?>
			$("[data-yamaps-placemark]").click(function(){
				var currentPlacemaker = eval('myPlacemark'+$(this).attr('data-yamaps-placemark'));
				myMap.setCenter(currentPlacemaker.geometry.getCoordinates(),15);
				currentPlacemaker.balloon.open();
			});
		};
		var obStoreAmount = new JCCatalogStoreSKU(<? echo CUtil::PhpToJSObject($arResult['JS'], false, true, true); ?>);
	</script>
	<script src="https://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU&onload=init"></script>
	<?
endif;?>