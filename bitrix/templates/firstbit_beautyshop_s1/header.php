<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true ");
header("Access-Control-Allow-Methods: OPTIONS, GET, POST");
header("Access-Control-Allow-Headers: Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
CJSCore::Init(array("ajax"));
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<? $APPLICATION->ShowHead(); ?>
	<title><? $APPLICATION->ShowTitle(); ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico<? if(file_exists($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'favicon.ico')) { ?>?v=<?=md5_file($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'favicon.ico')?><? } ?>"/>
	<?
	$APPLICATION->AddHeadString('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">');

	$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . "/css/font-awesome.min.css");
	$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . "/css/normalize.css");
	$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . "/css/960.css");
	$APPLICATION->AddHeadScript("//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js");
	$APPLICATION->AddHeadScript('//ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/zoomsl-3.0.min.js");

	$APPLICATION->AddHeadScript("//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/js.cookie.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.storageapi.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.mCustomScrollbar.min.js");
	$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . "/css/jquery.mCustomScrollbar.min.css");

	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/flexmenu.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/flexibility.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/modernizr-custom.js");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/select2.min.css");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/select2.min.js");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.minicolors.css");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.minicolors.min.js");

	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/owl.carousel.min.js");
	$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . "/css/owl.carousel.css");
	$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . "/css/owl.theme.default.css");
	$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . "/css/animate.css");

	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.plugin.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.countdown.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/lang/ru/js/jquery.countdown-ru.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/inputmask/inputmask.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/inputmask/inputmask.extensions.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/inputmask/inputmask.date.extensions.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/inputmask/inputmask.numeric.extensions.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/inputmask/inputmask.regex.extensions.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/inputmask/jquery.inputmask.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/starrr.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/script.js");

	?>
	<?

	$APPLICATION->AddHeadString('<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie8_960.css">
		<link rel="stylesheet" type="text/css" href="css/ie8fix.css">
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->');
	?>

</head>
<body>
<? $APPLICATION->ShowPanel(); ?>
<div class="btn_top" id="top_scroller"><i class="fa fa-chevron-up"></i></div>
<div id="body_container">
<div class="body_wrapper">
	<div id="overlayBlock" class="overlay_block"></div>
	<div class="popup_box" id="popup"></div>

	<?$APPLICATION->ShowViewContent('content_wrapper_type');?>

		<? $APPLICATION->IncludeComponent('firstbit:header',
			'',
			array(
				'CACHE_TIME' => 3600,
				'CACHE_TYPE' => 'A',
				"CATALOG_IBLOCK_ID" => "15",
				"NEWS_IBLOCK_ID" => "5",
				"SERVICES_IBLOCK_ID" => "7",
				"OFFERS_IBLOCK_ID" => "16",
				"BRANDS_IBLOCK_ID" => "8",
				"PATH_TO_SEARCH" => "/search/",
				"PATH_TO_BASKET" => "/personal/cart/",
				"PATH_TO_CATALOG" => "/catalog/",
				"PATH_TO_ORDER" => "/personal/order/make/",
				"PATH_TO_BRANDS" => "/brands/",
				"PATH_TO_PERSONAL" => "/personal/",
				"PATH_TO_LOGIN" => "/auth/",
				"PATH_TO_REGISTER" => "/auth/?register=yes",
				"PATH_TO_LOGOUT" => $APPLICATION->GetCurPage(false) . "?logout=yes",
				"PATH_TO_WISHLIST" => "/wishlist/",
				'PATH_TO_COMPARE' => '/catalog/compare/',
			),
			false,
			array('HIDE_ICONS' => 'Y'));
		?>
		<div class="content">
			<? if ($APPLICATION->GetCurPage() != "/index.php" && $APPLICATION->GetCurPage() != "/") { ?>
			<section class="breadcrumb hidden_xs" id="navigation">
				<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array("START_FROM" => "0", "PATH" => "", "SITE_ID" => SITE_ID));?>
			</section>
			<? } ?>
			<? if ($APPLICATION->GetCurPage() != "/index.php" && $APPLICATION->GetCurPage() != "/") { ?>
				<div class="row page-title">
					<div class="container_16">
						<div class="grid_16 margin_sm_10 margin_xs_10">
							<h1><?$APPLICATION->ShowTitle()?></h1>
							<?$APPLICATION->ShowViewContent('pageTitle_subTitle');?>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			<? } ?>
			<? if (!CSite::InDir("/catalog/") && !CSite::InDir("/brands/") && $APPLICATION->GetCurPage() != "/index.php" && $APPLICATION->GetCurPage() != "/") { ?>
			<div class="row page-container <?$APPLICATION->ShowProperty("pageContainerClass")?>">
				<div class="container_16">
					<?$APPLICATION->ShowViewContent('showLeftMenu');?>
					<? } ?>

