<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if (strlen($arResult["MESSAGE"]) > 0) {
	$arResult['MESSAGE'] = $arParams['SUCCESS_MESSAGE'] ? $arParams['SUCCESS_MESSAGE'] : GetMessage('IBLOCK_FORM_SUCCESS');
}
if(isset($arParams['PRODUCT_ID']) && $arParams['PRODUCT_ID']>0) {
	$rsProduct = CIBlockElement::GetByID($arParams['PRODUCT_ID']);
	if($arProduct = $rsProduct->GetNext()) {
		$arResult['PRODUCT_ID'] = $arParams['PRODUCT_ID'];
		$arResult['PRODUCT_NAME'] = $arProduct['NAME'];
	}
}