<?
$MESS["IBLOCK_FORM_SUBMIT"] = "Отправить";
$MESS["IBLOCK_FORM_SUCCESS"] = "Спасибо, за отзыв. Он будет опубликован после проверки администратором магазина.";
$MESS["IBLOCK_ELEMENT_NAME"] = "Отзыв от #DATE# на товар #PRODUCT_NAME#";
$MESS["IBLOCK_FORM_FIELD_USER_NAME"] = "Ваше имя";
$MESS["IBLOCK_FORM_FIELD_USER_EMAIL"] = "Ваш E-mail";
$MESS["IBLOCK_FORM_FIELD_USER_CITY"] = "Город";
$MESS["IBLOCK_FORM_FIELD_PRODUCT_RATING"] = "Оценка";
$MESS["IBLOCK_FORM_FIELD_REVIEW"] = "Ваш отзыв";