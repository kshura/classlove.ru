<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$frame = $this->createFrame()->begin();
$firstBit = new CFirstbitBeautyshop();

?>
<div class="feedback_wrapper">
	<h2><?=GetMessage('FEEDBACK_TITLE')?></h2>
	<p><?=GetMessage('FEEDBACK_TEXT')?></p>

	<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" class="form form_feedback_mainpage">
		<?= bitrix_sessid_post() ?>
		<?
		$elementName = GetMessage('IBLOCK_ELEMENT_NAME', array("#DATE#" => date('d.m.Y')));
		?>
		<input type="hidden" name="PROPERTY[NAME][]" value="<?= $elementName ?>"/>
		<?
		if($USER->IsAuthorized()) {
			$useridValue = $USER->GetID();
		} else {
			$useridValue = '';
		}
		?>
		<input type="hidden" name="PROPERTY[USER_ID][]" value="<?= $useridValue ?>"/>
		<input type="hidden" name="PROPERTY[USER_IP][]" value="<?= $firstBit->getRealIP() ?>"/>
		<input type="hidden" name="PROPERTY[FEEDBACK_TYPE][]" value="<?= $arParams['FEEDBACK_TYPE'] ?>"/>
		<?
		if($USER->IsAuthorized()) {
			$usernameValue = $USER->GetFullName();
		} else {
			$usernameValue = '';
		}
		?>
		<input type="text" name="PROPERTY[USER_NAME][]" placeholder="<?=GetMessage('IBLOCK_FORM_FIELD_USER_NAME')?>" value="<?= $usernameValue ?>" required="required" />
		<?
		if($USER->IsAuthorized()) {
			$emailValue = $USER->GetEmail();
		} else {
			$emailValue = '';
		}
		?>
		<input type="text" data-mask="email" name="PROPERTY[USER_EMAIL][]" placeholder="<?= GetMessage("IBLOCK_FORM_FIELD_USER_EMAIL") ?>" value="<?= $emailValue ?>" required="required" />
		<textarea name="PROPERTY[MESSAGE][]" placeholder="<?=GetMessage('IBLOCK_FORM_FIELD_MESSAGE')?>" required="required" ><?= $value ?></textarea>
		<?
		if ($firstBit->options['CONSENT_FORM'] === 'Y' && $firstBit->options['AGREEMENT_ID'] > 0) {
			?>
			<div class="legal">
				<input type="hidden" name="agreement_id" value="<?= $firstBit->options['AGREEMENT_ID']?>" />
				<input type="checkbox" required name="agreement_consent" id="legal" class="legal" value="Y"><label for="legal"><a href="<?= $firstBit->options['AGREEMENT_PAGE_LINK']?>" target="_blank" rel="nofollow"><?= $firstBit->options['AGREEMENT_LABEL_FORM_TEXT']?></a></label>
			</div>
			<?
		}
		?>
		<div id="status_FeedbackValidation" class="form_message">
			<?
			if (strlen($arResult["MESSAGE"]) > 0) {
				?>
				<p><span class="notetext"><? echo $arResult["MESSAGE"]; ?></span></p>
				<?
			}
			?>
		</div>
		<input type="submit" class="btn_round btn_square btn_color" name="iblock_submit" value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
	</form>
</div>

