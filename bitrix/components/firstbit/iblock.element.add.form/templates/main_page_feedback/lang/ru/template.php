<?
$MESS["IBLOCK_FORM_SUBMIT"] = "Отправить";
$MESS["IBLOCK_FORM_SUCCESS"] = "Ваше сообщение принято. Мы подготовим и отправим ответ на указанную электронную почту.";
$MESS["IBLOCK_ELEMENT_NAME"] = "Сообщение пользователя от #DATE#";
$MESS["IBLOCK_FORM_FIELD_USER_NAME"] = "Ваше имя";
$MESS["IBLOCK_FORM_FIELD_USER_EMAIL"] = "Ваш E-mail";
$MESS["IBLOCK_FORM_FIELD_MESSAGE"] = "Сообщение";
$MESS["FEEDBACK_TITLE"] = "Напишите нам";
$MESS["FEEDBACK_TEXT"] = "Если у вас есть вопросы, смело пишите нам.<br/>Мы с радостью поможем!";