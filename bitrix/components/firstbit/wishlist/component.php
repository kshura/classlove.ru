<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$this->IncludeComponentTemplate();

return $arResult['WISHLIST_COUNT'];
