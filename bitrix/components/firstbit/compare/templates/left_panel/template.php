<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
?>
<div class="compare_wrapper" id="compare_left_panel">
	<?$frame = $this->createFrame()->begin();?>
	<a href="<? echo $arParams['PATH_TO_COMPARE'];?>">
		<i class="fa fa-bar-chart custom_color_hover"></i>
		<span class="compare_title"><?=GetMessage('COMPARE_TITLE')?></span>
		<span class="compare_item_count"><span data-compare-count=""><?=$arResult['COMPARE_COUNT']?></span> <?=GetMessage('COMPARE_MEASURE')?></span>
	</a>
	<? $frame->end(); ?>
</div>