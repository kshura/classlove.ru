<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<div class="compare_wrapper <?=$arParams['ADDITIONAL_CLASS']?>" id="compare_header">
	<?$frame = $this->createFrame()->begin();?>
	<a href="<? echo $arParams['PATH_TO_COMPARE'];?>"> <i class="fa fa-bar-chart"></i> <span data-compare-count=""><?=$arResult['COMPARE_COUNT']?></span> </a>
	<? $frame->end(); ?>
</div>