<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if(count($_SESSION['CATALOG_COMPARE_LIST'])>0) {
	foreach($_SESSION['CATALOG_COMPARE_LIST'] as $compareList) {
		$countCompare = count($compareList['ITEMS']);
		if ($countCompare > 0) {
			$arResult['COMPARE_COUNT'] = $countCompare;
		}
		else {
			$arResult['COMPARE_COUNT'] = '0';
		}
	}
} else {
	$arResult['COMPARE_COUNT'] = '0';
}

$this->IncludeComponentTemplate();

return $arResult['COMPARE_COUNT'];