<?
$MESS['SLIDER_VIEW'] = "Вид слайдера";
$MESS['SLIDER_VIEW_WIDE'] = "Слайдер во всю ширину экрана";
$MESS['SLIDER_VIEW_SHORT'] = "Слайдер по ширине страницы";
$MESS['SLIDER_VIEW_TWO_BANNERS'] = "Два квадратных баннера";
$MESS['SLIDER_VIEW_FOUR_BANNERS'] = "Слайдер и два баннера";
$MESS['BANNERS_VIEW'] = "Вид баннеров";
$MESS['VARIANT_NUM_'] = "Вариант №";
$MESS['MAIN_PAGE_ORDER'] = "Элементы страницы";
$MESS['MAIN_PAGE_ORDER_SLIDER'] = "Слайдер";
$MESS['MAIN_PAGE_ORDER_BANNERS'] = "Баннеры";
$MESS['MAIN_PAGE_ORDER_HITS'] = "Хиты продаж";
$MESS['MAIN_PAGE_ORDER_NEWPRODUCT'] = "Новинки";
$MESS['MAIN_PAGE_ORDER_BRANDS'] = "Бренды";
$MESS['MAIN_PAGE_ORDER_ADVANTAGES'] = "Преимущества";
$MESS['MAIN_PAGE_ORDER_NEWS'] = "Новости";
$MESS['MAIN_PAGE_ORDER_COMPANY'] = "О компании";
$MESS['MAIN_PAGE_ORDER_BRANDS'] = "Бренды";
$MESS['MAIN_PAGE_ORDER_FEEDBACK'] = "Обратная связь";
$MESS['MAIN_PAGE_ORDER_STORES'] = "Наши магазины";
$MESS['COLOR_THEME'] = "Цветовая схема";
$MESS['COLOR_MAIN'] = "Основной цвет";
$MESS['COLOR_ADDITIONAL'] = "Дополнительный цвет";
$MESS['COLOR_SAMPLE'] = "Пример цветовой схемы";
$MESS['COLOR_RED'] = "Красный";
$MESS['COLOR_ORANGE'] = "Оранжевый";
$MESS['COLOR_YELLOW'] = "Желтый";
$MESS['COLOR_GREEN'] = "Зеленый";
$MESS['COLOR_SKY_BLUE'] = "Голубой";
$MESS['COLOR_BLUE'] = "Синий";
$MESS['COLOR_BROWN'] = "Коричневый";
$MESS['COLOR_PURPLE'] = "Фиолетовый";
$MESS['COLOR_LILAC'] = "Сиреневый";
$MESS['HEADER_TYPE'] = "Шапка";
$MESS['FOOTER_TYPE'] = "Подвал";
$MESS['FLYING_BASKET'] = "Плавающая корзина";
$MESS['USE_FLYING_BASKET'] = "Плавающая корзина";
$MESS['MENU_MIDDLE_TYPE'] = "Тип выпадающего меню";
$MESS['VARIANT_SHOW_YES'] = "Показывать";
$MESS['VARIANT_SHOW_NO'] = "Не показывать";
$MESS['USE_BOTTOM_PANEL'] = "Нижняя панель";
$MESS['BASE_CURRENCY'] = "Базовая валюта";
$MESS['USE_FLYING_MENU'] = "Плавающее меню";

$MESS['SETTINGS_RESET'] = "Сбросить все настройки";
$MESS['SETTINGS_APPLY'] = "Применить настройки";

$MESS['BRANDS_VIEW'] = "Отображение брендов";
$MESS['BRANDS_VIEW_VARIANT_IMAGES'] = "Изображения";
$MESS['BRANDS_VIEW_VARIANT_LIST'] = "Список";

$MESS['HELP_INFO_'] = "Подсказка не задана";
$MESS['HELP_INFO_COLOR_THEME'] = "Нажмите на изображение цвета для выбора.";
$MESS['HELP_INFO_COLOR_MAIN'] = "Укажите свой цвет с помошью кода или палитры, если ни один из предложенных вариантов Вам не подходит.";
$MESS['HELP_INFO_COLOR_ADDITIONAL'] = "Укажите свой цвет с помошью кода или палитры.";
$MESS['HELP_INFO_HEADER_TYPE'] = "Выберите вид отображения шапки сайта.";
$MESS['HELP_INFO_MENU_MIDDLE_TYPE'] = "Выберите тип отображения выпадающего меню.";
$MESS['HELP_INFO_FLYING_BASKET'] = "Плавающая корзина отображается в правом верхнем углу сайта и перемещается при прокрутке страницы.";
$MESS['HELP_INFO_USE_BOTTOM_PANEL'] = "Нижняя панель закреплена в нижней части окна браузера. В нижней панели отображаются просмотренные товары, Избранные товары, Товары к сравнению и Корзина.";
$MESS['HELP_INFO_SLIDER_VIEW'] = "Выберите вид отображения слайдера.";
$MESS['HELP_INFO_BANNERS_VIEW'] = "Выберите вид отображения баннеров.";
$MESS['HELP_INFO_MAIN_PAGE_ORDER'] = "Поменяйте местами названия блоков в списке для изменения порядка отображения элементов на главной странице. Просто перетащите элемент. Что бы не отображать элемент на странице - снимите галочку в чек-боксе.";

