<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$frame = $this->createFrame()->begin('');
?>
<div class="settings_label" onclick="settingsPanelToggle();"><i class="fa fa-cog"></i></div>
<div class="settings_wrapper">
<div class="settings_container <?=($_COOKIE['settings_panel'] == '1' ? 'open' : '')?>">
	<form method="POST" id="settings_panel" action="<?=$APPLICATION->GetCurPage(false)?>">
		<div class="settings_body">
			<div class="settings_close"><i class="fa fa-times"></i></div>
			<div class="column1_2">
				<div class="column1">
					<div class="settings_inner color_settings">
						<div class="settings_title"><span><?=GetMessage('COLOR_THEME')?></span>
							<div class="help_wrapper" onclick="openHelpBaloon(this);">
								<i class="fa fa-question-circle-o"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									<?=GetMessage('HELP_INFO_COLOR_THEME')?>
								</div>
							</div>
						</div>
						<div class="settings_item">
							<a href="#" class="color">
								<div class="color_block" style="background: rgb(227, 33, 42);" data-color-rgb="rgb(227, 33, 42)" title="<?=GetMessage('COLOR_RED')?>"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: rgb(238, 117, 24);" data-color-rgb="rgb(238, 117, 24)" title="<?=GetMessage('COLOR_ORANGE')?>"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: rgb(253, 196, 29);" data-color-rgb="rgb(253, 196, 29)" title="<?=GetMessage('COLOR_YELLOW')?>"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: rgb(89, 175, 48);" data-color-rgb="rgb(89, 175, 48)" title="<?=GetMessage('COLOR_GREEN')?>"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: rgb(62, 162, 238);" data-color-rgb="rgb(62, 162, 238)" title="<?=GetMessage('COLOR_SKY_BLUE')?>"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: rgb(41, 105, 169);" data-color-rgb="rgb(41, 105, 169)" title="<?=GetMessage('COLOR_BLUE')?>"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: rgb(101, 38, 47);" data-color-rgb="rgb(101, 38, 47)" title="<?=GetMessage('COLOR_BROWN')?>"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: rgb(135, 33, 99);" data-color-rgb="rgb(135, 33, 99)" title="<?=GetMessage('COLOR_PURPLE')?>"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: rgb(187, 15, 91);" data-color-rgb="rgb(187, 15, 91)" title="<?=GetMessage('COLOR_LILAC')?>"></div>
							</a>
						</div>
					</div>
					<div class="settings_inner">
						<div class="settings_item">
							<div class="colorPicker">
								<div id="colorpicker_sample" style="background-color: <?=$arResult['COLOR_MAIN']?>; color: <?=$arResult['COLOR_ADDITIONAL']?>;"><span><?=GetMessage('COLOR_SAMPLE')?></span></div>
							</div>
						</div>
					</div>

					<div class="settings_inner">
						<div class="settings_title"><span><?=GetMessage('COLOR_MAIN')?></span>
							<div class="help_wrapper" onclick="openHelpBaloon(this);">
								<i class="fa fa-question-circle-o"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									<?=GetMessage('HELP_INFO_COLOR_MAIN')?>
								</div>
							</div>
						</div>
						<div class="settings_item">
							<input type="text" id="colorpicker_main" name="OPTION[COLOR_MAIN]" value="<?=$arResult['COLOR_MAIN']?>"/>
						</div>
					</div>
					<div class="settings_inner">
						<div class="settings_title"><span><?=GetMessage('COLOR_ADDITIONAL')?></span>
							<div class="help_wrapper" onclick="openHelpBaloon(this);">
								<i class="fa fa-question-circle-o"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									<?=GetMessage('HELP_INFO_COLOR_ADDITIONAL')?>
								</div>
							</div>
						</div>
						<div class="settings_item">
							<div class="colorPicker">
								<input type="text" id="colorpicker_additional" name="OPTION[COLOR_ADDITIONAL]" value="<?=$arResult['COLOR_ADDITIONAL']?>"/>
							</div>
						</div>
					</div>

				</div>
				<div class="column2">
					<!-- выбор шапки -->
					<div class="settings_inner">
						<div class="settings_title"><span><?=GetMessage('HEADER_TYPE')?></span>
							<div class="help_wrapper" onclick="openHelpBaloon(this);">
								<i class="fa fa-question-circle-o"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									<?=GetMessage('HELP_INFO_HEADER_TYPE')?>
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="OPTION[HEADER_TYPE]" class="select_nofilter" id="settings_header_type">
								<?
								for($i=1;$i<=6;$i++) {
									?>
									<option value="<?=$i?>" <?=($arResult['HEADER_TYPE'] == $i ? 'selected="selected"' : '')?>><?=GetMessage('VARIANT_NUM_').$i?></option>
									<?
								}
								?>
							</select>
							<div class="checkbox_wrapper" id="settings_flying_menu" <?=($arResult['HEADER_TYPE'] == '4' || $arResult['HEADER_TYPE'] == '6' ? 'style="display: none;"' : '')?>>
								<input type="checkbox" name="OPTION[USE_FLYING_MENU]" id="flying_menu_on" <?=($arResult['USE_FLYING_MENU'] == 'Y' ? 'checked="checked"' : '')?>>
								<label for="flying_menu_on"><?=GetMessage('USE_FLYING_MENU')?></label>
							</div>
						</div>
					</div>
					<div class="settings_inner">
						<div class="settings_title"><span><?=GetMessage('MENU_MIDDLE_TYPE')?></span>
							<div class="help_wrapper" onclick="openHelpBaloon(this);">
								<i class="fa fa-question-circle-o"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									<?=GetMessage('HELP_INFO_MENU_MIDDLE_TYPE')?>
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="OPTION[MENU_MIDDLE_TYPE]" class="select_nofilter">
								<?
								for($i=1;$i<=3;$i++) {
									?>
									<option value="<?=$i?>" <?=($arResult['MENU_MIDDLE_TYPE'] == $i ? 'selected="selected"' : '')?>><?=GetMessage('VARIANT_NUM_').$i?></option>
									<?
								}
								?>
							</select>
						</div>
					</div>

					<div class="settings_inner ">
						<div class="settings_title"><span><?=GetMessage('FLYING_BASKET')?></span>
							<div class="help_wrapper" onclick="openHelpBaloon(this);">
								<i class="fa fa-question-circle-o"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									<?=GetMessage('HELP_INFO_FLYING_BASKET')?>
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="OPTION[USE_FLYING_BASKET]" class="select_nofilter">
								<?
								$boolSelect = array('YES', 'NO');
								foreach($boolSelect as $boolSelectVar) {
									?>
									<option value="<?=$boolSelectVar{0}?>" <?=($arResult['USE_FLYING_BASKET'] == $boolSelectVar{0} ? 'selected="selected"' : '')?>><?=GetMessage('VARIANT_SHOW_'.$boolSelectVar)?></option>
									<?
								}
								?>
							</select>
						</div>
					</div>

					<div class="settings_inner">
						<div class="settings_title"><span><?=GetMessage('FOOTER_TYPE')?></span>
						</div>
						<div class="settings_item">
							<select name="OPTION[FOOTER_TYPE]" class="select_nofilter">
								<?
								for($i=1;$i<=2;$i++) {
									?>
									<option value="<?=$i?>" <?=($arResult['FOOTER_TYPE'] == $i ? 'selected="selected"' : '')?>><?=GetMessage('VARIANT_NUM_').$i?></option>
									<?
								}
								?>
							</select>
						</div>
					</div>

					<div class="settings_inner">
						<div class="settings_title"><span><?=GetMessage('USE_BOTTOM_PANEL')?></span>
							<div class="help_wrapper" onclick="openHelpBaloon(this);">
								<i class="fa fa-question-circle-o"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									<?=GetMessage('HELP_INFO_USE_BOTTOM_PANEL')?>
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="OPTION[USE_BOTTOM_PANEL]" class="select_nofilter">
								<?
								$boolSelect = array('YES', 'NO');
								foreach($boolSelect as $boolSelectVar) {
									?>
									<option value="<?=$boolSelectVar{0}?>" <?=($arResult['USE_BOTTOM_PANEL'] == $boolSelectVar{0} ? 'selected="selected"' : '')?>><?=GetMessage('VARIANT_SHOW_'.$boolSelectVar)?></option>
									<?
								}
								?>
							</select>
						</div>
					</div>

					<div class="settings_inner">
						<div class="settings_title"><span><?=GetMessage('BRANDS_VIEW')?></span>
						</div>
						<div class="settings_item">
							<select name="OPTION[BRANDS_VIEW]" class="select_nofilter">
								<?
								$variants = array('IMAGES', 'LIST');
								foreach($variants as $variant) {
									?>
									<option value="<?=$variant?>" <?=($arResult['BRANDS_VIEW'] == $variant ? 'selected="selected"' : '')?>><?=GetMessage('BRANDS_VIEW_VARIANT_'.$variant)?></option>
									<?
								}
								?>
							</select>
						</div>
					</div>
				</div>
			</div>

			<div class="column3">
				<div class="settings_inner slider_settings">
					<div class="settings_title"><span><?=GetMessage('SLIDER_VIEW')?></span>
						<div class="help_wrapper" onclick="openHelpBaloon(this);">
							<i class="fa fa-question-circle-o"></i>
							<div class="help_inner">
								<div class="help_angle"></div>
								<?=GetMessage('HELP_INFO_SLIDER_VIEW')?>
							</div>
						</div>
					</div>
					<div class="settings_item">
						<input type="hidden" name="OPTION[SLIDER_VIEW]" id="slider_view" value="<?=$arResult['SLIDER_VIEW']?>">
						<a href="#" class="filters_block <?=($arResult['SLIDER_VIEW']=='1' ? 'active' : '')?>" data-firstbit-option="1">
							<div class="select_block" title="<?=GetMessage('SLIDER_VIEW_WIDE')?>">
								<div class="select_block_inner">
									<div class="blue_top"></div>
								</div>
							</div>
						</a>
						<a href="#" class="filters_block <?=($arResult['SLIDER_VIEW']=='2' ? 'active' : '')?>" data-firstbit-option="2">
							<div class="select_block" title="<?=GetMessage('SLIDER_VIEW_SHORT')?>">
								<div class="select_block_inner">
									<div class="blue_top_short"></div>
								</div>
							</div>
						</a>
						<a href="#" class="filters_block <?=($arResult['SLIDER_VIEW']=='3' ? 'active' : '')?>" data-firstbit-option="3">
							<div class="select_block" title="<?=GetMessage('SLIDER_VIEW_TWO_BANNERS')?>">
								<div class="select_block_inner">
									<div class="blue_top_short">
										<div class="blue_top2"></div>
									</div>
								</div>
							</div>
						</a>
						<a href="#" class="filters_block <?=($arResult['SLIDER_VIEW']=='4' ? 'active' : '')?>" data-firstbit-option="4">
							<div class="select_block" title="<?=GetMessage('SLIDER_VIEW_FOUR_BANNERS')?>">
								<div class="select_block_inner">
									<div class="blue_top_short">
										<div class="blue_top1"></div>
										<div class="blue_top1">
											<div class="blue_top1"></div>
											<div class="blue_top1"></div>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="settings_inner banners_settings">
					<div class="settings_title"><span><?=GetMessage('BANNERS_VIEW')?></span>
						<div class="help_wrapper" onclick="openHelpBaloon(this);">
							<i class="fa fa-question-circle-o"></i>
							<div class="help_inner">
								<div class="help_angle"></div>
								<?=GetMessage('HELP_INFO_BANNERS_VIEW')?>
							</div>
						</div>
					</div>
					<div class="settings_item">
						<div class="settings_item">
							<select id="banners_view" name="OPTION[BANNERS_VIEW]" class="select_nofilter">
								<?
								for($i=1;$i<=6;$i++) {
									?>
									<option value="<?=$i?>" <?=($arResult['BANNERS_VIEW'] == $i ? 'selected="selected"' : '')?>><?=GetMessage('VARIANT_NUM_').$i?></option>
									<?
								}
								?>
							</select>
							<div class="selected_banners_types">
								<div class="select_banners_inner">
									<div class="banners_img banners_variant<?=$arResult['BANNERS_VIEW']?>"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="settings_inner">
					<div class="settings_title"><span><?=GetMessage('MAIN_PAGE_ORDER')?></span>
						<div class="help_wrapper" onclick="openHelpBaloon(this);">
							<i class="fa fa-question-circle-o"></i>
							<div class="help_inner">
								<div class="help_angle"></div>
								<?=GetMessage('HELP_INFO_MAIN_PAGE_ORDER')?>
							</div>
						</div>
					</div>
					<div class="settings_item">
						<ul id="sortable_blocks_settings">
							<? foreach($arResult['MAIN_PAGE_ORDER'] as $arBlockName=>$arBlockSort) { ?>
								<li class="ui-state-default">
									<span class="sort_num"></span>
									<input type="checkbox" name="OPTION[MAIN_PAGE_ORDER][CHECKED][<?=$arBlockName?>]" <?=($arResult['MAIN_PAGE_HIDE'][$arBlockName] == 'N' ? 'checked' : '') ?>>
									<input type="hidden" name="OPTION[MAIN_PAGE_ORDER][HIDE][<?=$arBlockName?>]" value="<?=$arResult['MAIN_PAGE_HIDE'][$arBlockName]?>">
									<label for="offers_themes1"><?=GetMessage('MAIN_PAGE_ORDER_'.$arBlockName)?></label>
									<i class="fa fa-sort"></i>
								</li>
							<? } ?>

						</ul>
					</div>
				</div>

				<script>
					$('#sortable_blocks_settings li').mouseenter(function () {
						for (var i = 0; i < $('#sortable_blocks_settings').children('li').length; i++) {
							$('#sortable_blocks_settings li').children('.sort_num').eq(i).text(i + 1);
						}
					});
				</script>
			</div>
		</div>
		<div class="settings_footer_wrapper">
			<div class="">
				<input type="submit" name="settings_reset" class="btn_round btn_disabled btn_hover_color" value="<?=GetMessage('SETTINGS_RESET')?>">
			</div>
			<div class="">
				<input type="submit" name="settings_panel" class="btn_round btn_color" value="<?=GetMessage('SETTINGS_APPLY')?>">
			</div>
		</div>
	</form>
</div>
</div>
<script><? require_once('script.js'); ?></script>