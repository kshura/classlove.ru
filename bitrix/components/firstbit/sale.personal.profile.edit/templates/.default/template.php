<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<a href="<?=$arParams["PATH_TO_LIST"]?>" class="link_back"><i class="fa fa-angle-left"></i><?=GetMessage("SPPD_RECORDS_LIST")?></a>

<?if($arResult["ID"]>=0):?>
	<?if($arResult["ID"]>0):?>
	<form method="post" action="<?=POST_FORM_ACTION_URI?>" class="form form_general">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="ID" value="<?=$arResult["ID"]?>">
		<ul class="form_fields">
			<li>
				<label><?echo GetMessage("SALE_PNAME")?>:</label>
				<input type="text" value="<?echo $arResult["NAME"];?>" name="NAME" required="required">
				<i class="fa fa-check-circle not-active"></i>
			</li>
		</ul>
		<?
		foreach($arResult["ORDER_PROPS"] as $val) {
			if(!empty($val["PROPS"])) {
				?>
				<div class="form_fields_section">
					<h2><?=$val["NAME"];?></h2>
				</div>
				<ul class="form_fields">
					<?
					foreach($val["PROPS"] as $vval) {
						$currentValue = $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_".$vval["ID"]];
						$name = "ORDER_PROP_".$vval["ID"];
						?>
						<li>
							<label><?=$vval["NAME"] ?>:</label>
							<?
							if ($vval["TYPE"]=="CHECKBOX") { ?>
								<input type="hidden" name="<?=$name?>" value="">
								<input type="checkbox" name="<?=$name?>" value="Y"<?if ($currentValue=="Y" || !isset($currentValue) && $vval["DEFAULT_VALUE"]=="Y") echo " checked";?>>
							<? }
							else if ($vval["TYPE"]=="TEXT") { ?>
								<input type="text" size="<?echo (IntVal($vval["SIZE1"])>0)?$vval["SIZE1"]:30; ?>" maxlength="250" value="<?echo (isset($currentValue)) ? $currentValue : $vval["DEFAULT_VALUE"];?>" name="<?=$name?>" <?if ($vval["REQUIED"]=="Y") {?>required="required"<?}?> <? if($vval["IS_EMAIL"]=="Y") {?>data-mask="email"<?}?> <? if($vval["IS_PHONE"]=="Y") {?>data-mask="phone"<?}?>>
							<? }
							else if ($vval["TYPE"]=="SELECT") { ?>
								<? foreach($vval['VALUES'] as $vvval) { ?>
									<label><input type="radio" name="<?=$name?>" name="REGISTER[<?=$arProperty["CODE"]?>]" value="<?=$vvval['VALUE']?>" <? if($vvval["VALUE"]==$currentValue || !isset($currentValue) && $vvval["VALUE"]==$vval["DEFAULT_VALUE"]) : ?>checked="checked"<? endif; ?>><?=$vvval['NAME']?></label>
								<? } ?>
							<? }
							else if ($vval["TYPE"]=="MULTISELECT") { ?>
								<select multiple name="<?=$name?>[]" size="<?echo (IntVal($vval["SIZE1"])>0)?$vval["SIZE1"]:5; ?>">
									<?
									$arCurVal = array();
									$arCurVal = explode(",", $currentValue);
									for ($i = 0, $cnt = count($arCurVal); $i < $cnt; $i++)
										$arCurVal[$i] = trim($arCurVal[$i]);
									$arDefVal = explode(",", $vval["DEFAULT_VALUE"]);
									for ($i = 0, $cnt = count($arDefVal); $i < $cnt; $i++)
										$arDefVal[$i] = trim($arDefVal[$i]);
									foreach($vval["VALUES"] as $vvval) { ?>
										<option value="<?=$vvval["VALUE"]?>"<?if (in_array($vvval["VALUE"], $arCurVal) || !isset($currentValue) && in_array($vvval["VALUE"], $arDefVal)) echo" selected"?>><?=$vvval["NAME"]?></option>
									<? } ?>
								</select>
							<? }
							else if ($vval["TYPE"]=="TEXTAREA") { ?>
								<textarea rows="<?echo (IntVal($vval["SIZE2"])>0)?$vval["SIZE2"]:4; ?>" cols="<?echo (IntVal($vval["SIZE1"])>0)?$vval["SIZE1"]:40; ?>" name="<?=$name?>" <?if ($vval["REQUIED"]=="Y") {?>required="required"<?}?>><?echo (isset($currentValue)) ? $currentValue : $vval["DEFAULT_VALUE"];?></textarea>
							<? }
							else if ($vval["TYPE"]=="LOCATION") { ?>
								<? if ($arParams['USE_AJAX_LOCATIONS'] == 'Y') { ?>
									<? $locationValue = intval($currentValue) ? $currentValue : $vval["DEFAULT_VALUE"]; ?>
									<? CSaleLocation::proxySaleAjaxLocationsComponent(
										array(
											"AJAX_CALL" => "N",
											'CITY_OUT_LOCATION' => 'Y',
											'COUNTRY_INPUT_NAME' => $name . '_COUNTRY',
											'CITY_INPUT_NAME' => $name,
											'LOCATION_VALUE' => $locationValue,
										),
										array(),
										$locationTemplate,
										true,
										'location-block-wrapper'
									) ?>
									<?
								} else {
									?>
									<select name="<?= $name ?>" size="<? echo (IntVal($vval["SIZE1"]) > 0) ? $vval["SIZE1"] : 1; ?>">
										<? foreach($vval["VALUES"] as $vvval) { ?>
											<option value="<?= $vvval["ID"] ?>"<? if(IntVal($vvval["ID"]) == IntVal($currentValue) || !isset($currentValue) && IntVal($vvval["ID"]) == IntVal($vval["DEFAULT_VALUE"])) echo " selected" ?>><? echo $vvval["COUNTRY_NAME"] . " - " . $vvval["CITY_NAME"] ?></option>
										<? } ?>
									</select>
									<?
								}
									?>
							<? }
							else if ($vval["TYPE"]=="RADIO") { ?>
								<? foreach($vval["VALUES"] as $vvval) { ?>
									<input type="radio" name="<?=$name?>" value="<?=$vvval["VALUE"]?>"<?if ($vvval["VALUE"]==$currentValue || !isset($currentValue) && $vvval["VALUE"]==$vval["DEFAULT_VALUE"]) echo " checked"?>><?=$vvval["NAME"]?><br />
								<? } ?>
							<? } ?>
							<? if (strlen($vval["DESCRIPTION"])>0) { ?>
								<br /><small><?=$vval["DESCRIPTION"] ?></small>
							<? } ?>
							<?
							if ($vval["REQUIED"]=="Y") {
								?>
								<i class="fa fa-check-circle not-active" title="<?=GetMessage("AUTH_REQUIRED")?>"></i>
								<?
							}
							?>
						</li>
						<?
					}
					?>
				</ul>
				<?
			}
		}
		?>
		<div class="form_message">
			<? ShowError($arResult["ERROR_MESSAGE"]); ?>
		</div>

		<div class="form_btn_wrapper btn_wrapper">
			<input type="submit" value="<?=GetMessage("SALE_SAVE") ?>" class="btn_round btn_color" name="save" value="<?=GetMessage("SALE_SAVE") ?>">
			<a href="<?=$arParams['PATH_TO_LIST']?>" class="btn_round btn_disabled btn_hover_color"><?=GetMessage("SALE_RESET") ?></a>
		</div>
	</form>
	<?elseif($arResult["ID"]==0):?>
		<div class="profile_tabs common_tabs">
			<? if(count($arResult["PERSON_TYPES"]) > 1) { ?>
				<ul class="btn-panel">
					<? foreach($arResult["PERSON_TYPES"] as $arPersonTypeIndex => $arPersonType) { ?>
						<li class="btn-panel-item"><a
								href="#tabs-<?= $arPersonTypeIndex ?>"><?= $arPersonType["NAME"] ?></a></li>
					<? } ?>
				</ul>
			<? } ?>
			<div>
				<? foreach($arResult["PERSON_TYPES"] as $arPersonTypeIndex=>$arPersonType) { ?>
					<div id="tabs-<?=$arPersonTypeIndex?>" class="tabs_inner">
						<form method="post" action="<?=POST_FORM_ACTION_URI?>" class="form form_general">
							<?=bitrix_sessid_post()?>
							<input type="hidden" name="ID" value="<?=$arResult["ID"]?>">
							<input type="hidden" name="PERSON_TYPE_ID" value="<?=$arPersonType["ID"]?>">
							<ul class="form_fields">
								<li>
									<label><?echo GetMessage("SALE_PNAME")?>:</label>
									<input type="text" value="<?echo $arResult["NAME"];?>" name="NAME" required="required">
									<i class="fa fa-check-circle not-active"></i>
								</li>
							</ul>
							<?
							foreach($arResult["ORDER_PROPS"] as $val) {
								if(!empty($val["PROPS"]) && $val['PERSON_TYPE_ID'] == $arPersonType['ID']) {
									?>
									<div class="form_fields_section">
										<h2><?=$val["NAME"];?></h2>
									</div>
									<ul class="form_fields">
										<?
										foreach($val["PROPS"] as $vval) {
											$currentValue = $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_".$vval["ID"]];
											$name = "ORDER_PROP_".$vval["ID"];
											?>
											<li>
												<label><?=$vval["NAME"] ?>:</label>
												<?
												if ($vval["TYPE"]=="CHECKBOX") { ?>
													<input type="hidden" name="<?=$name?>" value="">
													<input type="checkbox" name="<?=$name?>" value="Y"<?if ($currentValue=="Y" || !isset($currentValue) && $vval["DEFAULT_VALUE"]=="Y") echo " checked";?>>
												<? }
												else if ($vval["TYPE"]=="TEXT") { ?>
													<input type="text" size="<?echo (IntVal($vval["SIZE1"])>0)?$vval["SIZE1"]:30; ?>" maxlength="250" value="<?echo (isset($currentValue)) ? $currentValue : $vval["DEFAULT_VALUE"];?>" name="<?=$name?>" <?if ($vval["REQUIED"]=="Y") {?>required="required"<?}?> <? if($vval["IS_EMAIL"]=="Y") {?>data-mask="email"<?}?> <? if($vval["IS_PHONE"]=="Y") {?>data-mask="phone"<?}?>>
												<? }
												else if ($vval["TYPE"]=="SELECT") { ?>
													<? foreach($vval['VALUES'] as $vvval) { ?>
														<label><input type="radio" name="<?=$name?>" name="REGISTER[<?=$arProperty["CODE"]?>]" value="<?=$vvval['VALUE']?>" <? if($vvval["VALUE"]==$currentValue || !isset($currentValue) && $vvval["VALUE"]==$vval["DEFAULT_VALUE"]) : ?>checked="checked"<? endif; ?>><?=$vvval['NAME']?></label>
													<? } ?>
												<? }
												else if ($vval["TYPE"]=="MULTISELECT") { ?>
													<select multiple name="<?=$name?>[]" size="<?echo (IntVal($vval["SIZE1"])>0)?$vval["SIZE1"]:5; ?>">
														<?
														$arCurVal = array();
														$arCurVal = explode(",", $currentValue);
														for ($i = 0, $cnt = count($arCurVal); $i < $cnt; $i++)
															$arCurVal[$i] = trim($arCurVal[$i]);
														$arDefVal = explode(",", $vval["DEFAULT_VALUE"]);
														for ($i = 0, $cnt = count($arDefVal); $i < $cnt; $i++)
															$arDefVal[$i] = trim($arDefVal[$i]);
														foreach($vval["VALUES"] as $vvval) { ?>
															<option value="<?=$vvval["VALUE"]?>"<?if (in_array($vvval["VALUE"], $arCurVal) || !isset($currentValue) && in_array($vvval["VALUE"], $arDefVal)) echo" selected"?>><?=$vvval["NAME"]?></option>
														<? } ?>
													</select>
												<? }
												else if ($vval["TYPE"]=="TEXTAREA") { ?>
													<textarea rows="<?echo (IntVal($vval["SIZE2"])>0)?$vval["SIZE2"]:4; ?>" cols="<?echo (IntVal($vval["SIZE1"])>0)?$vval["SIZE1"]:40; ?>" name="<?=$name?>" <?if ($vval["REQUIED"]=="Y") {?>required="required"<?}?>><?echo (isset($currentValue)) ? $currentValue : $vval["DEFAULT_VALUE"];?></textarea>
												<? }
												else if ($vval["TYPE"]=="LOCATION") { ?>
													<? if ($arParams['USE_AJAX_LOCATIONS'] == 'Y') { ?>
														<? $locationValue = intval($currentValue) ? $currentValue : $vval["DEFAULT_VALUE"]; ?>
														<? CSaleLocation::proxySaleAjaxLocationsComponent(
															array(
																"AJAX_CALL" => "N",
																'CITY_OUT_LOCATION' => 'Y',
																'COUNTRY_INPUT_NAME' => $name . '_COUNTRY',
																'CITY_INPUT_NAME' => $name,
																'LOCATION_VALUE' => $locationValue,
															),
															array(),
															$locationTemplate,
															true,
															'location-block-wrapper'
														) ?>
														<?
													} else {
														?>
														<select name="<?= $name ?>" size="<? echo (IntVal($vval["SIZE1"]) > 0) ? $vval["SIZE1"] : 1; ?>">
															<? foreach($vval["VALUES"] as $vvval) { ?>
																<option value="<?= $vvval["ID"] ?>"<? if(IntVal($vvval["ID"]) == IntVal($currentValue) || !isset($currentValue) && IntVal($vvval["ID"]) == IntVal($vval["DEFAULT_VALUE"])) echo " selected" ?>><? echo $vvval["COUNTRY_NAME"] . " - " . $vvval["CITY_NAME"] ?></option>
															<? } ?>
														</select>
														<?
													}
													?>
												<? }
												else if ($vval["TYPE"]=="RADIO") { ?>
													<? foreach($vval["VALUES"] as $vvval) { ?>
														<input type="radio" name="<?=$name?>" value="<?=$vvval["VALUE"]?>"<?if ($vvval["VALUE"]==$currentValue || !isset($currentValue) && $vvval["VALUE"]==$vval["DEFAULT_VALUE"]) echo " checked"?>><?=$vvval["NAME"]?><br />
													<? } ?>
												<? } ?>
												<? if (strlen($vval["DESCRIPTION"])>0) { ?>
													<br /><small><?=$vval["DESCRIPTION"] ?></small>
												<? } ?>
												<?
												if ($vval["REQUIED"]=="Y") {
													?>
													<i class="fa fa-check-circle not-active" title="<?=GetMessage("AUTH_REQUIRED")?>"></i>
													<?
												}
												?>
											</li>
											<?
										}
										?>
									</ul>
									<?
								}
							}
							?>
							<div class="form_btn_wrapper btn_wrapper">
								<input type="submit" value="<?=GetMessage("SALE_SAVE") ?>" class="btn_round btn_color" name="save" value="<?=GetMessage("SALE_SAVE") ?>">
								<a href="<?=$arParams['PATH_TO_LIST']?>" class="btn_round btn_disabled btn_hover_color"><?=GetMessage("SALE_RESET") ?></a>
							</div>
						</form>
					</div>
				<? } ?>
			</div>
		</div>

	<?endif;?>
<?else:?>
	<? ShowError($arResult["ERROR_MESSAGE"]);?>
<?endif;?>
