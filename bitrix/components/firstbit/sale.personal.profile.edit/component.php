<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("sale"))
{
	ShowError(GetMessage("SALE_MODULE_NOT_INSTALL"));
	return;
}
if (!$USER->IsAuthorized())
{
	$APPLICATION->AuthForm(GetMessage("SALE_ACCESS_DENIED"));
}

$errorMessage = '';

$ID = 0;
if (isset($arParams['ID']))
	$ID = (int)$arParams['ID'];
if ($ID < 0)
	$ID = 0;

$arParams['PATH_TO_LIST'] = (isset($arParams['PATH_TO_LIST']) ? trim($arParams['PATH_TO_LIST']) : '');
if ($arParams['PATH_TO_LIST'] == '')
	$arParams['PATH_TO_LIST'] = htmlspecialcharsbx($APPLICATION->GetCurPage());
$arParams["PATH_TO_EDIT"] = trim($arParams["PATH_TO_EDIT"]);
if ($arParams["PATH_TO_EDIT"] == '')
	$arParams["PATH_TO_EDIT"] = htmlspecialcharsbx($APPLICATION->GetCurPage()."?ID=#ID#");

if ($ID < 0 || !empty($_POST["reset"]))
	LocalRedirect($arParams["PATH_TO_LIST"]);

if ($_SERVER["REQUEST_METHOD"]=="POST" && (!empty($_POST["save"]) || !empty($_POST["apply"])) && check_bitrix_sessid()) {
	if($_POST['ID']>0) {
		$dbUserProps = CSaleOrderUserProps::GetList(
			array("DATE_UPDATE" => "DESC"),
			array(
				"ID" => $ID,
				"USER_ID" => IntVal($USER->GetID())
			),
			false,
			false,
			array("ID", "PERSON_TYPE_ID", "DATE_UPDATE")
		);
		if(!($arUserProps = $dbUserProps->Fetch())) {
			$errorMessage .= GetMessage("SALE_NO_PROFILE") . "<br />";
		}
	}
	if(strlen($errorMessage) <= 0) {
		$NAME = Trim($_POST["NAME"]);
		if(strlen($NAME) <= 0) {
			$errorMessage .= GetMessage("SALE_NO_NAME") . "<br />";
		}
		if($_POST['ID']==0) {
			$arUserProps["PERSON_TYPE_ID"] = $_POST['PERSON_TYPE_ID'];
		}
		$dbOrderProps = CSaleOrderProps::GetList(
			array("SORT" => "ASC", "NAME" => "ASC"),
			array(
				"PERSON_TYPE_ID" => $arUserProps["PERSON_TYPE_ID"],
				"USER_PROPS" => "Y", "ACTIVE" => "Y", "UTIL" => "N"
			),
			false,
			false,
			array("ID", "PERSON_TYPE_ID", "NAME", "TYPE", "REQUIED", "DEFAULT_VALUE", "SORT", "USER_PROPS", "IS_LOCATION", "PROPS_GROUP_ID", "SIZE1", "SIZE2", "DESCRIPTION", "IS_EMAIL", "IS_PROFILE_NAME", "IS_PAYER", "IS_LOCATION4TAX", "CODE", "SORT")
		);
		while($arOrderProps = $dbOrderProps->GetNext()) {
			$bErrorField = false;
			$curVal = $_POST["ORDER_PROP_" . $arOrderProps["ID"]];
			if($arOrderProps["TYPE"] == "LOCATION" && $arOrderProps["IS_LOCATION"] == "Y") {
				$DELIVERY_LOCATION = IntVal($curVal);
				if(IntVal($curVal) <= 0) {
					$bErrorField = true;
				}
			} elseif($arOrderProps["IS_PROFILE_NAME"] == "Y" || $arOrderProps["IS_PAYER"] == "Y" || $arOrderProps["IS_EMAIL"] == "Y") {
				if($arOrderProps["IS_PROFILE_NAME"] == "Y") {
					$PROFILE_NAME = Trim($curVal);
					if(strlen($PROFILE_NAME) <= 0) {
						$bErrorField = true;
					}
				}
				if($arOrderProps["IS_PAYER"] == "Y") {
					$PAYER_NAME = Trim($curVal);
					if(strlen($PAYER_NAME) <= 0) {
						$bErrorField = true;
					}
				}
				if($arOrderProps["IS_EMAIL"] == "Y") {
					$USER_EMAIL = Trim($curVal);
					if(strlen($USER_EMAIL) <= 0 || !check_email($USER_EMAIL)) {
						$bErrorField = true;
					}
				}
			} elseif($arOrderProps["REQUIED"] == "Y") {
				if($arOrderProps["TYPE"] == "TEXT" || $arOrderProps["TYPE"] == "TEXTAREA" || $arOrderProps["TYPE"] == "RADIO" || $arOrderProps["TYPE"] == "SELECT") {
					if(strlen($curVal) <= 0) {
						$bErrorField = true;
					}
				} elseif($arOrderProps["TYPE"] == "LOCATION") {
					if(IntVal($curVal) <= 0) {
						$bErrorField = true;
					}
				} elseif($arOrderProps["TYPE"] == "MULTISELECT") {
					if(!is_array($curVal) || count($curVal) <= 0) {
						$bErrorField = true;
					}
				}
			}
			if($bErrorField) {
				$errorMessage .= GetMessage("SALE_NO_FIELD") . " \"" . $arOrderProps["NAME"] . "\".<br />";
			}
		}
	}

	if(strlen($errorMessage) <= 0) {
		if($_POST['ID']>0) {
			$arFields = array("NAME" => $NAME);
			if(!CSaleOrderUserProps::Update($ID, $arFields)) {
				$errorMessage .= GetMessage("SALE_ERROR_EDIT_PROF") . "<br />";
			}
		} else if($_POST['ID']==0) {
			$arFUser = CSaleUser::GetList(array('USER_ID' => IntVal($GLOBALS["USER"]->GetID())));
			if($arFUser<=0) {
				$arFUser = CSaleUser::_Add(array(
					'USER_ID' => IntVal($GLOBALS["USER"]->GetID()),
					'DATE_INSERT' => date('d.m.Y h:i:s'),
					'DATE_UPDATE' => date('d.m.Y h:i:s')
				));
			}
			$arProfileFields = array(
				"NAME" => $NAME,
				"USER_ID" => IntVal($GLOBALS["USER"]->GetID()),
				"PERSON_TYPE_ID" => $_POST["PERSON_TYPE_ID"],
			);
			$PROFILE_ID = CSaleOrderUserProps::Add($arProfileFields);
		}
	}

	if(strlen($errorMessage) <= 0) {
		CSaleOrderUserPropsValue::DeleteAll($ID);
		$dbOrderProps = CSaleOrderProps::GetList(
			array("SORT" => "ASC", "NAME" => "ASC"),
			array(
				"PERSON_TYPE_ID" => $arUserProps["PERSON_TYPE_ID"],
				"USER_PROPS" => "Y", "ACTIVE" => "Y", "UTIL" => "N"
			),
			false,
			false,
			array("ID", "PERSON_TYPE_ID", "NAME", "TYPE", "REQUIED", "DEFAULT_VALUE", "SORT", "USER_PROPS", "IS_LOCATION", "PROPS_GROUP_ID", "SIZE1", "SIZE2", "DESCRIPTION", "IS_EMAIL", "IS_PROFILE_NAME", "IS_PAYER", "IS_LOCATION4TAX", "CODE", "SORT")
		);
		while($arOrderProps = $dbOrderProps->GetNext()) {
			$curVal = $_POST["ORDER_PROP_" . $arOrderProps["ID"]];
			if($arOrderProps["TYPE"] == "MULTISELECT") {
				$curVal = "";
				for($i = 0, $cnt = count($_POST["ORDER_PROP_" . $arOrderProps["ID"]]); $i < $cnt; $i++) {
					if($i > 0) {
						$curVal .= ",";
					}
					$curVal .= $_POST["ORDER_PROP_" . $arOrderProps["ID"]][$i];
				}
			}
			if(isset($_POST["ORDER_PROP_" . $arOrderProps["ID"]])) {
				if($_POST['ID'] == 0 && $PROFILE_ID >0) $ID = $PROFILE_ID;
				$arFields = array(
					"USER_PROPS_ID" => $ID,
					"ORDER_PROPS_ID" => $arOrderProps["ID"],
					"NAME" => $arOrderProps["NAME"],
					"VALUE" => $curVal
				);
				CSaleOrderUserPropsValue::Add($arFields);
			}
		}
	}
	if(strlen($errorMessage) > 0) {
		$bInitVars = True;
	}
	if(strlen($_POST["save"]) > 0 && strlen($errorMessage) <= 0) {
		LocalRedirect($arParams["PATH_TO_LIST"]);
	} elseif(strlen($_POST["apply"]) > 0 && strlen($errorMessage) <= 0) {
		LocalRedirect(CComponentEngine::MakePathFromTemplate($arParams["PATH_TO_EDIT"], Array("ID" => $ID)));
	}
	$arResult["ERROR_MESSAGE"] = $errorMessage;
}

if($ID>0) {
	$arResult["ORDER_PROPS"] = Array();
	$dbUserProps = CSaleOrderUserProps::GetList(
		array("DATE_UPDATE" => "DESC"),
		array(
			"ID" => $ID,
			"USER_ID" => IntVal($GLOBALS["USER"]->GetID())
		),
		false,
		false,
		array("ID", "NAME", "USER_ID", "PERSON_TYPE_ID", "DATE_UPDATE")
	);
	if($arUserProps = $dbUserProps->GetNext()) {
		if(!$bInitVars) {
			$arResult = $arUserProps;
		} else {
			foreach($_POST as $k => $v) {
				$arResult[$k] = htmlspecialcharsbx($v);
				$arResult['~' . $k] = $v;
			}
		}
		$arResult["TITLE"] = str_replace("#ID#", $arUserProps["ID"], GetMessage("SPPD_PROFILE_NO"));
		$arResult["PERSON_TYPE"] = CSalePersonType::GetByID($arUserProps["PERSON_TYPE_ID"]);
		$arResult["PERSON_TYPE"]["NAME"] = htmlspecialcharsEx($arResult["PERSON_TYPE"]["NAME"]);
		$arrayTmp = Array();
		$propsOfTypeLocation = array();
		$dbOrderPropsGroup = CSaleOrderPropsGroup::GetList(
			array("SORT" => "ASC", "NAME" => "ASC"),
			array("PERSON_TYPE_ID" => $arUserProps["PERSON_TYPE_ID"]),
			false,
			false,
			array("ID", "PERSON_TYPE_ID", "NAME", "SORT")
		);
		while($arOrderPropsGroup = $dbOrderPropsGroup->GetNext()) {
			$arrayTmp[$arOrderPropsGroup["ID"]] = $arOrderPropsGroup;
			$dbOrderProps = CSaleOrderProps::GetList(
				array("SORT" => "ASC", "NAME" => "ASC"),
				array(
					"PERSON_TYPE_ID" => $arUserProps["PERSON_TYPE_ID"],
					"PROPS_GROUP_ID" => $arOrderPropsGroup["ID"],
					"USER_PROPS" => "Y", "ACTIVE" => "Y", "UTIL" => "N"
				),
				false,
				false,
				array("ID", "PERSON_TYPE_ID", "NAME", "TYPE", "REQUIED", "DEFAULT_VALUE", "SORT", "USER_PROPS", "IS_LOCATION", "PROPS_GROUP_ID", "SIZE1", "SIZE2", "DESCRIPTION", "IS_EMAIL", "IS_PROFILE_NAME", "IS_PAYER", "IS_LOCATION4TAX", "CODE", "SORT")
			);
			while($arOrderProps = $dbOrderProps->GetNext()) {
				if($arOrderProps["REQUIED"] == "Y" || $arOrderProps["IS_EMAIL"] == "Y" || $arOrderProps["IS_PROFILE_NAME"] == "Y" || $arOrderProps["IS_LOCATION"] == "Y" || $arOrderProps["IS_PAYER"] == "Y") {
					$arOrderProps["REQUIED"] = "Y";
				}
				if(in_array($arOrderProps["TYPE"], Array("SELECT", "MULTISELECT", "RADIO"))) {
					$dbVars = CSaleOrderPropsVariant::GetList(($by = "SORT"), ($order = "ASC"), Array("ORDER_PROPS_ID" => $arOrderProps["ID"]));
					while($vars = $dbVars->GetNext())
						$arOrderProps["VALUES"][] = $vars;
				} elseif($arOrderProps["TYPE"] == "LOCATION") {
					$propsOfTypeLocation[$arOrderProps['ID']] = true;

					$dbVars = CSaleLocation::GetList(Array("SORT" => "ASC", "COUNTRY_NAME_LANG" => "ASC", "CITY_NAME_LANG" => "ASC"), array(), LANGUAGE_ID);
					while($vars = $dbVars->GetNext())
						$arOrderProps["VALUES"][] = $vars;
				}
				$arrayTmp[$arOrderPropsGroup["ID"]]["PROPS"][] = $arOrderProps;
			}
		}
		$arResult["ORDER_PROPS"] = $arrayTmp;

		$arPropValsTmp = Array();
		if(!$bInitVars) {
			$dbPropVals = CSaleOrderUserPropsValue::GetList(
				array("SORT" => "ASC"),
				array("USER_PROPS_ID" => $arUserProps["ID"]),
				false,
				false,
				array("ID", "ORDER_PROPS_ID", "VALUE", "SORT")
			);
			while($arPropVals = $dbPropVals->GetNext()) {
				$arPropValsTmp["ORDER_PROP_" . $arPropVals["ORDER_PROPS_ID"]] = $arPropVals["VALUE"];
			}
		} else {
			foreach($_REQUEST as $key => $value) {
				if(substr($key, 0, strlen("ORDER_PROP_")) == "ORDER_PROP_") {
					$arPropValsTmp[$key] = htmlspecialcharsbx($value);
				}
			}
		}
		$arResult["ORDER_PROPS_VALUES"] = $arPropValsTmp;
	} else {
		$arResult["ERROR_MESSAGE"] = GetMessage("SALE_NO_PROFILE");
	}
}
else if($ID==0){
	$arResult['ID'] = 0;
	$arResult['USER_ID'] = IntVal($GLOBALS["USER"]->GetID());

	$arrayTmp = Array();
	$arrayPersonTypesTmp = Array();

	$dbPersonTypes = CSalePersonType::GetList(
		array("SORT" => "ASC", "NAME" => "ASC"),
		array("ACTIVE" => "Y", "LID" => SITE_ID),
		false,
		false,
		array("ID", "PERSON_TYPE_ID", "NAME", "SORT")
	);
	while($arPersonTypes = $dbPersonTypes->GetNext()) {
		$arrayPersonTypesTmp[$arPersonTypes["ID"]] = $arPersonTypes;
	}

	$dbOrderPropsGroup = CSaleOrderPropsGroup::GetList(
		array("SORT" => "ASC", "NAME" => "ASC"),
		array("ACTIVE" => "Y"),
		false,
		false,
		array("ID", "PERSON_TYPE_ID", "NAME", "SORT")
	);
	while($arOrderPropsGroup = $dbOrderPropsGroup->GetNext()) {
		$arrayTmp[$arOrderPropsGroup["ID"]] = $arOrderPropsGroup;
		$dbOrderProps = CSaleOrderProps::GetList(
			array("SORT" => "ASC", "NAME" => "ASC"),
			array(
				"PROPS_GROUP_ID" => $arOrderPropsGroup["ID"],
				"USER_PROPS" => "Y", "ACTIVE" => "Y", "UTIL" => "N"
			),
			false,
			false,
			array("ID", "PERSON_TYPE_ID", "NAME", "TYPE", "REQUIED", "DEFAULT_VALUE", "SORT", "USER_PROPS", "IS_LOCATION", "PROPS_GROUP_ID", "SIZE1", "SIZE2", "DESCRIPTION", "IS_EMAIL", "IS_PROFILE_NAME", "IS_PAYER", "IS_LOCATION4TAX", "CODE", "SORT")
		);
		while($arOrderProps = $dbOrderProps->GetNext()) {
			if($arOrderProps["REQUIED"] == "Y" || $arOrderProps["IS_EMAIL"] == "Y" || $arOrderProps["IS_PROFILE_NAME"] == "Y" || $arOrderProps["IS_LOCATION"] == "Y" || $arOrderProps["IS_PAYER"] == "Y") {
				$arOrderProps["REQUIED"] = "Y";
			}
			if(in_array($arOrderProps["TYPE"], Array("SELECT", "MULTISELECT", "RADIO"))) {
				$dbVars = CSaleOrderPropsVariant::GetList(($by = "SORT"), ($order = "ASC"), Array("ORDER_PROPS_ID" => $arOrderProps["ID"]));
				while($vars = $dbVars->GetNext())
					$arOrderProps["VALUES"][] = $vars;
			} elseif($arOrderProps["TYPE"] == "LOCATION") {
				$propsOfTypeLocation[$arOrderProps['ID']] = true;

				$dbVars = CSaleLocation::GetList(Array("SORT" => "ASC", "COUNTRY_NAME_LANG" => "ASC", "CITY_NAME_LANG" => "ASC"), array(), LANGUAGE_ID);
				while($vars = $dbVars->GetNext())
					$arOrderProps["VALUES"][] = $vars;
			}
			$arrayTmp[$arOrderPropsGroup["ID"]]["PROPS"][] = $arOrderProps;
		}
	}
	$arResult["PERSON_TYPES"] = $arrayPersonTypesTmp;
	$arResult["ORDER_PROPS"] = $arrayTmp;

}
$this->IncludeComponentTemplate();
?>