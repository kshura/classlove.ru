<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
$firstBit = new CFirstbitBeautyshop(SITE_ID);

$arMainPageOrder = array();
foreach ($arParams['MAINPAGE_BLOCKS'] as $arBlock) {
	if($firstBit->options['MAIN_PAGE_ORDER_' . $arBlock . '_HIDE'] != 'Y') {
		$arMainPageOrder[$arBlock] = $firstBit->options['MAIN_PAGE_ORDER_' . $arBlock];
	}
}

natsort($arMainPageOrder);

$arMainPageOrderEmpty = array();
foreach ($arMainPageOrder as $arBlock => $arOrder) {
	if (!$arOrder) {
		$arMainPageOrderEmpty[$arBlock] = '';
		unset($arMainPageOrder[$arBlock]);
	}
}
$arResult['BLOCKS'] = $arMainPageOrder + $arMainPageOrderEmpty;

if(array_key_exists('SLIDER',$arResult['BLOCKS'])) {
	$arResult['SLIDER_TEMPLATE'] = $firstBit->options['SLIDER_VIEW'];
	switch($arResult['SLIDER_TEMPLATE']) {
		case 1:
		case 2:
			$arResult['SLIDER_PROPERTY_CODE'] = array("URL", "SLIDER_PICTURE_BIG", "SLIDER_TITLE", "SLIDER_PREVIEW_TEXT", "SLIDER_BUTTON_MESSAGE");
			$arResult['SLIDER_COUNT'] = 20;
			break;
		case 3:
			$arResult['SLIDER_PROPERTY_CODE'] = array("BANNER_PICTURE_MEDIUM", "BANNER_TITLE", "BANNER_PREVIEW_TEXT", "BANNER_BUTTON_MESSAGE");
			$arResult['SLIDER_COUNT'] = 2;
			break;
		case 4:
			$arResult['SLIDER_PROPERTY_CODE'] = array("URL", "SLIDER_PICTURE_SMALL", "SLIDER_TITLE", "SLIDER_PREVIEW_TEXT", "SLIDER_BUTTON_MESSAGE", "BANNER_PICTURE_SMALL", "BANNER_TITLE", "BANNER_PREVIEW_TEXT", "BANNER_BUTTON_MESSAGE");
			$arResult['SLIDER_COUNT'] = 20;
			break;
	}
}
if(array_key_exists('BANNERS',$arResult['BLOCKS'])) {
	$arResult['BANNERS_TEMPLATE'] = $firstBit->options['BANNERS_VIEW'];
	$arResult['BANNERS_TITLE_SHOW'] = $firstBit->options['BANNERS_TITLE_SHOW'];
	switch ($arResult['BANNERS_TEMPLATE']) {
		case 1:
		case 4:
			$arResult['BANNERS_COUNT'] = 4;
			break;
		case 2:
		case 5:
			$arResult['BANNERS_COUNT'] = 3;
			break;
		case 3:
		case 6:
			$arResult['BANNERS_COUNT'] = 5;
			break;
		default:
			$arResult['BANNERS_COUNT'] = 10;
	}
}

$this->IncludeComponentTemplate();
