<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.store.list",
	"main_page_store",
	Array(
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"PHONE" => "Y",
		"SCHEDULE" => "Y",
		"SET_TITLE" => "N",
		"PATH_TO_LIST" => $arParams['PATH_TO_STORES'],
		"PATH_TO_ELEMENT" => $arParams['PATH_TO_STORES']."#store_id#/",
		"MAP_TYPE" => "0"
	),
	$component
);?>