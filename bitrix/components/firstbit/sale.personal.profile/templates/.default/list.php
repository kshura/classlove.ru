<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$frame = $this->createFrame()->begin();
$APPLICATION->IncludeComponent(
	"firstbit:sale.personal.profile.list",
	"",
	array(
		"PATH_TO_EDIT" => $arResult["PATH_TO_EDIT"],
		"PER_PAGE" => $arParams["PER_PAGE"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"LIST_PROPERTIES" => $arParams["LIST_PROPERTIES"]
	),
	$component
);
?>
