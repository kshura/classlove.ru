<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$registrationProperties = array();
$rsPersonType = CSalePersonType::GetList(array("SORT" => "ASC"), array("ACTIVE" => "Y"), false, false, array("ID", "NAME", "LID"));
if($rsPersonType->SelectedRowsCount()>0) {
	while ($arPersonType = $rsPersonType->Fetch()) {
		$rsPropertiesGroup = CSaleOrderPropsGroup::GetList(array("SORT" => "ASC"), array("PERSON_TYPE_ID" => $arPersonType["ID"], "ACTIVE" => "Y"), false, false, array("ID", "NAME"));
		while ($arPropertiesGroup = $rsPropertiesGroup->Fetch()) {
			$rsProperty = CSaleOrderProps::GetList(array("SORT" => "ASC"), array("PROPS_GROUP_ID" => $arPropertiesGroup["ID"], "ACTIVE" => "Y", "USER_PROPS" => "Y"), false, false, array());
			while ($arProperty = $rsProperty->Fetch()) {
				$arPropertyName = $arPersonType['NAME'].' ('.$arPersonType['LID'].') / '.$arPropertiesGroup['NAME'].' / '.$arProperty['NAME'];
				if($arProperty['REQUIED'] == 'Y') $arPropertyName .= ' *';
				$registrationProperties['PROPERTY_'.$arProperty['ID']] = $arPropertyName;
			}
		}
	}
}
$arComponentParameters = array(
	"PARAMETERS" => array(
		"SEF_MODE" => Array(
			"list" => Array(
				"NAME" => GetMessage("SPP_LIST_DESC"),
				"DEFAULT" => "profile_list.php",
				"VARIABLES" => array()
			),
			"edit" => Array(
				"NAME" => GetMessage("SPP_EDIT_DESC"),
				"DEFAULT" => "profile_edit.php?ID=#ID#",
				"VARIABLES" => array("ID")
			),
		),

		'LIST_PROPERTIES' => array(
			'PARENT' => 'ADDITIONAL_SETTINGS',
			'NAME' => GetMessage('SPP_LIST_PROPERTIES'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'ADDITIONAL_VALUES' => 'N',
			'REFRESH' => 'N',
			'DEFAULT' => '-',
			'SIZE' => 7,
			'VALUES' => $registrationProperties
		),

		"PER_PAGE" => Array(
			"NAME" => GetMessage("SPP_PER_PAGE"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "20",
			"PARENT" => "ADDITIONAL_SETTINGS",
		),

		'USE_AJAX_LOCATIONS' => array(
			'NAME' => GetMessage("SPP_USE_AJAX_LOCATIONS"),
			'TYPE' => 'CHECKBOX',
			'MULTIPLE' => 'N',
			'DEFAULT' => 'N',
			"PARENT" => "ADDITIONAL_SETTINGS",
		),

		"SET_TITLE" => Array(),

	)
);
?>
