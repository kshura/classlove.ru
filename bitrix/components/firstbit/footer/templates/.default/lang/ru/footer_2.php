<?php
$MESS['BIT_CALLBACK_LINK'] = 'Обратный звонок';
$MESS['BIT_FEEDBACK_LINK'] = 'Обратная связь';
$MESS['FOOTER_CONTACTS'] = 'Контактные данные:';
$MESS['FOOTER_PHONE'] = 'Телефон:';
$MESS['FOOTER_EMAIL'] = 'E-mail:';
$MESS['FOOTER_CATALOG_LINK'] = 'Каталог продукции';
