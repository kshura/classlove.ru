<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$frame = $this->createFrame()->begin();
?>
<form action="<?=$arResult["FORM_ACTION"]?>" method="post" id="footer_subscribe_form">
	<span class="footer_block_title show_lg"><?=GetMessage('FOOTER_SUBSCRIBE_TITLE')?></span>
	<?echo bitrix_sessid_post();?>
	<?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
		<input type="checkbox" name="RUB_ID[]" value="<?=$itemValue["ID"]?>"<?=($itemValue['CHECKED'] == '1' || $arParams['RUBRIC_ID'] == $itemValue['ID'] ? ' checked="checked"' : '') ?> />
	<?endforeach;?>
	<input type="radio" name="FORMAT" value="text" checked />
	<input type="hidden" value="<?=$arParams['AJAX_OPTION_ADDITIONAL'];?>" name="ajax_opt_add">
	<input type="hidden" name="PostAction" value="<?echo ($arResult["ID"]>0? "Update":"Add")?>" />
	<input type="hidden" name="ID" value="<?echo $arResult["SUBSCRIPTION"]["ID"];?>" />
	<input type="text" data-mask="email" name="EMAIL" placeholder="E-mail" value="<?=$arResult["SUBSCRIPTION"]["EMAIL"]!=""?$arResult["SUBSCRIPTION"]["EMAIL"]:$arResult["REQUEST"]["EMAIL"];?>" title="<?=GetMessage("FOOTER_EMAIL_TITLE")?>" required />
		<div id="status_SubscribeValidation" class="form_message">
			<?
			if (count($arResult["MESSAGE"]) > 0) {
				foreach($arResult["MESSAGE"] as $itemID=>$itemValue) {
					?>
					<p><span class="notetext"><? echo $itemValue; ?></span></p>
					<?
				}
			}
			?>
			<?
			if (count($arResult["ERROR"]) > 0) {
				foreach($arResult["ERROR"] as $itemID=>$itemValue) {
					?>
					<p><span class="errortext"><? echo $itemValue; ?></span></p>
					<?
				}
			}
			?>
		</div>
	<span class="btn_subscribe">
		<input type="submit" name="Save" value="" />
		<i class="fa fa-arrow-circle-o-right"></i>
	</span>
</form>
