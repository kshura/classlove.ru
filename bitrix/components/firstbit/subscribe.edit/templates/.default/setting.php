<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="<?=$arResult["FORM_ACTION"]?>" method="post" class="form form_general">
	<?echo bitrix_sessid_post();?>
	<div class="form_fields_section">
		<h2><?echo GetMessage("subscr_title_settings")?></h2>
	</div>
	<ul class="form_fields">
		<li>
			<label><?echo GetMessage("subscr_email")?></label>
			<input type="text" data-mask="email" name="EMAIL" value="<?=$arResult["SUBSCRIPTION"]["EMAIL"]!=""?$arResult["SUBSCRIPTION"]["EMAIL"]:$arResult["REQUEST"]["EMAIL"];?>" required="required" />
			<i class="fa fa-check-circle not-active"></i>
		</li>
		<li>
			<label><?echo GetMessage("subscr_rub")?></label>
			<div>
				<?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
					<label><input type="checkbox" name="RUB_ID[]" value="<?=$itemValue["ID"]?>"<?if($itemValue["CHECKED"]) echo " checked"?> /><?=$itemValue["NAME"]?></label><br />
				<?endforeach;?>
			</div>
			<i class="fa fa-check-circle not-active"></i>
		</li>
		<li>
			<label><?echo GetMessage("subscr_fmt")?></label>
			<div>
				<label><input type="radio" name="FORMAT" value="text"<?if($arResult["SUBSCRIPTION"]["FORMAT"] == "text") echo " checked"?> /><?echo GetMessage("subscr_text")?></label>&nbsp;/&nbsp;<label><input type="radio" name="FORMAT" value="html"<?if($arResult["SUBSCRIPTION"]["FORMAT"] == "html") echo " checked"?> />HTML</label>
			</div>
		</li>
		<li class="form_text">
			<?echo GetMessage("subscr_settings_note1")?><br/>
			<?echo GetMessage("subscr_settings_note2")?>
		</li>
	</ul>
	<div class="form_btn_wrapper btn_wrapper">
		<input type="reset" class="btn_round btn_disabled btn_hover_color" value="<?echo GetMessage("subscr_reset")?>" name="reset" />
		<input type="submit" class="btn_round btn_color" name="Save" value="<?echo ($arResult["ID"] > 0? GetMessage("subscr_upd"):GetMessage("subscr_add"))?>" />
	</div>
	<input type="hidden" value="<?=$arParams['AJAX_OPTION_ADDITIONAL'];?>" name="ajax_opt_add">
	<input type="hidden" name="PostAction" value="<?echo ($arResult["ID"]>0? "Update":"Add")?>" />
	<input type="hidden" name="ID" value="<?echo $arResult["SUBSCRIPTION"]["ID"];?>" />
	<?if($_REQUEST["register"] == "YES"):?>
		<input type="hidden" name="register" value="YES" />
	<?endif;?>
	<?if($_REQUEST["authorize"]=="YES"):?>
		<input type="hidden" name="authorize" value="YES" />
	<?endif;?>
</form>
<br />
