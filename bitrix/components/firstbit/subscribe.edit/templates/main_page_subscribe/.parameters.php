<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule("subscribe");
$arRubricList = array();
$rsRubrics = CRubric::GetList(array("SORT"=>"ASC", "NAME"=>"ASC"), array("ACTIVE"=>"Y"));
while($arRubric = $rsRubrics->GetNext()) {
	$arRubricList[$arRubric['ID']] = '['.$arRubric['ID'].'] '.$arRubric['NAME'];
}
$arTemplateParameters = array(
	"RUBRIC_ID" => array(
		'NAME' => GetMessage('CP_BSS_RUBRIC'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'N',
		'DEFAULT' => '-',
		'VALUES' => $arRubricList
	)
);
?>
