<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$frame = $this->createFrame()->begin();
?>
	<div class="subscribe_wrapper grey">
		<div class="subscribe_inner">
			<h2><?=GetMessage('SUBSCRIBE_TITLE')?></h2>
			<p><?=GetMessage('SUBSCRIBE_TEXT')?></p>
			<form action="<?=$arResult["FORM_ACTION"]?>" method="post" id="subscribe_form">
				<?echo bitrix_sessid_post();?>
				<?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
					<input type="checkbox" name="RUB_ID[]" value="<?=$itemValue["ID"]?>"<?=($itemValue['CHECKED'] == '1' || $arParams['RUBRIC_ID'] == $itemValue['ID'] ? ' checked="checked"' : '') ?> />
				<?endforeach;?>
				<input type="radio" name="FORMAT" value="text" checked />
				<input type="hidden" value="<?=$arParams['AJAX_OPTION_ADDITIONAL'];?>" name="ajax_opt_add">
				<input type="hidden" name="PostAction" value="<?echo ($arResult["ID"]>0? "Update":"Add")?>" />
				<input type="hidden" name="ID" value="<?echo $arResult["SUBSCRIPTION"]["ID"];?>" />
				<input type="text" data-mask="email" name="EMAIL" placeholder="E-mail" value="<?=$arResult["SUBSCRIPTION"]["EMAIL"]!=""?$arResult["SUBSCRIPTION"]["EMAIL"]:$arResult["REQUEST"]["EMAIL"];?>" title="<?=GetMessage("EMAIL_TITLE")?>" required="required" />
				<div id="status_SubscribeValidation" class="form_message">
					<?
					if (count($arResult["MESSAGE"]) > 0) {
						foreach($arResult["MESSAGE"] as $itemID=>$itemValue) {
							?>
							<p><span class="notetext"><? echo $itemValue; ?></span></p>
							<?
						}
					}
					?>
					<?
					if (count($arResult["ERROR"]) > 0) {
						foreach($arResult["ERROR"] as $itemID=>$itemValue) {
							?>
							<p><span class="errortext"><? echo $itemValue; ?></span></p>
							<?
						}
					}
					?>
				</div>
				<input type="submit" name="Save" class="btn_round btn_square btn_color" value="<?echo GetMessage("SUBSCRIBE_BUTTON");?>" />
			</form>
		</div>
		<div class="envelope hidden_xs">
			<img src="<?= SITE_TEMPLATE_PATH ?>/img/mail.png" class="no_ie8"/>
		</div>
	</div>
