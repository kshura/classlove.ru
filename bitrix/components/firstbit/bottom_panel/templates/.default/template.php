<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
?>
<div class="bottom_panel_wrapper bottom_panel1 show_lg" id="bottom_panel">
	<div class="bottom_panel_header">
		<div class="row dark">
			<div class="container_16">
				<div class="grid_16">
					<ul class="tab_button_list">
						<li data-tab-type="viewed" >
							<a href="#tabs-viewed" class="tab_btn_inner">
								<i class="fa fa-eye"></i>
								<span class="title"><? echo GetMessage('BOTTOM_PANEL_VIEWED'); ?></span>
								<i class="fa fa-chevron-up"></i>
							</a>
						</li>
						<li data-tab-type="wishlist" >
							<a href="#tabs-wishlist" class="tab_btn_inner">
								<i class="fa fa-heart"></i>
								<span class="title"><? echo GetMessage('BOTTOM_PANEL_WISHLIST'); ?></span>
								<span class="count" id="bottom_panel_wishlist" data-wishlist-count="">0</span>
								<?$frame = $this->createFrame("bottom_panel_wishlist", false)->begin();?>
								<script type="text/javascript">
									wishlistSetCount();
								</script>
								<? $frame->end(); ?>
								<i class="fa fa-chevron-up"></i>
							</a>
						</li>
						<li data-tab-type="compare" >
							<a href="#tabs-compare" class="tab_btn_inner">
								<i class="fa fa-bar-chart"></i>
								<span class="title"><? echo GetMessage('BOTTOM_PANEL_COMPARE'); ?></span>
								<span class="count" data-compare-count="" id="bottom_panel_compare">
									<?$frame = $this->createFrame("bottom_panel_compare", false)->begin();?>
									<?= $APPLICATION->IncludeComponent('firstbit:compare', 'count', array('CATALOG_IBLOCK_ID' => $arParams['CATALOG_IBLOCK_ID']), $component, array('HIDE_ICONS' => 'Y')); ?></span>
								<? $frame->end(); ?>
								<i class="fa fa-chevron-up"></i>
							</a>
						</li>
						<li data-tab-type="basket" >
							<a href="#tabs-basket" class="tab_btn_inner">
								<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line","bottom_panel_button",Array(
									"HIDE_ON_BASKET_PAGES" => "Y",
									"SHOW_EMPTY_VALUES" => "Y",
									"SHOW_NUM_PRODUCTS" => "Y",
									"SHOW_PRICE" => "Y",
									"SHOW_PRODUCTS" => "Y",
									"SHOW_SUMMARY" => "Y",
									"SHOW_TOTAL_PRICE" => "Y"
								));?>
							</a>
						</li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="bottom_panel_body">
		<div class="container_16">
			<div class="grid_16">
				<div class="preloader" id="tab_preloader"></div>
				<div class="tab_body_wrapper" id="tabs-viewed"></div>
				<div class="tab_body_wrapper" id="tabs-wishlist"></div>
				<div class="tab_body_wrapper" id="tabs-compare"></div>
				<div class="tab_body_wrapper" id="tabs-basket"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>