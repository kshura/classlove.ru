<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Type\Collection;

if(!empty($arResult['ITEMS']) && is_array($arResult['ITEMS'])) {
	if(!CModule::IncludeModule("firstbit.beautyshop")) die();

	$arResult['ALL_FIELDS'] = array();
	$existShow = !empty($arResult['SHOW_FIELDS']);
	$existDelete = !empty($arResult['DELETED_FIELDS']);
	if ($existShow || $existDelete) {
		if ($existShow) {
			foreach ($arResult['SHOW_FIELDS'] as $propCode) {
				$arResult['SHOW_FIELDS'][$propCode] = array('CODE' => $propCode, 'IS_DELETED' => 'N', 'ACTION_LINK' => str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_FIELD_TEMPLATE']), 'SORT' => $arResult['FIELDS_SORT'][$propCode]);
			}
			unset($propCode);
			$arResult['ALL_FIELDS'] = $arResult['SHOW_FIELDS'];
		}
		if ($existDelete) {
			foreach ($arResult['DELETED_FIELDS'] as $propCode) {
				$arResult['ALL_FIELDS'][$propCode] = array('CODE' => $propCode, 'IS_DELETED' => 'Y', 'ACTION_LINK' => str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_FIELD_TEMPLATE']), 'SORT' => $arResult['FIELDS_SORT'][$propCode]);
			}
			unset($propCode, $arResult['DELETED_FIELDS']);
		}
		Collection::sortByColumn($arResult['ALL_FIELDS'], array('SORT' => SORT_ASC));
	}

	$arResult['ALL_PROPERTIES'] = array();
	$existShow = !empty($arResult['SHOW_PROPERTIES']);
	$existDelete = !empty($arResult['DELETED_PROPERTIES']);
	if ($existShow || $existDelete) {
		if ($existShow) {
			foreach ($arResult['SHOW_PROPERTIES'] as $propCode => $arProp) {
				$arResult['SHOW_PROPERTIES'][$propCode]['IS_DELETED'] = 'N';
				$arResult['SHOW_PROPERTIES'][$propCode]['ACTION_LINK'] = str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_PROPERTY_TEMPLATE']);
			}
			$arResult['ALL_PROPERTIES'] = $arResult['SHOW_PROPERTIES'];
		}
		unset($arProp, $propCode);
		if ($existDelete) {
			foreach ($arResult['DELETED_PROPERTIES'] as $propCode => $arProp) {
				$arResult['DELETED_PROPERTIES'][$propCode]['IS_DELETED'] = 'Y';
				$arResult['DELETED_PROPERTIES'][$propCode]['ACTION_LINK'] = str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_PROPERTY_TEMPLATE']);
				$arResult['ALL_PROPERTIES'][$propCode] = $arResult['DELETED_PROPERTIES'][$propCode];
			}
			unset($arProp, $propCode, $arResult['DELETED_PROPERTIES']);
		}
		Collection::sortByColumn($arResult["ALL_PROPERTIES"], array('SORT' => SORT_ASC, 'ID' => SORT_ASC));
	}

	$arResult["ALL_OFFER_FIELDS"] = array();
	$existShow = !empty($arResult["SHOW_OFFER_FIELDS"]);
	$existDelete = !empty($arResult["DELETED_OFFER_FIELDS"]);
	if ($existShow || $existDelete) {
		if ($existShow) {
			foreach ($arResult["SHOW_OFFER_FIELDS"] as $propCode) {
				$arResult["SHOW_OFFER_FIELDS"][$propCode] = array("CODE" => $propCode, "IS_DELETED" => "N", "ACTION_LINK" => str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_OF_FIELD_TEMPLATE']), 'SORT' => $arResult['FIELDS_SORT'][$propCode]);
			}
			unset($propCode);
			$arResult['ALL_OFFER_FIELDS'] = $arResult['SHOW_OFFER_FIELDS'];
		}
		if ($existDelete) {
			foreach ($arResult['DELETED_OFFER_FIELDS'] as $propCode) {
				$arResult['ALL_OFFER_FIELDS'][$propCode] = array("CODE" => $propCode, "IS_DELETED" => "Y", "ACTION_LINK" => str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_OF_FIELD_TEMPLATE']), 'SORT' => $arResult['FIELDS_SORT'][$propCode]);
			}
			unset($propCode, $arResult['DELETED_OFFER_FIELDS']);
		}
		Collection::sortByColumn($arResult['ALL_OFFER_FIELDS'], array('SORT' => SORT_ASC));
	}

	$arResult['ALL_OFFER_PROPERTIES'] = array();
	$existShow = !empty($arResult["SHOW_OFFER_PROPERTIES"]);
	$existDelete = !empty($arResult["DELETED_OFFER_PROPERTIES"]);
	if ($existShow || $existDelete) {
		if ($existShow) {
			foreach ($arResult['SHOW_OFFER_PROPERTIES'] as $propCode => $arProp) {
				$arResult["SHOW_OFFER_PROPERTIES"][$propCode]["IS_DELETED"] = "N";
				$arResult["SHOW_OFFER_PROPERTIES"][$propCode]["ACTION_LINK"] = str_replace('#CODE#', $propCode, $arResult['~DELETE_FEATURE_OF_PROPERTY_TEMPLATE']);
			}
			unset($arProp, $propCode);
			$arResult['ALL_OFFER_PROPERTIES'] = $arResult['SHOW_OFFER_PROPERTIES'];
		}
		if ($existDelete) {
			foreach ($arResult['DELETED_OFFER_PROPERTIES'] as $propCode => $arProp) {
				$arResult["DELETED_OFFER_PROPERTIES"][$propCode]["IS_DELETED"] = "Y";
				$arResult["DELETED_OFFER_PROPERTIES"][$propCode]["ACTION_LINK"] = str_replace('#CODE#', $propCode, $arResult['~ADD_FEATURE_OF_PROPERTY_TEMPLATE']);
				$arResult['ALL_OFFER_PROPERTIES'][$propCode] = $arResult["DELETED_OFFER_PROPERTIES"][$propCode];
			}
			unset($arProp, $propCode, $arResult['DELETED_OFFER_PROPERTIES']);
		}
		Collection::sortByColumn($arResult['ALL_OFFER_PROPERTIES'], array('SORT' => SORT_ASC, 'ID' => SORT_ASC));
	}

	$rowsGroups = array(0 => array('params' => 'SHOW_FIELDS', 'group' => 'FIELDS'), 1 => array('params' => 'SHOW_OFFER_FIELDS', 'group' => 'OFFER_FIELDS'), 2 => array('params' => 'SHOW_PROPERTIES', 'group' => 'DISPLAY_PROPERTIES'), 3 => array('params' => 'SHOW_OFFER_PROPERTIES', 'group' => 'OFFER_DISPLAY_PROPERTIES'));

	foreach ($arResult['ITEMS'] as $arItemIndex => $arItem) {
		if ($arItem['OFFER_FIELDS']['PREVIEW_PICTURE']) {
			$arImage = CFile::GetFileArray($arItem['OFFER_FIELDS']['PREVIEW_PICTURE']);
			$arResult['ITEMS'][$arItemIndex]['PREVIEW_PICTURE'] = $arImage;
			continue;
		}
		elseif ($arItem['OFFER_FIELDS']['DETAIL_PICTURE']) {
			$arImage = CFile::GetFileArray($arItem['OFFER_FIELDS']['DETAIL_PICTURE']);
			$arResult['ITEMS'][$arItemIndex]['PREVIEW_PICTURE'] = $arImage;
			continue;
		}
		elseif (is_array($arItem['OFFER_PROPERTIES']['MORE_PHOTO']['VALUE']) && count($arItem['OFFER_PROPERTIES']['MORE_PHOTO']['VALUE'] > 0)) {
			$arImage = CFile::GetFileArray($arItem['OFFER_PROPERTIES']['MORE_PHOTO']['VALUE'][0]);
			$arResult['ITEMS'][$arItemIndex]['PREVIEW_PICTURE'] = $arImage;
			continue;
		}
		elseif ($arItem['PREVIEW_PICTURE']) {
			continue;
		}
		elseif ($arItem['DETAIL_PICTURE']) {
			$arResult['ITEMS'][$arItemIndex]['PREVIEW_PICTURE'] = $arItem['DETAIL_PICTURE'];
			continue;
		}
		else {
			$arImage = CFile::GetFileArray(CFirstbitBeautyshop::queryOption('NO_PHOTO', SITE_ID));
			$arResult['ITEMS'][$arItemIndex]['PREVIEW_PICTURE'] = $arImage;
		}
		unset($arImage);
	}

	$arRows = array();
	foreach ($rowsGroups as $rowsGroup) {
		foreach ($arResult[$rowsGroup['params']] as $code => $arProperty) {
			if ($arResult['DIFFERENT']) {
				$arCompare = array();
				foreach ($arResult["ITEMS"] as &$arElement) {
					if ($rowsGroup['group'] == 'FIELDS' || $rowsGroup['group'] == 'OFFER_FIELDS') {
						$arPropertyValue = $arElement[$rowsGroup['group']][$code];
					}
					else {
						$arPropertyValue = $arElement[$rowsGroup['group']][$code]["VALUE"];
					}
					if (is_array($arPropertyValue)) {
						sort($arPropertyValue);
						$arPropertyValue = implode(" / ", $arPropertyValue);
					}
					$arCompare[] = $arPropertyValue;
				}
				unset($arElement);
				$showRow = (count(array_unique($arCompare)) > 1);
				if ($showRow) {
					$arRows[$rowsGroup['group']][$code] = ($arProperty['NAME'] ? $arProperty['NAME'] : GetMessage("IBLOCK_FIELD_" . $code));
				}
				unset($showRow);
			}
			else {
				$arRows[$rowsGroup['group']][$code] = ($arProperty['NAME'] ? $arProperty['NAME'] : GetMessage("IBLOCK_FIELD_" . $code));
			}
		}
	}
	$arResult['ROWS'] = $arRows;
}
?>