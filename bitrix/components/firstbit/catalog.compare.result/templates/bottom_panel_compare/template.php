<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

$isAjax = ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["ajax_action"]) && $_POST["ajax_action"] == "Y");
?>
<script><? require_once('script.js'); ?></script>
<?
if ($isAjax) {
	$APPLICATION->RestartBuffer();
}

if (!empty($arResult['ITEMS']) && is_array($arResult['ITEMS'])) {
?>
	<div class="bottom_panel_first_line">
		<div class="grid_16 relative owl_nav dark" id="owl_compare_bottom_nav"></div>
		<div class="carousel_wrap">
			<ul id="panel_carousel_compare" data-compare="" class="slider_initial">
				<?
				foreach ($arResult['ITEMS'] as $key => $arItem) {
					$strMainID = $this->GetEditAreaId($arItem['ID']);
					$productTitle = ($arItem['OFFER_FIELDS']['NAME'] ? $arItem['OFFER_FIELDS']['NAME'] : $arItem['NAME']);
					$minPrice = false;
					if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE'])) {
						$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
					}
					$canBuy = $arItem['CAN_BUY'];
					?>
					<li class="panel_carousel product_item" id="<? echo $strMainID; ?>" data-compare-item="<?=$arItem['ID']?>">
						<div class="close_btn">
							<a href="#" data-compare-id="<? echo $arItem['ID']; ?>" title="<? echo GetMessage('CT_BCS_TPL_MESS_BTN_DELETE'); ?>" onclick="compareHandler(this);compareRemoveItem(this);return false;"><i class="fa fa-times delete_panel_item"></i></a>
							<script type="text/javascript">
								compareCheck(<?=$arItem['ID']?>);
							</script>
						</div>
						<div class="product_image">
							<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="bx_catalog_item_images">
								<img id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" title="<? echo $productTitle; ?>" alt="<? echo $productTitle; ?>"/>
							</a>
						</div>
						<div class="product_description">
							<div class="product_title">
								<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $productTitle; ?>">
									<h3><?= ($arItem['OFFER_FIELDS']['NAME'] ? $arItem['OFFER_FIELDS']['NAME'] : $arItem['NAME']) ?></h3>
								</a>
							</div>
								<div class="price_wrap">
									<span id="<? echo $arItemIDs['PRICE']; ?>" class="current_price">
										<?
										if (!empty($minPrice) && $minPrice['DISCOUNT_VALUE']>0) {
											if (!empty($arItem['OFFERS'])) {
												echo GetMessage('CMP_TPL_PRICE_SIMPLE_MODE', array(
													'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE']
												));
											} else {
												echo $minPrice['PRINT_DISCOUNT_VALUE'];
											}
											if (empty($arItem['OFFERS'])) {
												if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) {
													?><span class="old_price"><? echo $minPrice['PRINT_VALUE']; ?></span><?
												}
											}
										} else {
											$notAvailableMessage = ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessage('CMP_TPL_PRODUCT_NULL_PRICE'));
											echo $notAvailableMessage;
										}
										?>
									</span>
								</div>
						</div>
					</li>
					<?
					unset($minPrice);
				}
				?>
			</ul>
		</div>
		<div style="clear: both;"></div>
		<div class="empty_message">
			<div><? echo GetMessage('CMP_MSG_YOU_HAVE_NOT_YET');?></div>
		</div>
	</div>
	<div class="bottom_panel_second_line">
		<a href="<?=$arParams['PATH_TO_COMPARE']?>" class="btn_round btn_color"><? echo GetMessage('CMP_TPL_GOTO_COMPARE'); ?></a>
		<a href="<?=$arParams['PATH_TO_CATALOG']?>" class="btn_round btn_color hidden"><? echo GetMessage('CMP_TPL_GOTO_CATALOG'); ?></a>
	</div>
	<?
} else {
	?>
	<div class="bottom_panel_first_line">
		<div class="empty_message active">
			<div><? echo GetMessage('CMP_MSG_YOU_HAVE_NOT_YET');?></div>
		</div>
	</div>
	<div class="bottom_panel_second_line empty_line">
		<a href="<?=$arParams['PATH_TO_CATALOG']?>" class="btn_round btn_color hidden"><? echo GetMessage('CMP_TPL_GOTO_CATALOG'); ?></a>
	</div>
	<?
}
?>
