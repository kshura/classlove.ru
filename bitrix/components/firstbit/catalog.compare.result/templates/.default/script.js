BX.namespace("BX.Iblock.Catalog");

BX.Iblock.Catalog.CompareClass = (function()
{
	var CompareClass = function(wrapObjId)
	{
		this.wrapObjId = wrapObjId;
	};

	CompareClass.prototype.MakeAjaxAction = function(url)
	{
		BX.showWait(BX(this.wrapObjId));
		BX.ajax.post(
			url,
			{
				ajax_action: 'Y'
			},
			BX.proxy(function(result)
			{
				BX.closeWait();
				BX(this.wrapObjId).innerHTML = result;
			}, this)
		);
	};

	return CompareClass;
})();

function foo() {
var owl_compare = $('#compare_carousel');
owl_compare.owlCarousel({
	responsive: {
		0: {
			items: 2,
		},
		719: {
			items: 3,
		},
		959: {
			items: 4,
		}
	},
	loop: false,
	nav: true,
	dots: false,
	smartSpeed: 500,
	navContainer: '#owl_compare_nav',
	navText: ["<i class=\"fa fa-angle-left\"></i>","<i class=\"fa fa-angle-right\"></i>"],
	afterInit: initSlider(owl_compare),
});

$('.compare .clear_product').click(function() {
	$('.compare_item').remove();
	$('.compare_item_descr').remove();
});
$('.compare .close_btn').click(function() {
	$('div[data-idproduct=' + $(this).parent().parent().data("idproduct") + ']').remove();
});
}