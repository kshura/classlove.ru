<?
AddEventHandler('main', 'OnBuildGlobalMenu', 'OnBuildGlobalMenuHandler');
function OnBuildGlobalMenuHandler(&$aGlobalMenu, &$aModuleMenu){
	IncludeModuleLangFile(__FILE__);
	$MODULE_ID = 'firstbit.beautyshop';

	if(!CModule::IncludeModule("firstbit.beautyshop"))
		die();
//	$firstBit = new CFirstbitBeautyshop(WIZARD_SITE_ID);

//	$rsOptionSites = COption::GetOptionString($MODULE_ID, "SITES");
//	if(strlen($rsOptionSites)>0) {
//		$arOptionSites = explode(',',$rsOptionSites);
//		print_r($arOptionSites);
//	}
	$rsSites = CSite::GetList($by="sort", $order="asc", array("ACTIVE" => "Y"));
	while ($arSite = $rsSites->Fetch()) {
		if(CFirstbitBeautyshop::queryOption('wizard_installed', $arSite['LID']) == "Y") {

//		if(in_array($arSite['LID'],$arOptionSites)) {
			$aModuleMenu[] = array(
				"parent_menu" => "global_menu_firstbit",
				"icon" => "firstbit_settings_icon",
				"page_icon" => "default_page_icon",
				"sort" => $arSite['SORT'],
				"text" => GetMessage('FIRSTBIT_MENU_SETTINGS_TITLE') . $arSite['SITE_NAME'] . ' (' . $arSite['LID'] . ')',
				"title" => GetMessage('FIRSTBIT_MENU_SETTINGS_TITLE'),
				'url' => '/bitrix/admin/' . $MODULE_ID . '_settings.php?site_id=' . $arSite['LID']
			);
		}
	}


	$arRes = array(
		"global_menu_firstbit" => array(
			"menu_id" => "firstbit",
			"text" => GetMessage('FIRSTBIT_GLOBAL_MENU_TEXT'),
			"title" => GetMessage('FIRSTBIT_GLOBAL_MENU_TITLE'),
			"sort" => 0,
			"items_id" => "global_menu_firstbit",
			"help_section" => "firstbit",
			"items" => array()
		),
	);

	return $arRes;
//	$arGlobalMenu[] = $arMenu;
}
?>