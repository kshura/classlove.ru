$(document).ready(function() {
	console.log('asd');
	$('input[name*="COLOR_MAIN"]').minicolors({
		format: 'rgb',
		position: 'bottom left',
		change: function(hex, opacity) {
			var log;
			try {
				log = hex ? hex : 'transparent';
				if( opacity ) log += ', ' + opacity;
			} catch(e) {}
		},
	});
	$('input[name*="COLOR_ADDITIONAL"]').minicolors({
		format: 'rgb',
		position: 'bottom left',
		change: function(hex, opacity) {
			var log;
			try {
				log = hex ? hex : 'transparent';
				if( opacity ) log += ', ' + opacity;
			} catch(e) {}
		},
	});
	
	$('#color_preset').change(function () {
		var color_preset = $(this).val();
		$('input[name*="COLOR_MAIN"]').minicolors('value', color_preset);
	});
	
});