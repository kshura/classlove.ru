<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
$APPLICATION->AddHeadScript("//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js");
?>
	<style><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/firstbit.beautyshop/admin/css/jquery.minicolors.css"); ?></style>
	<script type="text/javascript"><?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/firstbit.beautyshop/admin/js/jquery.minicolors.min.js");?></script>
	<script type="text/javascript"><?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/firstbit.beautyshop/admin/js/settings.js");?></script>
<?

CModule::IncludeModule("firstbit.beautyshop");

$rsSites = CSite::GetList($by="sort", $order="asc", array("LID" => $_REQUEST['site_id']));
while ($arSite = $rsSites->Fetch()) {
	$SITE = $arSite;
}

$firstBit = new CFirstbitBeautyshop($SITE['LID']);

IncludeModuleLangFile(__FILE__);

if (CMain::GetUserRight('firstbit.beautyshop') <= 'D' || !CModule::IncludeModule(FIRSTBIT_MODULE_ID) || !CModule::IncludeModule("iblock"))
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$readOnly = (CMain::GetUserRight('firstbit.beautyshop') <= 'R' ? 'disabled="disabled"' : false);

$APPLICATION->SetTitle(GetMessage("FIRSTBIT_BEAUTYSHOP_MODULE_TITLE").$SITE['SITE_NAME']." (".$SITE['LID'].")");

/* Color presets*/
$arColorPresets = array('PINK', 'RED', 'ORANGE', 'YELLOW', 'GREEN', 'SKY_BLUE', 'BLUE', 'BROWN', 'PURPLE', 'LILAC');

/* Sorting main page blocks */
$arMainPageBlocks = array('SLIDER', 'BANNERS', 'HITS', 'NEWPRODUCT', 'BRANDS', 'ADVANTAGES', 'NEWS', 'COMPANY', 'FEEDBACK', 'STORES');
$arMainPageOrder = array();
foreach($arMainPageBlocks as $arBlock) {
	$arMainPageOrder[$arBlock] = $firstBit->options['MAIN_PAGE_ORDER_'.$arBlock];
}
natsort($arMainPageOrder);
$arMainPageOrderEmpty = array();
foreach($arMainPageOrder as $arBlock=>$arOrder) {
	if(!$arOrder) {
		$arMainPageOrderEmpty['MAIN_PAGE_ORDER_'.$arBlock] = 'string_hide';
		unset($arMainPageOrder[$arBlock]);
	}
}
foreach($arMainPageOrder as $arBlock=>$arOrder) {
	unset($arMainPageOrder[$arBlock]);
	$arMainPageOrder['MAIN_PAGE_ORDER_'.$arBlock] = 'string_hide';
}
$arMainPageOrderResult = array("MAIN_PAGE_ORDER" => 'heading') + $arMainPageOrder + $arMainPageOrderEmpty;

/* Quering profile properties for registration */
$registrationProperties = array();
$rsPersonType = CSalePersonType::GetList(array("SORT" => "ASC"), array("ACTIVE" => "Y"), false, false, array("ID", "NAME", "LID"));
if($rsPersonType->SelectedRowsCount()>0) {
	while ($arPersonType = $rsPersonType->Fetch()) {
		$rsPropertiesGroup = CSaleOrderPropsGroup::GetList(array("SORT" => "ASC"), array("PERSON_TYPE_ID" => $arPersonType["ID"], "ACTIVE" => "Y"), false, false, array("ID", "NAME"));
		while ($arPropertiesGroup = $rsPropertiesGroup->Fetch()) {
			$rsProperty = CSaleOrderProps::GetList(array("SORT" => "ASC"), array("PROPS_GROUP_ID" => $arPropertiesGroup["ID"], "ACTIVE" => "Y", "USER_PROPS" => "Y"), false, false, array());
			while ($arProperty = $rsProperty->Fetch()) {
				$arPropertyName = $arPersonType['NAME'].' ('.$arPersonType['LID'].') / '.$arPropertiesGroup['NAME'].' / '.$arProperty['NAME'];
				if($arProperty['REQUIED'] == 'Y') $arPropertyName .= ' *';
				$registrationProperties[$arProperty['ID']] = $arPropertyName;
			}
		}
	}
}


/* Catalog view settings */
$arCatalogTemplates = array('TILE', 'LIST_BIG', 'LIST');
$arCatalogTemplatesOrder = array();
foreach($arCatalogTemplates as $arTemplate) {
	$arCatalogTemplatesOrder[$arTemplate] = $firstBit->options['CATALOG_TEMPLATE_ORDER_'.$arTemplate];
}
natsort($arCatalogTemplatesOrder);
$arCatalogTemplatesOrderEmpty = array();
$arCatalogElements = array();
foreach($arCatalogTemplatesOrder as $arTemplate=>$arOrder) {
	$arCatalogElements['CATALOG_ELEMENTS_'.$arTemplate] = 'string_hide';
}
foreach($arCatalogTemplatesOrder as $arTemplate=>$arOrder) {
	if(!$arOrder) {
		$arCatalogTemplatesOrderEmpty['CATALOG_TEMPLATE_ORDER_'.$arTemplate] = 'string_hide';
		unset($arCatalogTemplatesOrder[$arTemplate]);
	}
}
foreach($arCatalogTemplatesOrder as $arTemplate=>$arOrder) {
	unset($arCatalogTemplatesOrder[$arTemplate]);
	$arCatalogTemplatesOrder['CATALOG_TEMPLATE_ORDER_'.$arTemplate] = 'string_hide';
}
$arCatalogTemplateDefault = array(
	'CATALOG_TEMPLATE_INITIAL' => array(
		'VALUES' => array(
			'TILE' => '',
			'LIST' => '',
			'LIST_BIG' => ''
		)
	)
);
$arCatalogTemplatesOrderResult = array("CATALOG_TEMPLATE_ORDER" => 'heading') + $arCatalogTemplateDefault + $arCatalogTemplatesOrder + $arCatalogTemplatesOrderEmpty;
$arCatalogElementsDefault = array('CATALOG_ELEMENTS_INITIAL' => 'is_integer');
$arCatalogElementsResult = array("CATALOG_ELEMENTS" => 'heading') + $arCatalogElementsDefault + $arCatalogElements;

use Bitrix\Main\UserConsent\Agreement;
$arAgreements = Agreement::getActiveList();
$arAgreements[0] = GetMessage("FIRSTBIT_BEAUTYSHOP_OPTION_CONSENT_AGREEMENT_NONE");
ksort($arAgreements);

$arOptions = array(
	'OPTION_TEMPLATE' => array(
		'SITE_NAME' => 'string',
		'SITE_PHONE' => 'string_hide',
		'SITE_EMAIL' => 'string_hide',
		'COLOR_MAIN' => 'string',
		'COLOR_ADDITIONAL' => 'string',
		'LOGO' => 'image',
		'LOGO_ALT' => 'image',
		'FAVICON' => 'file',
	),
	'OPTION_HEADER' => array(
		'HEADER_TYPE' => array(
			'VALUES' => array(
				1 => '',
				2 => '',
				3 => '',
				4 => '',
				5 => '',
				6 => ''
			)
		),
		'USE_FLYING_MENU' => 'bool',
		'MENU_MIDDLE_TYPE' => array(
			'VALUES' => array(
				1 => '',
				2 => '',
				3 => '',
			)
		),
		'MIDDLE_MENU_LIMIT' => 'string',
		'MIDDLE_MENU_MAX_LEVEL' => 'string',
		'TOP_MENU_ITEMS' => array(
			'VALUES' => array(
				'click' => '',
				'hover' => ''
			)
		),
		'MIDDLE_MENU_ITEMS' => array(
			'VALUES' => array(
				'click' => '',
				'hover' => ''
			)
		),
	),
	'OPTION_FOOTER' => array(
		'FOOTER_TYPE' => array(
			'VALUES' => array(
				1 => '',
				2 => ''
			)
		),
		'USE_BOTTOM_PANEL' => array(
			'VALUES' => array(
				'Y' => '',
				'N' => ''
			)
		)
	),
	'OPTION_MAIN_PAGE' => $arMainPageOrderResult + array(
		'MAIN_PAGE_SLIDER' => 'heading',
		'SLIDER_VIEW' => array(
			'VALUES' => array(
				1 => '',
				2 => '',
				3 => '',
				4 => ''
			)
		),
		'MAIN_PAGE_BANNERS' => 'heading',
		'BANNERS_VIEW' => array(
			'VALUES' => array(
				1 => '',
				2 => '',
				3 => '',
				4 => '',
				5 => '',
				6 => ''
			)
		),
		'BANNERS_TITLE_SHOW' => 'bool',
		'MAIN_PAGE_BRANDS' => 'heading',
		'BRANDS_VIEW' => array(
			'VALUES' => array(
				'IMAGES' => '',
				'LIST' => ''
			)
		),
	),
	'OPTION_CATALOG' => array(
		'USE_FLYING_BASKET' => 'bool',
		'NO_PHOTO' => 'image',
	) + $arCatalogTemplatesOrderResult + $arCatalogElementsResult,
	'OPTION_COMPANY' => array(
		'COMPANY_NAME' => 'string',
		'COMPANY_PHONE' => 'string_hide',
		'COMPANY_EMAIL' => 'string_hide',
		'COMPANY_INN' => 'string_hide',
		'COMPANY_KPP' => 'string_hide',
		'COMPANY_ACCOUNT' => 'string_hide',
		'COMPANY_BANK' => 'string_hide',
		'COMPANY_BIK' => 'string_hide',
		'COMPANY_KS' => 'string_hide',
		'COMPANY_ADDRESS' => 'string_hide',
		'COMPANY_COORDINATES' => 'string_hide',
	),
	'OPTION_REGISTRATION' => array(
		'EMAIL_LOGIN' => 'bool',
		'SEPARATE_REGISTRATION' => 'bool',
		'CREATE_PROFILE_PROPERTIES' => array(
			'MULTIPLE' => 'Y',
			'VALUES' => $registrationProperties
		),
		'GROUP_PROFILE_PROPERTIES' => 'bool',
	),
	'OPTION_CONSENT' => array(
		'CONSENT_FORM' => 'bool',
		'AGREEMENT_PAGE_LINK' => 'string',
		'AGREEMENT_LABEL_FORM_TEXT' => 'text',
		'AGREEMENT_ID' => array(
			'VALUES' => $arAgreements
		),
		'SITE_LINK_LEGAL' => '', // deprecated
		'SITE_LINK_OFFER' => 'string', // deprecated
	),
);

$arTabs = array();

$doSave = check_bitrix_sessid();

$arNewValues = array();

foreach ($arOptions as $strTabName => $arTabOptions) {
	$arTabs[] = array(
		"DIV" => "edit_" . $strTabName,
		"TAB" => GetMessage("FIRSTBIT_BEAUTYSHOP_" . $strTabName),
		"ICON" => "main_settings",
		"TITLE" => GetMessage("FIRSTBIT_BEAUTYSHOP_" . $strTabName)
	);

	if($doSave) {
		foreach ($arTabOptions as $strOptionCode => $strOptionType) {
			if ($strOptionType == 'heading') {}
			else if ($strOptionType == 'image' || $strOptionType == 'file') {
				// ������������ ������ ��������
				if(count($_POST['FIRSTBIT_OPTIONS'][$strOptionCode])>0) {
					if(intval($_POST['FIRSTBIT_OPTIONS'][$strOptionCode][0])<=0 && file_exists($_SERVER['DOCUMENT_ROOT'].$_POST['FIRSTBIT_OPTIONS'][$strOptionCode][0])) {
						$fileman = true;
						$rsFilemanFile = CFile::GetList(array("ID"=>"asc"), array("MODULE_ID" => "fileman", 'SUBDIR' => str_replace('/upload/', '', dirname($_POST['FIRSTBIT_OPTIONS'][$strOptionCode][0])), "FILE_NAME" => basename($_POST['FIRSTBIT_OPTIONS'][$strOptionCode][0])));
						while($arFilemanFile = $rsFilemanFile->GetNext())
							$arImage = $arFilemanFile['ID'];
					}
					else {
						if(count($_POST['FIRSTBIT_OPTIONS'][$strOptionCode])>1) {
							$fileman = false;
							$arImage = [];
							// ��� ������ ��� �������� ����� ������� � ��������� ������
							foreach ($_POST['FIRSTBIT_OPTIONS'][ $strOptionCode ] as $arRowIndex => $arRow) {
								if (is_array($arRow)) {
									if (array_key_exists('name', $arRow) || array_key_exists('type', $arRow) || array_key_exists('tmp_name', $arRow) || array_key_exists('size', $arRow) || array_key_exists('error', $arRow)) {
										if (array_key_exists('tmp_name', $arRow)) {
											$arRow['tmp_name'] = CTempFile::GetAbsoluteRoot() . preg_replace("/^.*(\/BXTEMP)/", "$1", $arRow['tmp_name']);
										}
										$arImage = $arImage + $arRow;
										unset($_POST['FIRSTBIT_OPTIONS'][ $strOptionCode ][ $arRowIndex ]);
									}
								}
							}
							// ���������� ����� ��������� �������, �������� ������ ���������� � ������� �����
							sort($_POST['FIRSTBIT_OPTIONS'][ $strOptionCode ]);
						}
					}
				}

				// ���������, ���������� �� ���� �� �������� ��� �������� �����
				if(is_array($_POST['FIRSTBIT_OPTIONS_del']) && array_key_exists($strOptionCode, $_POST['FIRSTBIT_OPTIONS_del']) && $_POST['FIRSTBIT_OPTIONS_del'][$strOptionCode][0] == "Y") {
					if(strlen($firstBit->options[$strOptionCode]) > 0) {
						$arCurFile = $firstBit->options[$strOptionCode];
						$arCurFile = CFile::GetFileArray($arCurFile);
						if($arCurFile['MODULE_ID'] == 'fileman') {
							$arNewValues[$strOptionCode] = '';
						}
						else {
							$doDelete = true;
							if(is_array($arImage) && array_key_exists('size', $arImage) && array_key_exists('tmp_name', $arImage)) {
								if($arCurFile['FILE_SIZE'] == $arImage['size']) {
									$doDelete = false;
									if(md5_file($_SERVER['DOCUMENT_ROOT'] . $arCurFile['SRC']) != md5_file($arImage['tmp_name'])) {
										$doDelete = true;
									}
								}
							}
							// ������� ������� ���� � ���������� �������� ��� ������ ��������
							if($doDelete == true) {
								CFile::Delete($firstBit->options[$strOptionCode]);
								$arNewValues[$strOptionCode] = '';
							}
						}
					}
					unset($arCurFile, $doDelete);
				}

				if($fileman) {
					$arNewValues[$strOptionCode] = $arImage;
				}
				else {
					if(count($arImage) > 0) {
						$doUpdate = true;
						// �������� ������� ���� ��� ��������� � �����������
						if($arCurFile = $firstBit->options[$strOptionCode]) {
							$arCurFile = CFile::GetFileArray($arCurFile);
							if($arCurFile['FILE_SIZE'] == $arImage['size']) {
								$doUpdate = false;
								if(md5_file($_SERVER['DOCUMENT_ROOT'] . $arCurFile['SRC']) != md5_file($arImage['tmp_name'])) {
									$doUpdate = true;
								}
							}
						}
						unset($arCurFile);
						if($doUpdate == true) {
							$arFile = $arImage + array('MODULE_ID' => FIRSTBIT_MODULE_ID);
							if($fid = CFile::SaveFile($arFile, "magazine")) {
								$arNewValues[$strOptionCode] = $fid;
								if($strOptionCode == "FAVICON") {
									$arFavicon = CFile::GetFileArray($fid);
									copy($_SERVER['DOCUMENT_ROOT'] . $arFavicon['SRC'], $_SERVER["DOCUMENT_ROOT"]."/".$SITE['DIR']."favicon.ico");
								}
							}
						}
					}
				}
				unset($fileman,$arImage,$arFile,$fid,$doDelete,$doUpdate);
			}
			else {
				if ($strOptionType == 'string_hide') {
					$arNewValues[$strOptionCode . '_HIDE'] = $_REQUEST['FIRSTBIT_OPTIONS'][$strOptionCode . "_HIDE"] == 'Y' ? 'Y' : 'N';
				}
				if(is_array($strOptionType) && $strOptionType['MULTIPLE'] == 'Y') {
					$arNewValues[$strOptionCode] = serialize($_REQUEST['FIRSTBIT_OPTIONS'][$strOptionCode]);
				} else {
					$arNewValues[$strOptionCode] = (string)$_REQUEST['FIRSTBIT_OPTIONS'][$strOptionCode];
				}
			}
		}
		$firstBit->generateCSS(
			$arNewValues['COLOR_MAIN'],
			$arNewValues['COLOR_ADDITIONAL'],
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/firstbit_beautyshop_".$SITE['LID']."/styles.less",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/firstbit_beautyshop_".$SITE['LID']."/styles.css"
		);
	}
}
$firstBit->setOptions($arNewValues);

/*
* render form
*/
$tabControl = new CAdminTabControl("tabControl", $arTabs, true, true);
?>
	<form method="POST" action="<?=$APPLICATION->GetCurUri()?>" enctype="multipart/form-data">
		<?=bitrix_sessid_post()?>
		<? $tabControl->Begin(); ?>
		<? foreach ($arOptions as $tabCode => $arTab) { ?>
			<? $tabControl->BeginNextTab(); ?>
			<? foreach ($arOptions[$tabCode] as $strOptionCode => $mixOption) { ?>
				<? if ($mixOption == 'heading') { ?>
					<tr class="heading" id="tr_<?=$strOptionCode?>"><td colspan="2"><?=GetMessage('FIRSTBIT_BEAUTYSHOP_' . $tabCode . '_' . $strOptionCode)?></td></tr>
				<? } else { ?>
				<tr>
					<td>
						<?=GetMessage('FIRSTBIT_BEAUTYSHOP_' . $tabCode . '_' . $strOptionCode)?>:
					</td>
					<td>
						<?
						if (is_array($mixOption)) { ?>
							<? if($mixOption['MULTIPLE'] && $mixOption['MULTIPLE'] == 'Y') { ?>
								<select name="FIRSTBIT_OPTIONS[<?=$strOptionCode?>][]" multiple="multiple" size="7" <?=$readOnly?>>
									<? foreach ($mixOption['VALUES'] as $strMixOptionValue=>$strMixOptionName) { ?>
										<option value="<?=$strMixOptionValue?>"<?=(is_array(unserialize($firstBit->options[$strOptionCode])) && in_array($strMixOptionValue, unserialize($firstBit->options[$strOptionCode]))) ? 'selected="selected"' : '';?>><?=$strMixOptionName ? $strMixOptionName : GetMessage('FIRSTBIT_BEAUTYSHOP_' . $tabCode . '_' . $strOptionCode . '_' . $strMixOptionValue)?></option>
									<? } ?>
								</select>
							<? } else { ?>
								<select name="FIRSTBIT_OPTIONS[<?=$strOptionCode?>]" <?=$readOnly?>>
									<? foreach ($mixOption['VALUES'] as $strMixOptionValue=>$strMixOptionName) { ?>
										<option value="<?=$strMixOptionValue?>"<?=$strMixOptionValue == $firstBit->options[$strOptionCode] ? 'selected="selected"' : '';?>><?=$strMixOptionName ? $strMixOptionName : GetMessage('FIRSTBIT_BEAUTYSHOP_' . $tabCode . '_' . $strOptionCode . '_' . $strMixOptionValue)?></option>
									<? } ?>
								</select>
							<? }?>
						<? }
						else if ($mixOption == 'image' || $mixOption == 'file') {
							$mixOption == 'image' ? $allowUpload = 'I' : $allowUpload = 'F';
							if (class_exists('\Bitrix\Main\UI\FileInput', true)) {
								echo \Bitrix\Main\UI\FileInput::createInstance(array(
									"name" => "FIRSTBIT_OPTIONS[".$strOptionCode."][]",
									"description" => false,
									"upload" => ($readOnly == false ? true : false),
									"allowUpload" => $allowUpload,
									"medialib" => ($readOnly == false ? true : false),
									"fileDialog" => ($readOnly == false ? true : false),
									"cloud" => ($readOnly == false ? true : false),
									"delete" => ($readOnly == false ? true : false),
									"edit" => ($readOnly == false ? true : false),
									"maxCount" => 1
								))->show($firstBit->options[$strOptionCode]);
							}
						}
						else if ($mixOption == 'bool') { ?>
							<input type="hidden" name="FIRSTBIT_OPTIONS[<?=$strOptionCode?>]" value="N"/>
							<label>
								<input type="checkbox" name="FIRSTBIT_OPTIONS[<?=$strOptionCode?>]" <? if ($firstBit->options[$strOptionCode] == 'Y'){ ?>checked="checked"<? } ?> value="Y" <?=$readOnly?>/>
							</label>
						<? }
						else {
							if($strOptionCode == 'COLOR_MAIN') {
								?>
								<select id="color_preset" <?=$readOnly?>>
									<option>----</option>
									<?
									foreach($arColorPresets as $colorPreset) {
										GetMessage('COLOR_'.$colorPreset.'_VALUE') == $firstBit->options[$strOptionCode] ? $selected = "selected=\"selected\"" : $selected = "";
										?>
											<option value="<?=GetMessage('COLOR_'.$colorPreset.'_VALUE')?>" <?=$selected?>><?=GetMessage('COLOR_'.$colorPreset)?></option>
										<?
										unset($selected);
									}
									?>
								</select><br/><br/>
								<?
							}
							if($mixOption == 'text' || $mixOption == 'text_hide') {
								?>
								<textarea name="FIRSTBIT_OPTIONS[<?= $strOptionCode ?>]" <?=$readOnly?> style="width: 300px; height: 150px; resize: none;"><?= $firstBit->options[$strOptionCode] ?></textarea>
								<?
							}
							else {
								?>
								<input type="text" name="FIRSTBIT_OPTIONS[<?= $strOptionCode ?>]" value="<?= (htmlentities($firstBit->options[$strOptionCode], ENT_QUOTES, 'UTF-8') ? htmlentities($firstBit->options[$strOptionCode], ENT_QUOTES, "UTF-8") : htmlentities($firstBit->options[$strOptionCode], ENT_QUOTES, 'cp1251')) ?>" <?=$readOnly?>/>
								<?
							}
							if ($mixOption == 'string_hide') { ?>
								<input type="hidden" name="FIRSTBIT_OPTIONS[<?=$strOptionCode?>_HIDE]" value="N"/>
								<label>
									<input type="checkbox" name="FIRSTBIT_OPTIONS[<?=$strOptionCode?>_HIDE]" <? if ($firstBit->options[$strOptionCode . '_HIDE'] == 'Y'){ ?>checked="checked"<? } ?> value="Y" <?=$readOnly?>/>&nbsp;<?=GetMessage('FIRSTBIT_DO_NOT_SHOW')?>
								</label>
							<? }
						} ?>
					</td>
				</tr>
				<? } ?>
			<? } ?>

		<? } ?>
		<? $tabControl->Buttons(array("disabled" => false)) ?>
		<? $tabControl->End() ?>

	</form>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");