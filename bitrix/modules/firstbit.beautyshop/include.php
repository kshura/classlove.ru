<?

if(!defined('FIRSTBIT_PARTNER_ID')) {
	define('FIRSTBIT_PARTNER_ID', 'firstbit');
}
if(!defined('FIRSTBIT_MODULE_CODE')) {
	define('FIRSTBIT_MODULE_CODE', 'beautyshop');
}
if(!defined('FIRSTBIT_MODULE_ID')){
	define('FIRSTBIT_MODULE_ID', FIRSTBIT_PARTNER_ID.'.'.FIRSTBIT_MODULE_CODE);
}
CModule::AddAutoloadClasses(
	'firstbit.beautyshop',
	array(
		'CFirstbitBeautyshop' => 'classes/general/CFirstbitBeautyshop.php',
		"\\Firstbit\\Beautyshop\\UserConsent" => "lib/userconsent.php",
	)
);
?>