<?
$MESS['PARTNER_NAME'] = "Первый БИТ";
$MESS['PARTNER_URI'] = "http://bit-ecommerce.ru";
$MESS['WIZARD_TITLE'] = "Косметика: интернет-магазин конструктор";
$MESS['WIZARD_DESCRIPTION'] = 'Мастер установки интернет-магазина конструктора "Косметика"';

$MESS["MOD_UNINST_OK"] = "Удаление модуля успешно завершено";
$MESS["SCOM_INSTALL_TITLE"] = "Установка модуля";
$MESS["SCOM_UNINSTALL_TITLE"] = "Удаление модуля";
$MESS["OPEN_WIZARDS_LIST"] = "Открыть список мастеров";
$MESS["INSTALL_OPTIMUS_SITE"] = "Установить готовый сайт";
?>