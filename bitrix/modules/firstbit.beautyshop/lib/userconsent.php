<?php

namespace Firstbit\Beautyshop;

use Bitrix\Main\EventResult;
use Bitrix\Main\Localization\Loc;
use Bitrix\Iblock\Element;
use Bitrix\Main\UserConsent\Consent;

class UserConsent
{
	const PROVIDER_CODE = 'firstbit/form_message';

	/**
	 * Event `main/OnUserConsentProviderList` handler.
	 *
	 * @return EventResult
	 */
	public static function onProviderList()
	{
		$parameters = array(
			array(
				'CODE' => self::PROVIDER_CODE,
				'NAME' => Loc::getMessage('FIRSTBIT_USER_CONSENT_PROVIDER_NAME'),
				'DATA' => function ($id = null)
				{
					$rsIblock = \Bitrix\Iblock\ElementTable::getList(array(
						'select' => array('ID', 'IBLOCK_ID'),
						'filter' => array('ID' => $id)
					));
					$iblock_id = $rsIblock->fetch();

					$rsIblockType = \Bitrix\Iblock\IblockTable::getList(array(
						'select' => array('ID', 'IBLOCK_TYPE_ID'),
						'filter' => array('ID' => $iblock_id['IBLOCK_ID'])
					));
					$iblock_type = $rsIblockType->fetch();

					return array(
						'NAME' => Loc::getMessage('FIRSTBIT_USER_CONSENT_PROVIDER_ITEM_NAME', array('%id%' => $id)),
						'URL' => str_replace(array('%id%', '%iblock_id%', '%iblock_type%'), array($id, $iblock_id['IBLOCK_ID'], $iblock_type['IBLOCK_TYPE_ID']), '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=%iblock_id%&type=%iblock_type%&ID=%id%&lang=ru&find_section_section=0&WF=Y')
					);
				}
			)
		);

		return new EventResult(EventResult::SUCCESS, $parameters, 'firstbit.beautyshop');
	}
}