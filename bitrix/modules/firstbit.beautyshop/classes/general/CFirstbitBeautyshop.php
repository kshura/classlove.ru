<?php
IncludeModuleLangFile(__FILE__);

class CFirstbitBeautyshop {

	var $options = array(
		"AGREEMENT_ID" => null,
		"AGREEMENT_LABEL_FORM_TEXT" => null,
		'AGREEMENT_PAGE_LINK' => null,
		"BANNERS_TITLE_SHOW" => null,
		"BANNERS_VIEW" => null,
		"BRANDS_VIEW" => null,
		"CATALOG_ELEMENTS_INITIAL" => null,
		"CATALOG_ELEMENTS_LIST" => null,
		"CATALOG_ELEMENTS_LIST_BIG" => null,
		"CATALOG_ELEMENTS_LIST_BIG_HIDE" => null,
		"CATALOG_ELEMENTS_LIST_HIDE" => null,
		"CATALOG_ELEMENTS_TILE" => null,
		"CATALOG_ELEMENTS_TILE_HIDE" => null,
		"CATALOG_TEMPLATE_INITIAL" => null,
		"CATALOG_TEMPLATE_ORDER_LIST" => null,
		"CATALOG_TEMPLATE_ORDER_LIST_BIG" => null,
		"CATALOG_TEMPLATE_ORDER_LIST_BIG_HIDE" => null,
		"CATALOG_TEMPLATE_ORDER_LIST_HIDE" => null,
		"CATALOG_TEMPLATE_ORDER_TILE" => null,
		"CATALOG_TEMPLATE_ORDER_TILE_HIDE" => null,
		"COLOR_ADDITIONAL" => null,
		"COLOR_MAIN" => null,
		"COMPANY_ACCOUNT" => null,
		"COMPANY_ACCOUNT_HIDE" => null,
		"COMPANY_ADDRESS" => null,
		"COMPANY_ADDRESS_HIDE" => null,
		"COMPANY_BANK" => null,
		"COMPANY_BANK_HIDE" => null,
		"COMPANY_BIK" => null,
		"COMPANY_BIK_HIDE" => null,
		"COMPANY_COORDINATES" => null,
		"COMPANY_COORDINATES_HIDE" => null,
		"COMPANY_EMAIL" => null,
		"COMPANY_EMAIL_HIDE" => null,
		"COMPANY_INN" => null,
		"COMPANY_INN_HIDE" => null,
		"COMPANY_KPP" => null,
		"COMPANY_KPP_HIDE" => null,
		"COMPANY_KS" => null,
		"COMPANY_KS_HIDE" => null,
		"COMPANY_NAME" => null,
		"COMPANY_PHONE" => null,
		"COMPANY_PHONE_HIDE" => null,
		'CONSENT_FORM' => null,
		"CREATE_PROFILE_PROPERTIES" => null,
		"EMAIL_LOGIN" => null,
		"FAVICON" => null,
		"FOOTER_TYPE" => null,
		"GROUP_PROFILE_PROPERTIES" => null,
		"HEADER_TYPE" => null,
		"LOGO" => null,
		"LOGO_ALT" => null,
		"MAIN_PAGE_ORDER_ADVANTAGES" => null,
		"MAIN_PAGE_ORDER_ADVANTAGES_HIDE" => null,
		"MAIN_PAGE_ORDER_BANNERS" => null,
		"MAIN_PAGE_ORDER_BANNERS_HIDE" => null,
		"MAIN_PAGE_ORDER_BRANDS" => null,
		"MAIN_PAGE_ORDER_BRANDS_HIDE" => null,
		"MAIN_PAGE_ORDER_COMPANY" => null,
		"MAIN_PAGE_ORDER_COMPANY_HIDE" => null,
		"MAIN_PAGE_ORDER_FEEDBACK" => null,
		"MAIN_PAGE_ORDER_FEEDBACK_HIDE" => null,
		"MAIN_PAGE_ORDER_HITS" => null,
		"MAIN_PAGE_ORDER_HITS_HIDE" => null,
		"MAIN_PAGE_ORDER_NEWPRODUCT" => null,
		"MAIN_PAGE_ORDER_NEWPRODUCT_HIDE" => null,
		"MAIN_PAGE_ORDER_NEWS" => null,
		"MAIN_PAGE_ORDER_NEWS_HIDE" => null,
		"MAIN_PAGE_ORDER_SLIDER" => null,
		"MAIN_PAGE_ORDER_SLIDER_HIDE" => null,
		"MAIN_PAGE_ORDER_STORES" => null,
		"MAIN_PAGE_ORDER_STORES_HIDE" => null,
		"MENU_MIDDLE_TYPE" => null,
		"MIDDLE_MENU_ITEMS" => null,
		"MIDDLE_MENU_LIMIT" => null,
		"MIDDLE_MENU_MAX_LEVEL" => null,
		"NO_PHOTO" => null,
		"SEPARATE_REGISTRATION" => null,
		"SITE_EMAIL" => null,
		"SITE_EMAIL_HIDE" => null,
		"SITE_LINK_LEGAL" => null,
		"SITE_LINK_OFFER" => null,
		"SITE_NAME" => null,
		"SITE_PHONE" => null,
		"SITE_PHONE_HIDE" => null,
		"SLIDER_VIEW" => null,
		"TOP_MENU_ITEMS" => null,
		"USE_BOTTOM_PANEL" => null,
		"USE_FLYING_BASKET" => null,
		"USE_FLYING_MENU" => null
	);
	var $site = false;
	var $demo = false;

	function __construct($site = SITE_ID, $demo = false) {
		if($site) $this->site = $site;
		if($demo) $this->demo = true;
		$this->initSiteOptions();
	}

	static public function getRealIP() {
		$ip = false;
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ips = explode (', ', $_SERVER['HTTP_X_FORWARDED_FOR']);
			for ($i = 0; $i < count($ips); $i++) {
				if (!preg_match("/^(10|172\\.16|192\\.168|127\\.0)\\./", $ips[$i])) {
					$ip = $ips[$i];
					break;
				}
			}
		}
		return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
	}

	static public function queryOption($option, $site = false) {
		return COption::GetOptionString(FIRSTBIT_MODULE_ID, $option, false, $site);
	}

	function initSiteOptions() {
		foreach($this->options as $arOptionCode => $arOptionValue) {
			if($this->demo) {
				if(is_array($_SESSION['SETTINGS']) && array_key_exists($arOptionCode, $_SESSION['SETTINGS'])) {
					$this->options[$arOptionCode] = $_SESSION['SETTINGS'][$arOptionCode];
				} else {
					$this->options[$arOptionCode] = $this->queryOption($arOptionCode, $this->site);
				}
			} else {
				$this->options[$arOptionCode] = $this->queryOption($arOptionCode, $this->site);
			}
		}
	}

	function generateCSS($COLOR_MAIN, $COLOR_ADDITIONAL, $LESS_FILE, $CSS_FILE) {
		if($COLOR_MAIN && $COLOR_ADDITIONAL && $LESS_FILE && $CSS_FILE) {
			if(!$this->demo && $COLOR_MAIN != $this->options['COLOR_MAIN'] || !$this->demo && $COLOR_ADDITIONAL != $this->options['COLOR_ADDITIONAL'] || $this->demo && !file_exists($CSS_FILE)) {
				if(!class_exists('lessc')){
					include_once 'lessc.inc.php';
				}
				$less = new lessc;
				$less->setVariables(array(
					"color_main" => $COLOR_MAIN,
					"color_additional" => $COLOR_ADDITIONAL,
				));
				$less->compileFile($LESS_FILE, $CSS_FILE);
				$this->setOptions(array("COLOR_MAIN" => $COLOR_MAIN, "COLOR_ADDITIONAL" => $COLOR_ADDITIONAL));
			}
			return true;
		} else {
			return false;
		}
	}

	function setOptions($newOptions) {
		if(isset($newOptions) && is_array($newOptions) && count($newOptions) > 0) {
			foreach($newOptions as $newOptionCode => $newOptionValue) {
				if(array_key_exists($newOptionCode, $this->options) && $newOptionValue !== $this->options[$newOptionCode]) {
					if(!$this->demo) {
						COption::SetOptionString(FIRSTBIT_MODULE_ID, $newOptionCode, $newOptionValue, false, $this->site);
					} else {
						$_SESSION['SETTINGS'][$newOptionCode] = $newOptionValue;
					}
					$this->options[$newOptionCode] = $newOptionValue;
				}
			}
		}
		return true;
	}

	static public function onSetMinPrice($arPrice, $arFields) {
		$rsProduct = CCatalogSku::GetProductInfo($arFields['PRODUCT_ID']);
		if (is_array($rsProduct)) {
			$productID = $rsProduct['ID'];
		} else {
			$productID = $arFields['PRODUCT_ID'];
		}
		if($hasOffers = CCatalogSKU::getExistOffers($productID)) {
			if($hasOffers[$productID]) {
				$rsOffers = CCatalogSKU::getOffersList($productID,0,array('ACTIVE'=>'Y'));
				$arMinPrice = array();
				$arNullPrice = array();
				foreach($rsOffers[$productID] as $arOffer){
					$rsPrice = CPrice::GetList(array(), array("PRODUCT_ID" => $arOffer['ID'], "CAN_BUY" => "Y"));
					if ($arPrice = $rsPrice->Fetch()) {
						if($arPrice['PRICE']>0) {

							$discountsAll = array();
							$rsDiscounts = CCatalogDiscount::GetDiscountProductsList(array(), array("PRODUCT_ID" => $arOffer['ID']));
							if ($arDiscounts = $rsDiscounts->Fetch()) {
								$arDiscount = CCatalogDiscount::GetByID($arDiscounts['DISCOUNT_ID']);
								$discountsAll[] = array(
									"VALUE_TYPE" => $arDiscount["VALUE_TYPE"],
									"VALUE" => $arDiscount["VALUE"],
									"CURRENCY" => $arDiscount["CURRENCY"],
									"MAX_DISCOUNT" => $arDiscount["MAX_DISCOUNT"]
								);
							}
							if(is_array($discountsAll) && sizeof($discountsAll) > 0) {
								$arMinPrice[] = CCatalogProduct::CountPriceWithDiscount($arPrice['PRICE'], $arPrice['CURRENCY'], $discountsAll);
							} else {
								$arMinPrice[] = $arPrice['PRICE'];
							}
						} else {
							$arNullPrice['PRICE']['DISCOUNT_VALUE'] = '0.00';
						}
					}
				}
				if(!empty($arMinPrice)) {
					$minPrice = min($arMinPrice);
				} else {
					$minPrice = min($arNullPrice);
				}
			} else {
				$discountsAll = array();
				$rsDiscounts = CCatalogDiscount::GetDiscountProductsList(array(), array("PRODUCT_ID" => $productID));
				if ($arDiscounts = $rsDiscounts->Fetch()) {
					$arDiscount = CCatalogDiscount::GetByID($arDiscounts['DISCOUNT_ID']);
					$discountsAll[] = array(
						"VALUE_TYPE" => $arDiscount["VALUE_TYPE"],
						"VALUE" => $arDiscount["VALUE"],
						"CURRENCY" => $arDiscount["CURRENCY"],
						"MAX_DISCOUNT" => $arDiscount["MAX_DISCOUNT"]
					);
				}
				if(is_array($discountsAll) && sizeof($discountsAll) > 0) {
					$minPrice = CCatalogProduct::CountPriceWithDiscount($arFields['PRICE'], $arFields['CURRENCY'], $discountsAll);
				} else {
					$minPrice = $arFields['PRICE'];
				}
			}
		}
		$element = new CIBlockElement;
		$PROP['MINIMUM_PRICE'] = $minPrice;
		$element->SetPropertyValuesEx($productID, false, $PROP);
	}

	static public function onUserRegister($arFields) {
		if($arFields["USER_ID"] > 0) {
			if($_REQUEST['register']) {
				$arUser = new CUser;
				$arUserFields = Array(
					"NAME" => $_REQUEST['REGISTER']['FIRST_NAME'],
					"LAST_NAME" => $_REQUEST['REGISTER']['LAST_NAME'],
					"SECOND_NAME" => $_REQUEST['REGISTER']['MIDDLE_NAME'],
				);
				$arUser->Update($arFields["USER_ID"], $arUserFields);

				$arProfileFields = array(
					"NAME" => GetMessage('FIRSTBIT_BEAUTYSHOP_PROFILE_OPTION_TITLE'),
					"USER_ID" => $arFields["USER_ID"],
					"PERSON_TYPE_ID" => $_REQUEST['REGISTER']['PERSON_TYPE']
				);

				$arProfile = CSaleOrderUserProps::Add($arProfileFields + $_REQUEST['REGISTER']);

				if($arProfile > 0) {
					$rsOrderPropsFilter = array('ACTIVE'=>'Y', 'PERSON_TYPE_ID'=>$_REQUEST['REGISTER']['PERSON_TYPE'], 'USER_PROPS'=>'Y');
					$rsOrderPropsSelect = array('ID', 'CODE', 'NAME');
					$rsOrderProps = CSaleOrderProps::GetList(array('ID'=>'asc'), $rsOrderPropsFilter, false, false, $rsOrderPropsSelect);
					while ($arOrderProp = $rsOrderProps->GetNext()) {
						if(array_key_exists($arOrderProp['CODE'], $_REQUEST['REGISTER'])) {
							$arFields = array("USER_PROPS_ID" => $arProfile, "ORDER_PROPS_ID" => $arOrderProp['ID'], "NAME" => $arOrderProp['NAME'], "VALUE" => $_REQUEST['REGISTER'][$arOrderProp['CODE']]);
							CSaleOrderUserPropsValue::Add($arFields);
						}
					}
				}

				if (CFirstbitBeautyshop::queryOption('CONSENT_FORM', SITE_ID) === 'Y' && CFirstbitBeautyshop::queryOption('AGREEMENT_ID', SITE_ID) > 0) {
					\Bitrix\Main\UserConsent\Consent::addByContext(CFirstbitBeautyshop::queryOption('AGREEMENT_ID', SITE_ID));
				}

			}
		}
	}

	static public function searchNearest($value, $inArray) {
		$lastKey = null;
		$lastDif = null;
		foreach ($inArray as $k => $v) {
			if ($v == $value) {
				return $k;
			}
			$dif = abs ($value - $v);
			if (is_null($lastKey) || $dif < $lastDif) {
				$lastKey = $k;
				$lastDif = $dif;
			}
		}
		return $lastKey;
	}

	static public function parsePhone($phonesStr){
		$arReturn = array();
		if(strpos($phonesStr, ",")){
			$phones = explode(",",$phonesStr);
			foreach($phones as $phone){
				$phone = trim($phone);
				$arPhone = array();
				$arPhone["FORMAT"] = preg_replace("/[^0-9+]/","", $phone);
				$arPhone["DISPLAY"] = $phone;
				$arReturn[] = $arPhone;
			}
		} else {
			$arPhone = array();
			$phonesStr = trim($phonesStr);
			$arPhone = array();
			$arPhone["FORMAT"] = preg_replace("/[^0-9+]/","", $phonesStr);
			$arPhone["DISPLAY"] = $phonesStr;
			$arReturn[] = $arPhone;
		}
		return $arReturn;
	}

}