<?
$MESS["WIZ_TEMPLATE_UNIVERSAL"] = "Косметика: интернет-магазин конструктор";

$MESS["WIZ_MODULE_REQUIRED"] = "Для установки решения должен быть установлен модуль \"Косметика: интернет-магазин конструктор (firstbit.beautyshop)\". Пожалуйста, установите модуль в разделе <a href=\"/bitrix/admin/partner_modules.php\">\"Решения Marketplace\"</a>";

$MESS["WIZ_GENERAL_DATA"] = "Общая информация о сайте";
$MESS["WIZ_SITE_NAME"] = "Название сайта";
$MESS["WIZ_SITE_LOGO"] = "Логотип для светлого фона (рекомендуемый размер 220 X 50)";
$MESS["WIZ_SITE_LOGO_ALT"] = "Логотип для темного фона (рекомендуемый размер 220 X 50)";

$MESS["WIZ_CONTACTS_DATA"] = "Контактная информация (для обращений с сайта)";
$MESS["WIZ_SITE_PHONE"] = "Телефон";
$MESS["WIZ_SITE_EMAIL"] = "Электронная почта";

$MESS["WIZ_META_DATA"] = "Метаданные";
$MESS["WIZ_META_DESCRIPTION"] = "Описание сайта";
$MESS["WIZ_META_KEYWORDS"] = "Ключевые слова";
$MESS["wiz_site_desc"] = "Мы предлагаем широкий ассортимент качественных товаров по адекватным ценам.";
$MESS["wiz_keywords"] = "интернет-магазин, быстрая доставка, низкие цены, большой ассортимент";

$MESS["WIZ_STRUCTURE_DATA"] = "Установить демонстрационные данные интернет-магазина";

$MESS["WIZ_SHOP_PRICE_BASE_TITLE"] = "Базовая цена:";
$MESS["WIZ_SHOP_PRICE_BASE_TEXT1"] = "В решении \"Косметика: интернет-магазин конструктор\" используется в качестве публичных цен для просмотра и покупки базовая цена (BASE). В текущий момент для группы \"Все пользователи (в том числе неавторизованные)\" включено право на данный тип цены. Таким образом данная группа будет видеть цены в решении.";
$MESS["WIZ_SHOP_PRICE_BASE_TEXT2"] = "Добавить для группы \"Все пользователи (в том числе неавторизованные)\" право на просмотр и на покупку по этому типу цен.";


$MESS["WIZ_STEP_SS"] = "Реквизиты компании";

$MESS["WIZ_COMPANY_DATA"] = "Общая информация о компании";
$MESS["WIZ_COMPANY_NAME"] = "Название компании";
$MESS["WIZ_COMPANY_EMAIL"] = "Email компании";
$MESS["WIZ_COMPANY_PHONE"] = "Телефон компании";
$MESS["WIZ_COMPANY_ADDRESS"] = "Адрес компании";
$MESS["WIZ_COMPANY_COORDINATES"] = "Координаты компании на карте";

$MESS["WIZ_BANK_DATA"] = "Банковские реквизиты";
$MESS["WIZ_COMPANY_INN"] = "ИНН";
$MESS["WIZ_COMPANY_KPP"] = "КПП";
$MESS["WIZ_COMPANY_ACCOUNT"] = "Расчетный счет";
$MESS["WIZ_COMPANY_BANK"] = "Корреспондентский счет";
$MESS["WIZ_COMPANY_BIK"] = "БИК банка";
$MESS["WIZ_COMPANY_KS"] = "Корреспондентский счет";




$MESS["wiz_socserv_data"] = "Социальные сети:";

$MESS["WIZ_COMPANY_NAME_DEF"] = "Косметика";
$MESS["WIZ_COMPANY_ADDRESS"] = "Адрес компании";
$MESS["WIZ_COMPANY_TELEPHONE"] = "Телефон";
$MESS["CANCEL_BUTTON"] = "Отменить установку решения";




$MESS["WIZ_SHOP_SOCSERV_TITLE"] = "Страницы магазина в социальных сетях";
$MESS["WIZ_SHOP_FACEBOOK"] = "Страница магазина в facebook.com";
$MESS["WIZ_SHOP_FACEBOOK_DEF"] = "http://www.facebook.com/1CBitrix";
$MESS["WIZ_SHOP_TWITTER"] = "Страница магазина в twitter.com";
$MESS["WIZ_SHOP_TWITTER_DEF"] = "http://twitter.com/1C_Bitrix";
$MESS["WIZ_SHOP_VK"] = "Страница магазина в vk.com";
$MESS["WIZ_SHOP_VK_DEF"] = "http://vk.com/bitrix_1c";
$MESS["WIZ_SHOP_GOOGLE_PLUS"] = "Страница магазина в plus.google.com";
$MESS["WIZ_SHOP_GOOGLE_PLUS_DEF"] = "https://plus.google.com/111119180387208976312/";
$MESS["WIZ_COMPANY_COPY_DEF"] = "&copy; Интернет-магазин одежды, 2015";
$MESS["WIZ_REWRITE_INDEX_DESC"] = "Изменение главной страницы";
$MESS["wiz_site_name"] = "Интернет-магазин";
$MESS["WIZ_SHOP_LOCALIZATION"] = "Выбор локализации магазина";
$MESS["WIZ_SHOP_LOCALIZATION_RUSSIA"] = "Россия";
$MESS["WIZ_SHOP_LOCALIZATION_UKRAINE"] = "Украина";
$MESS["WIZ_SHOP_LOCALIZATION_KAZAKHSTAN"] = "Казахстан";
$MESS["WIZ_SHOP_LOCALIZATION_BELORUSSIA"] = "Белоруссия";
$MESS["WIZ_SHOP_LOCATION"] = "Местоположение компании";
$MESS["WIZ_SHOP_LOCATION_DESCR_UA"] = "Населённый пункт регистрации юридического лица или субъекта предпринимательской деятельности";
$MESS["WIZ_SHOP_OF_NAME_DESCR_UA"] = "Название юридического лица или субъекта предпринимательской деятельности";
$MESS["WIZ_SHOP_BANK_TITLE"] = "Банковские реквизиты";
$MESS["WIZ_SHOP_BANK"] = "Банк";
$MESS["WIZ_SHOP_EMAIL"] = "Email для получения информации о заказах";
$MESS["WIZ_SHOP_ADR_DESCR_UA"] = "Юридический адрес юридического лица или субъекта предпринимательской деятельности";
$MESS["WIZ_SHOP_ADR_DEF_UA"] = "Київ";
$MESS["WIZ_SHOP_STAMP"] = "Печать фирмы";
$MESS["WIZ_SHOP_RECV_UA"] = "Реквизиты предприятия";
$MESS["WIZ_SHOP_RECV_UA_DESC"] = "Внесенная информация используется при формировании украинской первичной документации (счет, акт, накладная, налоговая накладная).<br/>Обратите внимание, что согласно украинскому законодательству документооборот должен вестись на государственном языке, поэтому рекомендуем вам заполнять поля с реквизитами для документов на украинском.";
$MESS["WIZ_SHOP_COMPANY_UA"] = "Название предприятия или ФИО физического лица-предпринимателя";
$MESS["WIZ_SHOP_EGRPU_UA"] = "ЕГРПОУ";
$MESS["WIZ_SHOP_INN_UA"] = "ИНН";
$MESS["WIZ_SHOP_NDS_UA"] = "№ свидетельства НДС";
$MESS["WIZ_SHOP_NS_UA"] = "№ расчетного счета";
$MESS["WIZ_SHOP_BANK_UA"] = "Банк";
$MESS["WIZ_SHOP_MFO_UA"] = "МФО банка";
$MESS["WIZ_SHOP_PLACE_UA"] = "Место составления документа";
$MESS["WIZ_SHOP_FIO_UA"] = "ФИО и должность ответственного лица за осуществление хозяйственной операции";
$MESS["WIZ_SHOP_TAX_UA"] = "Система налогообложения";
$MESS["WIZ_PERSON_TYPE_TITLE"] = "Типы плательщиков";
$MESS["WIZ_PERSON_TYPE"] = "Выберите типы плательщиков, которым вы будете продавать на сайте. Для разных типов плательщиков возможны различные способы оплаты и доставки, а также различный набор свойств заказа.";
$MESS["WIZ_PERSON_TYPE_FIZ"] = "Физическое лицо";
$MESS["WIZ_PERSON_TYPE_UR"] = "Юридическое лицо";
$MESS["WIZ_PERSON_TYPE_FIZ_UA"] = "Физическое лицо предприниматель";
$MESS["WIZ_PAY_SYSTEM_TITLE"] = "Способы оплаты";
$MESS["WIZ_PAY_SYSTEM"] = "Выберите способы оплаты, которые будут возможны на вашем сайте. В дальнейшем в настройках магазина вы сможете активировать другие платежные системы (Яндекс.Деньги, Ассист, Деньги@mail.ru, ChronoPay и т.п.). Для работы этих платежных систем потребуется заключение с ними договоров.";
$MESS["WIZ_PAY_SYSTEM_C"] = "Наличные (при доставке курьером и самовывозе)";
$MESS["WIZ_PAY_SYSTEM_S"] = "Квитанция Сбербанк (банковский перевод для физических лиц)";
$MESS["WIZ_PAY_SYSTEM_O"] = "Квитанция Ощадбанк (банковский перевод для физических лиц)";
$MESS["WIZ_PAY_SYSTEM_B"] = "Безналичный расчет (банковский перевод для юридических лиц)";
$MESS["WIZ_PAY_SYSTEM_COL"] = "Наложенный платеж";
$MESS["WIZ_PAY_SYSTEM_B_UA"] = "Безналичный расчет (банковский перевод для всех типов плательщиков)";
$MESS["WIZ_DELIVERY_TITLE"] = "Способы доставки";
$MESS["WIZ_DELIVERY"] = "Выберите способы доставки, которые возможны для вашего магазина. В настройках магазина вы можете активировать  или добавить дополнительные службы доставки (EMS, UPS и т.п.).";
$MESS["WIZ_DELIVERY_C"] = "Курьер";
$MESS["WIZ_DELIVERY_S"] = "Самовывоз";
$MESS["WIZ_DELIVERY_R"] = "Почта России (расчёт на основании данных с сервера почты)";
$MESS["WIZ_DELIVERY_R2"] = "Почта России (расчёт на основании табличных данных)";
$MESS["WIZ_DELIVERY_RF"] = "Отправления 1 класса";
$MESS["WIZ_DELIVERY_UA"] = "Новая почта";
$MESS["WIZ_DELIVERY_KZ"] = "Казпочта";
$MESS["WIZ_DELIVERY_HINT"] = "В дальнейшем вы можете легко изменить и настроить платежные системы и службы доставки в настройках магазина в административной части.";
$MESS["WIZ_CATALOG_SUBSCRIBE"] = "Сервис оповещения покупателей о поступлении товара";
$MESS["WIZ_CATALOG_SUBSCRIBE_DESCR"] = "Магазин уведомит покупателя, что товар появился на складе";
$MESS["WIZ_CATALOG_VIEW"] = "Вид представления списка товаров";
$MESS["WIZ_CATALOG_VIEWS_TEXT"] = "Выберите вид представления списка товаров";
$MESS["WIZ_CATALOG_VIEW_BAR"] = "Плитка";
$MESS["WIZ_CATALOG_VIEW_BAR_DESCR"] = "Для товаров, которые легко различаются по картинке";
$MESS["WIZ_CATALOG_VIEW_LIST"] = "Список";
$MESS["WIZ_CATALOG_VIEW_LIST_DESCR"] = "Если для выбора товара важна как картинка, так и описание товара";
$MESS["WIZ_CATALOG_VIEW_PRICE_LIST"] = "Прайс лист";
$MESS["WIZ_CATALOG_VIEW_PRICE_LIST_DESCR"] = "Для каталогов, где не важны описания и фотографии товаров";
$MESS["WIZ_CATALOG_USE_STORE_CONTROL"] = "Складской учет";
$MESS["WIZ_STORE_CONTROL"] = "Включить складской учет";
$MESS["SALE_PRODUCT_RESERVE_CONDITION"] = "Когда резервировать товар на складе";
$MESS["SALE_PRODUCT_RESERVE_1_ORDER"] = "При оформлении заказа";
$MESS["SALE_PRODUCT_RESERVE_2_PAYMENT"] = "При оплате заказа";
$MESS["SALE_PRODUCT_RESERVE_3_DELIVERY"] = "При разрешении доставки";
$MESS["SALE_PRODUCT_RESERVE_4_DEDUCTION"] = "При отгрузке";
$MESS["WIZ_STEP_SITE_SET"] = "Информация о сайте";
$MESS["WIZ_STEP_CT"] = "Настройка каталога";
$MESS["WIZ_STEP_PT"] = "Типы плательщиков";
$MESS["WIZ_STEP_PS"] = "Оплата и доставка";
$MESS["WIZ_NO_PT"] = "Необходимо выбрать хотя бы один тип плательщика";
$MESS["WIZ_NO_PS"] = "Необходимо выбрать хотя бы один способ оплаты";
$MESS["wizard_store_mobile"] = "Создать мобильный магазин";
$MESS["WIZ_NO_MODULE_CATALOG"] = "Не установлен модуль торгового каталога. Для продолжения работы мастера <a href='/bitrix/admin/module_admin.php'>перейдите по ссылке</a> и установите модуль.";
$MESS["WIZ_LOCATION_TITLE"] = "Местоположения";
$MESS["WSL_STEP2_GFILE_USSR"] = "Россия и СНГ (страны и города)";
$MESS["WSL_STEP2_GFILE_UA"] = "Украина";
$MESS["WSL_STEP2_GFILE_KZ"] = "Казахстан";
$MESS["WSL_STEP2_GFILE_USA"] = "США (города)";
$MESS["WSL_STEP2_GFILE_CNTR"] = "Мир (страны)";
$MESS["WSL_STEP2_GFILE_NONE"] = "не загружать";
?>