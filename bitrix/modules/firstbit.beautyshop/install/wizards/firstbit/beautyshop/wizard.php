<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/wizard.php");
?>
<style>
<? include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/firstbit/beautyshop/css/styles.css"); ?>
</style>
<script>
<? include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/wizards/firstbit/beautyshop/js/jquery-3.1.1.min.js"); ?>
</script>
<?



class SelectSiteStep extends CSelectSiteWizardStep {
	function InitStep() {
		parent::InitStep();

		$this->SetStepID("select_site");
		$wizard =& $this->GetWizard();
		$wizard->solutionName = "beautyshop";
	}
//	function ShowStep() {
//		$this->content .= 'test';
//	}
}

class SelectTemplateStep extends CSelectTemplateWizardStep {
	function InitStep() {
		$this->SetStepID("select_template");
		$this->SetTitle(GetMessage("SELECT_TEMPLATE_TITLE"));
		$this->SetSubTitle(GetMessage("SELECT_TEMPLATE_SUBTITLE"));
//		$this->SetPrevStep("select_site");
		$this->SetNextStep("select_theme");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();

		$proactive = COption::GetOptionString("statistic", "DEFENCE_ON", "N");
		if ($proactive == "Y")
		{
			COption::SetOptionString("statistic", "DEFENCE_ON", "N");
			$wizard->SetVar("proactive", "Y");
		}
		else
		{
			$wizard->SetVar("proactive", "N");
		}

		if ($wizard->IsNextButtonClick())
		{
			$arTemplates = array("firstbit_beautyshop");

			$templateID = $wizard->GetVar("wizTemplateID");

			if (!in_array($templateID, $arTemplates))
				$this->SetError(GetMessage("wiz_template"));

			if (in_array($templateID,  array("firstbit_beautyshop")))
				$wizard->SetVar("templateID", "firstbit_beautyshop");
		}
	}

	function ShowStep() {
		if(!CModule::IncludeModule("firstbit.beautyshop")){
			$this->content .= "<div style=\"color: #ff0000;\">".GetMessage("WIZ_MODULE_REQUIRED")."</div>";
			$this->SetNextStep();
			$this->SetCancelStep("cancel");
			$this->SetCancelCaption(GetMessage("CANCEL_BUTTON"));
		}
		else {
			$wizard =& $this->GetWizard();

			$templatesPath = WizardServices::GetTemplatesPath($wizard->GetPath() . "/site");
			$arTemplates = WizardServices::GetTemplates($templatesPath);

			$arTemplateOrder = array();

			if(in_array("firstbit_beautyshop", array_keys($arTemplates))) {
				$arTemplateOrder[] = "firstbit_beautyshop";
			}

			$defaultTemplateID = COption::GetOptionString("main", "wizard_template_id", "firstbit_beautyshop", $wizard->GetVar("siteID"));
			if(!in_array($defaultTemplateID, array("firstbit_beautyshop"))) $defaultTemplateID = "firstbit_beautyshop";
			$wizard->SetDefaultVar("wizTemplateID", $defaultTemplateID);

			$arTemplateInfo = array(
				"firstbit_beautyshop" => array(
					"NAME" => GetMessage("WIZ_TEMPLATE_UNIVERSAL"),
					"DESCRIPTION" => "",
					"PREVIEW" => $wizard->GetPath() . "/site/templates/firstbit_beautyshop/img/preview.gif",
					"SCREENSHOT" => $wizard->GetPath() . "/site/templates/firstbit_beautyshop/img/screenshot.gif",
				),
			);

			global $SHOWIMAGEFIRST;
			$SHOWIMAGEFIRST = true;

			$this->content .= '<div class="inst-template-list-block">';
			foreach($arTemplateOrder as $templateID) {
				$arTemplate = $arTemplateInfo[$templateID];

				if(!$arTemplate) {
					continue;
				}

				$this->content .= '<div class="inst-template-description">';
				$this->content .= $this->ShowRadioField("wizTemplateID", $templateID, Array("id" => $templateID, "class" => "inst-template-list-inp"));

				global $SHOWIMAGEFIRST;
				$SHOWIMAGEFIRST = true;

				if($arTemplate["SCREENSHOT"] && $arTemplate["PREVIEW"]) {
					$this->content .= CFile::Show2Images($arTemplate["PREVIEW"], $arTemplate["SCREENSHOT"], 150, 150, ' class="inst-template-list-img"');
				} else {
					$this->content .= CFile::ShowImage($arTemplate["SCREENSHOT"], 150, 150, ' class="inst-template-list-img"', "", true);
				}

				$this->content .= '<label for="' . $templateID . '" class="inst-template-list-label">' . $arTemplate["NAME"] . "</label>";
				$this->content .= "</div>";
			}

			$this->content .= "</div>";
		}
	}

}

class SelectThemeStep extends CSelectThemeWizardStep {
}

class SiteSettingsStep extends CSiteSettingsWizardStep {
	function InitStep() {
		if(CModule::IncludeModule("firstbit.beautyshop")) {
			$wizard =& $this->GetWizard();
			$wizard->solutionName = "beautyshop";
			parent::InitStep();

			$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
			$this->SetTitle(GetMessage("WIZ_STEP_SITE_SET"));

			$siteID = $wizard->GetVar("siteID");
			$firstBit = new CFirstbitBeautyshop($siteID);

			$isWizardInstalled = COption::GetOptionString("firstbit.beautyshop", "wizard_installed", "N", $siteID) == "Y";

//			if(COption::GetOptionString("firstbit.beautyshop", "wizard_installed", "N", $siteID) == "Y" && !WIZARD_INSTALL_DEMO_DATA) {
//				$this->SetNextStep("data_install");
//			} else {
//				$this->SetNextStep("shop_settings");
//			}

			$this->SetNextStep("catalog_settings");

			$templateID = $wizard->GetVar("templateID");
			$themeID = $wizard->GetVar($templateID . "_themeID");

			$siteLogo = $firstBit->options["LOGO"];
			$siteLogoAlt = $firstBit->options["LOGO_ALT"];

			$wizard->SetDefaultVars(Array(
				"LOGO" => (is_int($siteLogo) ? 'siteLogo' : "/bitrix/wizards/firstbit/beautyshop/site/templates/firstbit_beautyshop/img/logo.png"),
				"LOGO_ALT" => (is_int($siteLogoAlt) ? 'siteLogoAlt' : "/bitrix/wizards/firstbit/beautyshop/site/templates/firstbit_beautyshop/img/logo_alt.png"),
				"SITE_NAME" => $firstBit->options["SITE_NAME"],
				"SITE_PHONE" => $firstBit->options["SITE_PHONE"],
				"SITE_EMAIL" => $firstBit->options["SITE_EMAIL"],
				"siteMetaDescription" => GetMessage("wiz_site_desc"),
				"siteMetaKeywords" => GetMessage("wiz_keywords"),
			));
		}
	}

	function ShowStep() {
		if(!CModule::IncludeModule("firstbit.beautyshop")){
			$this->content .= "<p style='color:red'>".GetMessage("WIZ_MODULE_REQUIRED")."</p>";
			$this->SetCancelStep("cancel");
			$this->SetCancelCaption(GetMessage("CANCEL_BUTTON"));
		}
		else {
			$wizard =& $this->GetWizard();

			$firstBit = new CFirstbitBeautyshop($wizard->GetVar("siteID"));

			$this->content .= '<div class="wizard-input-form">';

			$this->content .= '
				<div class="wizard-input-form-block">
					<div class="wizard-metadata-title">' . GetMessage("WIZ_GENERAL_DATA") . '</div>
					<label for="SITE_NAME" class="wizard-input-title">' . GetMessage("WIZ_SITE_NAME") . '</label>
					'.$this->ShowInputField('text', 'SITE_NAME', array("id" => "SITE_NAME", "class" => "wizard-field")) . '
				</div>';

			$siteLogo = $wizard->GetVar("LOGO", true);
			$this->content .= '
				<div class="wizard-input-form-block" style="background-color: #f4f5f6;   width: 571px; padding: 10px">
					<label for="LOGO">' . GetMessage("WIZ_SITE_LOGO") . '</label><br/>';
					$this->content .= CFile::ShowImage($siteLogo, 220, 50, "border=0 vspace=15");
					$this->content .= "<br/>" . $this->ShowFileField("LOGO", Array("show_file_info" => "N", "id" => "LOGO")) .
				'</div>';

			$siteLogoAlt = $wizard->GetVar("LOGO_ALT", true);
			$this->content .= '
				<div class="wizard-input-form-block"  style="background-color: #f4f5f6;   width: 571px; padding: 10px">
					<label for="LOGO_ALT">' . GetMessage("WIZ_SITE_LOGO_ALT") . '</label><br/>';
					$this->content .= CFile::ShowImage($siteLogoAlt, 220, 50, "border=0 vspace=15");
					$this->content .= "<br/>" . $this->ShowFileField("LOGO_ALT", Array("show_file_info" => "N", "id" => "LOGO_ALT")) .
				'</div>';

			$this->content .= '
				<div class="wizard-input-form-block">
					<div class="wizard-metadata-title">' . GetMessage("WIZ_CONTACTS_DATA") . '</div>
					<label for="SITE_PHONE" class="wizard-input-title">' . GetMessage("WIZ_SITE_PHONE") . '</label>
					' . $this->ShowInputField('text', 'SITE_PHONE', array("id" => "SITE_PHONE", "class" => "wizard-field")) . '
				</div>';
			$this->content .= '
				<div class="wizard-input-form-block">
					<label for="SITE_EMAIL" class="wizard-input-title">' . GetMessage("WIZ_SITE_EMAIL") . '</label>
					' . $this->ShowInputField('text', 'SITE_EMAIL', array("id" => "SITE_EMAIL", "class" => "wizard-field")) . '
				</div>';

			$firstStep = COption::GetOptionString("main", "wizard_first" . substr($wizard->GetID(), 7) . "_" . $wizard->GetVar("siteID"), false, $wizard->GetVar("siteID"));
			$styleMeta = 'style="display:block"';
			if($firstStep == "Y") $styleMeta = 'style="display:none"';

			$this->content .= '
				<div  id="bx_metadata" ' . $styleMeta . '>
					<div class="wizard-input-form-block">
						<div class="wizard-metadata-title">' . GetMessage("WIZ_META_DATA") . '</div>
						<label for="siteMetaDescription" class="wizard-input-title">' . GetMessage("WIZ_META_DESCRIPTION") . '</label>
						' . $this->ShowInputField("textarea", "siteMetaDescription", Array("id" => "siteMetaDescription", "rows" => "3", "class" => "wizard-field")) . '
					</div>';
			$this->content .= '
					<div class="wizard-input-form-block">
						<label for="siteMetaKeywords" class="wizard-input-title">' . GetMessage("WIZ_META_KEYWORDS") . '</label><br>
						' . $this->ShowInputField('text', 'siteMetaKeywords', array("id" => "siteMetaKeywords", "class" => "wizard-field")) . '
					</div>
				</div>';

			//install Demo data
			if($firstStep == "Y") {
				$this->content .= '
					<div class="wizard-input-form-block"' . (LANGUAGE_ID != "ru" ? ' style="display:none"' : '') . '>
						' . $this->ShowCheckboxField(
							"installDemoData",
							"Y",
							(array("id" => "installDemoData", "onClick" => "if(this.checked == true){document.getElementById('bx_metadata').style.display='block';}else{document.getElementById('bx_metadata').style.display='none';}"))
						) . '
						<label for="installDemoData">' . GetMessage("WIZ_STRUCTURE_DATA") . '</label>
					</div>';
			} else {
				$this->content .= $this->ShowHiddenField("installDemoData", "Y");
			}

			if(CModule::IncludeModule("catalog")) {
				$db_res = CCatalogGroup::GetGroupsList(array("CATALOG_GROUP_ID" => '1', "BUY" => "Y", "GROUP_ID" => 2));
				if(!$db_res->Fetch()) {
					$this->content .= '
						<div class="wizard-input-form-block">
							<label for="COMPANY_ADDRESS">' . GetMessage("WIZ_SHOP_PRICE_BASE_TITLE") . '</label>
							<div class="wizard-input-form-block-content">
								' . GetMessage("WIZ_SHOP_PRICE_BASE_TEXT1") . '<br><br>
								' . $this->ShowCheckboxField("installPriceBASE", "Y",
								(array("id" => "install-demo-data")))
								. ' <label for="install-demo-data">' . GetMessage("WIZ_SHOP_PRICE_BASE_TEXT2") . '</label><br />
							</div>
						</div>';
				}
			}

			$this->content .= '</div>';
		}
	}

	function OnPostForm(){
		$wizard =& $this->GetWizard();
		CModule::IncludeModule("firstbit.beautyshop");

		$siteID = $wizard->GetVar("siteID");
		$firstBit = new CFirstbitBeautyshop($siteID);

		$resLogo = $this->SaveFile("LOGO", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 50, "max_width" => 220, "make_preview" => "Y"));
		$resLogoAlt = $this->SaveFile("LOGO_ALT", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 50, "max_width" => 220, "make_preview" => "Y"));
		if(is_int($resLogo)) {
			$wizard->SetVar("LOGO", $resLogo);
		}
		if(is_int($resLogoAlt)) {
			$wizard->SetVar("LOGO_ALT", $resLogoAlt);
		}
	}

}

class CatalogSettings extends CWizardStep
{
	function InitStep()
	{
		$this->SetStepID("catalog_settings");
		$this->SetTitle(GetMessage("WIZ_STEP_CT"));
		if(LANGUAGE_ID != "ru")
			$this->SetNextStep("pay_system");
		else
			$this->SetNextStep("shop_settings");
		$this->SetPrevStep("site_settings");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));

		$wizard =& $this->GetWizard();
		$siteID = $wizard->GetVar("siteID");

		$subscribe = COption::GetOptionString("sale", "subscribe_prod", "");
		$arSubscribe = unserialize($subscribe);

		$wizard->SetDefaultVars(
			Array(
				"catalogSubscribe" => (isset($arSubscribe[$siteID])) ? ($arSubscribe[$siteID]['use'] == "Y" ? "Y" : false) : "Y",//COption::GetOptionString("firstbit.beautyshop", "catalogSubscribe", "Y", $siteID),
				"catalogView" => COption::GetOptionString("firstbit.beautyshop", "catalogView", "list", $siteID),
				"useStoreControl" => COption::GetOptionString("catalog", "default_use_store_control", "Y"),
				"productReserveCondition" => COption::GetOptionString("sale", "product_reserve_condition", "P")
			)
		);
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();

		/*$this->content .= '
			<div class="wizard-input-form-block">
				<div class="wizard-catalog-title">'.GetMessage("WIZ_STEP_CT").'</div>
				<div class="wizard-catalog-form">
					<div class="wizard-catalog-form-item">
						'. $this->ShowCheckboxField("catalogSubscribe", "Y", (array("id" => "catalog-suscribe")))
						.' <label for="catalog-suscribe">'.GetMessage("WIZ_CATALOG_SUBSCRIBE").'</label><br />'
						.' <p>'.GetMessage("WIZ_CATALOG_SUBSCRIBE_DESCR").'</p>
					</div>
				</div>
			</div>';

		$this->content .= '
			<div class="wizard-input-form-block">
				<div class="wizard-catalog-title">'.GetMessage("WIZ_CATALOG_VIEW").'</div>
				<div class="wizard-input-form-block-content">';


		$arCatalogViews = array(
			"bar" => array(
				"NAME" => GetMessage("WIZ_CATALOG_VIEW_BAR"),
				"DESCRIPTION" => GetMessage("WIZ_CATALOG_VIEW_BAR_DESCR"),
				"PREVIEW" => $wizard->GetPath()."/images/".LANGUAGE_ID."/view-bar-small.png",
				"SCREENSHOT" => $wizard->GetPath()."/images/".LANGUAGE_ID."/view-bar.png",
			),
			"list" => array(
				"NAME" => GetMessage("WIZ_CATALOG_VIEW_LIST"),
				"DESCRIPTION" => GetMessage("WIZ_CATALOG_VIEW_LIST_DESCR"),
				"PREVIEW" => $wizard->GetPath()."/images/".LANGUAGE_ID."/view-list-small.png",
				"SCREENSHOT" => $wizard->GetPath()."/images/".LANGUAGE_ID."/view-list.png",
			),
		);

		global $SHOWIMAGEFIRST;
		$SHOWIMAGEFIRST = true;

		$this->content .= '<div class="wizard-list-view-block ">';
		foreach ($arCatalogViews as $catalogViewID => $catalogView)
		{
			$this->content .= '<span class="wizard-list-view">';
			$this->content .= '
			<span class="wizard-list-view-top">
				'.$this->ShowRadioField("catalogView", $catalogViewID, Array("id" => $catalogViewID)).'
				<label for="'.$catalogViewID.'">'.$catalogView["NAME"].'</label>
			</span>';

			if ($catalogView["SCREENSHOT"] && $catalogView["PREVIEW"])
				$this->content .= CFile::Show2Images($catalogView["PREVIEW"], $catalogView["SCREENSHOT"], 100, 100, '');
			else
				$this->content .= CFile::ShowImage($catalogView["SCREENSHOT"], 100, 100, '', "", true);

			$this->content .= '<span class="wizard-list-view-description">'.$catalogView["DESCRIPTION"].'</span>';
			$this->content .= '</span>';

		}
		$this->content .= "</div>";

		$this->content .=
			'</div>
		</div>';*/
		$this->content .= '
			<div class="wizard-input-form-block">
				<div class="wizard-catalog-title">'.GetMessage("WIZ_CATALOG_USE_STORE_CONTROL").'</div>
				<div>
					<div class="wizard-catalog-form-item">
						'.$this->ShowCheckboxField("useStoreControl", "Y", array("id" => "use-store-control"))
			.'<label for="use-store-control">'.GetMessage("WIZ_STORE_CONTROL").'</label>
					</div>';

		$arConditions = array(
			"O" => GetMessage("SALE_PRODUCT_RESERVE_1_ORDER"),
			"P" => GetMessage("SALE_PRODUCT_RESERVE_2_PAYMENT"),
			"D" => GetMessage("SALE_PRODUCT_RESERVE_3_DELIVERY"),
			"S" => GetMessage("SALE_PRODUCT_RESERVE_4_DEDUCTION")
		);


		foreach($arConditions as $conditionID => $conditionName)
		{
			$arReserveConditions[$conditionID] = $conditionName;
		}
		$this->content .= '
			<div class="wizard-catalog-form-item">'
			.$this->ShowSelectField("productReserveCondition", $arReserveConditions).
			'<label>'.GetMessage("SALE_PRODUCT_RESERVE_CONDITION").'</label>
			</div>';
		$this->content .= '</div>
			</div>';

		$this->content .= '<script>
			function ImgShw(ID, width, height, alt)
			{
				var scroll = "no";
				var top=0, left=0;
				if(width > screen.width-10 || height > screen.height-28) scroll = "yes";
				if(height < screen.height-28) top = Math.floor((screen.height - height)/2-14);
				if(width < screen.width-10) left = Math.floor((screen.width - width)/2-5);
				width = Math.min(width, screen.width-10);
				height = Math.min(height, screen.height-28);
				var wnd = window.open("","","scrollbars="+scroll+",resizable=yes,width="+width+",height="+height+",left="+left+",top="+top);
				wnd.document.write(
					"<html><head>"+
						"<"+"script type=\"text/javascript\">"+
						"function KeyPress()"+
						"{"+
						"	if(window.event.keyCode == 27) "+
						"		window.close();"+
						"}"+
						"</"+"script>"+
						"<title></title></head>"+
						"<body topmargin=\"0\" leftmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" onKeyPress=\"KeyPress()\">"+
						"<img src=\""+ID+"\" border=\"0\" alt=\""+alt+"\" />"+
						"</body></html>"
				);
				wnd.document.close();
			}
		</script>';

	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();

	}
}

class ShopSettings extends CWizardStep {
	function InitStep() {
		if(CModule::IncludeModule("firstbit.beautyshop")) {
			$this->SetStepID("shop_settings");
			$this->SetTitle(GetMessage("WIZ_STEP_SS"));
			$this->SetNextStep("person_type");
			$this->SetPrevStep("catalog_settings");
			$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
			$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));

			$wizard =& $this->GetWizard();

			$siteID = $wizard->GetVar("siteID");
			$firstBit = new CFirstbitBeautyshop($siteID);

			$wizard->SetDefaultVars(
				Array(
					"shopLocalization" => CFirstbitBeautyshop::queryOption("shopLocalization", $siteID),
					"COMPANY_EMAIL" => $firstBit->options["COMPANY_EMAIL"],
					"COMPANY_PHONE" => $firstBit->options["COMPANY_PHONE"],
					"COMPANY_NAME" => $firstBit->options["COMPANY_NAME"],
					"shopLocation" => CFirstbitBeautyshop::queryOption("shopLocation", $siteID),
					"COMPANY_ADDRESS" => $firstBit->options["COMPANY_ADDRESS"],
					"COMPANY_COORDINATES" => $firstBit->options["COMPANY_COORDINATES"],
					"COMPANY_INN" => $firstBit->options["COMPANY_INN"],
					"COMPANY_KPP" => $firstBit->options["COMPANY_KPP"],
					"COMPANY_ACCOUNT" => $firstBit->options["COMPANY_ACCOUNT"],
					"COMPANY_BANK" => $firstBit->options["COMPANY_BANK"],
					"COMPANY_BIK" => $firstBit->options["COMPANY_BIK"],
					"COMPANY_KS" => $firstBit->options["COMPANY_KS"],
					"installPriceBASE" => CFirstbitBeautyshop::queryOption("installPriceBASE", $siteID),
				)
			);
		}
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();

		if (!CModule::IncludeModule("catalog"))
		{
			$this->content .= "<p style='color:red'>".GetMessage("WIZ_NO_MODULE_CATALOG")."</p>";
			$this->SetNextStep("shop_settings");
		}
		else
		{
			$currentLocalization = $wizard->GetVar("shopLocalization");
			if (empty($currentLocalization))
				$currentLocalization = $wizard->GetDefaultVar("shopLocalization");

			$this->content .= '
				<div class="wizard-input-form">';

			$this->content .= '
				<div class="wizard-input-form-block">
					<div class="wizard-metadata-title">' . GetMessage("WIZ_COMPANY_DATA") . '</div>
					<label class="wizard-input-title" for="COMPANY_NAME">'.GetMessage("WIZ_COMPANY_NAME").'</label>
					'.$this->ShowInputField('text', 'COMPANY_NAME', array("id" => "COMPANY_NAME", "class" => "wizard-field")).'
				</div>';

			$this->content .= '
				<div class="wizard-input-form-block">
					<label class="wizard-input-title" for="COMPANY_EMAIL">'.GetMessage("WIZ_COMPANY_EMAIL").'</label>
					'.$this->ShowInputField('text', 'COMPANY_EMAIL', array("id" => "COMPANY_EMAIL", "class" => "wizard-field")).'
				</div>';

			$this->content .= '
				<div class="wizard-input-form-block">
					<label class="wizard-input-title" for="COMPANY_PHONE">'.GetMessage("WIZ_COMPANY_PHONE").'</label>
					'.$this->ShowInputField('text', 'COMPANY_PHONE', array("id" => "COMPANY_PHONE", "class" => "wizard-field")).'
				</div>';

			$this->content .= '
				<div class="wizard-input-form-block">
					<label class="wizard-input-title" for="COMPANY_ADDRESS">'.GetMessage("WIZ_COMPANY_ADDRESS").'</label>
				'.$this->ShowInputField('text', 'COMPANY_ADDRESS', array("id" => "COMPANY_ADDRESS", "class" => "wizard-field")).'
				</div>';

			$this->content .= '
				<div class="wizard-input-form-block">
					<label for="COMPANY_COORDINATES" class="wizard-input-title">' . GetMessage("WIZ_COMPANY_COORDINATES") . '</label>'
				.$this->ShowInputField('text', 'COMPANY_COORDINATES', array("id" => "COMPANY_COORDINATES", "class" => "wizard-field")) . '
				</div>';
			$this->content .= '
				<div class="map_contacts">
					<div id="ya_map_container" style="width: 100%; height: 330px; margin-bottom: 40px;" onload="init();"></div>
				</div>
				<script>
					var myMap;
					function init() {
						var myMap = new ymaps.Map(\'ya_map_container\', {
							center: ['.$wizard->GetVar("COMPANY_COORDINATES", true).'],
							zoom: 15,
							behaviors: [\'default\', \'scrollZoom\'],
							controls: [\'zoomControl\', \'searchControl\']
						}),
						myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {
							draggable: true
						});
						myMap.geoObjects.add(myPlacemark);
						myMap.cursors.push(\'arrow\');
						myPlacemark.events.add(\'dragend\', function(e) {
							var thisPlacemark = e.get(\'target\');
							var coords = thisPlacemark.geometry.getCoordinates();
							$("#COMPANY_COORDINATES").val(coords.join(\',\'));
						});
						myMap.events.add(\'click\', function (e) {
							var coords = e.get(\'coords\');
							myPlacemark.geometry.setCoordinates(coords);
							$("#COMPANY_COORDINATES").val(coords.join(\',\'));
						});
					};
				</script>
				<script src="https://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU&onload=init"></script>
				';

			$this->content .= '
				<div class="wizard-input-form-block">
					<div class="wizard-metadata-title">' . GetMessage("WIZ_BANK_DATA") . '</div>
					<label class="wizard-input-title" for="COMPANY_INN">'.GetMessage("WIZ_COMPANY_INN").'</label>
					'.$this->ShowInputField('text', 'COMPANY_INN', array("id" => "COMPANY_INN", "class" => "wizard-field")).'
				</div>';

			$this->content .= '
				<div class="wizard-input-form-block">
					<label class="wizard-input-title" for="COMPANY_KPP">'.GetMessage("WIZ_COMPANY_KPP").'</label>
					'.$this->ShowInputField('text', 'COMPANY_KPP', array("id" => "COMPANY_KPP", "class" => "wizard-field")).'
				</div>';

			$this->content .= '
				<div class="wizard-input-form-block">
					<label class="wizard-input-title" for="COMPANY_ACCOUNT">'.GetMessage("WIZ_COMPANY_ACCOUNT").'</label>
					'.$this->ShowInputField('text', 'COMPANY_ACCOUNT', array("id" => "COMPANY_ACCOUNT", "class" => "wizard-field")).'
				</div>';

			$this->content .= '
				<div class="wizard-input-form-block">
					<label class="wizard-input-title" for="COMPANY_BANK">'.GetMessage("WIZ_COMPANY_BANK").'</label>
					'.$this->ShowInputField('text', 'COMPANY_BANK', array("id" => "COMPANY_BANK", "class" => "wizard-field")).'
				</div>';

			$this->content .= '
				<div class="wizard-input-form-block">
					<label class="wizard-input-title" for="COMPANY_BIK">'.GetMessage("WIZ_COMPANY_BIK").'</label>
					'.$this->ShowInputField('text', 'COMPANY_BIK', array("id" => "COMPANY_BIK", "class" => "wizard-field")).'
				</div>';

			$this->content .= '
				<div class="wizard-input-form-block">
					<label class="wizard-input-title" for="COMPANY_KS">'.GetMessage("WIZ_COMPANY_KS").'</label>
					'.$this->ShowInputField('text', 'COMPANY_KS', array("id" => "COMPANY_KS", "class" => "wizard-field")).'
				</div>';

			$this->content .= '
			</div><!--ru-->
			';

			if (CModule::IncludeModule("catalog")) {
				$db_res = CCatalogGroup::GetGroupsList(array("CATALOG_GROUP_ID"=>'1', "BUY"=>"Y", "GROUP_ID"=>2));
				if (!$db_res->Fetch()) {
					$this->content .= '
					<div class="wizard-input-form-block">
						<div class="wizard-catalog-title">'.GetMessage("WIZ_SHOP_PRICE_BASE_TITLE").'</div>
						<div class="wizard-input-form-block-content">
							'. GetMessage("WIZ_SHOP_PRICE_BASE_TEXT1") .'<br><br>
							'. $this->ShowCheckboxField("installPriceBASE", "Y",
							(array("id" => "install-demo-data")))
						. ' <label for="install-demo-data">'.GetMessage("WIZ_SHOP_PRICE_BASE_TEXT2").'</label><br />

						</div>
					</div>';
				}
			}

			$this->content .= '</div>';

			$this->content .= '
				<script>
					function langReload()
					{
						var objSel = document.getElementById("localization_select");
						var locSelected = objSel.options[objSel.selectedIndex].value;
						document.getElementById("ru_bank_details").style.display = (locSelected == "ru" || locSelected == "kz" || locSelected == "bl") ? "block" : "none";
						document.getElementById("ua_bank_details").style.display = (locSelected == "ua") ? "block" : "none";
						/*document.getElementById("kz_bank_details").style.display = (locSelected == "kz") ? "block" : "none";*/
					}
				</script>
			';
		}
	}

}

class PersonType extends CWizardStep
{
	function InitStep()
	{
		$this->SetStepID("person_type");
		$this->SetTitle(GetMessage("WIZ_STEP_PT"));
		$this->SetNextStep("pay_system");
		$this->SetPrevStep("shop_settings");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));

		$wizard =& $this->GetWizard();
		$shopLocalization = $wizard->GetVar("shopLocalization", true);
		$siteID = $wizard->GetVar("siteID");

			$wizard->SetDefaultVars(
				Array(
					"personType" => Array(
						"fiz" =>  COption::GetOptionString("firstbit.beautyshop", "personTypeFiz", "Y", $siteID),
						"ur" => COption::GetOptionString("firstbit.beautyshop", "personTypeUr", "Y", $siteID),
					)
				)
			);
	}

	function ShowStep()
	{

		$wizard =& $this->GetWizard();
		$shopLocalization = $wizard->GetVar("shopLocalization", true);

		$this->content .= '<div class="wizard-input-form">';
		$this->content .= '
		<div class="wizard-input-form-block">
			<!--<div class="wizard-catalog-title">'.GetMessage("WIZ_PERSON_TYPE_TITLE").'</div>-->
			<div style="padding-top:15px">
				<div class="wizard-input-form-field wizard-input-form-field-checkbox">
					<div class="wizard-catalog-form-item">
						'.$this->ShowCheckboxField('personType[fiz]', 'Y', (array("id" => "personTypeF"))).
			' <label for="personTypeF">'.GetMessage("WIZ_PERSON_TYPE_FIZ").'</label><br />
					</div>
					<div class="wizard-catalog-form-item">
						'.$this->ShowCheckboxField('personType[ur]', 'Y', (array("id" => "personTypeU"))).
			' <label for="personTypeU">'.GetMessage("WIZ_PERSON_TYPE_UR").'</label><br />
					</div>';
		if ($shopLocalization == "ua")
			$this->content .=
				'<div class="wizard-catalog-form-item">'
				.$this->ShowCheckboxField('personType[fiz_ua]', 'Y', (array("id" => "personTypeFua"))).
				' <label for="personTypeFua">'.GetMessage("WIZ_PERSON_TYPE_FIZ_UA").'</label>
					</div>';
		$this->content .= '
				</div>
			</div>
			<div class="wizard-catalog-form-item">'.GetMessage("WIZ_PERSON_TYPE").'<div>
		</div>';
		$this->content .= '</div>';
	}

	function OnPostForm()
	{
		$wizard = &$this->GetWizard();
		$personType = $wizard->GetVar("personType");

		if (empty($personType["fiz"]) && empty($personType["ur"]))
			$this->SetError(GetMessage('WIZ_NO_PT'));
	}

}

class PaySystem extends CWizardStep
{
	function InitStep()
	{
		$this->SetStepID("pay_system");
		$this->SetTitle(GetMessage("WIZ_STEP_PS"));
		$this->SetNextStep("data_install");
		if(LANGUAGE_ID != "ru")
			$this->SetPrevStep("catalog_settings");
		else
			$this->SetPrevStep("person_type");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));

		$wizard =& $this->GetWizard();

		if(LANGUAGE_ID == "ru")
		{
			$shopLocalization = $wizard->GetVar("shopLocalization", true);

			if ($shopLocalization == "ua")
				$wizard->SetDefaultVars(
					Array(
						"paysystem" => Array(
							"cash" => "Y",
							"oshad" => "Y",
							"bill" => "Y",
						),
						"delivery" => Array(
							"courier" => "Y",
							"self" => "Y",
						)
					)
				);
			else
				$wizard->SetDefaultVars(
					Array(
						"paysystem" => Array(
							"cash" => "Y",
							"sber" => "Y",
							"bill" => "Y",
							"collect" => "Y"  //cash on delivery
						),
						"delivery" => Array(
							"courier" => "Y",
							"self" => "Y",
							"russianpost" => "N",
							"rus_post" => "N",
							"rus_post_first" => "N",
							"ua_post" => "N",
							"kaz_post" => "N"
						)
					)
				);
		}
		else
		{
			$wizard->SetDefaultVars(
				Array(
					"paysystem" => Array(
						"cash" => "Y",
						"paypal" => "Y",
					),
					"delivery" => Array(
						"courier" => "Y",
						"self" => "Y",
						"dhl" => "Y",
						"ups" => "Y",
					)
				)
			);
		}
	}

	function OnPostForm()
	{
		$wizard = &$this->GetWizard();
		$paysystem = $wizard->GetVar("paysystem");

		if (
			empty($paysystem["cash"])
			&& empty($paysystem["sber"])
			&& empty($paysystem["bill"])
			&& empty($paysystem["paypal"])
			&& empty($paysystem["oshad"])
			&& empty($paysystem["collect"])
		)
			$this->SetError(GetMessage('WIZ_NO_PS'));
		/*payer type
				if(LANGUAGE_ID == "ru")
				{
					$personType = $wizard->GetVar("personType");

					if (empty($personType["fiz"]) && empty($personType["ur"]))
						$this->SetError(GetMessage('WIZ_NO_PT'));
				}
		===*/
	}

	function ShowStep()
	{

		$wizard =& $this->GetWizard();
		$shopLocalization = $wizard->GetVar("shopLocalization", true);

		$personType = $wizard->GetVar("personType");

		$arAutoDeliveries = array();
		if (CModule::IncludeModule("sale"))
		{
			$dbRes = \Bitrix\Sale\Delivery\Services\Table::getList(array(
				'filter' => array(
					'=CLASS_NAME' => array(
						'\Sale\Handlers\Delivery\SpsrHandler',
						'\Bitrix\Sale\Delivery\Services\Automatic'
					)
				),
				'select' => array('ID', 'CODE', 'ACTIVE', 'CLASS_NAME')
			));

			while($dlv = $dbRes->fetch())
			{
				if($dlv['CLASS_NAME'] == '\Sale\Handlers\Delivery\SpsrHandler')
					$arAutoDeliveries['spsr'] = $dlv['ACTIVE'];
				elseif(!empty($dlv['CODE']))
					$arAutoDeliveries[$dlv['CODE']] = $dlv['ACTIVE'];
			}
		}
		$siteID = WizardServices::GetCurrentSiteID($wizard->GetVar("siteID"));

		$this->content .= '<div class="wizard-input-form">';
		$this->content .= '
		<div class="wizard-input-form-block">
			<div class="wizard-catalog-title">'.GetMessage("WIZ_PAY_SYSTEM_TITLE").'</div>
			<div>
				<div class="wizard-input-form-field wizard-input-form-field-checkbox">
					<div class="wizard-catalog-form-item">
						'.$this->ShowCheckboxField('paysystem[cash]', 'Y', (array("id" => "paysystemC"))).
			' <label for="paysystemC">'.GetMessage("WIZ_PAY_SYSTEM_C").'</label>
					</div>';

		if(LANGUAGE_ID == "ru")
		{
			if($shopLocalization == "ua" && ($personType["fiz"] == "Y" || $personType["fiz_ua"] == "Y"))
				$this->content .=
					'<div class="wizard-catalog-form-item">'.
					$this->ShowCheckboxField('paysystem[oshad]', 'Y', (array("id" => "paysystemO"))).
					' <label for="paysystemS">'.GetMessage("WIZ_PAY_SYSTEM_O").'</label>
							</div>';
			if ($shopLocalization == "ru")
			{
				if ($personType["fiz"] == "Y")
					$this->content .=
						'<div class="wizard-catalog-form-item">'.
						$this->ShowCheckboxField('paysystem[sber]', 'Y', (array("id" => "paysystemS"))).
						' <label for="paysystemS">'.GetMessage("WIZ_PAY_SYSTEM_S").'</label>
								</div>';
				if ($personType["fiz"] == "Y" || $personType["ur"] == "Y")
					$this->content .=
						'<div class="wizard-catalog-form-item">'.
						$this->ShowCheckboxField('paysystem[collect]', 'Y', (array("id" => "paysystemCOL"))).
						' <label for="paysystemCOL">'.GetMessage("WIZ_PAY_SYSTEM_COL").'</label>
								</div>';
			}
			if($personType["ur"] == "Y")
			{
				$this->content .=
					'<div class="wizard-catalog-form-item">'.
					$this->ShowCheckboxField('paysystem[bill]', 'Y', (array("id" => "paysystemB"))).
					' <label for="paysystemB">';
				if ($shopLocalization == "ua")
					$this->content .= GetMessage("WIZ_PAY_SYSTEM_B_UA");
				else
					$this->content .= GetMessage("WIZ_PAY_SYSTEM_B");
				$this->content .= '</label>
							</div>';
			}
		}
		else
		{
			$this->content .=
				'<div class="wizard-catalog-form-item">'.
				$this->ShowCheckboxField('paysystem[paypal]', 'Y', (array("id" => "paysystemP"))).
				' <label for="paysystemP">PayPal</label>
						</div>';
		}
		$this->content .= '</div>
			</div>
			<div class="wizard-catalog-form-item">'.GetMessage("WIZ_PAY_SYSTEM").'</div>
		</div>';
		if (
			LANGUAGE_ID != "ru" ||
			LANGUAGE_ID == "ru" &&
			(
				COption::GetOptionString("firstbit.beautyshop", "wizard_installed", "N", $siteID) != "Y"
				|| $shopLocalization == "ru" && ($arAutoDeliveries["russianpost"] != "Y" || $arAutoDeliveries["rus_post"] != "Y" || $arAutoDeliveries["rus_post_first"] != "Y")
				|| $shopLocalization == "ua" && ($arAutoDeliveries["ua_post"] != "Y")
				|| $shopLocalization == "kz" && ($arAutoDeliveries["kaz_post"] != "Y")
			)
		)
		{
			$this->content .= '
			<div class="wizard-input-form-block">
				<div class="wizard-catalog-title">'.GetMessage("WIZ_DELIVERY_TITLE").'</div>
				<div>
					<div class="wizard-input-form-field wizard-input-form-field-checkbox">';

			if(COption::GetOptionString("firstbit.beautyshop", "wizard_installed", "N", $siteID) != "Y")
			{
				$this->content .= '<div class="wizard-catalog-form-item">
								'.$this->ShowCheckboxField('delivery[courier]', 'Y', (array("id" => "deliveryC"))).
					' <label for="deliveryC">'.GetMessage("WIZ_DELIVERY_C").'</label>
							</div>
							<div class="wizard-catalog-form-item">
								'.$this->ShowCheckboxField('delivery[self]', 'Y', (array("id" => "deliveryS"))).
					' <label for="deliveryS">'.GetMessage("WIZ_DELIVERY_S").'</label>
							</div>';
			}
			if(LANGUAGE_ID == "ru")
			{

				if ($shopLocalization == "ru")
				{
					if ($arAutoDeliveries["russianpost"] != "Y")
						$this->content .=
							'<div class="wizard-catalog-form-item">'.
							$this->ShowCheckboxField('delivery[russianpost]', 'Y', (array("id" => "deliveryR"))).
							' <label for="deliveryR">'.GetMessage("WIZ_DELIVERY_R").'</label>
										</div>';
					if ($arAutoDeliveries["rus_post"] != "Y")
						$this->content .=
							'<div class="wizard-catalog-form-item">'.
							$this->ShowCheckboxField('delivery[rus_post]', 'Y', (array("id" => "deliveryR2"))).
							' <label for="deliveryR2">'.GetMessage("WIZ_DELIVERY_R2").'</label>
										</div>';
					if ($arAutoDeliveries["rus_post_first"] != "Y")
						$this->content .=
							'<div class="wizard-catalog-form-item">'.
							$this->ShowCheckboxField('delivery[rus_post_first]', 'Y', (array("id" => "deliveryRF"))).
							' <label for="deliveryRF">'.GetMessage("WIZ_DELIVERY_RF").'</label>
										</div>';
				}
				elseif ($shopLocalization == "ua")
				{
					if ($arAutoDeliveries["ua_post"] != "Y")
						$this->content .=
							'<div class="wizard-catalog-form-item">'.
							$this->ShowCheckboxField('delivery[ua_post]', 'Y', (array("id" => "deliveryU"))).
							' <label for="deliveryU">'.GetMessage("WIZ_DELIVERY_UA").'</label>
										</div>';
				}
				elseif ($shopLocalization == "kz")
				{
					if ($arAutoDeliveries["kaz_post"] != "Y")
						$this->content .=
							'<div class="wizard-catalog-form-item">'.
							$this->ShowCheckboxField('delivery[kaz_post]', 'Y', (array("id" => "deliveryK"))).
							' <label for="deliveryK">'.GetMessage("WIZ_DELIVERY_KZ").'</label>
										</div>';
				}
			}
			else
			{
				$this->content .=
					'<div class="wizard-catalog-form-item">'.
					$this->ShowCheckboxField('delivery[dhl]', 'Y', (array("id" => "deliveryD"))).
					' <label for="deliveryD">DHL</label>
								</div>';
				$this->content .=
					'<div class="wizard-catalog-form-item">'.
					$this->ShowCheckboxField('delivery[ups]', 'Y', (array("id" => "deliveryU"))).
					' <label for="deliveryU">UPS</label>
								</div>';
			}
			$this->content .= '
					</div>
				</div>
				<div class="wizard-catalog-form-item">'.GetMessage("WIZ_DELIVERY").'</div>
			</div>';
		}

		$this->content .= '
		<div>
			<div class="wizard-catalog-title">'.GetMessage("WIZ_LOCATION_TITLE").'</div>
			<div>
				<div class="wizard-input-form-field wizard-input-form-field-checkbox">';
		if(in_array(LANGUAGE_ID, array("ru", "ua")))
		{
			$this->content .=
				'<div class="wizard-catalog-form-item">'.
				$this->ShowRadioField("locations_csv", "loc_ussr.csv", array("id" => "loc_ussr"))
				." <label for=\"loc_ussr\">".GetMessage('WSL_STEP2_GFILE_USSR')."</label>
				</div>";
			$this->content .=
				'<div class="wizard-catalog-form-item">'.
				$this->ShowRadioField("locations_csv", "loc_ua.csv", array("id" => "loc_ua"))
				." <label for=\"loc_ua\">".GetMessage('WSL_STEP2_GFILE_UA')."</label>
				</div>";
			$this->content .=
				'<div class="wizard-catalog-form-item">'.
				$this->ShowRadioField("locations_csv", "loc_kz.csv", array("id" => "loc_kz"))
				." <label for=\"loc_kz\">".GetMessage('WSL_STEP2_GFILE_KZ')."</label>
				</div>";
		}
		$this->content .=
			'<div class="wizard-catalog-form-item">'.
			$this->ShowRadioField("locations_csv", "loc_usa.csv", array("id" => "loc_usa"))
			." <label for=\"loc_usa\">".GetMessage('WSL_STEP2_GFILE_USA')."</label>
			</div>";
		$this->content .=
			'<div class="wizard-catalog-form-item">'.
			$this->ShowRadioField("locations_csv", "loc_cntr.csv", array("id" => "loc_cntr"))
			." <label for=\"loc_cntr\">".GetMessage('WSL_STEP2_GFILE_CNTR')."</label>
			</div>";
		$this->content .=
			'<div class="wizard-catalog-form-item">'.
			$this->ShowRadioField("locations_csv", "", array("id" => "none", "checked" => "checked"))
			." <label for=\"none\">".GetMessage('WSL_STEP2_GFILE_NONE')."</label>
			</div>";

		$this->content .= '
				</div>
			</div>
		</div>';

		$this->content .= '<div class="wizard-catalog-form-item">'.GetMessage("WIZ_DELIVERY_HINT").'</div>';

		$this->content .= '</div>';
	}
}

class DataInstallStep extends CDataInstallWizardStep
{
	function CorrectServices(&$arServices)
	{
		if($_SESSION["BX_ESHOP_LOCATION"] == "Y")
			$this->repeatCurrentService = true;
		else
			$this->repeatCurrentService = false;

		$wizard =& $this->GetWizard();
		if($wizard->GetVar("installDemoData") != "Y")
		{
		}
	}
}

class FinishStep extends CFinishWizardStep
{
	function InitStep()
	{
		$this->SetStepID("finish");
		$this->SetNextStep("finish");
		$this->SetTitle(GetMessage("FINISH_STEP_TITLE"));
		$this->SetNextCaption(GetMessage("wiz_go"));
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();
		if ($wizard->GetVar("proactive") == "Y")
			COption::SetOptionString("statistic", "DEFENCE_ON", "Y");

		$siteID = WizardServices::GetCurrentSiteID($wizard->GetVar("siteID"));
		$rsSites = CSite::GetByID($siteID);
		$siteDir = "/";
		if ($arSite = $rsSites->Fetch())
			$siteDir = $arSite["DIR"];

		$wizard->SetFormActionScript(str_replace("//", "/", $siteDir."/?finish"));

		$this->CreateNewIndex();

		COption::SetOptionString("main", "wizard_solution", $wizard->solutionName, false, $siteID);

		$this->content .=
			'<table class="wizard-completion-table">
				<tr>
					<td class="wizard-completion-cell">'
			.GetMessage("FINISH_STEP_CONTENT").
			'</td>
				</tr>
			</table>';
		//	$this->content .= "<br clear=\"all\"><a href=\"/bitrix/admin/wizard_install.php?lang=".LANGUAGE_ID."&site_id=".$siteID."&wizardName=bitrix:eshop.mobile&".bitrix_sessid_get()."\" class=\"button-next\"><span id=\"next-button-caption\">".GetMessage("wizard_store_mobile")."</span></a><br>";

		if ($wizard->GetVar("installDemoData") == "Y")
			$this->content .= GetMessage("FINISH_STEP_REINDEX");


	}

}

