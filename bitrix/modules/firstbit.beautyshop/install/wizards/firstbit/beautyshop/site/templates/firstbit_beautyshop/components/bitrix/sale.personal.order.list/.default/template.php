<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(!empty($arResult['ERRORS']['FATAL'])):?>

	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>

	<?$component = $this->__component;?>
	<?if($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])):?>
		<?$APPLICATION->AuthForm('', false, false, 'N', false);?>
	<?endif?>

<?else:?>
	<?if(!empty($arResult['ORDERS'])):?>
	<div class="order_filter_block">
		<div class="order_sort_order_wrapper">
			<select name="order_sort_order" class="select_nofilter" onchange="orderHistorySearch();">
				<option value=""><?=GetMessage('SPOL_SHOW_ALL')?></option>
				<?foreach($arResult["INFO"]['STATUS'] as $statusInfo):?>
					<option value="<?=$statusInfo['ID']?>"><?=$statusInfo['NAME']?></option>
				<?endforeach?>
			</select>
			<script type="text/javascript">
				$('.order_filter_block select').select2({
					minimumResultsForSearch: Infinity
				});
			</script>
		</div>
		<div class="order_search_wraper">
			<input type="text" placeholder="<?=GetMessage('SPOL_SEARCH_PLACEHOLDER')?>" id="order_search" onkeyup="orderHistorySearch();" class="order_search">
			<a href="javascript:void(0);"><i class="fa fa-search"></i></a>
		</div>
	</div>
	<div class="order_list">
		<div class="table">
			<div class="thead hidden_xs">
				<div>
					<div class="column1"><?=GetMessage('SPOL_NUM_SIGN')?></div>
					<div class="column2"><?=GetMessage('SPOL_DATE_TITLE')?></div>
					<div class="column3"><?=GetMessage('SPOL_SUM_TITLE')?></div>
					<div class="column4"><?=GetMessage('SPOL_PAYER_TITLE')?></div>
					<div class="column5"><?=GetMessage('SPOL_SATUS_TITLE')?></div>
					<div class="column6">&nbsp;</div>
				</div>
			</div>
			<div class="tbody" id="order_list">
				<?foreach($arResult["ORDERS"] as $key => $order):?>
					<div data-order-number="<?=$order["ORDER"]["ACCOUNT_NUMBER"]?>" data-order-status="<?=$order["ORDER"]["STATUS_ID"]?>">
						<div class="column1">
							<a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?>" class="show_xs"><?=GetMessage('SPOL_NUM_ORDER_TITLE')?><?=$order["ORDER"]["ACCOUNT_NUMBER"]?></a>
							<a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?>" class="hidden_xs"><?=$order["ORDER"]["ACCOUNT_NUMBER"]?></a>
						</div>
						<div class="column2 title show_xs"><?=GetMessage('SPOL_DATE_TITLE')?></div>
						<div class="column2"><?=$order["ORDER"]["DATE_INSERT_FORMATED"];?></div>
						<div class="column3 title show_xs"><?=GetMessage('SPOL_SUM_TITLE')?></div>
						<div class="column3"><?=CurrencyFormat($order["ORDER"]["PRICE"], $order["ORDER"]["CURRENCY"])?></div>
						<div class="column4 title show_xs"><?=GetMessage('SPOL_PAYER_TITLE')?></div>
						<div class="column4"><?=$order["ORDER"]["PAYER"]?></div>
						<div class="column5 title show_xs"><?=GetMessage('SPOL_SATUS_TITLE')?></div>
						<div class="column5"><?=$arResult["INFO"]["STATUS"][$order["ORDER"]['STATUS_ID']]["NAME"]?></div>
						<div class="column6">
							<a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?>" class="order_info hidden_xs"><i class="fa fa-info-circle"></i></a>
							<a href="<?=htmlentities($order["ORDER"]["URL_TO_COPY"])?>" class="order_refresh"><i class="fa fa-refresh"></i></a>
							<a href="<?=$order["ORDER"]["URL_TO_CANCEL"]?>" class="order_delete"><i class="fa fa-times"></i></a>
						</div>
					</div>
				<?endforeach?>
			</div>
		</div>
		<?if(strlen($arResult['NAV_STRING'])):?>
			<?=$arResult['NAV_STRING']?>
		<?endif?>
	</div>
	<?else:?>
		<div class="page_empty_message active">
			<h3 class="page_empty_message_title"><?=GetMessage('SPOL_NO_ORDERS_TITLE')?></h3>
			<p class="page_empty_message_text"><?=GetMessage('SPOL_NO_ORDERS_TEXT',array("#CATALOG_LINK#" => $arParams['PATH_TO_CATALOG']))?></p>
		</div>
	<?endif?>
<?endif?>

