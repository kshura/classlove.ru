<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!defined("WIZARD_TEMPLATE_ID"))
	return;

$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID."_".WIZARD_SITE_ID;

CopyDirFiles(
	$_SERVER["DOCUMENT_ROOT"].WizardServices::GetTemplatesPath(WIZARD_RELATIVE_PATH."/site")."/".WIZARD_TEMPLATE_ID,
	$bitrixTemplateDir,
	$rewrite = true,
	$recursive = true, 
	$delete_after_copy = false,
	$exclude = "themes"
);

include($_SERVER["DOCUMENT_ROOT"].WizardServices::GetTemplatesPath(WIZARD_RELATIVE_PATH."/site")."/".WIZARD_TEMPLATE_ID."/themes/".WIZARD_THEME_ID."/description.php");

if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
$firstBit = new CFirstbitBeautyshop(WIZARD_SITE_ID);
$firstBit->generateCSS($arTemplate['COLOR_MAIN'], $arTemplate['COLOR_ADDITIONAL'], $bitrixTemplateDir."/styles.less", $bitrixTemplateDir."/styles.css");

CWizardUtil::ReplaceMacros($bitrixTemplateDir . "/js/script.js", array("SITE_DIR" => WIZARD_SITE_DIR));
CWizardUtil::ReplaceMacros($bitrixTemplateDir . "/header.php", Array("SITE_DIR" => WIZARD_SITE_DIR));
CWizardUtil::ReplaceMacros($bitrixTemplateDir . "/footer.php", Array("SITE_DIR" => WIZARD_SITE_DIR));


//Attach template to default site
$obSite = CSite::GetList($by = "def", $order = "desc", Array("LID" => WIZARD_SITE_ID));
if ($arSite = $obSite->Fetch()) {
	$arTemplates = Array();
	$found = false;
	$foundEmpty = false;
	$obTemplate = CSite::GetTemplateList($arSite["LID"]);
	while($arTemplate = $obTemplate->Fetch()) {
		if(!$found && strlen(trim($arTemplate["CONDITION"]))<=0) {
			$arTemplate["TEMPLATE"] = WIZARD_TEMPLATE_ID."_".WIZARD_SITE_ID;
			$found = true;
		}
		if($arTemplate["TEMPLATE"] == "empty") {
			$foundEmpty = true;
			continue;
		}
		$arTemplates[]= $arTemplate;
	}

	if (!$found)
		$arTemplates[]= Array("CONDITION" => "", "SORT" => 150, "TEMPLATE" => WIZARD_TEMPLATE_ID."_".WIZARD_SITE_ID);

	$arFields = Array(
		"TEMPLATE" => $arTemplates,
		"NAME" => $arSite["NAME"],
	);

	$obSite = new CSite();
	$obSite->Update($arSite["LID"], $arFields);
}

$wizrdTemplateId = $wizard->GetVar("wizTemplateID");
$wizrdTemplateId = "firstbit_beautyshop";
COption::SetOptionString("main", "wizard_template_id", $wizrdTemplateId, false, WIZARD_SITE_ID);

function ___writeToAreasFile($fn, $text) {
	$fd = @fopen($fn, "wb");
	if(!$fd)
		return false;

	if(false === fwrite($fd, $text)) {
		fclose($fd);
		return false;
	}

	fclose($fd);

	if(defined("BX_FILE_PERMISSIONS"))
		@chmod($fn, BX_FILE_PERMISSIONS);
}

//logo
$templateID = $wizard->GetVar("templateID");
$themeID = $wizard->GetVar($templateID."_themeID");

$fLogo = CFile::GetByID($wizard->GetVar("LOGO"));
$logo = $fLogo->Fetch();
$fLogoAlt = CFile::GetByID($wizard->GetVar("LOGO_ALT"));
$logoAlt = $fLogoAlt->Fetch();
if($fLogo || $fLogoAlt) {
	if ($fLogo) {
		$logoFileArray = CFile::MakeFileArray($wizard->GetVar("LOGO", true));
		$saveLogo = false;
		if($firstBit->options['LOGO']) {
			$curLogoFileArray = CFile::MakeFileArray($firstBit->options['LOGO']);
			if(md5_file($logoFileArray['tmp_name']) != md5_file($curLogoFileArray['tmp_name'])) {
				$saveLogo = true;
			}
		} else {
			$saveLogo = true;
		}
		if($saveLogo) {
			$res = CFile::SaveFile($logoFileArray + array("MODULE_ID" => "firstbit.beautyshop"), 'firstbit.beautyshop');
			$firstBit->setOptions(array("LOGO" => $res));
		}
		CFile::Delete($wizard->GetVar("LOGO"));
	}
	if($fLogoAlt) {
		$logoFileArray = CFile::MakeFileArray($wizard->GetVar("LOGO_ALT", true));
		$saveLogo = false;
		if($firstBit->options['LOGO_ALT']) {
			$curLogoFileArray = CFile::MakeFileArray($firstBit->options['LOGO_ALT']);
			if(md5_file($logoFileArray['tmp_name']) != md5_file($curLogoFileArray['tmp_name'])) {
				$saveLogo = true;
			}
		} else {
			$saveLogo = true;
		}
		if($saveLogo) {
			$res = CFile::SaveFile($logoFileArray + array("MODULE_ID" => "firstbit.beautyshop"), 'firstbit.beautyshop');
			$firstBit->setOptions(array("LOGO_ALT" => $res));
		}
		CFile::Delete($wizard->GetVar("LOGO_ALT"));
	}
}
?>
