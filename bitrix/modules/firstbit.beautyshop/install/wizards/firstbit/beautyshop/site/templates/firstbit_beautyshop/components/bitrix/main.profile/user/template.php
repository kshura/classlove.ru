<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
$frame = $this->createFrame()->begin();
?>

<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>

<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data" class="form form_general">
	<?=$arResult["BX_SESSION_CHECK"]?>
	<input type="hidden" name="lang" value="<?=LANG?>" />
	<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />

	<ul id="user_div_reg" class="form_fields">
		<li>
			<label><?=GetMessage('LOGIN')?></label>
			<span class="input input-disable"><? echo $arResult["arUser"]["LOGIN"]?></span>
			<input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
			<i class="fa fa-check-circle not-active"></i>
		</li>
		<li>
			<label><?=GetMessage('LAST_NAME')?></label>
			<input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" />
		</li>
		<li>
			<label><?=GetMessage('NAME')?></label>
			<input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" />
		</li>
		<li>
			<label><?=GetMessage('SECOND_NAME')?></font></label>
			<input type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" />
		</li>
		<li>
			<label><?=GetMessage('EMAIL')?></label>
			<input type="text" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" />
			<?if($arResult["EMAIL_REQUIRED"]):?><i class="fa fa-check-circle not-active"></i><?endif?>
		</li>

	</ul>
	<div class="form_message">
		<?ShowError($arResult["strProfileError"]);?>
		<?
		if ($arResult['DATA_SAVED'] == 'Y')
			ShowNote(GetMessage('PROFILE_DATA_SAVED'));
		?>
	</div>

	<div class="form_btn_wrapper btn_wrapper">
		<input type="submit" name="save" class="btn_round btn_color" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
	</div>
</form>