<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$APPLICATION->SetTitle(GetMessage("S_TITLE").' '.$arResult["TITLE"]);
$APPLICATION->SetPageProperty('title', GetMessage("S_TITLE").' '.$arResult["TITLE"]);
$this->SetFrameMode(true);
?>

<div class="grid_8 grid_8_sm margin_xs_10">
	<div class="store_detail_inner">
		<div class="left_border">
			<b><?=GetMessage("S_ADDRESS")?></b>
			<span><?=$arResult["ADDRESS"]?></span>
		</div>
		<div class="left_border">
			<b><?=GetMessage("S_PHONE")?></b>
			<?
			foreach($arResult['PHONE'] as $arPhoneIndex=>$arPhone) {
				?>
				<b><a href="tel:<? echo $arPhone['FORMAT'];?>"><? echo $arPhone['DISPLAY'];?></a></b>
				<?
				if(($arPhoneIndex+1)!=count($arResult['PHONE'])) {
					?>, <?
				}
			}
			?>
		</div>
		<div class="left_border">
			<b><?=GetMessage("S_SCHEDULE")?></b>
			<span><?=$arResult["SCHEDULE"]?></span>
		</div>
		<div class="store_description">
			<p><?=$arResult["DESCRIPTION"]?></p>
		</div>
		<?
		if(!empty($arResult['USER_FIELDS']['UF_MORE_PHOTO']['VALUE'])) {
			?>
			<ul class="store_gallery hidden_xs">
				<?
				foreach ($arResult['USER_FIELDS']['UF_MORE_PHOTO']['VALUE'] as $arPhoto) {
					?>
					<li class="store_photo"><a href="<?=$arPhoto['SRC']?>" class="fancybox" rel="gr1"><img
								src="<?=$arPhoto['PREVIEW_PICTURE']['src']?>" class="">
							<div class="store_photo_open">+</div>
						</a></li>
					<?
				}
				?>
			</ul>
			<?
		}
		?>
	</div>
</div>
<?
if(($arResult["GPS_N"]) != 0 && ($arResult["GPS_S"]) != 0)
{
	?>
	<div class="grid_8 grid_8_sm margin_xs_10">
		<div class="store_detail_inner">
			<?
			$JSplacemarks = array();
			$i = 0;
			?>
			<div id="map" style="width: 100%; height: 420px"></div>
		</div>
	</div>
	<div class="clear"></div>
	<script type="text/javascript">
		var myMap;
		function init() {
			var myMap = new ymaps.Map("map", {
				center: [<?=$arResult["GPS_N"]?>,<?=$arResult["GPS_S"]?>],
				behaviors: ['default', 'scrollZoom'],
				zoom: 10
			});
			myPlacemark1 = new ymaps.Placemark([<?=$arResult["GPS_N"]?>,<?=$arResult["GPS_S"]?>], {
					hintContent: '<?=$arResult["TITLE"]?>',
					balloonContent: '<div style="width:200px;">' +
					'<h3 class="city"><?=$arResult["TITLE"]?></h3>' +
					'<span class="street"><?=$arResult["ADDRESS"]?></span>' +
					<? foreach($arResult['PHONE'] as $arPhoneIndex=>$arPhone) { ?>
					'<span class="phone"><?=$arPhone["DISPLAY"]?></span>' +
					<? } ?>
					'<br/>' +
					'<a href="<?=$arResult['URL']?>" class="btn green-bg" style="width:100%"><?=GetMessage("S_DETAIL")?></a>' +
					'</div>'},
				{
					iconLayout: 'default#image',
					iconImageHref: '<?=SITE_TEMPLATE_PATH?>/img/YaIcon.png',
					iconImageSize: [39, 35],
					iconImageOffset: [-15, -32],
				}
			);
			myMap.geoObjects.add(myPlacemark1);

			$("[data-yamaps-placemark]").click(function(){
				var currentPlacemaker = eval('myPlacemark'+$(this).attr('data-yamaps-placemark'));
				myMap.setCenter(currentPlacemaker.geometry.getCoordinates(),15);
				currentPlacemaker.balloon.open();
			});
		};
	</script>
	<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU&onload=init"></script>
	<?
}
?>