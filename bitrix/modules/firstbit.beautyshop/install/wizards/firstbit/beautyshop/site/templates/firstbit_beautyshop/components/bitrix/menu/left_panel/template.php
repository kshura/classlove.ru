<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);

?>

<?
if (!empty($arResult)) {
	?>
	<ul id="leftpanel_menu" class="left_panel_menu">
		<?
		foreach ($arResult as $arMenuLink) {
			?>
			<li class="<?= $arMenuLink['SELECTED'] == '1' ? 'active open' : '' ?>">
				<a href="<?= $arMenuLink['LINK'] ?>" class="not_underline"><?= $arMenuLink['TEXT'] ?></a>
			</li>
			<?
		}
		?>
	</ul>
	<?
}
?>
