<div class="mobile_nav_wrapper" id="mobile_nav_wrapper">
	<div class="mobile_nav_inner">
		<ul>
			<li class="mobile_nav_item">
				<? $APPLICATION->IncludeComponent('firstbit:user', '', array(
					"PATH_TO_PERSONAL" => $arParams['PATH_TO_PERSONAL'],
					"PATH_TO_LOGIN" => $arParams['PATH_TO_LOGIN'],
					"PATH_TO_REGISTER" => $arParams['PATH_TO_REGISTER'],
					"PATH_TO_LOGOUT" => $arParams['PATH_TO_LOGOUT'],
				), $component, array('HIDE_ICONS' => 'Y')); ?>
			</li>
			<li class="mobile_nav_item">
				<div class="search_wrapper">
					<div class="search_form">
						<? $APPLICATION->IncludeComponent(
							"bitrix:search.form",
							"",
							Array(
								"PAGE"  => $arParams['PATH_TO_SEARCH'],
								"USE_SUGGEST" => "Y",
							), $component, array('HIDE_ICONS' => 'Y')
						); ?>
					</div>
				</div>
			</li>
			<li class="mobile_nav_item">
				<div class="icon_wrapper">
					<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line","left_panel",Array(
							"HIDE_ON_BASKET_PAGES" => "Y",
							"PATH_TO_BASKET" => $arParams['PATH_TO_BASKET'],
							"SHOW_EMPTY_VALUES" => "Y",
							"SHOW_NUM_PRODUCTS" => "Y",
							"SHOW_PRICE" => "Y",
							"SHOW_PRODUCTS" => "Y",
							"SHOW_SUMMARY" => "Y",
							"SHOW_TOTAL_PRICE" => "Y"
						)
					);?>
					<? $APPLICATION->IncludeComponent('firstbit:wishlist',
						'left_panel',
						array(
							'PATH_TO_WISHLIST' => $arParams['PATH_TO_WISHLIST']
						),
						$component,
						array('HIDE_ICONS' => 'Y')); ?>
					<? $APPLICATION->IncludeComponent('firstbit:compare',
						'left_panel',
						array(
							'CATALOG_IBLOCK_ID' => $arParams['CATALOG_IBLOCK_ID'],
							'PATH_TO_COMPARE' => $arParams['PATH_TO_COMPARE']
						),
						$component,
						array('HIDE_ICONS' => 'Y')); ?>

				</div>
			</li>
			<li class="mobile_nav_item">
				<?$APPLICATION->IncludeComponent("bitrix:menu", "left_panel", Array(
					"ROOT_MENU_TYPE" => "top_middle",
					"CHILD_MENU_TYPE" => "",
					'HEADER_RESULT'  => $arResult,
					'ITEMS_SHOW'  => $arResult['MIDDLE_MENU_ITEMS'],
					"MAX_LEVEL"  => "1",
					"USE_EXT"  => "Y",
					"DELAY"   => "N",
					"ALLOW_MULTI_SELECT" => "Y",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_CACHE_GET_VARS" => ""
				));?>
			</li>
		</ul>
	</div>
</div>
