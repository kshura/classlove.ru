$(document).ready(function () {
	var owl_slider1 = $('#owl_slider1');
	owl_slider1.owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		smartSpeed: 500,
		navContainer: '#owl_slider1_nav',
		navText: ["<i class=\"fa fa-chevron-left\"></i>","<i class=\"fa fa-chevron-right\"></i>"],
		afterInit: initSlider(owl_slider1),
	});
});