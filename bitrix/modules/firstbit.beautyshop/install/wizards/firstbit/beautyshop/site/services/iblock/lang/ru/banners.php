<?
$MESS["WZD_OPTION_BANNERS_1"] = "Элемент";
$MESS["WZD_OPTION_BANNERS_2"] = "Активность";
$MESS["WZD_OPTION_BANNERS_3"] = "Начало активности";
$MESS["WZD_OPTION_BANNERS_4"] = "Окончание активности";
$MESS["WZD_OPTION_BANNERS_5"] = "*Название";
$MESS["WZD_OPTION_BANNERS_6"] = "*Символьный код";
$MESS["WZD_OPTION_BANNERS_7"] = "Сортировка";
$MESS["WZD_OPTION_BANNERS_8"] = "Значения свойств";
$MESS["WZD_OPTION_BANNERS_9"] = "Ссылка";
$MESS["WZD_OPTION_BANNERS_10"] = "Вертикальное изображение";
$MESS["WZD_OPTION_BANNERS_11"] = "Узкое горизонтальное изображение";
$MESS["WZD_OPTION_BANNERS_12"] = "Широкое горизонтальное изображение";
