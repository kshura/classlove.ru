<?
$MESS['SHIPPING_TIME_TITLE'] = "Время доставки";

$MESS["STORE_TITLE_1"] = "ст.м. \"Улица 1905 года\"";
$MESS["STORE_ADDRESS_1"] = "г. Москва, Славянский б-р, д.13, стр.1";
$MESS["STORE_DESCRIPTION_1"] = "";
$MESS["STORE_GPS_N_1"] = "55.725574";
$MESS["STORE_GPS_S_1"] = "37.461498	";
$MESS["STORE_PHONE_1"] = "+7 (495) 554-35-98";
$MESS["STORE_EMAIL_1"] = "mail@yout-shop.ru";
$MESS["STORE_SCHEDULE_1"] = "Ежедневно 09:00-20:00";
$MESS["UF_SHIPPING_TIME_1"] = "Сегодня до 20:00";
$MESS["CAT_STORE_NAME_1"] = "Склад";

$MESS["STORE_TITLE_2"] = "ст. м. «Алексеевская»";
$MESS["STORE_ADDRESS_2"] = "г. Москва, пр-т Мира, д.91, к.1	";
$MESS["STORE_DESCRIPTION_2"] = "Магазин находится на ст. м.Войковская. Вам нежно выйти из метро (посленимй вагон из центра). При выходе из вестибюля направо.";
$MESS["STORE_GPS_N_2"] = "55.805256";
$MESS["STORE_GPS_S_2"] = "37.635254";
$MESS["STORE_PHONE_2"] = "+7 (499) 356-88-96";
$MESS["STORE_EMAIL_2"] = "user@example.com";
$MESS["STORE_SCHEDULE_2"] = "Магазин открыт 24 часа";
$MESS["UF_SHIPPING_TIME_2"] = "Сегодня";
$MESS["CAT_STORE_NAME_2"] = "Склад";

$MESS["STORE_TITLE_3"] = "ст. м. «Калужская»";
$MESS["STORE_ADDRESS_3"] = "г. Москва, ул. Обручева, д.34/63";
$MESS["STORE_DESCRIPTION_3"] = "";
$MESS["STORE_GPS_N_3"] = "55.654258";
$MESS["STORE_GPS_S_3"] = "37.539350";
$MESS["STORE_PHONE_3"] = "+7 (499) 659-88-23";
$MESS["STORE_EMAIL_3"] = "info@shop.ru";
$MESS["STORE_SCHEDULE_3"] = "Магазин открыт 24 часа";
$MESS["UF_SHIPPING_TIME_3"] = "Через 1-2 дня";
$MESS["CAT_STORE_NAME_3"] = "Склад";

$MESS["STORE_TITLE_4"] = "ст. м. «Красные ворота»";
$MESS["STORE_ADDRESS_4"] = "г. Москва, ул. Садовая-Спасская, д.3, стр.3";
$MESS["STORE_DESCRIPTION_4"] = "";
$MESS["STORE_GPS_N_4"] = "55.771357";
$MESS["STORE_GPS_S_4"] = "37.641850";
$MESS["STORE_PHONE_4"] = "+7 (495) 887-65-45";
$MESS["STORE_EMAIL_4"] = "mail@email.do";
$MESS["STORE_SCHEDULE_4"] = "Магазин открыт 24 часа";
$MESS["UF_SHIPPING_TIME_4"] = "Уточняйте по телефону";
$MESS["CAT_STORE_NAME_4"] = "Склад";
?>