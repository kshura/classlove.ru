<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
$aMenuLinksExt = array();

if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
$firstBit = new CFirstbitBeautyshop(SITE_ID);

if($firstBit->options['MENU_MIDDLE_TYPE'] != 1) {
	$max_level = $firstBit->options['MIDDLE_MENU_MAX_LEVEL'];
} else {
	$max_level = 2;
}

?>
<?
$aMenuLinksExt = $APPLICATION->IncludeComponent("bitrix:menu.sections", "", array(
	"IS_SEF" => "Y",
	"SEF_BASE_URL" => "#SITE_DIR#catalog/",
	"SECTION_PAGE_URL" => "#SECTION_CODE#/",
	"DETAIL_PAGE_URL" => "#SECTION_CODE#/#ELEMENT_CODE#",
	"IBLOCK_TYPE" => "firstbit_beautyshop_catalog",
	"IBLOCK_ID" => "#CATALOG_IBLOCK_ID#",
	"DEPTH_LEVEL" => $max_level,
	"CACHE_TYPE" => "N",
), false, Array('HIDE_ICONS' => 'Y'));
?>
<?
foreach ($aMenuLinksExt as $aMenuLinkIndex=>$aMenuLink) {
	$aMenuLinksExt[$aMenuLinkIndex][3]['IS_CAT'] = 'Y';
}
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>