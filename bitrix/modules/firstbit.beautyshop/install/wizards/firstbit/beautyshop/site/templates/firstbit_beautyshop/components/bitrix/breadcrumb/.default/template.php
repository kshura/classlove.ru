<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;
if(empty($arResult))
	return "";

$strReturn = '';

$strReturn .= '<section class="breadcrumb hidden_xs" id="navigation"><div class="container_16"><div class="grid_16 margin_sm_10"><ul class="breadcrumb_lvl1">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
	$child = ($index > 0? ' itemprop="child"' : '');

		$strReturn .= '<li class="breadcrumb_lvl1_item"><a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url">'.$title.'</a></li>';
}

$strReturn .= '</ul></div><div class="clear"></div></div></section>';

return $strReturn;
