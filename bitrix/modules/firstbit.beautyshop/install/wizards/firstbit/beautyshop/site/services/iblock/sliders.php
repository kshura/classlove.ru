<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock"))
	return;

if(COption::GetOptionString("firstbit.beautyshop", "wizard_installed", "N", WIZARD_SITE_ID) == "Y" && !WIZARD_INSTALL_DEMO_DATA){
	if($wizard->GetVar('rewriteIndex', true) && $wizard->GetVar('siteLogoSet', true)){
		$iblockCode = "firstbit_beautyshop_sliders_".WIZARD_SITE_ID;
		$iblockType = "firstbit_beautyshop_promotional";
		
		$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
		$iblockID = false; 
		if ($arIBlock = $rsIBlock->Fetch()) {
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", array("SLIDERS_IBLOCK_ID" => $iblockID));
		}
	}
	return;
}

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/sliders.xml";
$iblockXMLFileTmp = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/_tmp_sliders.xml";

$iblockCode = "firstbit_beautyshop_sliders_".WIZARD_SITE_ID;
$iblockType = "firstbit_beautyshop_promotional";

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
$iblockID = false; 
if ($arIBlock = $rsIBlock->Fetch()) {
	$iblockID = $arIBlock["ID"]; 
	if (WIZARD_INSTALL_DEMO_DATA) {
		CIBlock::Delete($arIBlock["ID"]); 
		$iblockID = false; 
	}
}

if($iblockID == false) {
	$permissions = Array(
			"1" => "X",
			"2" => "R"
		);
	$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "content_editor"));
	if($arGroup = $dbGroup -> Fetch())
	{
		$permissions[$arGroup["ID"]] = 'W';
	};

	if(file_exists($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile)) @copy($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile, $_SERVER["DOCUMENT_ROOT"].$iblockXMLFileTmp);
	CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"].$iblockXMLFileTmp, Array("IN_XML_SITE_ID" => WIZARD_SITE_ID));
	$iblockID = WizardServices::ImportIBlockFromXML($iblockXMLFileTmp, $iblockCode, $iblockType, WIZARD_SITE_ID, $permissions);
	if(file_exists($_SERVER["DOCUMENT_ROOT"].$iblockXMLFileTmp)) unlink($_SERVER["DOCUMENT_ROOT"].$iblockXMLFileTmp);

	if ($iblockID < 1)
		return;
	
	//IBlock fields
	$iblock = new CIBlock;
	$arFields = Array(
		"ACTIVE" => "Y",
		"FIELDS" => array (
			'IBLOCK_SECTION' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'ACTIVE' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => 'Y',
			),
			'ACTIVE_FROM' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '=today',
			),
			'ACTIVE_TO' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SORT' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'NAME' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => '',
				),
			'PREVIEW_PICTURE' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array (
					'FROM_DETAIL' => 'N',
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
					'DELETE_WITH_DETAIL' => 'N',
					'UPDATE_WITH_DETAIL' => 'N',
				),
			),
			'PREVIEW_TEXT_TYPE' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => 'text',
			),
			'PREVIEW_TEXT' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'DETAIL_PICTURE' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array (
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
				),
			),
			'DETAIL_TEXT_TYPE' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => 'text',
			),
			'DETAIL_TEXT' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'XML_ID' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'CODE' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => array (
					'UNIQUE' => 'Y',
					'TRANSLITERATION' => 'Y',
					'TRANS_LEN' => 100,
					'TRANS_CASE' => 'L',
					'TRANS_SPACE' => '_',
					'TRANS_OTHER' => '_',
					'TRANS_EAT' => 'Y',
					'USE_GOOGLE' => 'Y',
				),
			),
			'TAGS' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SECTION_NAME' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => '',
			),
			'SECTION_PICTURE' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array (
					'FROM_DETAIL' => 'N',
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
					'DELETE_WITH_DETAIL' => 'N',
					'UPDATE_WITH_DETAIL' => 'N',
				),
			),
			'SECTION_DESCRIPTION_TYPE' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => 'text',
			),
			'SECTION_DESCRIPTION' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SECTION_DETAIL_PICTURE' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array (
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
				),
			),
			'SECTION_XML_ID' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SECTION_CODE' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array (
					'UNIQUE' => 'N',
					'TRANSLITERATION' => 'N',
					'TRANS_LEN' => 100,
					'TRANS_CASE' => 'L',
					'TRANS_SPACE' => '_',
					'TRANS_OTHER' => '_',
					'TRANS_EAT' => 'Y',
					'USE_GOOGLE' => 'N',
				),
			),
		),
		"CODE" => $iblockCode,
		"XML_ID" => $iblockCode,
	);
	
	$iblock->Update($iblockID, $arFields);
}
else
{
	$arSites = array(); 
	$db_res = CIBlock::GetSite($iblockID);
	while ($res = $db_res->Fetch())
		$arSites[] = $res["LID"]; 
	if (!in_array(WIZARD_SITE_ID, $arSites))
	{
		$arSites[] = WIZARD_SITE_ID;
		$iblock = new CIBlock;
		$iblock->Update($iblockID, array("LID" => $arSites));
	}
}
$dbSite = CSite::GetByID(WIZARD_SITE_ID);
if($arSite = $dbSite -> Fetch())
	$lang = $arSite["LANGUAGE_ID"];
if(strlen($lang) <= 0)
	$lang = "ru";

$rsProperties = CIBlock::GetProperties($iblockID, Array("id" => "asc"), Array('IBLOCK_ID' => $iblockID));
$arIblockProperties = array();
while($arProperty = $rsProperties->GetNext()) {
	$arIblockProperties[$arProperty['CODE']] = $arProperty;
}

WizardServices::IncludeServiceLang("sliders.php", $lang);
//CUserOptions::SetOption(
//	"form",
//	"form_element_".$iblockID,
//	array (
//		'tabs' => 'edit1--#--'.GetMessage("WZD_OPTION_SLIDERS_1").'--,--ACTIVE--#--'.GetMessage("WZD_OPTION_SLIDERS_2").'--,--ACTIVE_FROM--#--'.GetMessage("WZD_OPTION_SLIDERS_3").'--,--ACTIVE_TO--#--'.GetMessage("WZD_OPTION_SLIDERS_4").'--,--NAME--#--'.GetMessage("WZD_OPTION_SLIDERS_5").'--,--CODE--#--'.GetMessage("WZD_OPTION_SLIDERS_6").'--,--SORT--#--'.GetMessage("WZD_OPTION_SLIDERS_7").'--,--IBLOCK_ELEMENT_PROP_VALUE--#----'.GetMessage("WZD_OPTION_SLIDERS_8").'--,--PROPERTY_'.$arIblockProperties["URL"]["ID"].'--#--'.GetMessage("WZD_OPTION_SLIDERS_9").'--,--PROPERTY_'.$arIblockProperties["SLIDER_PICTURE_BIG"]["ID"].'--#--'.GetMessage("WZD_OPTION_SLIDERS_10").'--,--PROPERTY_'.$arIblockProperties["SLIDER_PICTURE_SMALL"]["ID"].'--#--'.GetMessage("WZD_OPTION_SLIDERS_11").'--,--PROPERTY_'.$arIblockProperties["SLIDER_TITLE"]["ID"].'--#--'.GetMessage("WZD_OPTION_SLIDERS_12").'--,--PROPERTY_'.$arIblockProperties["SLIDER_PREVIEW_TEXT"]["ID"].'--#--'.GetMessage("WZD_OPTION_SLIDERS_13").'--,--PROPERTY_'.$arIblockProperties["SLIDER_BUTTON_MESSAGE"]["ID"].'--#--'.GetMessage("WZD_OPTION_SLIDERS_14").'--,--PROPERTY_'.$arIblockProperties["BANNER_PICTURE_MEDIUM"]["ID"].'--#--'.GetMessage("WZD_OPTION_SLIDERS_15").'--,--PROPERTY_'.$arIblockProperties["BANNER_PICTURE_SMALL"]["ID"].'--#--'.GetMessage("WZD_OPTION_SLIDERS_16").'--,--PROPERTY_'.$arIblockProperties["BANNER_BUTTON_MESSAGE"]["ID"].'--#--'.GetMessage("WZD_OPTION_SLIDERS_17").'--;--',
//	),
//	"Y"
//);
//CUserOptions::SetOption(
//	"list",
//	"tbl_iblock_list_".md5($iblockType.".".$iblockID),
//	array (
//		'columns' => 'ID,ACTIVE,SORT,NAME,PROPERTY_'.$arIblockProperties["URL"]["ID"].',PROPERTY_'.$arIblockProperties["SLIDER_PICTURE_BIG"]["ID"].',PROPERTY_'.$arIblockProperties["SLIDER_PICTURE_SMALL"]["ID"].',PROPERTY_'.$arIblockProperties["BANNER_PICTURE_MEDIUM"]["ID"].',PROPERTY_'.$arIblockProperties["BANNER_PICTURE_SMALL"]["ID"].',TIMESTAMP_X',
//		'by' => 'sort',
//		'order' => 'asc',
//		'page_size' => '20',
//	),
//	"Y"
//);

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", array("SLIDERS_IBLOCK_ID" => $iblockID));
?>