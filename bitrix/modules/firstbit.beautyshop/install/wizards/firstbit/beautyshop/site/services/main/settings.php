<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("firstbit.beautyshop")) die();
$firstBit = new CFirstbitBeautyshop(WIZARD_SITE_ID);

COption::SetOptionString("sale", "SHOP_SITE_".WIZARD_SITE_ID, WIZARD_SITE_ID);
COption::SetOptionString("main", "auth_components_template", "", false, WIZARD_SITE_ID);

COption::SetOptionString("fileman", "propstypes", serialize(array("description"=>GetMessage("MAIN_OPT_DESCRIPTION"), "keywords"=>GetMessage("MAIN_OPT_KEYWORDS"), "title"=>GetMessage("MAIN_OPT_TITLE"), "keywords_inner"=>GetMessage("MAIN_OPT_KEYWORDS_INNER"))), false, $siteID);
COption::SetOptionInt("search", "suggest_save_days", 250);
COption::SetOptionString("search", "use_tf_cache", "Y");
COption::SetOptionString("search", "use_word_distance", "Y");
COption::SetOptionString("search", "use_social_rating", "Y");
COption::SetOptionString("iblock", "use_htmledit", "Y");

COption::SetOptionString("main", "captcha_registration", "N");

if(!$firstBit->options['NO_PHOTO']) {
	$imageNoPhoto = WIZARD_ABSOLUTE_PATH . "/site/services/main/images/no_photo.png";
	if (file_exists($imageNoPhoto)) {
		$imageNoPhotoArray = CFile::MakeFileArray($imageNoPhoto);
		$res = CFile::SaveFile($imageNoPhotoArray + array("MODULE_ID" => "firstbit.beautyshop"), 'firstbit.beautyshop');
		$firstBit->setOptions(array("NO_PHOTO" => $res));
	}
}

$firstBit->setOptions(array(
	"SITE_LINK_LEGAL" => WIZARD_SITE_DIR."about/info/legal/", // deprecated
	"SITE_LINK_OFFER" => WIZARD_SITE_DIR."about/info/offer/", // deprecated
	"AGREEMENT_PAGE_LINK" => WIZARD_SITE_DIR."about/info/agreement/",
));

//socialservices
if (COption::GetOptionString("socialservices", "auth_services") == "") {
	$bRu = (LANGUAGE_ID == 'ru');
	$arServices = array(
		"VKontakte" => "N",
		"MyMailRu" => "N",
		"Twitter" => "N",
		"Facebook" => "N",
		"Livejournal" => "Y",
		"YandexOpenID" => ($bRu? "Y":"N"),
		"Rambler" => ($bRu? "Y":"N"),
		"MailRuOpenID" => ($bRu? "Y":"N"),
		"Liveinternet" => ($bRu? "Y":"N"),
		"Blogger" => "Y",
		"OpenID" => "Y",
		"LiveID" => "N",
	);
	COption::SetOptionString("socialservices", "auth_services", serialize($arServices));
}
?>