<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

	CModule::IncludeModule('fileman');
	$arMenuTypes = GetMenuTypes(WIZARD_SITE_ID);

	if(!array_key_exists('top_middle', $arMenuTypes) || $arMenuTypes['top_middle'] != GetMessage("WIZ_MENU_TOP_MIDDLE_DEFAULT"))
		$arMenuTypes['top_middle'] =  GetMessage("WIZ_MENU_TOP_MIDDLE_DEFAULT");

	if(!array_key_exists('top_middle_submenu', $arMenuTypes) || $arMenuTypes['top_middle_submenu'] != GetMessage("WIZ_MENU_TOP_MIDDLE_SUBMENU_DEFAULT"))
		$arMenuTypes['top_middle_submenu'] =  GetMessage("WIZ_MENU_TOP_MIDDLE_SUBMENU_DEFAULT");

	if(!array_key_exists('top', $arMenuTypes) || $arMenuTypes['top'] != GetMessage("WIZ_MENU_TOP_DEFAULT"))
		$arMenuTypes['top'] =  GetMessage("WIZ_MENU_TOP_DEFAULT");

	if(!array_key_exists('bottom', $arMenuTypes) || $arMenuTypes['bottom'] != GetMessage("WIZ_MENU_BOTTOM_DEFAULT"))
		$arMenuTypes['bottom'] =  GetMessage("WIZ_MENU_BOTTOM_DEFAULT");

	if(!array_key_exists('left', $arMenuTypes) || $arMenuTypes['left'] != GetMessage("WIZ_MENU_LEFT_DEFAULT"))
		$arMenuTypes['left'] =  GetMessage("WIZ_MENU_LEFT_DEFAULT");

	SetMenuTypes($arMenuTypes, WIZARD_SITE_ID);
	COption::SetOptionInt("fileman", "num_menu_param", 2, false ,WIZARD_SITE_ID);
?>