<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult["ITEMS"] as $arItemIndex=>$arItem) {
	$rsProduct = CIBlockElement::GetByID($arItem['PROPERTIES']['PRODUCT_ID']['VALUE']);
	if($arProduct = $rsProduct->GetNext()) {
		$arResult['ITEMS'][$arItemIndex]['PROPERTIES']['PRODUCT_ID']['VALUE'] = $arProduct;
	}
}