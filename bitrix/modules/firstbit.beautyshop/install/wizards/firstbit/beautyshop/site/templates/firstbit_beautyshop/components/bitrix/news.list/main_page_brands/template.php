<?php
/**
* Date: 028 28.03
* Time: 13:43
*/
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

$this->SetFrameMode(true);
?>
<div class="row brands <? if($arResult['TEMPLATE_VIEW'] == 'LIST') {?>grey<?}?>">
	<div class="container_16 section_inner">
		<div class="grid_16 section_line_head">
			<h1 class="section_title"><? echo GetMessage('TPL_BRANDS_TITLE'); ?></h1>
			<a href="<? echo $arResult['ITEMS'][0]['LIST_PAGE_URL']; ?>" class="section_more"><? echo GetMessage('TPL_BRANDS_SHOW_ALL'); ?></a>
			<?
			if($arResult['TEMPLATE_VIEW'] == 'IMAGES') {
				?>
				<div class="owl_nav small dark" id="owl_brands_nav"></div>
				<?
			}
			?>
		</div>
		<div class="clear"></div>
		<?
		if($arResult['TEMPLATE_VIEW'] == 'IMAGES') {
			?>
			<div id="owl_brands" class="slider_initial">
				<?
				foreach ($arResult['ITEMS'] as $arItem) {
					?>
					<div class="brand_item">
						<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $arItem['PREVIEW_PICTURE'] ?>" alt="<?= $arItem['NAME'] ?>" class="img-responsive"></a>
					</div>
					<?
				}
				?>
			</div>
			<?
		} elseif($arResult['TEMPLATE_VIEW'] == 'LIST') {
			?>
			<ul class="mainpage_brands_letter hidden_xs">
				<?
				foreach($arResult['ITEMS'] as $brandGroupIndex=>$brandGroup) {
					?>
					<li <?if($brandGroupIndex==0){?>class="active"<?}?>>
						<a href="#<?=$brandGroup['LETTER']?>" data-letter="<?=$brandGroup['LETTER']?>"><?=($brandGroup['LETTER'] == 'NUMERIC' || $brandGroup['LETTER'] == 'NON_LATIN' ? GetMessage('CT_BRANDS_LIST_'.$brandGroup['LETTER']) : $brandGroup['LETTER'])?></a>
					</li>
					<?
				}
				?>
			</ul>
			<div class="container_16">
				<?foreach($arResult['ITEMS'] as $brandGroup){?>
					<ul class="mainpage_brands_list" rel="<?=$brandGroup['LETTER']?>">
						<?foreach($brandGroup['ITEMS'] as $arBrand){?>
							<li class="grid_4 grid_3_sm grid_6_xs">
								<a href="<?=$arBrand['DETAIL_PAGE_URL']?>"><?=$arBrand['NAME']?></a>
							</li>
						<?}?>
					</ul>
				<?}?>
			</div>
			<div class="clear"></div>
			<?
		}
		?>
	</div>
</div>
