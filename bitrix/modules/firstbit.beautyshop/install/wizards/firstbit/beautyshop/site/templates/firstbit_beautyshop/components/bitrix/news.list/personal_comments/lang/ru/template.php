<?php
$MESS['TPL_REVIEW_ANSWER_TITLE'] = 'Ответ администрации магазина:';
$MESS['TPL_REVIEW_EMPTY_TITLE'] = 'Вы еще не оставили ни одного отзыва.';
$MESS['TPL_REVIEW_EMPTY_TEXT'] = 'Добавить отзыв на товар вы можете в <a href="#CATALOG_LINK#">каталоге</a> в карточке товара.';
