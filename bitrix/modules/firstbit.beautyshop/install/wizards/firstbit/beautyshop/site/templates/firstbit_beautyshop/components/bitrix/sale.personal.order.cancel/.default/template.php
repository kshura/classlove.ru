<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<a class="link_back" href="<?=$arResult["URL_TO_LIST"]?>"><i class="fa fa-angle-left "></i><?=GetMessage("SALE_RECORDS_LIST")?></a>

<div class="sale_order_full_table">
	<?if(strlen($arResult["ERROR_MESSAGE"])<=0):?>
		<form method="post" action="<?=POST_FORM_ACTION_URI?>" class="form form_general">
			<input type="hidden" name="CANCEL" value="Y">
			<?=bitrix_sessid_post()?>
			<input type="hidden" name="ID" value="<?=$arResult["ID"]?>">

			<ul id="user_div_reg" class="form_fields">
				<li class="form_text"><?=GetMessage("SALE_CANCEL_ORDER1") ?> <a href="<?=$arResult["URL_TO_DETAIL"]?>"><?=GetMessage("SALE_CANCEL_ORDER2")?> #<?=$arResult["ACCOUNT_NUMBER"]?></a>?<br/><b><?= GetMessage("SALE_CANCEL_ORDER3") ?></b></li>
				<li>
					<label><?= GetMessage("SALE_CANCEL_ORDER4") ?></label>
					<textarea name="REASON_CANCELED"></textarea><br /><br />
				</li>
			</ul>
			<div class="form_btn_wrapper btn_wrapper">
				<input type="submit" class="btn_round btn_color" name="action" value="<?=GetMessage("SALE_CANCEL_ORDER_BTN") ?>">
			</div>
		</form>
	<?else:?>
		<?=ShowError($arResult["ERROR_MESSAGE"]);?>
	<?endif;?>

</div>