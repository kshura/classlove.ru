<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arServices = Array(
	"main" => Array(
		"NAME" => GetMessage("SERVICE_MAIN_SETTINGS"),
		"STAGES" => Array(
			"files.php", // Copy bitrix files
			"search.php", // Indexing files
			"template.php", // Install template
			"menu.php", // Install menu
			"settings.php",
		),
	),
	"catalog" => Array(
		"NAME" => GetMessage("SERVICE_CATALOG_SETTINGS"),
		"STAGES" => Array(
			"index.php"
		),
	),
	"iblock" => Array(
		"NAME" => GetMessage("SERVICE_IBLOCK_DEMO_DATA"),
		"STAGES" => Array(
			"types.php", // Y
			"news.php", // Y
			"articles.php", // Y
			"services.php", // Y
			"references.php", // Y
			"references2.php", // Y
			"brands.php", // Y
			"sliders.php",
			"banners.php",
			"advantages.php",
			"social_services.php",
			"feedback.php",
			"callback.php",
			"catalog.php",
			"catalog2.php",//offers iblock import
			"catalog3.php",
			"catalog4.php",
			"sale.php",
			"reviews.php",
			"price_request.php",
			"quickbuy.php",
		),
	),
	"sale" => Array(
		"NAME" => GetMessage("SERVICE_SALE_DEMO_DATA"),
		"STAGES" => Array(
			"locations.php",
			"step1.php",
			"step2.php",
			"step3.php"
		),
	),
//	"forum" => Array(
//		"NAME" => GetMessage("SERVICE_FORUM")
//	),
//	"advertising" => Array(
//		"NAME" => GetMessage("SERVICE_ADVERTISING"),
//	)
);
?>