<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("pageContainerClass", "static");
$APPLICATION->SetPageProperty('title', "Оплата");
$APPLICATION->SetTitle("Оплата");
?><img src="#SITE_DIR#img/payment.jpg"><br>
<p>
		Способ оплаты покупок Вы можете выбрать при оформлении заказа. Интернет-магазин предоставляет следующие способы оплаты товаров:
</p>
<p>
</p>
<ul>
	<li>Наличными курьеру;</li>
	<li>Банковской картой при оформлении заказа;</li>
	<li>Электронными деньгами;</li>
	<li>Квитанция для оплаты в банке;</li>
	<li>Наложенным платежом при доставке почтой России.</li>
</ul>
<blockquote>
	<p>
 <b>Внимание!</b> Для оплаты одного заказа необходимо выбрать один способ оплаты. Оплата заказа по частям разными способами - не возможна.
	</p>
</blockquote>
<p>
		В случае вопросов, пожеланий и претензий обращайтесь к нам по следующим координатам:
</p>
<p>
 <b>Служба доставки</b>: 8 (495) 212 85 06 (многоканальный).
</p>
<p>
 <b>Отдел продаж</b>: <a href="mailto:sale@shop.ru">sale@shop.ru</a>
</p>
<p>
 <b>Skype</b>: <a href="skype:sale_shop_ru">sale_shop_ru</a>
</p>
 <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>