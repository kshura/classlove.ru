less = {
	env: "production",
	async: false,
	fileAsync: false,
	poll: 1000,
	functions: {},
	dumpLineNumbers: "comments",
	relativeUrls: true
};