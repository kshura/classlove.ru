<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$catalogSubscribe = $wizard->GetVar("catalogSubscribe");
$curSiteSubscribe = ($catalogSubscribe == "Y") ? array("use" => "Y", "del_after" => "100") : array("del_after" => "100");
$subscribe = COption::GetOptionString("sale", "subscribe_prod", "");
$arSubscribe = unserialize($subscribe);
$arSubscribe[WIZARD_SITE_ID] = $curSiteSubscribe;
COption::SetOptionString("sale", "subscribe_prod", serialize($arSubscribe));

$useStoreControl = $wizard->GetVar("useStoreControl");
$useStoreControl = ($useStoreControl == "Y") ? "Y" : "N";
$curUseStoreControl = COption::GetOptionString("catalog", "default_use_store_control", "N");
COption::SetOptionString("catalog", "default_use_store_control", $useStoreControl);

$productReserveCondition = $wizard->GetVar("productReserveCondition");
$productReserveCondition = (in_array($productReserveCondition, array("O", "P", "D", "S"))) ? $productReserveCondition : "P";
COption::SetOptionString("sale", "product_reserve_condition", $productReserveCondition);

global $USER_FIELD_MANAGER;

if (CModule::IncludeModule("catalog")) {
	$arUserFields = array (
		array (
			'ENTITY_ID' => 'CAT_STORE',
			'FIELD_NAME' => 'UF_SHIPPING_TIME',
			'USER_TYPE_ID' => 'string',
			'XML_ID' => 'UF_SHIPPING_TIME',
			'SORT' => '100',
			'MULTIPLE' => 'N',
			'MANDATORY' => 'N',
			'SHOW_FILTER' => 'N',
			'SHOW_IN_LIST' => 'Y',
			'EDIT_IN_LIST' => 'Y',
			'IS_SEARCHABLE' => 'Y',
			'EDIT_FORM_LABEL' => array(
				'ru' => GetMessage('SHIPPING_TIME_TITLE'),
				'en' => GetMessage('SHIPPING_TIME_TITLE'),
			),
			'LIST_COLUMN_LABEL' => array(
				'ru' => GetMessage('SHIPPING_TIME_TITLE'),
				'en' => GetMessage('SHIPPING_TIME_TITLE'),
			),
			'LIST_FILTER_LABEL' => array(
				'ru' => GetMessage('SHIPPING_TIME_TITLE'),
				'en' => GetMessage('SHIPPING_TIME_TITLE'),
			),
		),
	);
	$obUserField = new CUserTypeEntity;
	foreach ($arUserFields as $arFields) {
		$dbRes = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => $arFields["ENTITY_ID"], "FIELD_NAME" => $arFields["FIELD_NAME"]));
		if ($dbRes->Fetch())
			continue;

		$ID_USER_FIELD = $obUserField->Add($arFields);
	}

	for($s=1; $s<=4; $s++) {
		$arStoreFields = array(
			"SORT" => $s.'00',
			"TITLE" => GetMessage("STORE_TITLE_".$s),
			"ADDRESS" => GetMessage("STORE_ADDRESS_".$s),
			"DESCRIPTION" => GetMessage("STORE_DESCRIPTION_".$s),
			"GPS_N" => GetMessage("STORE_GPS_N_".$s),
			"GPS_S" => GetMessage("STORE_GPS_S_".$s),
			"PHONE" => GetMessage("STORE_PHONE_".$s),
			"EMAIL" => GetMessage("STORE_EMAIL_".$s),
			"SCHEDULE" => GetMessage("STORE_SCHEDULE_".$s),
			"ISSUING_CENTER" => "Y",
			"SHIPPING_CENTER" => "Y",
			"SITE_ID" => WIZARD_SITE_ID
		);
		$newStoreId = CCatalogStore::Add($arStoreFields);
		if($newStoreId) {
			$USER_FIELD_MANAGER->Update('CAT_STORE', $newStoreId, array(
				'UF_SHIPPING_TIME' => GetMessage("UF_SHIPPING_TIME_".$s)
			));
			CCatalogDocs::synchronizeStockQuantity($newStoreId);
		}
	}
}

if(COption::GetOptionString("firstbit_beautyshop", "wizard_installed", "N", WIZARD_SITE_ID) == "Y" && !WIZARD_INSTALL_DEMO_DATA)
	return;

COption::SetOptionString("catalog", "allow_negative_amount", "N");
COption::SetOptionString("catalog", "default_can_buy_zero", "N");
COption::SetOptionString("catalog", "default_quantity_trace", "Y");