BX.showWait = function(node, msg) {
	preload(node);
};
BX.closeWait = function(node, obMsg) {
};

var storageLocal = $.localStorage;
var storageCookie = $.cookieStorage;

function preload(node) {
	$(node).css('position','relative').append('<div class="preloader"></div>');
}

$(document).ready(function () {
	var overlayBlock = $('#overlayBlock'),
		bottomPanel = $('#bottom_panel'),
		owl_banners = $('#banners_slider'),
		owl_news = $('#owl_news'),
		owl_hit = $('#owl_hit'),
		owl_newproduct = $('#owl_newproduct'),
		owl_related = $('#owl_related'),
		owl_viewed = $('#owl_viewed'),
		searchResult = $('.title-search-result'),
		tabType;
	$('ul.menu_flex.hover').flexMenu();
	$('ul.menu_flex.click').flexMenu({
		showOnHover: false
	});

	$("[data-mask=phone]").inputmask("9{1,20}");
	$("[data-mask=email]").inputmask({
		mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
		greedy: false,
		onBeforePaste: function (pastedValue, opts) {
			pastedValue = pastedValue.toLowerCase();
			return pastedValue.replace("mailto:", "");
		},
		definitions: {
			'*': {
				validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
				cardinality: 1,
				casing: "lower"
			}
		}
	});
	$('.city_menu_head').click(function () {
		$(this).parent('.city_wrapper').find('.city_menu_body').toggle('blind', 200);
		$(this).parent('.city_wrapper').find('.fa-angle-down').toggleClass('up');
	});
	$('.city_wrapper.small ul').mCustomScrollbar();
	$('.store_map_tab .menu_container').mCustomScrollbar();
	$('.filter_wrapper').mCustomScrollbar();
	$('.bx-added-item-table-container').mCustomScrollbar({axis:'x',advanced:{autoExpandHorizontalScroll:true}});
	$('.bx-catalog-set-topsale-slider-container').mCustomScrollbar({axis:'x',advanced:{autoExpandHorizontalScroll:true}});
	$('#leftpanel_menu > li span').click(function () {
		$(this).parent().toggleClass('open');
	});
	$('select.select_filter').select2();
	$('select').select2({
		minimumResultsForSearch: Infinity
	});
	$('.filters_settings a.filters_block').click(function () {
		$(this).parent('.settings_item').find('a.filters_block').removeClass('active');
		$(this).addClass('active');
	});
	owl_banners.owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		dots: false,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		smartSpeed: 500,
		navContainer: '#owl_banners_nav',
		navText: ["<i class=\"fa fa-chevron-left\"></i>","<i class=\"fa fa-chevron-right\"></i>"],
		afterInit: initSlider(owl_banners),
	});
	owl_news.owlCarousel({
		responsive: {
			0: {
				items: 1,
			},
			719: {
				items: 3,
			},
			959: {
				items: 4,
			}
		},
		loop: false,
		nav: true,
		dots: false,
		smartSpeed: 500,
		navContainer: '#owl_news_nav',
		navText: ["<i class=\"fa fa-angle-left\"></i>","<i class=\"fa fa-angle-right\"></i>"],
		afterInit: initSlider(owl_news),
	});
	$('.news .right_slide').click(function(){
		owl_news.trigger('owl.next');
	});
	$('.news .left_slide').click(function(){
		owl_news.trigger('owl.prev');
	});
	owl_hit.owlCarousel({
		responsive: {
			0: {
				items: 1,
			},
			719: {
				items: 3,
			},
			959: {
				items: 4,
			}
		},
		loop: false,
		nav: true,
		dots: false,
		smartSpeed: 500,
		navContainer: '#owl_hit_nav',
		navText: ["<i class=\"fa fa-angle-left\"></i>","<i class=\"fa fa-angle-right\"></i>"],
		afterInit: initSlider(owl_hit),
	});
	owl_newproduct.owlCarousel({
		responsive: {
			0: {
				items: 1,
			},
			719: {
				items: 3,
			},
			959: {
				items: 4,
			}
		},
		loop: false,
		nav: true,
		dots: false,
		smartSpeed: 500,
		navContainer: '#owl_newproduct_nav',
		navText: ["<i class=\"fa fa-angle-left\"></i>","<i class=\"fa fa-angle-right\"></i>"],
		afterInit: initSlider(owl_newproduct),
	});
	owl_related.owlCarousel({
		responsive: {
			0: {
				items: 1,
			},
			719: {
				items: 3,
			},
			959: {
				items: 4,
			}
		},
		loop: false,
		nav: true,
		dots: false,
		smartSpeed: 500,
		navContainer: '#owl_related_nav',
		navText: ["<i class=\"fa fa-angle-left\"></i>","<i class=\"fa fa-angle-right\"></i>"],
		afterInit: initSlider(owl_related),
	});
	owl_viewed.owlCarousel({
		responsive: {
			0: {
				items: 1,
			},
			719: {
				items: 3,
			},
			959: {
				items: 4,
			}
		},
		loop: false,
		nav: true,
		dots: false,
		smartSpeed: 500,
		navContainer: '#owl_viewed_nav',
		navText: ["<i class=\"fa fa-angle-left\"></i>","<i class=\"fa fa-angle-right\"></i>"],
		afterInit: initSlider(owl_viewed),
	});
	$('.profile_tabs').tabs();
	$('.product_tabs').tabs({
		collapsible: false,
		active: 0
	});
	var mobile_tabs_title = $('.mobile_tabs_title');
	$.each(mobile_tabs_title, function(index, tab) {
		$(this).click(function() {
			var active = $('.product_tabs').tabs("option", "active");
			if(active === index) {
				$('.product_tabs').tabs({
					collapsible: true,
					active: false
				});
			} else {
				$('.product_tabs').tabs({
					active: index
				});
			}
		});
	});
	$('.product_bottom_tabs').tabs({
		active: 0
	})
	$('#product_bottom_tabs_select').change(function() {
		$('.product_bottom_tabs').tabs({
			active: $('#product_bottom_tabs_select').val()
		})
	});
	$('#bottom_panel').tabs({
		collapsible: true,
		active: false,
		beforeActivate: function (event, ui) {
			tabType = ui.newTab.attr('data-tab-type');
			if(tabType!==undefined) {
				$('#tab_preloader').show();
				bottomPanelLoadTab(ui, tabType);
			}
		},
		activate: function( event, ui ) {
			var activePanel = $(this).tabs( "option", "active" );
			if(ui.oldPanel.selector) {
				ui.oldPanel.empty();
			} else {
				overlayBody();
			}
			if(activePanel === false) {
				overlayBody();
			}
		},
		create: function( event, ui ) {
			ui.panel.show();
		}
	});
	$(document).on('click', '.mobile_search', function (e) {
		if($(this).siblings('.search_form').css('display') == 'block') {
			$(this).siblings('.search_form').css('display', '');
		} else {
			$(this).siblings('.search_form').show();
		}
	});
	$(document).on('click', '.menu2 .level2 > li', function (e) {
		$(this).toggleClass('open');
	});
	$(document).on('click', '.menu3 .level2 > li', function (e) {
		$(this).toggleClass('open');
	});
	$(document).on('click', '.menu2 .level3 > li', function (e) {
		e.stopPropagation();
		$(this).toggleClass('open');
	});
	$(window).resize(function () {
		width = $(window).width();

		searchResult.hide();
		if (width < 720) {
		}

		if (width < 960) {
			bottomPanelClose();
			flyingCartClose();
		}

		if (width > 719) {
		}

		if (width > 959) {
			mobileMenuClose();
			mobileFilterClose();
		}
	});
	$(overlayBlock).click(function(){
		bottomPanelClose();
		mobileMenuClose();
		mobileFilterClose();
	});
	$(document).on('click','.filter_header', function(e) {
		$(this).parent('.filter_block').find('.filter_body').toggle();
		if($(this).find('i.filter_toggle').hasClass('fa-chevron-circle-down')) {
			$(this).find('i.filter_toggle').removeClass('fa-chevron-circle-down').addClass('fa-chevron-circle-up');
		}
		else {
			$(this).find('i.filter_toggle').removeClass('fa-chevron-circle-up').addClass('fa-chevron-circle-down');
		};
	});
	$('[data-brands-view]').click(function(){
		var brandsView = $(this).attr('data-brands-view');
		$(this).addClass('select').siblings().removeClass('select');
		$('.brands_item_list').removeClass().addClass('brands_item_list ' + brandsView);
	});

	$('#popup').click(function(event) {
		if ($(event.target).closest("#modal_window").length) return;
		modalClose();
		event.stopPropagation();
	});

	$('.settings_wrapper').click(function () {
		if ($(event.target).closest(".settings_container").length) return;
		settingsPanelClose();
	});
	$('.settings_close').click(function () {
		settingsPanelClose();
	});
	$('.settings_container').click(function (e) {
		if ($(event.target).closest(".help_wrapper").length) return;
		closeHelpBaloon();
		event.stopPropagation();
	});

	$('.store_list_table').click(function () {
		$('.store_list_maps').removeClass('active');
		$(this).addClass('active');
		$('.store_list_tab3').hide();
		$('.store_list_tab1').show();
	});
	$('.store_list_maps').click(function () {
		$('.store_list_table').removeClass('active');
		$(this).addClass('active');
		$('.store_list_tab1').hide();
		$('.store_list_tab3').show();
	});

	$('.store_list_table').click()

	$(window).scroll(function(){
		if ($(window).scrollTop() >= '300') {
			$('#top_scroller').show()
		} else {
			$('#top_scroller').hide()
		}
		if($(window).width() > 719) {
			if($('.flying_menu').length > 0) {
				if ($(window).scrollTop() >= '150') {
					$('.flying_menu').addClass('on');
					$('.content').css('marginTop', 50);
				} else {
					$('.flying_menu').removeClass('on');
					$('.content').css('marginTop', 0);
				}
			}
		}
	});
	$('#top_scroller').click(function() {
		$('body').animate({
			scrollTop: 0
		}, 500)
	});

	$('.personal_item h2').click(function(){
		$(this).parent().toggleClass('open');
	});
	window.addEventListener("storage", localStorageHandler, false);
});

function settingsPanelToggle() {
	if($('.settings_wrapper').hasClass('on')) {
		settingsPanelClose();
	} else {
		settingsPanelOpen();
	}
}

function settingsPanelOpen() {
	bottomPanelClose();
	mobileMenuClose();
	mobileFilterClose();
	$('.settings_wrapper').addClass('on');
	lockBody();
}
function settingsPanelClose() {
	closeHelpBaloon();
	$('.settings_wrapper').removeClass('on');
	unlockBody();
}

function orderHistorySearch() {
	var orders = $('#order_list > div'),
		numberInput = $('#order_search').val(),
		statusInput = $('select[name=order_sort_order]').val();
	$.each(orders, function() {
		$(this).show();
		if($(this).attr('data-order-number').indexOf(numberInput) >= 0 && (!statusInput || statusInput == $(this).attr('data-order-status'))) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
}

function closeHelpBaloon() {
	$('.help_wrapper.open').removeClass('open');
}
function openHelpBaloon(node) {
	if(!$(node).hasClass('open')) {
		closeHelpBaloon();
		$(node).addClass('open');
	} else {
		closeHelpBaloon();
	}
}

function modalFeedback(node) {
	var modal = node;
	$.ajax({
		url: "#SITE_DIR#ajax/modalFeedback.php",
		method: "POST",
		dataType: 'html',
		success: function (response) {
			modalOpen(response, $(modal).attr('title'), "modal feedback");
		}
	});
}
function modalCallback(node) {
	var modal = node;
	$.ajax({
		url: "#SITE_DIR#ajax/modalCallback.php",
		method: "POST",
		dataType: 'html',
		success: function (response) {
			modalOpen(response, $(modal).attr('title'), "modal callback");
		}
	});
}

function modalCommentAdd(node) {
	var modal = node,
		productID;
	productID = $(modal).attr('data-product-id');
	$.ajax({
		url: "#SITE_DIR#ajax/modalCommentAdd.php",
		method: "POST",
		data: {
			PRODUCT_ID: productID
		},
		dataType: 'html',
		success: function (response) {
			modalOpen(response, $(modal).attr('title'), "modal commentAdd");
		}
	});
}

function modalQuickBuy(node) {
	var modal = node,
		productID;
	productID = $(modal).attr('data-product-id');
	$.ajax({
		url: "#SITE_DIR#ajax/modalQuickBuy.php",
		method: "POST",
		data: {
			PRODUCT_ID: productID
		},
		dataType: 'html',
		success: function (response) {
			modalOpen(response, $(modal).attr('title'), "modal quickBuy");
		}
	});
}

function modalPriceRequest(node) {
	var modal = node,
		productID;
	productID = $(modal).attr('data-product-id');
	$.ajax({
		url: "#SITE_DIR#ajax/modalPriceRequest.php",
		method: "POST",
		data: {
			PRODUCT_ID: productID
		},
		dataType: 'html',
		success: function (response) {
			modalOpen(response, $(modal).attr('title'), "modal priceRequest");
		}
	});
}

function modalOpen(modalBody, modalTitle, modalClass) {
	var modalWindowCotainer = $('#popup'),
		currentWindow = $('#modal_window'),
		modalWindow = $('<div id="modal_window" class="modal_window ' + modalClass + '"></div>'),
		modalWindowTitlebar = $('<div class="modal_titlebar"></div>'),
		modalWindowTitle = $('<span class="modal_title"></span>'),
		modalWindowBody = $('<div class="modal_body"></div>'),
		modalWindowCloseButton = $('<div class="modal_close"><i class="fa fa-times"></i></div>');

	if(currentWindow.length <= 0) {
		modalWindowCotainer.append(modalWindow);
		modalWindow.append(modalWindowTitlebar);
		modalWindow.append(modalWindowBody);
		modalWindowTitlebar.append(modalWindowTitle);
		if(!!modalTitle) {
			modalWindowTitle.append(modalTitle);
		}
		modalWindowCloseButton.appendTo(modalWindowTitlebar).click(function(){
			modalClose();
		});
		modalWindowBody.html(modalBody);
		modalWindowCotainer.show();
		overlayBody();
		modalWindow.mCustomScrollbar();
		modalArrange();
	}
}

function modalClose() {
	var modalWindowCotainer = $('#popup');
	modalWindowCotainer.hide().empty();
	overlayBody();
}

function modalArrange() {
	var currentWindow = $('#modal_window'),
		modalHeight,
		modalMarginTop,
		modalWidth;
	if($(window).width() > 719) {
		if (currentWindow.length > 0) {
			currentWindow.css('margin-top', '0');
			modalHeight = currentWindow.outerHeight(true);
			modalMarginTop = Math.round(modalHeight / 2)
			if (!!modalMarginTop) {
				currentWindow.css('margin-top', '-' + modalMarginTop + 'px');
			}
		}
	}
}

function modalInitJS() {
	$(".form_modal [data-mask=phone]").inputmask("9{1,20}");
	$(".form_modal [data-mask=email]").inputmask({
		mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
		greedy: false,
		onBeforePaste: function (pastedValue, opts) {
			pastedValue = pastedValue.toLowerCase();
			return pastedValue.replace("mailto:", "");
		},
		definitions: {
			'*': {
				validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
				cardinality: 1,
				casing: "lower"
			}
		}
	});
}

function initSlider(node) {
	$(node).css('visibility', 'visible').show();
}
function mobileFilterOpen() {
	var filter = $('#filter_wrapper');
	if(!filter.hasClass('open')) {
		filter.addClass('open');
	}
}
function mobileFilterClose() {
	var filter = $('#filter_wrapper');
	if(filter.hasClass('open')) {
		filter.removeClass('open');
	}
}
function catalogSortToggle() {
	var sort = $('#sort_wrapper');
	sort.toggle();
}
function mobileMenuOpen() {
	var menu = $('#mobile_nav_wrapper'),
		scrollTop = $(window).scrollTop();
	if(!menu.hasClass('open')) {
		menu.addClass('open');
		$('body').toggleClass('right').animate({
			scrollTop: 0
		}, 0);
		overlayBody();
		unlockBody();
		menu.css('min-height', $(document).height());
	}
}
function mobileMenuClose() {
	var menu = $('#mobile_nav_wrapper');
	if(menu.hasClass('open')) {
		menu.removeClass('open');
		var scrollTop = $.cookie('scrollTop');
		$.cookie("scrollTop", null, { path: '/' });
		$('body').toggleClass('right').animate({
			scrollTop: scrollTop
		}, 0);
		overlayBody();
	}
}
function overlayBody() {
	var overlayBlock = $('#overlayBlock');
	if(overlayBlock.hasClass('on')) {
		overlayBlock.removeClass('on');
		overlayBlock.empty();
		unlockBody();
	} else {
		overlayBlock.addClass('on');
		lockBody();
	}
}
function lockBody() {
	if(!$('body').hasClass('overflow')) {
		$('body').addClass('overflow');
	}
}
function unlockBody() {
	if($('body').hasClass('overflow')) {
		$('body').removeClass('overflow');
	}
}
function bottomPanelLoadTab(ui,tabType) {
	$.ajax({
		url: "#SITE_DIR#ajax/bottom_panel/" + tabType + ".php",
		method: "POST",
		dataType: 'html',
		data: {
			URI: $(location).attr('href')
		},
		success: function (response) {
			if(ui.oldPanel.selector) {
				ui.oldPanel.empty();
			}
			if(!!ui.newPanel) {
				ui.newPanel.empty().append(response);
				$('#tab_preloader').hide();
			}
		}
	});
}
function bottomPanelClose() {
	$('#bottom_panel').tabs( "option", "active", false );
}

function flyingCartToggle() {
	var cartLabel = $('#flying_cart_label');
	if(cartLabel.hasClass('open')) {
		flyingCartClose();
	} else {
		flyingCartOpen();
	}
}
function flyingCartOpen() {
	var cartLabel = $('#flying_cart_label'),
		cartBody = $('#flying_cart_body');
	cartLabel.addClass('open');
	cartBody.addClass('open');
}
function flyingCartClose() {
	var cartLabel = $('#flying_cart_label'),
		cartBody = $('#flying_cart_body');
	cartLabel.removeClass('open');
	cartBody.removeClass('open');
}

function openProduct(item) {
	event.preventDefault();
	$(item).toggleClass('open');
	$(item).closest('.product_item').find('.product_offers_list').toggleClass('open');
	var productListItem = $(item).closest('.product_item');
	if(productListItem.hasClass('open')) {
		productListItem.removeClass('open');
	}
	else {
		productListItem.toggleClass('open');
	}
}

function wishlistHandler(item) {
	var productID = $(item).attr('data-wishlist-id');

	if(storageLocal.isSet('wishlist_items.' + productID)) {
		storageLocal.remove('wishlist_items.' + productID);
	} else {
		storageLocal.set('wishlist_items.' + productID, "");
	}
	wishlistCookie(productID);
	wishlistSetItem(productID);
	storageLocal.set('wishlist_count', Object.keys(storageLocal.get('wishlist_items')).length);
	wishlistRemoveItem(productID);
	wishlistSetCount(storageLocal.get('wishlist_count'));
}
function wishlistSetItem(productID) {
	if(storageLocal.isSet('wishlist_items.' + productID)) {
		$('[data-wishlist-id=' + productID + ']').addClass('active');
	} else {
		$('[data-wishlist-id=' + productID + ']').removeClass('active');
	}
}
function wishlistCookie(item) {
	if(storageCookie.isSet('wishlist_items.' + item)) {
		storageCookie.setPath('/').remove('wishlist_items.' + item);
	} else {
		storageCookie.setPath('/').set('wishlist_items.' + item, "");
	}
}
function wishlistSetCount(count) {
	if(!count) {
		count = storageLocal.get('wishlist_count');
		if(count <= 0) {
			count = 0;
		}
	}
	$('[data-wishlist-count]').html(count);
}
function wishlistRemoveItem(item) {
	var wishlists = $('[data-wishlist]'),
		items = $('[data-wishlist-item=' + item + ']');

	$.each(items, function() {
		if($(this).parent().hasClass('owl_item')) {
			$(this).parent().remove();
		} else {
			$(this).remove();
		}
	});

	if(parseInt(storageLocal.get('wishlist_count')) <= 0) {
		console.log('EMPTY!');
		$.each(wishlists, function() {
			console.log('@');
			$(this).find('.empty_message').addClass('active');
			$(this).find('.page_empty_message').addClass('active');
			$(this).find('.bottom_panel_second_line').addClass('empty');
		});
	}
}

function compareHandler(item) {
	var item = $(item).attr('data-compare-id');
	$.ajax({
		url: "#SITE_DIR#ajax/compare.php",
		method: "POST",
		data: {
			action: 'update',
			PRODUCT_ID: item
		},
		dataType: 'json',
		success: function (response) {
			if (response.status) {
				if(response.mes == 'add') {
					compareSetItem(item);
				} else if(response.mes == 'delete') {
					compareUnsetItem(item);
				}
				compareCount(response.iblock);
			}
		},
		error: function (error) {
			console.log(error);
		}
	});
}
function compareSetItem(productID) {
	$('[data-compare-id=' + productID + ']').addClass('active');
}
function compareUnsetItem(productID) {
	$('[data-compare-id=' + productID + ']').removeClass('active');
}
function compareCheck(item) {
	$.ajax({
		url: "#SITE_DIR#ajax/compare.php",
		method: "POST",
		data: {
			action: 'check',
			PRODUCT_ID: item
		},
		dataType: 'json',
		success: function (response) {
			if (response.status) {
				if (response.mes) {
					compareSetItem(item);
				} else {
					compareUnsetItem(item);
				}
			} else {
				compareUnsetItem(item);
			}
		}
	});
}
function compareRemoveItem(item) {
	var current = $(item).closest('.product_item'),
		currentID = $(item).attr('data-product-id'),
		items = $('#tabs-compare').find('.product_item');
	current.remove();
	compareUnsetItem(currentID);
	if(items.length <= 1) {
		$('#tabs-compare').find('.empty_message').addClass('active');
		$('#tabs-compare').find('.bottom_panel_second_line').addClass('empty');
	}
}
function compareCount(iblock) {
	var count;
	$.ajax({
		url: "#SITE_DIR#ajax/compare.php",
		method: "POST",
		data: {
			action: 'count',
			IBLOCK_ID: iblock
		},
		dataType: 'json',
		success: function (response) {
			if (response.status) {
				count = response.mes;
				$('[data-compare-count]').html(count);
			} else {
				return false;
			}
		},
		error: function (error) {
			console.log(error);
		}
	});
}

function localStorageHandler(e){
	var event = e || window.event,
		source, target, item;
	if(event.key == 'wishlist_items') {
		var oldValue = $.map($.parseJSON(event.oldValue), function(value, index) {
			return [index];
		});
		var newValue = $.map($.parseJSON(event.newValue), function(value, index) {
			return [index];
		});

		if(oldValue.length > newValue.length) {
			source = oldValue;
			target = newValue;
		} else {
			source = newValue;
			target = oldValue;
		}
		item = source.filter(function(el) {
			return target.indexOf(el) == -1;
		});
		wishlistSetItem(item);
		wishlistRemoveItem(item);
	}
	if(event.key == 'wishlist_count') {
		wishlistSetCount(event.newValue);
	}
}

function bottompanelCartRefresh() {
	var bottompanelBasketContainer = $('#tabs-basket'),
		preloader = $('#tab_preloader');
	$.ajax({
		url: "#SITE_DIR#ajax/bottom_panel/basket.php",
		method: "POST",
		dataType: 'html',
		success: function (response) {
			if (!!bottompanelBasketContainer && bottompanelBasketContainer.children().length > 0) {
				bottompanelBasketContainer.html(response);
				$(preloader).hide();
			}
		}
	});
}
function deleteFromBasket(id) {
	var preloader = $('#tab_preloader');
	$(preloader).show();
	$.ajax({
		url: "#SITE_DIR#ajax/deleteFromBasket.php?basketAction=delete&id=" + id,
		method: "POST",
		dataType: 'html',
		success: function (response) {
			BX.onCustomEvent('OnBasketChange');
			bottompanelCartRefresh();
		}
	});
	return false;
}
function moreProperties(item) {
	$(item).parent().find('.product_params_additional_wrapper').toggle('blind');
	$(item).find('i.fa').toggleClass('rotate180');
}
function setCatalogView(item, action) {

	var catalog_veiw = $.parseJSON($.cookie('CATALOG_VIEW'));

	switch(action) {
		case 'setSort':
			catalog_veiw.SORT_FIELD = $(item).attr('data-catalog-sort-field');
			catalog_veiw.SORT_ORDER = $(item).attr('data-catalog-sort-order');
			break;
		case 'setShowElement':
			catalog_veiw.ELEMENTS = $(item).val();
			$(item).siblings('input').click();
			break;
		case 'setTemplate':
			catalog_veiw.TEMPLATE = $(item).attr('data-catalog-template');
			break;
	}
	$.cookie('CATALOG_VIEW', JSON.stringify(catalog_veiw), { expires: 30, path: '/' });
}

function setModef(url) {
	var bxajaxid = $("[data-bxajaxid]").attr('data-bxajaxid'),
		modef = $('#modef');

	modef.find('a').attr("onclick", "BX.ajax.insertToNode('" + url + "index.php?bxajaxid=" + bxajaxid + "', 'comp_" + bxajaxid + "'); return false;")
}