<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock"))
	return;

if(COption::GetOptionString("firstbit.beautyshop", "wizard_installed", "N", WIZARD_SITE_ID) == "Y" && !WIZARD_INSTALL_DEMO_DATA){
	if($wizard->GetVar('rewriteIndex', true) && $wizard->GetVar('siteLogoSet', true)){
		$iblockCode = "firstbit_beautyshop_sale_".WIZARD_SITE_ID;
		$iblockType = "firstbit_beautyshop_catalog";
		
		$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
		$iblockID = false; 
		if ($arIBlock = $rsIBlock->Fetch()) {
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", array("SALE_IBLOCK_ID" => $iblockID));
		}
	}
	return;
}

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/sale.xml";
$iblockXMLFileTmp = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/_tmp_sale.xml";
$iblockCode = "firstbit_beautyshop_sale_".WIZARD_SITE_ID;
$iblockType = "firstbit_beautyshop_catalog";

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType));
$iblockID = false; 
if ($arIBlock = $rsIBlock->Fetch()) {
	$iblockID = $arIBlock["ID"]; 
	if (WIZARD_INSTALL_DEMO_DATA) {
		CIBlock::Delete($arIBlock["ID"]); 
		$iblockID = false; 
	}
}

if($iblockID == false) {
	$permissions = Array(
			"1" => "X",
			"2" => "R"
		);
	$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "content_editor"));
	if($arGroup = $dbGroup -> Fetch())
	{
		$permissions[$arGroup["ID"]] = 'W';
	};

	if(file_exists($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile)) @copy($_SERVER["DOCUMENT_ROOT"].$iblockXMLFile, $_SERVER["DOCUMENT_ROOT"].$iblockXMLFileTmp);
	CWizardUtil::ReplaceMacros($_SERVER["DOCUMENT_ROOT"].$iblockXMLFileTmp, Array("IN_XML_SITE_ID" => WIZARD_SITE_ID));
	$iblockID = WizardServices::ImportIBlockFromXML($iblockXMLFileTmp, $iblockCode, $iblockType, WIZARD_SITE_ID, $permissions);
	if(file_exists($_SERVER["DOCUMENT_ROOT"].$iblockXMLFileTmp)) unlink($_SERVER["DOCUMENT_ROOT"].$iblockXMLFileTmp);

	if ($iblockID < 1)
		return;
	
	//IBlock fields
	$iblock = new CIBlock;
	$arFields = Array(
		"ACTIVE" => "Y",
		"FIELDS" => array (
			'IBLOCK_SECTION' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'ACTIVE' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => 'Y',
			),
			'ACTIVE_FROM' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '=today',
			),
			'ACTIVE_TO' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SORT' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'NAME' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => '',
				),
			'PREVIEW_PICTURE' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array (
					'FROM_DETAIL' => 'N',
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
					'DELETE_WITH_DETAIL' => 'N',
					'UPDATE_WITH_DETAIL' => 'N',
				),
			),
			'PREVIEW_TEXT_TYPE' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => 'text',
			),
			'PREVIEW_TEXT' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'DETAIL_PICTURE' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array (
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
				),
			),
			'DETAIL_TEXT_TYPE' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => 'text',
			),
			'DETAIL_TEXT' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'XML_ID' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'CODE' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => array (
					'UNIQUE' => 'Y',
					'TRANSLITERATION' => 'Y',
					'TRANS_LEN' => 100,
					'TRANS_CASE' => 'L',
					'TRANS_SPACE' => '_',
					'TRANS_OTHER' => '_',
					'TRANS_EAT' => 'Y',
					'USE_GOOGLE' => 'Y',
				),
			),
			'TAGS' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SECTION_NAME' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => '',
			),
			'SECTION_PICTURE' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array (
					'FROM_DETAIL' => 'N',
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
					'DELETE_WITH_DETAIL' => 'N',
					'UPDATE_WITH_DETAIL' => 'N',
				),
			),
			'SECTION_DESCRIPTION_TYPE' => array (
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => 'text',
			),
			'SECTION_DESCRIPTION' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SECTION_DETAIL_PICTURE' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array (
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => 'resample',
					'COMPRESSION' => 95,
				),
			),
			'SECTION_XML_ID' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SECTION_CODE' => array (
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array (
					'UNIQUE' => 'N',
					'TRANSLITERATION' => 'N',
					'TRANS_LEN' => 100,
					'TRANS_CASE' => 'L',
					'TRANS_SPACE' => '_',
					'TRANS_OTHER' => '_',
					'TRANS_EAT' => 'Y',
					'USE_GOOGLE' => 'N',
				),
			),
		),
		"CODE" => $iblockCode,
		"XML_ID" => $iblockCode,
	);
	
	$iblock->Update($iblockID, $arFields);
}
else
{
	$arSites = array(); 
	$db_res = CIBlock::GetSite($iblockID);
	while ($res = $db_res->Fetch())
		$arSites[] = $res["LID"]; 
	if (!in_array(WIZARD_SITE_ID, $arSites))
	{
		$arSites[] = WIZARD_SITE_ID;
		$iblock = new CIBlock;
		$iblock->Update($iblockID, array("LID" => $arSites));
	}
}
$dbSite = CSite::GetByID(WIZARD_SITE_ID);
if($arSite = $dbSite -> Fetch())
	$lang = $arSite["LANGUAGE_ID"];
if(strlen($lang) <= 0)
	$lang = "ru";

$rsProperties = CIBlock::GetProperties($iblockID, Array("id" => "asc"), Array('IBLOCK_ID' => $iblockID));
$arIblockProperties = array();
while($arProperty = $rsProperties->GetNext()) {
	$arIblockProperties[$arProperty['CODE']] = $arProperty;
}

WizardServices::IncludeServiceLang("sale.php", $lang);
//CUserOptions::SetOption(
//	"form",
//	"form_element_".$iblockID,
//	array (
//		'tabs' => 'edit1--#--'.GetMessage("WZD_OPTION_SALE_1").'--,--ACTIVE--#--'.GetMessage("WZD_OPTION_SALE_2").'--,--ACTIVE_FROM--#--'.GetMessage("WZD_OPTION_SALE_3").'--,--ACTIVE_TO--#--'.GetMessage("WZD_OPTION_SALE_4").'--,--NAME--#--'.GetMessage("WZD_OPTION_SALE_5").'--,--CODE--#--'.GetMessage("WZD_OPTION_SALE_6").'--,--PROPERTY_'.$arIblockProperties["ISSALE"]["ID"].'--#--'.GetMessage("WZD_OPTION_SALE_7").'--,--PROPERTY_'.$arIblockProperties["SALE_FROM"]["ID"].'--#--'.GetMessage("WZD_OPTION_SALE_8").'--,--PROPERTY_'.$arIblockProperties["SALE_TO"]["ID"].'--#--'.GetMessage("WZD_OPTION_SALE_9").'--,--PROPERTY_'.$arIblockProperties["SALE_SECTIONS"]["ID"].'--#--'.GetMessage("WZD_OPTION_SALE_10").'--,--PROPERTY_'.$arIblockProperties["SALE_PRODUCTS"]["ID"].'--#--'.GetMessage("WZD_OPTION_SALE_11").'--;--edit5--#--'.GetMessage("WZD_OPTION_SALE_12").'--,--PREVIEW_PICTURE--#--'.GetMessage("WZD_OPTION_SALE_13").'--,--PREVIEW_TEXT--#--'.GetMessage("WZD_OPTION_SALE_14").'--;--edit6--#--'.GetMessage("WZD_OPTION_SALE_15").'--,--DETAIL_PICTURE--#--'.GetMessage("WZD_OPTION_SALE_16").'--,--DETAIL_TEXT--#--'.GetMessage("WZD_OPTION_SALE_17").'--;--edit14--#--SEO--,--IPROPERTY_TEMPLATES_ELEMENT_META_TITLE--#--'.GetMessage("WZD_OPTION_SALE_18").'--,--IPROPERTY_TEMPLATES_ELEMENT_META_KEYWORDS--#--'.GetMessage("WZD_OPTION_SALE_19").'--,--IPROPERTY_TEMPLATES_ELEMENT_META_DESCRIPTION--#--'.GetMessage("WZD_OPTION_SALE_20").'--,--IPROPERTY_TEMPLATES_ELEMENT_PAGE_TITLE--#--'.GetMessage("WZD_OPTION_SALE_21").'--,--IPROPERTY_TEMPLATES_ELEMENTS_PREVIEW_PICTURE--#----'.GetMessage("WZD_OPTION_SALE_22").'--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_ALT--#--'.GetMessage("WZD_OPTION_SALE_23").'--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_TITLE--#--'.GetMessage("WZD_OPTION_SALE_24").'--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_NAME--#--'.GetMessage("WZD_OPTION_SALE_25").'--,--IPROPERTY_TEMPLATES_ELEMENTS_DETAIL_PICTURE--#----'.GetMessage("WZD_OPTION_SALE_26").'--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_ALT--#--'.GetMessage("WZD_OPTION_SALE_27").'--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_TITLE--#--'.GetMessage("WZD_OPTION_SALE_28").'--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_NAME--#--'.GetMessage("WZD_OPTION_SALE_29").'--,--SEO_ADDITIONAL--#----'.GetMessage("WZD_OPTION_SALE_30").'--,--TAGS--#--'.GetMessage("WZD_OPTION_SALE_31").'--;--',
//	),
//	"Y"
//);
//
//CUserOptions::SetOption(
//	"list",
//	"tbl_iblock_list_".md5($iblockType.".".$iblockID),
//	array (
//		'columns' => 'ID,NAME,ACTIVE,DATE_ACTIVE_FROM,DATE_ACTIVE_TO,PROPERTY_'.$arIblockProperties["ISSALE"]["ID"].',PROPERTY_'.$arIblockProperties["SALE_SECTIONS"]["ID"].',PROPERTY_'.$arIblockProperties["SALE_PRODUCTS"]["ID"].',PROPERTY_'.$arIblockProperties["SALE_FROM"]["ID"].',PROPERTY_'.$arIblockProperties["SALE_TO"]["ID"].',TIMESTAMP_X',
//		'by' => 'timestamp_x',
//		'order' => 'desc',
//		'page_size' => '20',
//	),
//	"Y"
//);

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", array("SALE_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/sale/index.php", array("SALE_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/brands/index.php", array("SALE_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/wishlist/index.php", array("SALE_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/catalog/index.php", array("SALE_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/ajax/bottom_panel/wishist.php", array("SALE_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/search/index.php", array("SALE_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/personal/special/sale/index.php", array("SALE_IBLOCK_ID" => $iblockID));
?>