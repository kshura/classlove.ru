<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty('title', "Поиск");
$APPLICATION->SetTitle("Поиск");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:search.page",
	"",
	array(
		"RESTART" => "Y",
		"CHECK_DATES" => "Y",
		"USE_TITLE_RANK" => "N",
		"DEFAULT_SORT" => "rank",
		"arrFILTER" => array(
			0 => "main",
			1 => "iblock_firstbit_beautyshop_catalog",
			2 => "iblock_firstbit_beautyshop_news",
			3 => "iblock_firstbit_beautyshop_services",
		),
		"arrFILTER_main" => array(
		),
		"arrFILTER_iblock_firstbit_beautyshop_services" => array(
			0 => "#SERVICES_IBLOCK_ID#",
		),
		"arrFILTER_iblock_firstbit_beautyshop_news" => array(
			0 => "#NEWS_IBLOCK_ID#",
		),
		"arrFILTER_iblock_firstbit_beautyshop_catalog" => array(
			0 => "#CATALOG_IBLOCK_ID#",
			1 => "#BRANDS_IBLOCK_ID#",
			2 => "#SALE_IBLOCK_ID#",
			3 => "#OFFERS_IBLOCK_ID#",
		),
		"SHOW_WHERE" => "N",
		"SHOW_WHEN" => "N",
		"PAGE_RESULT_COUNT" => "25",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_SHADOW" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"USE_SUGGEST" => "Y",
		"SHOW_ITEM_TAGS" => "N",
		"SHOW_ITEM_DATE_CHANGE" => "N",
		"SHOW_ORDER_BY" => "N",
		"SHOW_TAGS_CLOUD" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "suggest",
		"NO_WORD_LOGIC" => "N",
		"FILTER_NAME" => "",
		"USE_LANGUAGE_GUESS" => "Y",
		"SHOW_RATING" => "",
		"RATING_TYPE" => "",
		"PATH_TO_USER_PROFILE" => ""
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
