<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if(strlen($arResult["ERROR_MESSAGE"])>0)
	ShowError($arResult["ERROR_MESSAGE"]);
$arPlacemarks = array();
$gpsN = '';
$gpsS = '';
$this->SetFrameMode(true);
?>

<section class="row grey stores">
	<div class="container_16 section_inner">
		<div class="grid_16 section_line_head">
			<h1 class="section_title"><? echo GetMessage('S_TITLE'); ?></h1>
			<a href="<? echo $arParams['PATH_TO_LIST'];?>" class="section_more"><? echo GetMessage('S_SHOW_ALL'); ?></a>
		</div>
		<div class="grid_16 store_list_wrapper">
			<div class="store_list_body">
				<div class="store_list_tab3">
					<div class="store_map_tab">
						<div class="container_16">
							<div class="map">
								<div class="grid_4 grid_5_sm alpha omega menu_container">
									<ul class="store_list">
										<?
										$JSplacemarks = array();
										$i = 0;
										?>
										<? foreach($arResult["STORES"] as $pid => $arProperty) { ?>
											<li class="store_item store1" data-yamaps-placemark="<?=$i?>">
												<h3 class="city"><?=$arProperty["TITLE"]?></h3>
												<span class="street"><?=$arProperty["ADDRESS"]?></span>
												<?
												if(!empty($arProperty['PHONE']) && is_array($arProperty['PHONE'])) {
													foreach ($arProperty['PHONE'] as $arPhoneIndex => $arPhone) {
														?>
														<span class="phone"><? echo $arPhone['DISPLAY']; ?></span>
														<?
													}
												}
												?>
												<div class="btn_container show_xs"><a href="<? echo $arProperty['URL'] ?>" class="btn_round btn_color"><? echo GetMessage("S_DETAIL");?></a></div>
											</li>
											<?
											$JSplacemark = "
										myPlacemark".$i." = new ymaps.Placemark([".$arProperty["GPS_N"].",".$arProperty["GPS_S"]."], {
											hintContent: '".$arStore["TITLE"]."',
											balloonContent: '<div style=\"width:200px;\">' +
												'<h3 class=\"city\">".$arProperty["TITLE"]."</h3>' +
												'<span class=\"street\">".$arProperty["ADDRESS"]."</span>' +";
											if(!empty($arProperty['PHONE']) && is_array($arProperty['PHONE'])) {
												foreach ($arProperty['PHONE'] as $arPhoneIndex => $arPhone) {
													$JSplacemark .= "
												'<span class=\"phone\">" . $arPhone["DISPLAY"] . "</span>' +
										";
												}
											}
											$JSplacemark .= "
												'<br/>' +
												'<a href=\"".$arProperty['URL']."\" class=\"btn_round btn_color btn_wide\">".GetMessage("S_DETAIL")."</a>' +
												'</div>'},
											{
												iconLayout: 'default#image',
											});
									";
											$JSplacemarks[] = $JSplacemark;
											unset($JSplacemark);
											$i++;
											?>
										<? } ?>
									</ul>
								</div>
								<div class="grid_12 grid_11_sm alpha omega map_container hidden_xs">
									<div id="map" style="width: 100%; height: 330px"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</section>

<script type="text/javascript">
	var myMap;
	function init() {
		var myMap = new ymaps.Map("map", {
			center: [55.76, 37.64],
			behaviors: ['default'],
			zoom: 10,
			controls: ['zoomControl']
		});
		<?
		foreach ($JSplacemarks as $placemarkIndex => $placemark) {
			echo $placemark;
			echo 'myMap.geoObjects.add(myPlacemark' . $placemarkIndex . ');';
		}
		?>
		$("[data-yamaps-placemark]").click(function(){
			var currentPlacemaker = eval('myPlacemark'+$(this).attr('data-yamaps-placemark'));
			myMap.setCenter(currentPlacemaker.geometry.getCoordinates(),15);
			currentPlacemaker.balloon.open();
		});
	};
</script>
<script src="https://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU&onload=init"></script>

