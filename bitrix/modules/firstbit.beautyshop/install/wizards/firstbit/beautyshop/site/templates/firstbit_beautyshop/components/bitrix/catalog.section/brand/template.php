<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$frame = $this->createFrame()->begin();

if (!empty($arResult['ITEMS'])) {
	?>
	<div class="catalog_section_title"><h2><?= GetMessage(CT_BCS_TPL_BRAND_ITEMS); ?></h2></div>

	<!-- ���������� ������ -->
	<div data-bxajaxid=""></div>

	<?
	CModule::IncludeModule("iblock");
	$arSections = array("0" => 'all');
	GLOBAL $arrFilterBrands;

	$rsBrandSections = CIBlockElement::GetList(Array("IBLOCK_SECTION_ID" => "ASC"), array("PROPERTY_BRAND_REF" => $arrFilterBrands['PROPERTY_BRAND_REF'], "IBLOCK_ID" => $arParams['CATALOG_IBLOCK_ID'], "ACTIVE" => "Y", "SECTION_GLOBAL_ACTIVE" => "Y"), false, false, array("IBLOCK_SECTION_ID"));
	if (intval($rsBrandSections->SelectedRowsCount()) > 0) {
		while ($arBrandSections = $rsBrandSections->Fetch()) {
			$arSections[$arBrandSections['IBLOCK_SECTION_ID']] = '';
		}
	}
	if (count($arSections) > 2) {
		foreach ($arSections as $arSectionID => $arSectionTitle) {
			$rsSection = CIBlockSection::GetByID($arSectionID);
			if ($arSection = $rsSection->GetNext()) {
				$arSections[$arSection['ID']] = $arSection['NAME'];
			}
		}
		?>
		<div class="catalog_sections_block">
			<?
			foreach ($arSections as $arSectionKey => $arSectionName) {
				?>
				<a href="<?= $APPLICATION->GetCurDir() ?><?= ($arSectionKey != 0 ? '?section=' . $arSectionKey : '') ?>" class="btn_round btn_square btn_hover_color <? if ($_REQUEST['section'] == $arSectionKey) { ?>btn_color<? }
				else { ?>btn_border<?
				} ?>"><?= ($arSectionKey != 0 ? $arSectionName : GetMessage('CT_BCS_TPL_ALL_SECTION')) ?></a>
				<?
			}
			?>
		</div>
		<?
	}
	?>

	<div class="sort_wrapper_button show_xs" onclick="catalogSortToggle();">
		<span><? echo GetMessage('CT_BCS_CATALOG_SORT_SHOW'); ?></span></div>
	<section class="sort_wrapper" id="sort_wrapper">
		<?
		foreach ($arParams['CATALOG_SORT_PARAMS']['SORT_PROPERTIES'] as $sortCode => $sortProps) {
			if ($arParams['CATALOG_SORT_PARAMS']['CURRENT']['SORT_FIELD'] == $sortCode) {
				if ($sortCode == 'PROPERTY_MINIMUM_PRICE') {
					if ($arParams['CATALOG_SORT_PARAMS']['CURRENT']['SORT_ORDER'] == $sortProps['ORDER_1']) {
						$sortOrder = $sortProps['ORDER_2'];
					}
					else {
						$sortOrder = $sortProps['ORDER_1'];
					}
				}
				else {
					$sortOrder = $sortProps['ORDER_1'];
				}
				$sortClass = 'sorted';
			}
			else {
				$sortOrder = $sortProps['ORDER_1'];
				$sortClass = '';
			}
			?>
			<div class="sort_item <? echo $sortClass; ?>">
				<a href="<?= $APPLICATION->GetCurPage(false) ?><?= ($_REQUEST['section'] ? '?section=' . $_REQUEST['section'] : '') ?>" onmousedown="setCatalogView(this,'setSort')" data-catalog-sort-field="<? echo $sortCode; ?>" data-catalog-sort-order="<? echo $sortOrder; ?>"><? echo $sortProps['NAME']; ?>
					<?
					if ($sortCode == 'PROPERTY_MINIMUM_PRICE') {
						if ($sortOrder == $sortProps['ORDER_1']) {
							?>
							<i class="fa fa-caret-up"></i>
							<?
						}
						else {
							?>
							<i class="fa fa-caret-down"></i>
							<?
						}
					}
					?>
				</a>
			</div>
			<?
		}
		?>
		<div class="sort_right_block">
			<?
			if (count($arParams['CATALOG_SORT_PARAMS']['SAVED'][$arParams['CATALOG_SORT_PARAMS']['CURRENT']['INDEX']]['ELEMENTS']) > 1) {
				?>
				<div class="count_select">
					<span><? echo GetMessage('CT_BCS_CATALOG_SHOW_PER_QUANTITY'); ?></span>
					<form action="<?= $APPLICATION->GetCurPage(false) ?><?= ($_REQUEST['section'] ? '?section=' . $_REQUEST['section'] : '') ?>" method="post">
						<input type="submit" style="display: none;"/>
						<select name="" class="select_nofilter catalog_show_elements" onchange="setCatalogView(this,'setShowElement')">
							<?
							foreach ($arParams['CATALOG_SORT_PARAMS']['SAVED'][$arParams['CATALOG_SORT_PARAMS']['CURRENT']['INDEX']]['ELEMENTS'] as $countElements) {
								if ($countElements == $arParams['CATALOG_SORT_PARAMS']['CURRENT']['ELEMENTS']) {
									$selected = ' selected="selected"';
								}
								else {
									$selected = '';
								}
								?>
								<option value="<? echo $countElements; ?>"<? echo $selected; ?>><? echo $countElements ?></option>
								<?
								unset($selected);
							}

							if ($arParams['CATALOG_SORT_PARAMS']['CURRENT']['ELEMENTS'] == 10000) {
								$selected = ' selected="selected"';
							}
							else {
								$selected = '';
							}
							?>
							<option value="10000"<? echo $selected; ?>><? echo GetMessage('CT_BCS_CATALOG_SHOW_ALL_ELEMENTS'); ?></option>
						</select>
					</form>
				</div>
				<?
			}
			?>

			<?
			if (count($arParams['CATALOG_SORT_PARAMS']['SAVED']) > 1) {
				?>
				<div class="position_wrapper">
					<div class="position-container">
						<?
						foreach ($arParams['CATALOG_SORT_PARAMS']['SAVED'] as $catalogTemplateIndex => $catalogTemplate) {
							if ($catalogTemplate['CODE'] == $arParams['CATALOG_SORT_PARAMS']['CURRENT']['TEMPLATE']) {
								$selected = 'select';
							}
							else {
								$selected = '';
							}
							if ($catalogTemplate['CODE'] != 'LIST') {
								if ($arParams['CATALOG_SORT_PARAMS']['CURRENT']['TEMPLATE'] != 'LIST') {
									if ($selected == '') {
										$selected .= 'hidden_xs';
									}
								}
								else {
									if ($arParams['CATALOG_SORT_PARAMS']['SAVED'][0]['CODE'] != 'LIST' && $catalogTemplateIndex != 0 && $selected == '') {
										$selected .= 'hidden_xs';
									}
								}
							}
							?>
							<a href="<?= $APPLICATION->GetCurPage(false) ?><?= ($_REQUEST['section'] ? '?section=' . $_REQUEST['section'] : '') ?>" class="position-item view-<? echo strtolower($catalogTemplate['CODE']); ?> <? echo $selected; ?>" onmousedown="setCatalogView(this,'setTemplate');" data-catalog-template="<? echo $catalogTemplate['CODE']; ?>"><i class="fa fa-<? echo strtolower($catalogTemplate['CODE']); ?>"></i></a>
							<?
							unset($selected);
						}
						?>
					</div>
				</div>
				<?
			}
			?>
		</div>
	</section>
	<div class="clear"></div>
	<!-- ���������� ����� -->

	<?

	if ($arParams['CATALOG_SECTION_TEMPLATE']) {
		?>
		<script><? require_once('script.js'); ?></script>
		<?
		if ($arParams['CATALOG_SECTION_TEMPLATE'] == 'LIST') {
			?>
			<script type="text/javascript"><? require_once('script.list.js'); ?></script>
			<?
		}
		else {
			?>
			<script type="text/javascript"><? require_once('script.common.js'); ?></script>
			<?
		}
		require(realpath(dirname(__FILE__)) . '/' . strtolower($arParams['CATALOG_SECTION_TEMPLATE']) . '_template.php');
	}
	?>
	<?
	if ($arResult['NAV_RESULT']->NavPageCount > 1) {
		if ($_REQUEST['p']) {
			$catalogNavpage = $_REQUEST['p']++;
		}
		else {
			$catalogNavpage = '2';
		}
		?>
		<div class="btn_wrapper catalog">
			<a href="<?= $APPLICATION->GetCurPage(false) . '?p=' . $catalogNavpage ?>" class="btn_round btn_border btn_hover_color btn_big"><?= GetMessage('CT_BCS_CATALOG_SHOW_MORE_ELEMENTS') ?></a>
		</div>
		<?
	}
	?>
	<?
	if ($arParams['HIDE_SECTION_DESCRIPTION'] !== 'Y') { ?>
		<div class="catalog_section_description">
			<p><?= $arResult["DESCRIPTION"] ?></p>
		</div>
	<? } ?>
	<script type="text/javascript">
		var bxajaxid_container,
			bxajaxid;
		bxajaxid_container = $("[data-bxajaxid]").closest("[id^=comp_]");
		bxajaxid = bxajaxid_container.attr('id').replace("comp_", "");
		$("[data-bxajaxid]").attr('data-bxajaxid', bxajaxid);
	</script>
	<?
	$frame->beginStub();
	?>
	<div class="preloader single"><?=GetMessage('PRELOADER')?></div>
	<?
}
$frame->end();
?>
