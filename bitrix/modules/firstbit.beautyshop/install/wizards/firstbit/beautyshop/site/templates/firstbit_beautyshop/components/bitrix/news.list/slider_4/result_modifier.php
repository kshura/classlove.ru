<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if (!empty($arResult['ITEMS'])) {
	foreach ($arResult['ITEMS'] as $key => $arItem) {
		if($arItem['PROPERTIES']['SLIDER_PICTURE_SMALL']['VALUE']) {
			$arSlider = CFile::GetFileArray($arItem['PROPERTIES']['SLIDER_PICTURE_SMALL']['VALUE']);
			$arResult['ITEMS'][$key]['PROPERTIES']['SLIDER_PICTURE_SMALL']['VALUE'] = $arSlider;
		}
		if($arItem['PROPERTIES']['BANNER_PICTURE_SMALL']['VALUE']) {
			$arBanner = CFile::GetFileArray($arItem['PROPERTIES']['BANNER_PICTURE_SMALL']['VALUE']);
			$arResult['ITEMS'][$key]['PROPERTIES']['BANNER_PICTURE_SMALL']['VALUE'] = $arBanner;
		}
	}
}
