<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle(GetMessage("AUTH_AUTHORIZE_TITLE"));
$APPLICATION->SetPageProperty('title', GetMessage("AUTH_AUTHORIZE_TITLE"));
$APPLICATION->SetPageProperty("showLeftMenu", "N");
?>
<?
ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>

						<form name="form_auth" class="form form_general form_registration" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
							<input type="hidden" name="AUTH_FORM" value="Y" />
							<input type="hidden" name="TYPE" value="AUTH" />
							<? if (strlen($arResult["BACKURL"]) > 0) { ?>
								<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
							<? } ?>
							<? foreach ($arResult["POST"] as $key => $value) { ?>
								<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
							<? } ?>
							<div class="form_required_wrapper">
								<ul class="form_fields">
									<li>
										<label><?=GetMessage("AUTH_LOGIN")?></label>
										<input class="bx-auth-input" type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
										<i class="fa fa-check-circle not-active" title="<?=GetMessage("AUTH_FIELD_REQUIRED")?>"></i>
									</li>
									<li>
										<label><?=GetMessage("AUTH_PASSWORD")?></label>
										<input class="bx-auth-input" type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" />
										<i class="fa fa-check-circle not-active" title="<?=GetMessage("AUTH_FIELD_REQUIRED")?>"></i>
									</li>
									<li>
										<label></label>
										<label for="USER_REMEMBER" class="checkbox"><input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" />&nbsp;<?=GetMessage("AUTH_REMEMBER_ME")?></label>
									</li>
								</ul>
								<div class="form_btn_wrapper btn_wrapper">
									<?if($arResult["CAPTCHA_CODE"]) { ?>
										<label><?=GetMessage("AUTH_CAPTCHA_PROMT")?>:</label>
										<div class="captha_wrapper">
											<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
											<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
											<input class="bx-auth-input" type="text" name="captcha_word" maxlength="50" value="" size="15" />
											<input type="text" name="captcha_word" maxlength="50" value="" />
										</div>
									<? } ?>
										<? if ($arParams["NOT_SHOW_LINKS"] != "Y") { ?><noindex><a class="btn_wrapper_link" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a></noindex><? } ?><input type="submit" class="btn_round btn_color btn_big" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" />
								</div>
								<? if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y") { ?>
									<?$this->SetViewTarget('pageTitle_subTitle');?>
									<noindex>
										<a href="<?=$arResult["AUTH_REGISTER_URL"]?>" class="btn_round btn_border btn_hover_color pull_right" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a>
									</noindex>
									<?$this->EndViewTarget();?>
								<? } ?>
							</div>
						</form>
<script type="text/javascript">
<? if (strlen($arResult["LAST_LOGIN"])>0) { ?>
try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<? } else { ?>
try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<? } ?>
</script>

<? if($arResult["AUTH_SERVICES"]) { ?>
<? } ?>
