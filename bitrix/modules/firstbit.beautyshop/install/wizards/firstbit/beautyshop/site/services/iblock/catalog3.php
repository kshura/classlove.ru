<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog"))
	return;

if(COption::GetOptionString("firstbit.beautyshop", "wizard_installed", "N", WIZARD_SITE_ID) == "Y" && !WIZARD_INSTALL_DEMO_DATA)
	return;

//update iblocks, demo discount and precet
$shopLocalization = $wizard->GetVar("shopLocalization");

$IBLOCK_CATALOG_ID = (isset($_SESSION["WIZARD_CATALOG_IBLOCK_ID"]) ? (int)$_SESSION["WIZARD_CATALOG_IBLOCK_ID"] : 0);
$IBLOCK_OFFERS_ID = (isset($_SESSION["WIZARD_OFFERS_IBLOCK_ID"]) ? (int)$_SESSION["WIZARD_OFFERS_IBLOCK_ID"] : 0);
//reference update
/*$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => "clothes_colors", "TYPE" => "references"));
if ($arIBlock = $rsIBlock->Fetch())
{
	if (WIZARD_INSTALL_DEMO_DATA)
	{
		$ib = new CIBlock;
		$ib->Update($arIBlock["ID"], array("XML_ID" => "clothes_colors_".WIZARD_SITE_ID));
	}
}*/

if ($IBLOCK_OFFERS_ID)
{
	$iblockCodeOffers = "firstbit_beautyshop_offers_".WIZARD_SITE_ID;
	//IBlock fields
	$iblock = new CIBlock;
	$arFields = Array(
		"ACTIVE" => "Y",
		"FIELDS" => array (
			'IBLOCK_SECTION' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ),
			'ACTIVE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'Y', ), 'ACTIVE_FROM' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ),
			'ACTIVE_TO' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SORT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ),
			'NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ),
			'PREVIEW_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'N', 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', ), ),
			'PREVIEW_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'PREVIEW_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ),
			'DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, ), ),
			'DETAIL_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ),
			'DETAIL_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ),
			'XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ),
			'CODE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'Y', 'TRANSLITERATION' => 'Y', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '_', 'TRANS_OTHER' => '_', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'Y', ), ),
			'TAGS' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_NAME' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ),
			'SECTION_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'N', 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', ), ),
			'SECTION_DESCRIPTION_TYPE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => 'text', ),
			'SECTION_DESCRIPTION' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, ), ),
			'SECTION_XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ),
			'SECTION_CODE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'Y', 'TRANSLITERATION' => 'Y', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '_', 'TRANS_OTHER' => '_', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'Y', ), ), ),
		"CODE" => $iblockCodeOffers,
		"XML_ID" => $iblockCodeOffers
	);
	$iblock->Update($IBLOCK_OFFERS_ID, $arFields);
}

if ($IBLOCK_CATALOG_ID) {
	$iblockCode = "firstbit_beautyshop_catalog_".WIZARD_SITE_ID;
	//IBlock fields
	$iblock = new CIBlock;
	$arFields = Array(
		"ACTIVE" => "Y",
		"FIELDS" => array ( 'IBLOCK_SECTION' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'ACTIVE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'Y', ), 'ACTIVE_FROM' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'ACTIVE_TO' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SORT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'PREVIEW_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'N', 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', ), ), 'PREVIEW_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'PREVIEW_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, ), ), 'DETAIL_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'DETAIL_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'CODE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'Y', 'TRANSLITERATION' => 'Y', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '_', 'TRANS_OTHER' => '_', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'Y', ), ), 'TAGS' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'SECTION_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'N', 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', ), ), 'SECTION_DESCRIPTION_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'SECTION_DESCRIPTION' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, ), ), 'SECTION_XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_CODE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'Y', 'TRANSLITERATION' => 'Y', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '_', 'TRANS_OTHER' => '_', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'Y', ), ), ),
		"CODE" => $iblockCode,
		"XML_ID" => $iblockCode
	);
	$iblock->Update($IBLOCK_CATALOG_ID, $arFields);

	if ($IBLOCK_OFFERS_ID)
	{
		$ID_SKU = CCatalog::LinkSKUIBlock($IBLOCK_CATALOG_ID, $IBLOCK_OFFERS_ID);

		$rsCatalogs = CCatalog::GetList(
			array(),
			array('IBLOCK_ID' => $IBLOCK_OFFERS_ID),
			false,
			false,
			array('IBLOCK_ID')
		);
		if ($arCatalog = $rsCatalogs->Fetch())
		{
			CCatalog::Update($IBLOCK_OFFERS_ID,array('PRODUCT_IBLOCK_ID' => $IBLOCK_CATALOG_ID,'SKU_PROPERTY_ID' => $ID_SKU));
		}
		else
		{
			CCatalog::Add(array('IBLOCK_ID' => $IBLOCK_OFFERS_ID, 'PRODUCT_IBLOCK_ID' => $IBLOCK_CATALOG_ID, 'SKU_PROPERTY_ID' => $ID_SKU));
		}
	}

	//user fields for sections
	$arLanguages = Array();
	$rsLanguage = CLanguage::GetList($by, $order, array());
	while($arLanguage = $rsLanguage->Fetch())
		$arLanguages[] = $arLanguage["LID"];

	$arUserFields = array("UF_BROWSER_TITLE", "UF_KEYWORDS", "UF_META_DESCRIPTION");
	foreach ($arUserFields as $userField)
	{
		$arLabelNames = Array();
		foreach($arLanguages as $languageID)
		{
			WizardServices::IncludeServiceLang("property_names.php", $languageID);
			$arLabelNames[$languageID] = GetMessage($userField);
		}

		$arProperty["EDIT_FORM_LABEL"] = $arLabelNames;
		$arProperty["LIST_COLUMN_LABEL"] = $arLabelNames;
		$arProperty["LIST_FILTER_LABEL"] = $arLabelNames;

		$dbRes = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => 'IBLOCK_'.$IBLOCK_CATALOG_ID.'_SECTION', "FIELD_NAME" => $userField));
		if ($arRes = $dbRes->Fetch())
		{
			$userType = new CUserTypeEntity();
			$userType->Update($arRes["ID"], $arProperty);
		}
		//if($ex = $APPLICATION->GetException())
			//$strError = $ex->GetString();
	}

//precet
	$dbProperty = CIBlockProperty::GetList(Array(), Array("IBLOCK_ID"=>$IBLOCK_CATALOG_ID, "CODE"=>"SALELEADER"));
	$arFields = array();
	while($arProperty = $dbProperty->GetNext())
	{
		$arFields["find_el_property_".$arProperty["ID"]] = "";
	}
	$dbProperty = CIBlockProperty::GetList(Array(), Array("IBLOCK_ID"=>$IBLOCK_CATALOG_ID, "CODE"=>"NEWPRODUCT"));
	while($arProperty = $dbProperty->GetNext())
	{
		$arFields["find_el_property_".$arProperty["ID"]] = "";
	}
	$dbProperty = CIBlockProperty::GetList(Array(), Array("IBLOCK_ID"=>$IBLOCK_CATALOG_ID, "CODE"=>"SPECIALOFFER"));
	while($arProperty = $dbProperty->GetNext())
	{
		$arFields["find_el_property_".$arProperty["ID"]] = "";
	}
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/interface/admin_lib.php");
	CAdminFilter::AddPresetToBase( array(
			"NAME" => GetMessage("WIZ_PRECET"),
			"FILTER_ID" => "tbl_product_admin_".md5($iblockType.".".$IBLOCK_CATALOG_ID)."_filter",
			"LANGUAGE_ID" => $lang,
			"FIELDS" => $arFields
		)
	);
	CUserOptions::SetOption("filter", "tbl_product_admin_".md5($iblockType.".".$IBLOCK_CATALOG_ID)."_filter", array("rows" => "find_el_name, find_el_active, find_el_timestamp_from, find_el_timestamp_to"), true);

	CAdminFilter::SetDefaultRowsOption("tbl_product_admin_".md5($iblockType.".".$IBLOCK_CATALOG_ID)."_filter", array("miss-0","IBEL_A_F_PARENT"));

//delete 1c props
	$arPropsToDelete = array("CML2_TAXES", "CML2_BASE_UNIT", "CML2_TRAITS", "CML2_ATTRIBUTES", "CML2_ARTICLE", "CML2_BAR_CODE", "CML2_FILES", "CML2_MANUFACTURER", "CML2_PICTURES");
	foreach ($arPropsToDelete as $code)
	{
		$dbProperty = CIBlockProperty::GetList(Array(), Array("IBLOCK_ID"=>$IBLOCK_CATALOG_ID, "XML_ID"=>$code));
		if($arProperty = $dbProperty->GetNext())
		{
			CIBlockProperty::Delete($arProperty["ID"]);
		}
		if ($IBLOCK_OFFERS_ID)
		{
			$dbProperty = CIBlockProperty::GetList(Array(), Array("IBLOCK_ID"=>$IBLOCK_OFFERS_ID, "XML_ID"=>$code));
			if($arProperty = $dbProperty->GetNext())
			{
				CIBlockProperty::Delete($arProperty["ID"]);
			}
		}
	}

	$rsProperties = CIBlock::GetProperties($IBLOCK_CATALOG_ID, Array("id" => "asc"), Array('IBLOCK_ID' => $IBLOCK_CATALOG_ID));
	$arIblockProperties = array();
	while($arProperty = $rsProperties->GetNext()) {
		$arIblockProperties[$arProperty['CODE']] = $arProperty;
	}

	WizardServices::IncludeServiceLang("catalog3.php", $lang);
//	CUserOptions::SetOption(
//		"form",
//		"form_element_".$IBLOCK_CATALOG_ID,
//		array (
//			'tabs' => 'edit1--#--'.GetMessage("WZD_OPTION_CATALOG_1").'--,--ACTIVE--#--'.GetMessage("WZD_OPTION_CATALOG_2").'--,--ACTIVE_FROM--#--'.GetMessage("WZD_OPTION_CATALOG_3").'--,--ACTIVE_TO--#--'.GetMessage("WZD_OPTION_CATALOG_4").'--,--NAME--#--'.GetMessage("WZD_OPTION_CATALOG_5").'--,--CODE--#--'.GetMessage("WZD_OPTION_CATALOG_6").'--,--SORT--#--'.GetMessage("WZD_OPTION_CATALOG_7").'--,--IBLOCK_ELEMENT_PROP_VALUE--#----'.GetMessage("WZD_OPTION_CATALOG_8").'--,--PROPERTY_'.$arIblockProperties["MINIMUM_PRICE"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_9").'--,--PROPERTY_'.$arIblockProperties["MORE_PHOTO"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_10").'--,--PROPERTY_'.$arIblockProperties["ARTNUMBER"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_11").'--,--PROPERTY_'.$arIblockProperties["BRAND_REF"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_12").'--,--PROPERTY_'.$arIblockProperties["MANUFACTURER"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_13").'--,--PROPERTY_'.$arIblockProperties["ALL_COLOR"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_14").'--,--PROPERTY_'.$arIblockProperties["ALL_SOSTAV"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_15").'--,--PROPERTY_'.$arIblockProperties["ALL_MATERIAL"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_16").'--,--PROPERTY_'.$arIblockProperties["ALL_VYSOTA"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_17").'--,--PROPERTY_'.$arIblockProperties["ALL_GLUBINA"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_18").'--,--PROPERTY_'.$arIblockProperties["ALL_DLINA"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_19").'--,--PROPERTY_'.$arIblockProperties["ALL_SHIRINA"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_20").'--,--edit1_csection1--#----'.GetMessage("WZD_OPTION_CATALOG_21").'--,--PROPERTY_'.$arIblockProperties["ADVANTAGE_REF"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_22").'--,--PROPERTY_'.$arIblockProperties["NEWPRODUCT"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_23").'--,--PROPERTY_'.$arIblockProperties["SALELEADER"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_24").'--,--PROPERTY_'.$arIblockProperties["SPECIALOFFER"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_25").'--,--PROPERTY_'.$arIblockProperties["RECOMMEND"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_26").'--,--edit1_csection4--#----'.GetMessage("WZD_OPTION_CATALOG_27").'--,--PROPERTY_'.$arIblockProperties["EL_GARANT"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_28").'--,--PROPERTY_'.$arIblockProperties["EL_NET_CLASS"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_29").'--,--PROPERTY_'.$arIblockProperties["EL_MAT_KORP"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_30").'--,--PROPERTY_'.$arIblockProperties["EL_RAZEM"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_31").'--,--PROPERTY_'.$arIblockProperties["EL_SERIYA"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_32").'--,--PROPERTY_'.$arIblockProperties["EL_BLUETOOTH"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_33").'--,--PROPERTY_'.$arIblockProperties["EL_NET_DIAG"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_34").'--,--PROPERTY_'.$arIblockProperties["EL_NET_DIAG_RAZ"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_35").'--,--PROPERTY_'.$arIblockProperties["EL_NET_HDD"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_36").'--,--PROPERTY_'.$arIblockProperties["EL_NET_SLOT_OP_PAM"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_37").'--,--PROPERTY_'.$arIblockProperties["EL_NET_OP_PAM_MAX"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_38").'--,--PROPERTY_'.$arIblockProperties["EL_NET_OP_PAM"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_39").'--,--PROPERTY_'.$arIblockProperties["EL_WI_FI"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_40").'--,--PROPERTY_'.$arIblockProperties["EL_RAB_ACCOMEL"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_41").'--,--PROPERTY_'.$arIblockProperties["EL_NET_TEX_DISP"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_42").'--,--PROPERTY_'.$arIblockProperties["EL_NET_TIP_OP_PAM"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_43").'--,--PROPERTY_'.$arIblockProperties["EL_NET_CHAST_PAM"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_44").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_LTE"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_45").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_SCREEN_DIAGONAL"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_46").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_SIM_NUMBER"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_47").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_MEMORY_VOLUME"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_48").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_OS"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_49").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_CARD"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_50").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_SCREEN_RESOLUTION"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_51").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_TOUCH_SCREEN"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_52").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_SIM_TYPE"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_53").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_CASE_TYPE"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_54").'--,--PROPERTY_'.$arIblockProperties["EL_SMART_CPU_TYPE"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_55").'--,--edit1_csection2--#----'.GetMessage("WZD_OPTION_CATALOG_56").'--,--PROPERTY_'.$arIblockProperties["OD_YBK_DLINA"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_57").'--,--PROPERTY_'.$arIblockProperties["OD_ZASTEZHKA"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_58").'--,--PROPERTY_'.$arIblockProperties["OD_SEZON"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_59").'--,--PROPERTY_'.$arIblockProperties["OD_STYLE"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_60").'--,--PROPERTY_'.$arIblockProperties["OD_TIP_YBK"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_61").'--,--edit1_csection3--#----'.GetMessage("WZD_OPTION_CATALOG_62").'--,--PROPERTY_'.$arIblockProperties["KOS_PRIM"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_63").'--,--PROPERTY_'.$arIblockProperties["KOS_TIP"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_64").'--,--edit1_csection5--#----'.GetMessage("WZD_OPTION_CATALOG_65").'--,--PROPERTY_'.$arIblockProperties["OB_H_KABLUK"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_66").'--;--edit5--#--'.GetMessage("WZD_OPTION_CATALOG_67").'--,--PREVIEW_PICTURE--#--'.GetMessage("WZD_OPTION_CATALOG_68").'--,--PREVIEW_TEXT--#--'.GetMessage("WZD_OPTION_CATALOG_69").'--;--edit6--#--'.GetMessage("WZD_OPTION_CATALOG_70").'--,--DETAIL_PICTURE--#--'.GetMessage("WZD_OPTION_CATALOG_71").'--,--DETAIL_TEXT--#--'.GetMessage("WZD_OPTION_CATALOG_72").'--;--edit8--#--'.GetMessage("WZD_OPTION_CATALOG_73").'--,--OFFERS--#--'.GetMessage("WZD_OPTION_CATALOG_74").'--;--cedit1--#--'.GetMessage("WZD_OPTION_CATALOG_75").'--,--PROPERTY_'.$arIblockProperties["DOCUMENTS"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_76").'--,--PROPERTY_'.$arIblockProperties["VIDEO"]["ID"].'--#--'.GetMessage("WZD_OPTION_CATALOG_77").'--;--edit2--#--'.GetMessage("WZD_OPTION_CATALOG_78").'--,--SECTIONS--#--'.GetMessage("WZD_OPTION_CATALOG_79").'--;--edit14--#--'.GetMessage("WZD_OPTION_CATALOG_80").'--,--IPROPERTY_TEMPLATES_ELEMENT_META_TITLE--#--'.GetMessage("WZD_OPTION_CATALOG_81").'--,--IPROPERTY_TEMPLATES_ELEMENT_META_KEYWORDS--#--'.GetMessage("WZD_OPTION_CATALOG_82").'--,--IPROPERTY_TEMPLATES_ELEMENT_META_DESCRIPTION--#--'.GetMessage("WZD_OPTION_CATALOG_83").'--,--IPROPERTY_TEMPLATES_ELEMENT_PAGE_TITLE--#--'.GetMessage("WZD_OPTION_CATALOG_84").'--,--IPROPERTY_TEMPLATES_ELEMENTS_PREVIEW_PICTURE--#----'.GetMessage("WZD_OPTION_CATALOG_85").'--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_ALT--#--'.GetMessage("WZD_OPTION_CATALOG_86").'--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_TITLE--#--'.GetMessage("WZD_OPTION_CATALOG_87").'--,--IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_NAME--#--'.GetMessage("WZD_OPTION_CATALOG_88").'--,--IPROPERTY_TEMPLATES_ELEMENTS_DETAIL_PICTURE--#----'.GetMessage("WZD_OPTION_CATALOG_89").'--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_ALT--#--'.GetMessage("WZD_OPTION_CATALOG_90").'--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_TITLE--#--'.GetMessage("WZD_OPTION_CATALOG_91").'--,--IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_NAME--#--'.GetMessage("WZD_OPTION_CATALOG_92").'--,--SEO_ADDITIONAL--#----'.GetMessage("WZD_OPTION_CATALOG_93").'--,--TAGS--#--'.GetMessage("WZD_OPTION_CATALOG_94").'--;--',
//		),
//		"Y"
//	);
//	CUserOptions::SetOption(
//		"list",
//		"tbl_iblock_list_".md5("firstbit_beautyshop_catalog.".$IBLOCK_CATALOG_ID),
//		array (
//			'columns' => 'ACTIVE,ID,NAME,PROPERTY_'.$arIblockProperties["ARTNUMBER"]["ID"].',CATALOG_TYPE,CATALOG_GROUP_1,SORT,DATE_CREATE,TIMESTAMP_X',
//			'by' => 'timestamp_x',
//			'order' => 'desc',
//			'page_size' => '20',
//		),
//		"Y"
//	);

	$bitrixTemplateDir = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".WIZARD_TEMPLATE_ID."_".WIZARD_SITE_ID;
	CWizardUtil::ReplaceMacros($bitrixTemplateDir . "/header.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID, "OFFERS_IBLOCK_ID" => $IBLOCK_OFFERS_ID));
	CWizardUtil::ReplaceMacros($bitrixTemplateDir . "/footer.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/_index.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID, "OFFERS_IBLOCK_ID" => $IBLOCK_OFFERS_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/catalog/index.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/ajax/bottom_panel/wishlist.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/ajax/bottom_panel/compare.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/ajax/bottom_panel/viewed.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/search/index.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID, "OFFERS_IBLOCK_ID" => $IBLOCK_OFFERS_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/brands/index.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/sale/index.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/wishlist/index.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/catalog/.top_middle_submenu.menu_ext.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
//	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/sect_inc.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
//	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/catalog/sect_sidebar.php.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID));
//	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/include/viewed_product.php", array("CATALOG_IBLOCK_ID" => $IBLOCK_CATALOG_ID, "OFFERS_IBLOCK_ID" => $IBLOCK_OFFERS_ID));
}
?>