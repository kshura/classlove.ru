<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>
<?
CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");

switch($_POST['action']) {
	case 'update':

		try {
			if (!isset($_POST['PRODUCT_ID']))
				throw new Exception('ERROR PRODUCT ID');

			$rsProduct = CCatalogSku::GetProductInfo($_POST['PRODUCT_ID']);
			if (is_array($rsProduct)) {
				$IBLOCK_ID = CIBlockElement::GetIBlockByID($rsProduct['ID']);
			} else {
				$IBLOCK_ID = CIBlockElement::GetIBlockByID($_POST['PRODUCT_ID']);
			}
			$PRODUCT_ID = $_POST['PRODUCT_ID'];

			if(!$IBLOCK_ID)
				throw new Exception('ERROR IBLOCK ID');

			if ($_SESSION['CATALOG_COMPARE_LIST']) {
				if ($_SESSION['CATALOG_COMPARE_LIST'][$IBLOCK_ID]) {
					if ($_SESSION['CATALOG_COMPARE_LIST'][$IBLOCK_ID]['ITEMS']) {
						if (array_key_exists($PRODUCT_ID, $_SESSION['CATALOG_COMPARE_LIST'][$IBLOCK_ID]['ITEMS'])) {
							$compareAction = 'delete';
						} else {
							$compareAction = 'add';
						}
					} else {
						$compareAction = 'add';
					}
				} else {
					$compareAction = 'add';
				}
			} else {
				$compareAction = 'add';
			}

			switch ($compareAction) {
				case 'add':
					$_SESSION['CATALOG_COMPARE_LIST'][$IBLOCK_ID]['ITEMS'][$PRODUCT_ID] = array();
					break;
				case 'delete':
					unset($_SESSION['CATALOG_COMPARE_LIST'][$IBLOCK_ID]['ITEMS'][$PRODUCT_ID]);
					break;
			}

			$mes = $compareAction;
			if(!$mes) $mes = true;
			echo json_encode(array(
				'status' => true,
				'mes' => $mes,
				'iblock' => $IBLOCK_ID
			));
		} catch (Exception $e) {
			echo json_encode(array(
				'status' => false,
				'mes' => $mes . 'ERROR: ' . $e->getMessage()
			));
		}

	break;
	case 'check':
		try {
			if (!isset($_POST['PRODUCT_ID']))
				throw new Exception('ERROR PRODUCT ID');

			$rsProduct = CCatalogSku::GetProductInfo($_POST['PRODUCT_ID']);
			if (is_array($rsProduct)) {
				$PRODUCT_ID = $rsProduct['ID'];
				$OFFER_ID = $_POST['PRODUCT_ID'];
			} else {
				$PRODUCT_ID = $_POST['PRODUCT_ID'];
			}

			if(!$IBLOCK_ID = CIBlockElement::GetIBlockByID($PRODUCT_ID))
				throw new Exception('ERROR IBLOCK ID');

			if(count($_SESSION['CATALOG_COMPARE_LIST'])>0) {
				if (array_key_exists($_POST['PRODUCT_ID'], $_SESSION['CATALOG_COMPARE_LIST'][$IBLOCK_ID]['ITEMS'])) {
					$mes = true;
				} else {
					$mes = false;
				}
			} else {
				$mes = false;
			}

			echo json_encode(array(
				'status' => true,
				'mes' => $mes
			));
		} catch (Exception $e) {
			echo json_encode(array(
				'status' => false,
				'mes' => $mes . 'ERROR: ' . $e->getMessage()
			));
		}
		break;
	case 'count':
		try {
			if ($_SESSION['CATALOG_COMPARE_LIST']) {
				if ($_SESSION['CATALOG_COMPARE_LIST'][$_POST['IBLOCK_ID']]) {
					$countCompare = count($_SESSION['CATALOG_COMPARE_LIST'][$_POST['IBLOCK_ID']]['ITEMS']);
					if ($countCompare > 0) {
						$mes = $countCompare;
					}
					else {
						$mes = '0';
					}
				}
			} else {
				$mes = '0';
			}
			echo json_encode(array(
				'status' => true,
				'mes' => $mes
			));
		} catch (Exception $e) {
			echo json_encode(array(
				'status' => false,
				'mes' => $mes . 'ERROR: ' . $e->getMessage()
			));
		}
		break;
}