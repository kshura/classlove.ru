<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

$total_quantity = 0;
if(is_array($arResult["CATEGORIES"]["READY"]) && count($arResult["CATEGORIES"]["READY"])>0) {
	foreach ($arResult["CATEGORIES"]["READY"] as $arItem) {
		$total_quantity = $total_quantity + $arItem['QUANTITY'];
	}
}
$arResult['NUM_PRODUCTS'] = $total_quantity;