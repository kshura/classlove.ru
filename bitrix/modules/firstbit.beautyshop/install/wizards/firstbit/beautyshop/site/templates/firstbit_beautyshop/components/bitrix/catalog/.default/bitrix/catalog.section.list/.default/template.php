<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="row catalog_main">
	<div class="container_16">
		<div class="catalog_container">
			<div class="catalog_container_inner">
				<?
				$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
				$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
				$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

				?><?
				if (0 < $arResult["SECTIONS_COUNT"]) {
					$evenCount = 0;
				?>
					<?
					foreach ($arResult['SECTIONS'] as $arSection) {
						$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
						$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

						if (false === $arSection['PICTURE'])
							$arSection['PICTURE'] = array(
								'SRC' => $arCurView['EMPTY_IMG'],
								'ALT' => (
								'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
									? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
									: $arSection["NAME"]
								),
								'TITLE' => (
								'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
									? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
									: $arSection["NAME"]
								)
							);
						if ($arSection['DEPTH_LEVEL'] == 1) {
							$evenCount++;
							if(($evenCount%2) == 1) {
							?>
							<div class="catalog_container_row">
							<?
							} ?>
								<div class="catalog_container_cell">
									<div class="catalog_container_item">
										<div class="catalog_container_row">
											<div class="catalog_container_cell catalog_container_img">
												<img src="<?=$arSection['PICTURE']['SRC']?>" alt="<?=$arSection['PICTURE']['ALT']?>" title="<?=$arSection['PICTURE']['TITLE']?>">
											</div>
											<div class="catalog_container_cell catalog_list_wrapper">
												<div class="catalog_list_inner">
													<h2><a class="catalog_section_top" href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a></h2>
													<ul>
														<? foreach ($arResult['SECTIONS'] as &$arSubsection):
															if ($arSubsection['DEPTH_LEVEL'] == 2 && $arSubsection['IBLOCK_SECTION_ID'] == $arSection['ID']): ?>
																<li><a href="<?=$arSubsection['SECTION_PAGE_URL']?>"><?=$arSubsection['NAME']?></a></li>
																<?
															endif;
														endforeach; ?>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?
							if(($evenCount%2) == 0) {
								?>
									</div>
								<?
							}
						}
					}

					if(($evenCount%2) == 1) {
					?>
						</div>
					<?
					}
				} ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>