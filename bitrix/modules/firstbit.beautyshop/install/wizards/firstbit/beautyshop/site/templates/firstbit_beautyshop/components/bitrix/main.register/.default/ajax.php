<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
global $APPLICATION;
$APPLICATION->RestartBuffer();

switch ($_POST['action']) {
	case 'checkLocationByZip':
		$rsLocationByZip = CSaleLocation::GetByZIP($_POST['location']);
		try {
			if (count($rsLocationByZip) <= 1) {
				throw new Exception('ERROR LOCATIONS FOUND');
			}
			echo json_encode(array('status' => 'OK', 'data' => $rsLocationByZip, 'mes' => 'LOCATION FOUND'));
		} catch (Exception $e) {
			echo json_encode(array('status' => 'ERR', 'mes' => $e->getMessage()));
		}
		break;
	case 'updateLocationByZip':
		$APPLICATION->IncludeComponent(
			"bitrix:sale.location.selector.steps",
			"",
			Array(
				"COMPONENT_TEMPLATE" => ".default",
				"ID" => $_POST['location'],
				"CODE" => "",
				"INPUT_NAME" => "REGISTER['LOCATION']",
				"PROVIDE_LINK_BY" => "id",
				"JSCONTROL_GLOBAL_ID" => "",
				"JS_CALLBACK" => "",
				"SEARCH_BY_PRIMARY" => "N",
				"FILTER_BY_SITE" => "Y",
				"SHOW_DEFAULT_LOCATIONS" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"FILTER_SITE_ID" => "s1"
			)
		);
		break;
}
?>
