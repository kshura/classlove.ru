$(document).ready(function() {
	$('.menu-list-level1 > .menu-list-item > a').click(function () {
		$('.menu-list-level1 > .menu-list-item').removeClass('active');
		$(this).parent().toggleClass('active');
		if($(this).parent().hasClass('dropdown')) {
			return false;
		}
	});
});