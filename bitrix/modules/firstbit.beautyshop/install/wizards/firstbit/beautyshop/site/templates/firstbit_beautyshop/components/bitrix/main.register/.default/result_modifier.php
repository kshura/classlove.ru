<?php
if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
$firstBit = new CFirstbitBeautyshop(SITE_ID);

$separateRegistration = $firstBit->options['SEPARATE_REGISTRATION'];
if($separateRegistration == 'Y') {
	$arParams['SEPARATE_REGISTRATION'] = $separateRegistration;
	$profileProperties = unserialize($firstBit->options['CREATE_PROFILE_PROPERTIES']);
	$arParams['PROPS_GROUP_USE'] = $firstBit->options['GROUP_PROFILE_PROPERTIES'];
}
if($separateRegistration == 'Y') {
	$rsPersonType = CSalePersonType::GetList(array("SORT" => "ASC"), array("ACTIVE" => "Y"), false, false, array("ID", "NAME"));
	if($rsPersonType->SelectedRowsCount()>0) {
		while($arPersonType = $rsPersonType->Fetch()) {
			$rsPropertiesGroup = CSaleOrderPropsGroup::GetList(array("SORT" => "ASC"), array("PERSON_TYPE_ID" => $arPersonType["ID"], "ACTIVE" => "Y"), false, false, array("ID", "NAME"));
			while ($arPropertiesGroup = $rsPropertiesGroup->Fetch()) {
				$rsProperty = CSaleOrderProps::GetList(array("SORT" => "ASC"), array("PROPS_GROUP_ID" => $arPropertiesGroup["ID"], "ACTIVE" => "Y", "USER_PROPS" => "Y"), false, false, array());
				while ($arProperty = $rsProperty->Fetch()) {
					if(in_array($arProperty['ID'], $profileProperties)) {
						if ($arProperty['TYPE'] == 'SELECT') {
							$rsPropertyVariant = CSaleOrderPropsVariant::GetList(array("SORT" => "ASC"), array("ORDER_PROPS_ID" => $arProperty["ID"]), false, false, array());
							while ($arPropertyVariant = $rsPropertyVariant->Fetch()) {
								$arProperty['VARIANTS'][] = $arPropertyVariant;
							}
						}
						$arPropertiesGroup['PROPERTIES'][] = $arProperty;
					}
				}
				if(!empty($arPropertiesGroup['PROPERTIES'])) {
					$arPersonType['PROPERTIES_GROUPS'][] = $arPropertiesGroup;
				}
			}
			if(!empty($arPersonType['PROPERTIES_GROUPS'])) {
				$arResult['PERSON_TYPES'][] = $arPersonType;
			}
		}
	}
}

$emailLogin = COption::GetOptionString('firstbit.mag', 'EMAIL_LOGIN', 'N');
if($emailLogin == 'Y') {
	$arParams['EMAIL_LOGIN'] = $emailLogin;
	$emailField = array_search('EMAIL', $arResult['SHOW_FIELDS']);
	if ($emailField) {
		array_unshift($arResult['SHOW_FIELDS'], $arResult['SHOW_FIELDS'][$emailField]);
		unset($arResult['SHOW_FIELDS'][$emailField + 1]);
	}
	$emailFieldRequired = array_search('EMAIL', $arResult['REQUIRED_FIELDS']);
	if ($emailFieldRequired) {
		array_unshift($arResult['REQUIRED_FIELDS'], $arResult['REQUIRED_FIELDS'][$emailFieldRequired]);
		unset($arResult['REQUIRED_FIELDS'][$emailFieldRequired + 1]);
	}
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_REQUEST["register_submit_button"]) && !$USER->IsAuthorized()) {
	foreach ($_REQUEST["REGISTER"] as $requestFieldName=>$requestFieldValue) {
		$arResult["VALUES"][$requestFieldName] = htmlspecialcharsbx($requestFieldValue, ENT_QUOTES);
	}
}

