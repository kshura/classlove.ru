<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
use Bitrix\Sale\DiscountCouponsManager;
if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

$arEmptyPreview = CFile::GetFileArray(CFirstbitBeautyshop::queryOption('NO_PHOTO', SITE_ID));

if ($normalCount > 0):
	?>
	<div id="basket_items_list">
		<div class="cart_form_inner bottom_panel_first_line">
			<table id="basket_items" class="cart_list">
				<thead>
				<tr class="cart_item">
					<?
					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader) {
						$arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
						if ($arHeader["name"] == '')
							$arHeader["name"] = GetMessage("SALE_" . $arHeader["id"]);
						$arHeaders[] = $arHeader["id"];

						if ($arHeader["id"] == "NAME") {
							?>
							<td class="cart_column1"><div class="cart_item_title"><?= $arHeader["name"]; ?></div></td>
							<td class="cart_column_group">
							<?
						}
						elseif ($arHeader["id"] == "QUANTITY") {
							?>
							<div class="cart_column4">
								<div class="cart_item_title"><?= $arHeader["name"]; ?></div>
							</div>
							<?
						}
						elseif ($arHeader["id"] == "PRICE") {
							?>
							<div class="cart_column3">
								<div class="cart_item_title"><?= $arHeader["name"]; ?></div>
							</div>
							<?
						}
						elseif ($arHeader["id"] == "DISCOUNT") {
							?>
							<div class="cart_column2">
								<div class="cart_item_title"><?= $arHeader["name"]; ?></div>
							</div>
							<?
						}
						else {
							?>
							<div class="cart_column5">
								<div class="cart_item_title"><?= $arHeader["name"]; ?></div>
							</div>
							<?
						}
					}
					?>
						<div class="cart_column6">
							<div class="cart_item_title"></div>
						</div>
					</td>
				</tr>
				</thead>
				<tbody>
				<?
				foreach($arResult["GRID"]["ROWS"] as $k => $arItem) {
					if($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y") {
						?>
						<tr id="<?= $arItem["ID"] ?>" class="cart_item">
							<?
							foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader) {
							if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE", "WEIGHT"))) {
								continue;
							}
							if ($arHeader["name"] == '') {
								$arHeader["name"] = GetMessage("SALE_" . $arHeader["id"]);
							}
							if ($arHeader["id"] == "NAME") {
							?>
							<td class="cart_column1">
								<div class="cart_item_info">
									<?
									if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0) {
										$url = $arItem["PREVIEW_PICTURE_SRC"];
									} elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0) {
										$url = $arItem["DETAIL_PICTURE_SRC"];
									} else {
										$url = $arEmptyPreview['SRC'];
									}
									?>
									<div class="cart_img">
										<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" target="">
											<img src="<?= $url ?>"/>
										</a>
									</div>
									<div class="cart_descr">
										<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" target="">
											<h2><?= $arItem["NAME"] ?></h2>
										</a>
										<div class="bx_ordercart_itemart">
											<?
											if ($bPropsColumn) {
												foreach ($arItem["PROPS"] as $val) {
													if (is_array($arItem["SKU_DATA"])) {
														$bSkip = false;
														foreach ($arItem["SKU_DATA"] as $propId => $arProp) {
															if ($arProp["CODE"] == $val["CODE"]) {
																$bSkip = true;
																break;
															}
														}
														if ($bSkip) {
															continue;
														}
													}
													echo $val["NAME"] . ":&nbsp;<span>" . $val["VALUE"] . "<span><br/>";
												}
											}
											?>
										</div>
										<?
										if (is_array($arItem["SKU_DATA"]) && !empty($arItem["SKU_DATA"])) {
											foreach ($arItem["SKU_DATA"] as $propId => $arProp) {
												$isImgProperty = false;
												if (!empty($arProp["VALUES"]) && is_array($arProp["VALUES"])) {
													foreach ($arProp["VALUES"] as $id => $arVal) {
														if (!empty($arVal["PICT"]) && is_array($arVal["PICT"]) && !empty($arVal["PICT"]['SRC'])) {
															$isImgProperty = true;
															break;
														}
													}
												}
												$countValues = count($arProp["VALUES"]);
												$full = ($countValues > 5) ? "full" : "";
												if ($isImgProperty) {
													?>
													<div
														class="bx_item_detail_scu_small_noadaptive <?= $full ?>">
																<span
																	class="bx_item_section_name_gray"><?= $arProp["NAME"] ?>
																	:</span>
														<div class="bx_scu_scroller_container">
															<div class="bx_scu">
																<ul id="prop_<?= $arProp["CODE"] ?>_<?= $arItem["ID"] ?>"
																	style="width: 200%; margin-left:0;"
																	class="sku_prop_list">
																	<?
																	foreach ($arProp["VALUES"] as $valueId =>
																			 $arSkuValue) {
																		$selected = "";
																		foreach ($arItem["PROPS"] as $arItemProp) {
																			if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"]) {
																				if ($arItemProp["VALUE"] == $arSkuValue["NAME"] || $arItemProp["VALUE"] == $arSkuValue["XML_ID"]) {
																					$selected = " bx_active";
																				}
																			}
																		}
																		?>
																		<li style="width:10%;"
																			class="sku_prop<?= $selected ?>"
																			data-value-id="<?= $arSkuValue["XML_ID"] ?>"
																			data-element="<?= $arItem["ID"] ?>"
																			data-property="<?= $arProp["CODE"] ?>">
																			<a href="javascript:void(0)"
																			 class="cnt"><span
																					class="cnt_item"
																					style="background-image:url(<?= $arSkuValue["PICT"]["SRC"]; ?>)"></span></a>
																		</li>
																		<?
																	}
																	?>
																</ul>
															</div>
															<div class="bx_slide_left"
																 onclick="leftScroll('<?= $arProp["CODE"] ?>', <?= $arItem["ID"] ?>, <?= $countValues ?>);"></div>
															<div class="bx_slide_right"
																 onclick="rightScroll('<?= $arProp["CODE"] ?>', <?= $arItem["ID"] ?>, <?= $countValues ?>);"></div>
														</div>
													</div>
													<?
												} else {
													?>
													<div
														class="bx_item_detail_size_small_noadaptive <?= $full ?>">
																<span
																	class="bx_item_section_name_gray"><?= $arProp["NAME"] ?>
																	:</span>
														<div class="bx_size_scroller_container">
															<div class="bx_size">
																<ul id="prop_<?= $arProp["CODE"] ?>_<?= $arItem["ID"] ?>"
																	style="width: 200%; margin-left:0;"
																	class="sku_prop_list">
																	<?
																	foreach ($arProp["VALUES"] as $valueId =>
																			 $arSkuValue) {
																		$selected = "";
																		foreach ($arItem["PROPS"] as $arItemProp) {
																			if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"]) {
																				if ($arItemProp["VALUE"] == $arSkuValue["NAME"]) {
																					$selected = " bx_active";
																				}
																			}
																		}
																		?>
																		<li style="width:10%;"
																			class="sku_prop<?= $selected ?>"
																			data-value-id="<?= ($arProp['TYPE'] == 'S' && $arProp['USER_TYPE'] == 'directory' ? $arSkuValue['XML_ID'] : $arSkuValue['NAME']); ?>"
																			data-element="<?= $arItem["ID"] ?>"
																			data-property="<?= $arProp["CODE"] ?>">
																			<a href="javascript:void(0)"
																			 class="cnt"><?= $arSkuValue["NAME"] ?></a>
																		</li>
																		<?
																	}
																	?>
																</ul>
															</div>
															<div class="bx_slide_left"
																 onclick="leftScroll('<?= $arProp["CODE"] ?>', <?= $arItem["ID"] ?>, <?= $countValues ?>);"></div>
															<div class="bx_slide_right"
																 onclick="rightScroll('<?= $arProp["CODE"] ?>', <?= $arItem["ID"] ?>, <?= $countValues ?>);"></div>
														</div>
													</div>
													<?
												}
											}
										}
										?>
									</div>
								</div>
							</td>
							<td class="cart_column_group">
								<?
								}
								elseif ($arHeader["id"] == "QUANTITY") {
									?>
									<div class="cart_column4">
										<div class="cart_item_info">
											<div class="centered counter-wrapper">
												<?
												$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
												$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"" . $arItem["AVAILABLE_QUANTITY"] . "\"" : "";
												$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
												$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
												?>
												<input
													type="text"
													size="3"
													id="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
													name="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
													size="2"
													maxlength="18"
													min="0"
													<?= $max ?>
													step="<?= $ratio ?>"
													style="max-width: 50px"
													value="<?= $arItem["QUANTITY"] ?>"
													onchange="updateQuantity('QUANTITY_INPUT_<?= $arItem["ID"] ?>', '<?= $arItem["ID"] ?>', <?= $ratio ?>, <?= $useFloatQuantityJS ?>)"
												>
												<?
												if (!isset($arItem["MEASURE_RATIO"])) {
													$arItem["MEASURE_RATIO"] = 1;
												}

												if (
													floatval($arItem["MEASURE_RATIO"]) != 0
												):
													?>
													<a href="javascript:void(0);" class="plus"
													 onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'up', <?= $useFloatQuantityJS ?>);">+</a>
													<a href="javascript:void(0);" class="minus"
													 onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'down', <?= $useFloatQuantityJS ?>);">-</a>
													<?
												endif;
												?>
											</div>
											<input type="hidden" id="QUANTITY_<?= $arItem['ID'] ?>"
												 name="QUANTITY_<?= $arItem['ID'] ?>"
												 value="<?= $arItem["QUANTITY"] ?>"/>
										</div>
									</div>
									<?
								} elseif ($arHeader["id"] == "PRICE") {
									?>
									<div class="cart_column3">
										<div class="cart_item_info">
											<div class="old_price" id="old_price_<?= $arItem["ID"] ?>">
												<? if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0): ?>
													<?= $arItem["FULL_PRICE_FORMATED"] ?>
												<? endif; ?>
											</div>
											<div class="current_price" id="current_price_<?= $arItem["ID"] ?>">
												<?= $arItem["PRICE_FORMATED"] ?>
											</div>
											<? if ($bPriceType && strlen($arItem["NOTES"]) > 0): ?>
												<div class="type_price"><?= GetMessage("SALE_TYPE") ?></div>
												<div class="type_price_value"><?= $arItem["NOTES"] ?></div>
											<? endif; ?>
										</div>
									</div>
									<?
								} elseif ($arHeader["id"] == "DISCOUNT") {
									?>
									<div class="cart_column2">
										<div class="cart_item_info">
											<div
												id="discount_value_<?= $arItem["ID"] ?>"><?= $arItem["DISCOUNT_PRICE_PERCENT_FORMATED"] ?></div>
										</div>
									</div>
									<?
								} else {
									?>
									<div class="cart_column5">
										<div class="cart_item_info">
											<div id="sum_<?= $arItem["ID"] ?>">
												<? echo $arItem[$arHeader["id"]]; ?>
											</div>
										</div>
									</div>
									<?
								}
								}
								?>
								<div class="cart_column6">
									<div class="cart_item_info">
										<a href="javascript:void(0)" onclick="deleteFromBasket(<?=$arItem["ID"]?>);" class="" title="<?=GetMessage("SALE_DELETE")?>"><div class="btn_delete"><i class="fa fa-times"></i></div></a>
									</div>
								</div>
							</td>
						</tr>
						<?
					}
				}
				?>
				</tbody>
			</table>
			<input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
			<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
			<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
			<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
			<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
		</div>
		<div class="bottom_panel_second_line">
			<a href="<?=$arParams['PATH_TO_BASKET']?>" class="btn_round btn_border btn_hover_color"><?=GetMessage("SALE_GOTO_BASKET")?></a>
			<a href="javascript:void(0)" onclick="checkOut();" class="btn_round btn_color"><?=GetMessage("SALE_ORDER")?>:&nbsp;<strong id="allSum_FORMATED"><?=$arResult["allSum_FORMATED"]?></strong></a>
		</div>
	</div>
<?
else:
?>
<div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td style="text-align:center">
					<div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?
endif;
?>