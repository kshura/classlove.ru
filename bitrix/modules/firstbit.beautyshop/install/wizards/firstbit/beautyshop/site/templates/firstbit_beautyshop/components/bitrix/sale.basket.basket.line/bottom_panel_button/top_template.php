<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>

<i class="fa fa-shopping-cart"></i>
<span class="title"><? echo GetMessage('TSB1_CART'); ?></span>
<span class="count <? if($arResult['NUM_PRODUCTS']<=0) echo 'null';?>" id="pannel_bottom_cart_container">
	<span class="cart_item_count"><?=$arResult['NUM_PRODUCTS']?> <span class="hidden_xs"><?=GetMessage('TSB1_MEASURE')?></span></span><span class="cart_price_total hidden_xs"><?=$arResult['TOTAL_PRICE']?></span>
</span>
<i class="fa fa-chevron-up"></i>