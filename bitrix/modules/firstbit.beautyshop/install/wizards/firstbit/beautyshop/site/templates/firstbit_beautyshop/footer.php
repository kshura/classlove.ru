<? if (!CSite::InDir("#SITE_DIR#catalog/") && !CSite::InDir("#SITE_DIR#brands/") && $APPLICATION->GetCurPage()!="#SITE_DIR#index.php" && $APPLICATION->GetCurPage() != "#SITE_DIR#") : ?>
				</div>
				<div class="clear"></div>
			</div>
		</div>
<? endif; ?>
	</div>
</div>
<div class="footer_wrapper">
	<?
	ob_start();
	$pageClass = $APPLICATION->GetProperty('pageContainerClass');
	$APPLICATION->AddViewContent('pageContainerClass', ob_get_contents());
	ob_end_clean();
	ob_start();
	if($APPLICATION->GetProperty('showLeftMenu') != 'N') {
		$APPLICATION->IncludeComponent("bitrix:menu", "left", Array(
			"ROOT_MENU_TYPE" => "left",
			"CHILD_MENU_TYPE" => "left_submenu",
			"MAX_LEVEL" => "2",
			"USE_EXT" => "Y",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "Y",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => ""
		));

		$APPLICATION->GetProperty('showLeftMenu') == 'Y' ? $gridClass = 12 : $gridClass = 16;
	} else {
		$gridClass = 16;
	}
	?>
	<div class="grid_<?=$gridClass?> grid_16_sm grid_16_xs margin_sm_10 margin_xs_10">
		<?
		$APPLICATION->AddViewContent('showLeftMenu', ob_get_contents());
		ob_end_clean();
		?>

		<?
		if($gridClass == 16)
			$APPLICATION->AddViewContent('pageTitleBorder','<div class="border_tabs"></div>');
		?>
		<? $APPLICATION->IncludeComponent('firstbit:footer', '', array(
			'CACHE_TIME' => 3600,
			'CACHE_TYPE' => 'A',
			"SOCIAL_SERVICES_IBLOCK_ID" => "#SOCIAL_SERVICES_IBLOCK_ID#",
			"SUBSCRIBE_RUBRIC_ID" => "#SUBSCRIBE_RUBRIC_ID#"
		), false, array('HIDE_ICONS' => 'Y')) ?>
		<div id="bx-composite-banner"></div>
	</div>
</div>
<?
if(!CModule::IncludeModule("firstbit.beautyshop")) die();
$firstBit = new CFirstbitBeautyshop(SITE_ID);
if($firstBit->options['USE_BOTTOM_PANEL'] != 'N') {
	?>
	<? $APPLICATION->IncludeComponent('firstbit:bottom_panel', '', array(
		'CATALOG_IBLOCK_ID' => "#CATALOG_IBLOCK_ID#"
	), false, array('HIDE_ICONS' => 'Y')) ?>
	<?
}
?>
</div>
</body>
</html>