<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php'); ?>
<?
$APPLICATION->SetTitle('Главная');
?>
<?$APPLICATION->IncludeComponent('firstbit:mainpage',
	'',
	array(
		"MAINPAGE_BLOCKS" => array('SLIDER', 'BANNERS', 'HITS', 'NEWPRODUCT', 'BRANDS', 'ADVANTAGES', 'NEWS', 'COMPANY', 'FEEDBACK', 'STORES'),
		"SLIDER_IBLOCK_ID" => "#SLIDERS_IBLOCK_ID#",
		"BANNERS_IBLOCK_ID" => "#BANNERS_IBLOCK_ID#",
		"ADVANTAGES_IBLOCK_ID" => "#ADVANTAGES_IBLOCK_ID#",
		"NEWS_IBLOCK_ID" => "#NEWS_IBLOCK_ID#",
		"BRANDS_IBLOCK_ID" => "#BRANDS_IBLOCK_ID#",
		"FEEDBACK_IBLOCK_ID" => "#FEEDBACK_IBLOCK_ID#",
		"SALE_IBLOCK_ID" => "#SALE_IBLOCK_ID#",
		"CATALOG_IBLOCK_ID" => "#CATALOG_IBLOCK_ID#",
		"SITE_DIR" => "#SITE_DIR#",
		"PATH_TO_CATALOG" => "#SITE_DIR#catalog/",
		"PATH_TO_COMPARE" => "#SITE_DIR#catalog/compare/",
		"PATH_TO_BASKET" => "#SITE_DIR#personal/cart/",
		"PATH_TO_NEWS" => "#SITE_DIR#news/",
		"PATH_TO_BRANDS" => "#SITE_DIR#brands/",
		"PATH_TO_COMPANY" => "#SITE_DIR#about/company/",
		"PATH_TO_STORES" => "#SITE_DIR#stores/",
		"FILTER_HITS" => array("!PROPERTY_SALELEADER_VALUE" => false),
		"FILTER_NEWPRODUCT" => array("!PROPERTY_NEWPRODUCT_VALUE" => false),
		"SUBSCRIBE_RUBRIC_ID" => "#SUBSCRIBE_RUBRIC_ID#"
	),
	false,
	array('HIDE_ICONS' => 'Y'));
?>
<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>
