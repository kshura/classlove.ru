<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])) {
	$arSkuTemplate = array();
	if (is_array($arResult['SKU_PROPS'])) {
		foreach ($arResult['SKU_PROPS'] as $iblockId => $skuProps) {
			$arSkuTemplate[$iblockId] = array();
			foreach ($skuProps as &$arProp) {
				ob_start();
				if ('TEXT' == $arProp['SHOW_MODE']) {
					if (5 < $arProp['VALUES_COUNT']) {
						$strClass = 'bx_item_detail_size full';
						$strWidth = ($arProp['VALUES_COUNT'] * 20) . '%';
						$strOneWidth = (100 / $arProp['VALUES_COUNT']) . '%';
						$strSlideStyle = '';
					} else {
						$strClass = 'bx_item_detail_size';
						$strWidth = '100%';
						$strOneWidth = '20%';
						$strSlideStyle = 'display: none;';
					}
					?>

				<div class="<? echo $strClass; ?>" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_cont">
					<span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>

					<div class="bx_size_scroller_container">
						<div class="bx_size">
							<ul id="#ITEM#_prop_<? echo $arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;"><?
								foreach ($arProp['VALUES'] as $arOneValue) {
									?>
								<li
									data-treevalue="<? echo $arProp['ID'] . '_' . $arOneValue['ID']; ?>"
									data-onevalue="<? echo $arOneValue['ID']; ?>"
									style="width: <? echo $strOneWidth; ?>;"
								><i></i><span class="cnt"><? echo htmlspecialcharsex($arOneValue['NAME']); ?></span>
									</li><?
								}
								?></ul>
						</div>
						<div class="bx_slide_left" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
						<div class="bx_slide_right" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
					</div>
					</div><?
				} elseif ('PICT' == $arProp['SHOW_MODE']) {
					if (5 < $arProp['VALUES_COUNT']) {
						$strClass = 'bx_item_detail_scu full';
						$strWidth = ($arProp['VALUES_COUNT'] * 20) . '%';
						$strOneWidth = (100 / $arProp['VALUES_COUNT']) . '%';
						$strSlideStyle = '';
					} else {
						$strClass = 'bx_item_detail_scu';
						$strWidth = '100%';
						$strOneWidth = '20%';
						$strSlideStyle = 'display: none;';
					}
					?>
				<div class="<? echo $strClass; ?>" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_cont">
					<span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>

					<div class="bx_scu_scroller_container">
						<div class="bx_scu">
							<ul id="#ITEM#_prop_<? echo $arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;"><?
								foreach ($arProp['VALUES'] as $arOneValue) {
									?>
								<li
									data-treevalue="<? echo $arProp['ID'] . '_' . $arOneValue['ID'] ?>"
									data-onevalue="<? echo $arOneValue['ID']; ?>"
									style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>;"
								><i title="<? echo htmlspecialcharsbx($arOneValue['NAME']); ?>"></i>
							<span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOneValue['PICT']['SRC']; ?>');" title="<? echo htmlspecialcharsbx($arOneValue['NAME']); ?>"
								></span></span></li><?
								}
								?></ul>
						</div>
						<div class="bx_slide_left" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
						<div class="bx_slide_right" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
					</div>
					</div><?
				}
				$arSkuTemplate[$iblockId][$arProp['CODE']] = ob_get_contents();
				ob_end_clean();
				unset($arProp);
			}
		}
	}
	?>
	<script type="text/javascript">
		BX.message({
			CVP_MESS_BTN_BUY: '<? echo('' != $arParams['MESS_BTN_BUY'] ? CUtil::JSEscape($arParams['MESS_BTN_BUY']) : GetMessageJS('CVP_TPL_MESS_BTN_BUY')); ?>',
			CVP_MESS_BTN_ADD_TO_BASKET: '<? echo('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? CUtil::JSEscape($arParams['MESS_BTN_ADD_TO_BASKET']) : GetMessageJS('CVP_TPL_MESS_BTN_ADD_TO_BASKET')); ?>',

			CVP_MESS_BTN_DETAIL: '<? echo('' != $arParams['MESS_BTN_DETAIL'] ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CVP_TPL_MESS_BTN_DETAIL')); ?>',

			CVP_MESS_NOT_AVAILABLE: '<? echo('' != $arParams['MESS_BTN_DETAIL'] ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CVP_TPL_MESS_BTN_DETAIL')); ?>',
			CVP_BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
			CVP_BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
			CVP_ADD_TO_BASKET_OK: '<? echo GetMessageJS('CVP_ADD_TO_BASKET_OK'); ?>',
			CVP_TITLE_ERROR: '<? echo GetMessageJS('CVP_CATALOG_TITLE_ERROR') ?>',
			CVP_TITLE_BASKET_PROPS: '<? echo GetMessageJS('CVP_CATALOG_TITLE_BASKET_PROPS') ?>',
			CVP_TITLE_SUCCESSFUL: '<? echo GetMessageJS('CVP_ADD_TO_BASKET_OK'); ?>',
			CVP_BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CVP_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
			CVP_BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
			CVP_BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_CLOSE') ?>'
		});
	</script>

	<div class="owl_nav small dark" id="owl_viewed_nav"></div>

	<div id="owl_viewed" class="slider_initial">
		<?
		$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
		$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
		$elementDeleteParams = array('CONFIRM' => GetMessage('CVP_TPL_ELEMENT_DELETE_CONFIRM'));
		foreach ($arResult['ITEMS'] as $key => $arItem) {
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $elementEdit);
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $elementDelete, $elementDeleteParams);
			$strMainID = $this->GetEditAreaId($arItem['ID']);
			$arItemIDs = array(
				'ID' => $strMainID,
				'PICT' => $strMainID . '_pict',
				'MAIN_PROPS' => $strMainID . '_main_props',
				'QUANTITY' => $strMainID . '_quantity',
				'QUANTITY_DOWN' => $strMainID . '_quant_down',
				'QUANTITY_UP' => $strMainID . '_quant_up',
				'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
				'BUY_LINK' => $strMainID . '_buy_link',
				'SUBSCRIBE_LINK' => $strMainID . '_subscribe',
				'PRICE' => $strMainID . '_price',
				'DSC_PERC' => $strMainID . '_dsc_perc',
				'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',
				'PROP_DIV' => $strMainID . '_sku_tree',
				'PROP' => $strMainID . '_prop_',
				'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
				'BASKET_PROP_DIV' => $strMainID . '_basket_prop'
			);
			$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
			$strTitle = (isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]) && '' != isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]) ? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"] : $arItem['NAME']);
			$showImgClass = $arParams['SHOW_IMAGE'] != "Y" ? "no-imgs" : "";

			$minPrice = false;
			if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE'])) {
				$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
			}

			$canBuy = $arItem['CAN_BUY'];

			$wishlistBtnMessage = ($arParams['MESS_BTN_WISHLIST'] != '' ? $arParams['MESS_BTN_WISHLIST'] : GetMessage('CVP_TPL_MESS_BTN_WISHLIST'));
			?>
			<div class="product_item" id="<? echo $strMainID; ?>">
				<div class="product_item_inner">
					<div class="product_header">
						<?
						if ($arParams['DISPLAY_WISHLIST']) {
							?>
							<a data-wishlist-id="item_<? echo $arItem['ID']; ?>" class="fa fa-heart" title="<? echo $wishlistBtnMessage; ?>" onclick="wishlistHandler(this);return false;"></a>
							<script type="text/javascript">
								wishlistSetItem('item_<?=$arItem['ID']?>');
							</script>
							<?
						}
						?>
						<?
						if($arParams['USE_SALE'] == 'Y') {
							if($arItem['SALE_ITEM']) {
								?>
								<a href="<? echo $arItem['SALE_ITEM']['DETAIL_PAGE_URL']; ?>" title="<? echo $arItem['SALE_ITEM']['NAME']; ?>" class="i-timer"><span class="timer-inner" data-final-date="<? echo $arItem['SALE_ITEM']['PROPERTY_SALE_TO_VALUE']; ?>"><span>00</span><?=GetMessage('COUNTER_SECTION_DAYS')?> <span>01</span><?=GetMessage('COUNTER_SECTION_HOURS')?> <span>00</span><?=GetMessage('COUNTER_SECTION_MINUTES')?> <span>00</span><?=GetMessage('COUNTER_SECTION_SECONDS')?></span></a><br/>
								<?
							}
						}
						?>
						<?
						if ($arItem['PROPERTIES']['NEWPRODUCT']['VALUE']) {
							?>
							<div class="i-new"><? echo $arItem['PROPERTIES']['NEWPRODUCT']['NAME']; ?></div><br/>
							<?
						}
						?>
						<?
						if ($arItem['PROPERTIES']['SALELEADER']['VALUE']) {
							?>
							<label class="i-hit"><? echo $arItem['PROPERTIES']['SALELEADER']['NAME'];?></label><br/>
							<?
						}
						?>
					</div>
					<div class="product_image">
						<a id="<? echo $arItemIDs['PICT']; ?>" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>">
							<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" title="<? echo $strTitle; ?>">
						</a>
					</div>
					<div class="product_title">
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<? echo $arItem['NAME']; ?>"><h3><?=$arItem['NAME']?></h3></a>
					</div>
					<div class="buy_wrap">
						<div class="price_wrap">
								<span id="<? echo $arItemIDs['PRICE']; ?>" class="current_price">
									<?
									if (!empty($minPrice) && $minPrice['DISCOUNT_VALUE']>0) {
										if(!empty($arItem['OFFERS'])) {
											echo GetMessage('CVP_TPL_MESS_PRICE_SIMPLE_MODE', array(
												'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE']
											));
										} else {
											echo $minPrice['PRINT_DISCOUNT_VALUE'];
										}
										if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE']) {
											?> <span class="old_price"><? echo $minPrice['PRINT_VALUE']; ?></span><?
										}
									} else {
										$notAvailableMessage = ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessage('CVP_TPL_MESS_PRODUCT_NULL_PRICE'));
										echo $notAvailableMessage;
									}
									?>
								</span>
						</div>
						<?
						if ($canBuy) {
					if(empty($arItem['OFFERS']) && !empty($minPrice) && $minPrice['DISCOUNT_VALUE']>0) {
						?>
						<div class="buy_btn_group">
							<?
							if ('Y' == $arParams['USE_PRODUCT_QUANTITY']) {
								?>
								<div class="buy_count_wrap">
									<div class="product_count">
										<a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0)" class="btn_minus" rel="nofollow">-</a>
										<input type="text" class="count_value" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
										<a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)" class="btn_plus" rel="nofollow">+</a>
									</div>
								</div>
								<?
							}
							?>
							<div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="bx_catalog_item_controls_blocktwo">
								<?
								if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY') {
									$basketTitle = ('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CVP_TPL_MESS_BTN_BUY'));
								} else {
									$basketTitle = ('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CVP_TPL_MESS_BTN_ADD_TO_BASKET'));
								}
								?>
								<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="cart_icon add_to_cart btn_round btn_color btn_wide" href="javascript:void(0)" rel="nofollow" title="<? echo $basketTitle; ?>"><span class="show_xs"><? echo $basketTitle; ?></span></a>
								<a class="cart_icon product_detail btn_round btn_disabled btn_hover_color" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" rel="nofollow" title="<? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CVP_TPL_MESS_BTN_DETAIL')); ?>"><span class="show_xs"><? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CVP_TPL_MESS_BTN_DETAIL')); ?></span></a>
							</div>
						</div>
					<?
					$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
					if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties) {
					?>
						<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
							<?
							if (!empty($arItem['PRODUCT_PROPERTIES_FILL'])) {
								foreach ($arItem['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
									?>
									<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
									<?
									if (isset($arItem['PRODUCT_PROPERTIES'][$propID])) {
										unset($arItem['PRODUCT_PROPERTIES'][$propID]);
									}
								}
							}
							$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
							if (!$emptyProductProperties) {
								?>
								<table>
									<?
									foreach ($arItem['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
										?>
										<tr>
											<td><? echo $arItem['PROPERTIES'][$propID]['NAME']; ?></td>
											<td>
												<?
												if ('L' == $arItem['PROPERTIES'][$propID]['PROPERTY_TYPE'] && 'C' == $arItem['PROPERTIES'][$propID]['LIST_TYPE']) {
													foreach ($propInfo['VALUES'] as $valueID => $value) {
														?>
														<label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br>
														<?
													}
												} else {
													?>
													<select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]">
														<?
														foreach ($propInfo['VALUES'] as $valueID => $value) {
															?>
															<option value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option>
															<?
														}
														?>
													</select>
													<?
												}
												?>
											</td>
										</tr>
										<?
									}
									?>
								</table>
								<?
							}
							?>
						</div>
					<?
					}
					$arJSParams = array(
						'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
						'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
						'SHOW_ADD_BASKET_BTN' => false,
						'SHOW_BUY_BTN' => true,
						'SHOW_ABSENT' => true,
						'PRODUCT' => array(
							'ID' => $arItem['ID'],
							'NAME' => $arItem['~NAME'],
							'PICT' => $arItem['PREVIEW_PICTURE'],
							'CAN_BUY' => $arItem["CAN_BUY"],
							'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
							'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
							'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
							'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
							'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
							'ADD_URL' => $arItem['~ADD_URL'],
							'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL']
						),
						'BASKET' => array(
							'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
							'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
							'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
							'EMPTY_PROPS' => $emptyProductProperties
						),
						'VISUAL' => array(
							'ID' => $arItemIDs['ID'],
							'PICT_ID' => $arItemIDs['PICT'],
							'QUANTITY_ID' => $arItemIDs['QUANTITY'],
							'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
							'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
							'PRICE_ID' => $arItemIDs['PRICE'],
							'BUY_ID' => $arItemIDs['BUY_LINK'],
							'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV']
						),
						'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
					);
					unset($emptyProductProperties);
					?>
						<script type="text/javascript">
							var <? echo $strObName; ?> = new JCCatalogSectionViewed(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
						</script>
					<?
					} else {
						?>
						<div class="buy_btn_group not_available">
							<input type="hidden" class="count_value" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
							<div class="bx_catalog_item_controls_blocktwo">
								<a class="cart_icon product_detail btn_round btn_disabled btn_hover_color" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" rel="nofollow" title="<? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CVP_TPL_MESS_BTN_DETAIL')); ?>"><span class="show_xs"><? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CVP_TPL_MESS_BTN_DETAIL')); ?></span></a>
							</div>
						</div>
						<?
					}
					} else {
						?>
						<div class="buy_btn_group not_available">
							<input type="hidden" class="count_value" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
							<div class="bx_catalog_item_controls_blocktwo">
								<a class="cart_icon product_detail btn_round btn_disabled btn_hover_color" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" rel="nofollow" title="<? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CVP_TPL_MESS_BTN_DETAIL')); ?>"><span class="show_xs"><? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CVP_TPL_MESS_BTN_DETAIL')); ?></span></a>
							</div>
						</div>
						<?
					}
					?>
					</div>
				</div>
			</div>
			<?
			}
				unset($elementDeleteParams, $elementDelete, $elementEdit);
				?>
	</div>
	<script type="text/javascript">
		$.each($('#owl_viewed .timer-inner'), function(){
			$(this).countdown({until: new Date($(this).attr('data-final-date')), layout: '{dn}{dl} {hnn}{hl} {mnn}{ml} {snn}{sl}', compact: true});
		});
	</script>
	<?
} else {
	?>
	<div class="page_empty_message active">
		<h3 class="page_empty_message_title"><?=GetMessage('CVP_MSG_YOU_HAVE_NOT_YET')?></h3>
	</div>
	<?
}
?>