<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
IncludeTemplateLangFile(__FILE__);
$this->setFrameMode(true);
?>
<section class="article_list_wrapper">
	<? if($arParams["DISPLAY_TOP_PAGER"]) { ?>
		<?=$arResult["NAV_STRING"]?><br />
	<? } ?>
	<ul class="article_list">
		<? foreach($arResult["ITEMS"] as $arItem) { ?>
			<li class="article_items">
				<? if ($arItem['PREVIEW_PICTURE']["SRC"]) { ?>
					<div class="grid_5 grid_5_sm">
						<div class="article_img">
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
								<img src="<?=$arItem['PREVIEW_PICTURE']["SRC"]?>" alt="<?=$arItem['PREVIEW_PICTURE']["ALT"]?>" title="<?=$arItem['PREVIEW_PICTURE']["TITLE"]?>" />
							</a>
						</div>
					</div>
					<div class="grid_11 grid_11_sm">
				<? } else { ?>
					<div class="grid_16 grid_16_sm">
				<? } ?>
					<div class="article_title"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><h2><?=$arItem['NAME']?></h2></a></div>
					<? if($arItem['DISPLAY_ACTIVE_FROM']) { ?>
						<div class="article_time"><?=$arItem['DISPLAY_ACTIVE_FROM']?></div>
					<? } ?>
					<? if($arItem['PROPERTIES']["ISSALE"]["VALUE"]) { ?>
						<div class="article_icon"><i class="i-sale-txt"><?=GetMessage("SALE")?></i></div>
					<? } ?>
					<div class="article_description"><?=$arItem['PREVIEW_TEXT']?></div>
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="btn_round btn_border btn_hover_color"><?=GetMessage("TO_DETAIL")?></a>
				</div>
				<div class="clear"></div>
			</li>
		<? } ?>
	</ul>
	<? if($arParams["DISPLAY_BOTTOM_PAGER"]) { ?>
		<br /><?=$arResult["NAV_STRING"]?>
	<? } ?>
</section>