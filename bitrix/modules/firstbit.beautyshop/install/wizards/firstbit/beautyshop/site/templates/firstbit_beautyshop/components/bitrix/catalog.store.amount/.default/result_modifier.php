<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();

foreach ($arResult['STORES'] as $storeIndex => $arStore) {
	if(mb_strlen($arStore['TITLE']) > 0) {
		$title = explode(" (", $arStore['TITLE']);
		$arResult['STORES'][$storeIndex]['TITLE'] = $title[0];
		$arResult['STORES'][$storeIndex]['ADDRESS'] = str_replace(")","",$title[1]);
	}

	$arResult['STORES'][$storeIndex]['PHONE'] = CFirstbitBeautyshop::parsePhone($arStore['PHONE']);

	$arResult['JS']['MESSAGES'] = array(
		'ABSENT' => GetMessage('ABSENT'),
		'FEW' => GetMessage('FEW'),
		'NOT_MUCH' => GetMessage('NOT_MUCH'),
		'MUCH' => GetMessage('MUCH')
	);

	switch($arStore['REAL_AMOUNT']) {
		case '0':
			$arResult['STORES'][$storeIndex]['AMOUNT'] = 'ABSENT';
			break;
		case ($arStore['REAL_AMOUNT'] > 0 && $arStore['REAL_AMOUNT'] <= 10 ):
			$arResult['STORES'][$storeIndex]['AMOUNT'] = 'FEW';
			break;
		case ($arStore['REAL_AMOUNT'] > 10 && $arStore['REAL_AMOUNT'] <= 30 ):
			$arResult['STORES'][$storeIndex]['AMOUNT'] = 'NOT_MUCH';
			break;
		case ($arStore['REAL_AMOUNT'] > 30):
			$arResult['STORES'][$storeIndex]['AMOUNT'] = 'MUCH';
			break;
	}
}