<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line","flying_cart",Array(
		"HIDE_ON_BASKET_PAGES" => "Y",
		"PATH_TO_BASKET" => $arParams['PATH_TO_BASKET'],
		"PATH_TO_ORDER" => $arParams['PATH_TO_ORDER'],
		"PATH_TO_CATALOG" => $arParams['PATH_TO_CATALOG'],
		"SHOW_EMPTY_VALUES" => "Y",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_PRICE" => "Y",
		"SHOW_PRODUCTS" => "Y",
		"SHOW_SUMMARY" => "Y",
		"SHOW_TOTAL_PRICE" => "Y"
	)
);?>