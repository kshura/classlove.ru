<?
$MESS["WZD_OPTION_SLIDERS_1"] = "Элемент";
$MESS["WZD_OPTION_SLIDERS_2"] = "Активность";
$MESS["WZD_OPTION_SLIDERS_3"] = "Начало активности";
$MESS["WZD_OPTION_SLIDERS_4"] = "Окончание активности";
$MESS["WZD_OPTION_SLIDERS_5"] = "*Название";
$MESS["WZD_OPTION_SLIDERS_6"] = "*Символьный код";
$MESS["WZD_OPTION_SLIDERS_7"] = "Сортировка";
$MESS["WZD_OPTION_SLIDERS_8"] = "Значения свойств";
$MESS["WZD_OPTION_SLIDERS_9"] = "Ссылка";
$MESS["WZD_OPTION_SLIDERS_10"] = "Изображение слайдера 1920х370";
$MESS["WZD_OPTION_SLIDERS_11"] = "Изображение слайдера 625х370";
$MESS["WZD_OPTION_SLIDERS_12"] = "Заголовок слайдера";
$MESS["WZD_OPTION_SLIDERS_13"] = "Анонс слайдера";
$MESS["WZD_OPTION_SLIDERS_14"] = "Подпись кнопки слайдера";
$MESS["WZD_OPTION_SLIDERS_15"] = "Изображение баннера среднее";
$MESS["WZD_OPTION_SLIDERS_16"] = "Изображение баннера малое";
$MESS["WZD_OPTION_SLIDERS_17"] = "Подпись кнопки баннера";
?>