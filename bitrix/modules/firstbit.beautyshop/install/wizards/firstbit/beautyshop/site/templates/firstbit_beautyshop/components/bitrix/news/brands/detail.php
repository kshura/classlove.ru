<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
$firstBit = new CFirstbitBeautyshop();

?>
<?$ElementID = $APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"",
	Array(
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"META_KEYWORDS" => $arParams["META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
		"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
		"ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
		"PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
		"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
		"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"USE_SHARE" => $arParams["USE_SHARE"],
		"SHARE_HIDE" => $arParams["SHARE_HIDE"],
		"SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
		"SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
		"SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
		"SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
		"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : '')
	),
	$component
);?>
<div class="row">

	<div class="container_16">
		<div class="grid_16 relative">
		<?
		\Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("brand_section_area");

		GLOBAL $arrFilterBrands;

		$arrFilterBrands = array(
			"PROPERTY_BRAND_REF" => $ElementID,
			"INCLUDE_SUBSECTIONS" => "Y",
			"SECTION_GLOBAL_ACTIVE" => "Y",
			"ACTIVE" => "Y",
		);

		if($_REQUEST['section']) {
			$arrFilterBrands["SECTION_ID"] = $_REQUEST['section'];
		}

		$catalogView['DEFAULT'] = array("TEMPLATE" => $firstBit->options['CATALOG_TEMPLATE_INITIAL']) +
			array("ELEMENTS" => $firstBit->options['CATALOG_ELEMENTS_INITIAL']) +
			array("SORT_FIELD" => 'SHOW') +
			array("SORT_ORDER" => 'desc');

		$catalogView['SORT_PROPERTIES'] = array(
			"SHOW" => array('NAME' => GetMessage('CT_BCS_CATALOG_SORT_FIELD_SHOW'), 'ORDER_1' => "desc"),
			"PROPERTY_MINIMUM_PRICE" => array('NAME' => GetMessage('CT_BCS_CATALOG_SORT_FIELD_PRICE'), 'ORDER_1' => 'asc', 'ORDER_2' => 'desc'),
			"NAME" => array('NAME' => GetMessage('CT_BCS_CATALOG_SORT_FIELD_NAME'), 'ORDER_1' => 'asc')
		);

		$catalogView['TEMPLATES'] = array('TILE', 'LIST_BIG', 'LIST');
		foreach($catalogView['TEMPLATES'] as $arTemplate) {
			if($firstBit->options['CATALOG_TEMPLATE_ORDER_'.$arTemplate.'_HIDE'] != 'Y') {
				$arTemplateOrder = $firstBit->options['CATALOG_TEMPLATE_ORDER_' . $arTemplate];
				$catalogView['SAVED'][$arTemplateOrder]['CODE'] = $arTemplate;
				if($firstBit->options['CATALOG_ELEMENTS_'.$arTemplate.'_HIDE'] != 'Y') {
					$catalogView['SAVED'][$arTemplateOrder]['ELEMENTS'] = explode(',',str_replace(" ","",$firstBit->options['CATALOG_ELEMENTS_'.$arTemplate]));
					if(
						!isset($catalogView['SAVED'][$arTemplateOrder]['ELEMENTS']) ||
						!is_array($catalogView['SAVED'][$arTemplateOrder]['ELEMENTS']) ||
						count($catalogView['SAVED'][$arTemplateOrder]['ELEMENTS'])<=0
					) {
						$catalogView['SAVED'][$arTemplateOrder]['ELEMENTS'][] = $catalogView['DEFAULT']['ELEMENTS'];
					}
				} else {
					$catalogView['SAVED'][$arTemplateOrder]['ELEMENTS'][] = $catalogView['DEFAULT']['ELEMENTS'];
				}
			}
		}
		unset($arTemplate);
		ksort($catalogView['SAVED']);
		foreach($catalogView['SAVED'] as $arTemplate) {
			$catalogView['SAVED_tmp'][] = $arTemplate;
		}
		$catalogView['SAVED'] = $catalogView['SAVED_tmp'];
		unset($catalogView['SAVED_tmp']);

		if(count($catalogView['SAVED'])>0) {

			if(!$catalogView['COOKIE'] = json_decode($_COOKIE["CATALOG_VIEW"])) {
				$defaultOptions = $catalogView['DEFAULT'];
			} else {
				$catalogView['USER']['TEMPLATE'] = $catalogView['COOKIE']->TEMPLATE;
				$catalogView['USER']['ELEMENTS'] = $catalogView['COOKIE']->ELEMENTS;
				$catalogView['USER']['SORT_FIELD'] = $catalogView['COOKIE']->SORT_FIELD;
				$catalogView['USER']['SORT_ORDER'] = $catalogView['COOKIE']->SORT_ORDER;
				$defaultOptions = $catalogView['USER'];
			}
			foreach ($catalogView['SAVED'] as $arTemplateIndex=>$arTemplate) {
				if($defaultOptions['TEMPLATE'] == $arTemplate['CODE']) {
					$catalogView['CURRENT']['INDEX'] = $arTemplateIndex;
					break;
				}
			}
			if(!isset($catalogView['CURRENT']['INDEX'])) {
				$catalogView['CURRENT']['INDEX'] = 0;
			}
			$catalogView['CURRENT']['TEMPLATE'] = $catalogView['SAVED'][$catalogView['CURRENT']['INDEX']]['CODE'];
			if(!$catalogView['COOKIE']) {
				$catalogView['CURRENT']['ELEMENTS'] = $catalogView['SAVED'][$catalogView['CURRENT']['INDEX']]['ELEMENTS'][0];
			} else {
				if($catalogView['USER']['ELEMENTS'] != 10000 && !in_array($catalogView['USER']['ELEMENTS'], $catalogView['SAVED'][$catalogView['CURRENT']['INDEX']]['ELEMENTS'])) {
					$catalogView['CURRENT']['ELEMENTS'] = $catalogView['SAVED'][$catalogView['CURRENT']['INDEX']]['ELEMENTS'][$firstBit->searchNearest($catalogView['USER']['ELEMENTS'], $catalogView['SAVED'][$catalogView['CURRENT']['INDEX']]['ELEMENTS'])];
				} else {
					$catalogView['CURRENT']['ELEMENTS'] = $catalogView['USER']['ELEMENTS'];
				}
			}
			$catalogView['CURRENT']['SORT_FIELD'] = $defaultOptions['SORT_FIELD'];
			$catalogView['CURRENT']['SORT_ORDER'] = $defaultOptions['SORT_ORDER'];
		} else {
			$catalogView['CURRENT'] = $catalogView['DEFAULT'];
		}
		setcookie("CATALOG_VIEW", json_encode($catalogView['CURRENT']),time()+60*60*24*30,'/');
		?>
		<?
		\Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("brand_section_area", "");
		?>
<? $APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"brand",
	array(
		"IBLOCK_TYPE" => 'firstbit_beautyshop_catalog',
		"IBLOCK_ID" => $arParams['CATALOG_IBLOCK_ID'],
		"CATALOG_SECTION_TEMPLATE" => $catalogView['CURRENT']['TEMPLATE'],
		"CATALOG_SORT_PARAMS" => $catalogView,
		"ELEMENT_SORT_FIELD" => $catalogView['CURRENT']['SORT_FIELD'],
		"ELEMENT_SORT_ORDER" => $catalogView['CURRENT']['SORT_ORDER'],
		"ELEMENT_SORT_FIELD2" => "",
		"ELEMENT_SORT_ORDER2" => "",
		"PROPERTY_CODE" => array(
			0 => "SMART_SIM_NUMBER",
			1 => "SMART_SIM_TYPE",
			2 => "SMART_CASE_TYPE",
			3 => "",
		),
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"BASKET_URL" => $arParams['PATH_TO_BASKET'],
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "arrFilterBrands",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"MESSAGE_404" => "",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "N",
		"FILE_404" => "",
		"DISPLAY_COMPARE" => "Y",
		"PAGE_ELEMENT_COUNT" => $_REQUEST['p'] ? $catalogView['CURRENT']['ELEMENTS'] * $_REQUEST['p']++ : $catalogView['CURRENT']['ELEMENTS'],
		"LINE_ELEMENT_COUNT" => "4",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"USE_PRODUCT_QUANTITY" => "Y",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "round",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "OBUV_SIZE",
			1 => "OD_SIZE",
			2 => "SMART_VOLUME",
			3 => "COLOR",
		),
		"LIST_OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"LIST_OFFERS_PROPERTY_CODE" => array(
			0 => "ARTNUMBER",
			1 => "MORE_PHOTO",
			2 => "OBUV_SIZE",
			3 => "OD_SIZE",
			4 => "SMART_VOLUME",
			5 => "COLOR",
			6 => "",
		),
		"OFFERS_SORT_FIELD" => "CATALOG_AVAILABLE",
		"OFFERS_SORT_ORDER" => "desc",
		"OFFERS_SORT_FIELD2" => "CATALOG_PRICE_1",
		"OFFERS_SORT_ORDER2" => "asc",
		"LIST_OFFERS_LIMIT" => "0",
		"SECTION_ID" => "",
		"DETAIL_URL" => "",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"CONVERT_CURRENCY" => "N",
		"CURRENCY_ID" => "RUB",
		"HIDE_NOT_AVAILABLE" => "N",
		"LABEL_PROP" => "NEWPRODUCT",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"OFFER_TREE_PROPS" => array(
			0 => "OD_SIZE",
			1 => "OBUV_SIZE",
			2 => "SMART_VOLUME",
			3 => "COLOR",
		),
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_OLD_PRICE" => "Y",
		"MESS_BTN_SUBSCRIBE" => "",
		"TEMPLATE_THEME" => "site",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"COMMON_SHOW_CLOSE_POPUP" => "N",
		"COMPARE_PATH" => $arParams['PATH_TO_COMPARE'],
		"SECTION_BACKGROUND_IMAGE" => "-",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"USE_SALE" => "Y",
		"SALE_IBLOCK_TYPE_ID" => "catalog",
		"SALE_IBLOCK_ID" => $arParams['SALE_IBLOCK_ID'],
		"DISPLAY_WISHLIST" => "Y",
		"COMPONENT_TEMPLATE" => "wishlist",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_ALL_WO_SECTION" => "Y",
		"OFFERS_FIELD_CODE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "OBUV_SIZE",
			1 => "OD_SIZE",
			2 => "SMART_VOLUME",
			3 => "COLOR",
			4 => "",
		),
		"OFFERS_LIMIT" => "0",
		"BACKGROUND_IMAGE" => "-",
		"ADD_PICT_PROP" => "-",
		"SHOW_CLOSE_POPUP" => "N",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"USE_QUICKBUY" => "Y",
	),
	$component
); ?>
	</div>
	<div class="clear"></div>
</div>
</div>