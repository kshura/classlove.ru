<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!CModule::IncludeModule("highloadblock"))
	return;

if (!WIZARD_INSTALL_DEMO_DATA)
	return;

$COLOR_ID = $_SESSION["FIRSTBIT_HBLOCK_COLOR_ID"];
unset($_SESSION["FIRSTBIT_HBLOCK_COLOR_ID"]);

$ADVANTAGE_ID = $_SESSION["FIRSTBIT_HBLOCK_ADVANTAGE_ID"];
unset($_SESSION["FIRSTBIT_HBLOCK_ADVANTAGE_ID"]);

//adding rows
WizardServices::IncludeServiceLang("references.php", LANGUAGE_ID);

use Bitrix\Highloadblock as HL;
global $USER_FIELD_MANAGER;

if ($COLOR_ID)
{
	$hldata = HL\HighloadBlockTable::getById($COLOR_ID)->fetch();
	$hlentity = HL\HighloadBlockTable::compileEntity($hldata);

	$entity_data_class = $hlentity->getDataClass();
	$arColors = array(
		"BLUE" => "references_files/uf/4b7/4b711bca8c5fcc81553c79b6dd62b0bc.png",
		"RED" => "references_files/uf/8ef/8ef0f7691a895981964f1f98dbc7793a.png",
		"GREEN" => "references_files/uf/5d2/5d2113590576f1a0f09265e42bb8bedb.png",
		"WHITE" => "references_files/uf/488/48828b72c2441332e5f96e0ff1673d01.png",
		"BLACK" => "references_files/uf/041/0417b83ce3bf1aaee719462b08c3ab91.png",
		"PINK" => "references_files/uf/cfa/cfa9e7eb27afa2cddc84a8fc0670ed99.png",
		"AZURE" => "references_files/uf/184/184d4249b18123ce19ad7798f0dbbf78.png",
		"GRAY" => "references_files/uf/57b/57b785716b2b56d085db4473bd66065a.jpg",
		"ROSE_GOLD" => "references_files/uf/f32/f32b0f8c1cb2184e88c97a3f4d1ee319.jpg",
		"YELLOW" => "references_files/uf/813/813716901b37b4b03824470f07455ac9.png",
		"LEMON_CREAM" => "references_files/uf/66b/66bf40793e2cddf6ef2216691dc79b0a.png",
		"PEACH" => "references_files/uf/798/7980109d3dbfd4fee990d52c34a7a4f7.png",
		"FUCHSIA" => "references_files/uf/b98/b981b43d112ec15ebdf06af103c0441f.png",
		"GOLD" => "references_files/uf/995/99503af4d5ac592312b5196f858d84e7.jpg",
		"SILVER" => "references_files/uf/ea6/ea63ad8a30bae1b049b41dc7e3e2cbe7.jpg",
		"VENGE" => "references_files/uf/609/609aee823c1fe70b9f7bc7846203165f.jpg",
		"LISTV_WHITE" => "references_files/uf/40d/40d69f925786c41ff4fefe1ec6764d68.jpg",
		"NO" => "",
		"BLACK_ONYX" => "references_files/uf/e1b/e1bf6c264cf0a46d2fa4f9fcda35007f.png",
		"LIGHT_GREEN" => "references_files/uf/565/565cc6241b47d93d438f8da85f25ac60.png",
		"ORANGE" => "references_files/uf/bad/bad75a8131e27c8df1bfdb5aabfe7f66.png",
		"BORDO" => "references_files/uf/244/2448be551d80ea1ef09d2b3370751ec8.png",
		"BRAUN" => "references_files/uf/645/645f2bf3cdf064d5d6b3e9a71b6ec619.png",
		"PURPLE" => "references_files/uf/a7a/a7ae4374669675ed8220ddafd78a7925.png",
		"BEG" => "references_files/uf/c64/c64cdca2b8e677e18ad16eb1c0038784.png",
		"01" => "references_files/uf/7a7/7a7ac6cfffa835e205cf8b41b608c054.jpg",
		"02" => "references_files/uf/641/6413776a6cc7324bb0e855983910ddad.jpg",
		"03" => "references_files/uf/10f/10f253781cf69a9151d46b612ca517d5.jpg",
	);
	$sort = 0;
	foreach($arColors as $colorName => $colorFile) {
		$sort+= 100;
		$arData = array(
			'UF_NAME' => GetMessage("WZD_REF_COLOR_".$colorName),
			'UF_FILE' =>
				array (
					'name' => ToLower($colorName).".jpg",
					'type' => 'image/jpeg',
					'tmp_name' => WIZARD_ABSOLUTE_PATH."/site/services/iblock/".$colorFile
				),
			'UF_SORT' => $sort,
			'UF_DEF' => ($sort > 100) ? "0" : "1",
			'UF_XML_ID' => ToLower($colorName)
		);
		$USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_'.$COLOR_ID, $arData);
		$USER_FIELD_MANAGER->checkFields('HLBLOCK_'.$COLOR_ID, null, $arData);

		$result = $entity_data_class::add($arData);
	}
}

if ($ADVANTAGE_ID)
{
	$hldata = HL\HighloadBlockTable::getById($ADVANTAGE_ID)->fetch();
	$hlentity = HL\HighloadBlockTable::compileEntity($hldata);

	$entity_data_class = $hlentity->getDataClass();
	$arAdvantages = array(
		"FREE_SHIPPING" => "references_files/uf/200/20001798a4c7a1ee83c5e42ea7187f11.png",
		"BEST_PRICE" => "references_files/uf/214/214dc8eb91c6c6c2fc276311e801f3f2.png",
		"CUSTOMER_CHOICE" => "references_files/uf/857/8574b433e90a57c249570042bbd72393.png",
		"ECO" => "references_files/uf/f80/f80252722945ac8f0dcdee224cbcc312.png",
		"KREDIT" => "references_files/uf/64a/64a97dbb21b0582c19e85600f19cc844.png",
		"HIT" => "references_files/uf/dfe/dfe3822609377c8d37d957fb446f9b91.png",
	);
	$sort = 0;
	foreach($arAdvantages as $advantageName=>$advantageFile) {
		$sort+= 100;
		$arData = array(
			'UF_NAME' => GetMessage("WZD_REF_ADVANTAGE_".$advantageName),
			'UF_FILE' =>
				array (
					'name' => ToLower($advantageName).".png",
					'type' => 'image/png',
					'tmp_name' => WIZARD_ABSOLUTE_PATH."/site/services/iblock/".$advantageFile
				),
			'UF_SORT' => $sort,
			'UF_XML_ID' => ToLower($advantageName)
		);
		$USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_'.$ADVANTAGE_ID, $arData);
		$USER_FIELD_MANAGER->checkFields('HLBLOCK_'.$ADVANTAGE_ID, null, $arData);

		$result = $entity_data_class::add($arData);
	}
}
?>