<?
$MESS["WIZ_PRICE_NAME"] = "Розничная цена";
$MESS["WIZ_DISCOUNT"] = "Скидка на нижнее белье";
$MESS["WIZ_PRECET"] = "Специальные товары";
$MESS["WIZ_DISCOUNT_BASKET_GIFTS"] = "Подарки в корзине";
$MESS["WZD_OPTION_CATALOG_1"] = "Товар";
$MESS["WZD_OPTION_CATALOG_2"] = "Активность";
$MESS["WZD_OPTION_CATALOG_3"] = "Начало активности";
$MESS["WZD_OPTION_CATALOG_4"] = "Окончание активности";
$MESS["WZD_OPTION_CATALOG_5"] = "*Название";
$MESS["WZD_OPTION_CATALOG_6"] = "*Символьный код";
$MESS["WZD_OPTION_CATALOG_7"] = "Сортировка";
$MESS["WZD_OPTION_CATALOG_8"] = "Общие свойства";
$MESS["WZD_OPTION_CATALOG_9"] = "Минимальная цена";
$MESS["WZD_OPTION_CATALOG_10"] = "Картинки галереи";
$MESS["WZD_OPTION_CATALOG_11"] = "Артикул";
$MESS["WZD_OPTION_CATALOG_12"] = "Бренд";
$MESS["WZD_OPTION_CATALOG_13"] = "Производитель";
$MESS["WZD_OPTION_CATALOG_14"] = "Цвет";
$MESS["WZD_OPTION_CATALOG_15"] = "Состав";
$MESS["WZD_OPTION_CATALOG_16"] = "Материал";
$MESS["WZD_OPTION_CATALOG_17"] = "Высота, мм";
$MESS["WZD_OPTION_CATALOG_18"] = "Глубина, мм";
$MESS["WZD_OPTION_CATALOG_19"] = "Длина, мм";
$MESS["WZD_OPTION_CATALOG_20"] = "Ширина, мм";
$MESS["WZD_OPTION_CATALOG_21"] = "Маркетинг";
$MESS["WZD_OPTION_CATALOG_22"] = "Преимущества";
$MESS["WZD_OPTION_CATALOG_23"] = "Новинка";
$MESS["WZD_OPTION_CATALOG_24"] = "ХИТ";
$MESS["WZD_OPTION_CATALOG_25"] = "Спецпредложение";
$MESS["WZD_OPTION_CATALOG_26"] = "Связанные товары";
$MESS["WZD_OPTION_CATALOG_27"] = "Электроника";
$MESS["WZD_OPTION_CATALOG_28"] = "Гарантия";
$MESS["WZD_OPTION_CATALOG_29"] = "Класс ноутбука";
$MESS["WZD_OPTION_CATALOG_30"] = "Материал корпуса";
$MESS["WZD_OPTION_CATALOG_31"] = "Разъем";
$MESS["WZD_OPTION_CATALOG_32"] = "Серия";
$MESS["WZD_OPTION_CATALOG_33"] = "Bluetooth (версия)";
$MESS["WZD_OPTION_CATALOG_34"] = "Диагональ экрана";
$MESS["WZD_OPTION_CATALOG_35"] = "Диагональ/разрешение";
$MESS["WZD_OPTION_CATALOG_36"] = "Жесткий диск (HDD)";
$MESS["WZD_OPTION_CATALOG_37"] = "Количество слотов памяти";
$MESS["WZD_OPTION_CATALOG_38"] = "Макс. оперативная память";
$MESS["WZD_OPTION_CATALOG_39"] = "Оперативная память (RAM)";
$MESS["WZD_OPTION_CATALOG_40"] = "Поддержка Wi-Fi";
$MESS["WZD_OPTION_CATALOG_41"] = "Работа от аккумулятора";
$MESS["WZD_OPTION_CATALOG_42"] = "Технология дисплея";
$MESS["WZD_OPTION_CATALOG_43"] = "Тип памяти";
$MESS["WZD_OPTION_CATALOG_44"] = "Частота памяти";
$MESS["WZD_OPTION_CATALOG_45"] = "4G (LTE)";
$MESS["WZD_OPTION_CATALOG_46"] = "Диагональ экрана";
$MESS["WZD_OPTION_CATALOG_47"] = "Количество SIM-карт";
$MESS["WZD_OPTION_CATALOG_48"] = "Объем встроенной памяти";
$MESS["WZD_OPTION_CATALOG_49"] = "Операционная система";
$MESS["WZD_OPTION_CATALOG_50"] = "Поддержка карт памяти";
$MESS["WZD_OPTION_CATALOG_51"] = "Разрешение экрана";
$MESS["WZD_OPTION_CATALOG_52"] = "Сенсорный экран";
$MESS["WZD_OPTION_CATALOG_53"] = "Тип SIM-карты";
$MESS["WZD_OPTION_CATALOG_54"] = "Тип корпуса";
$MESS["WZD_OPTION_CATALOG_55"] = "Тип процессора";
$MESS["WZD_OPTION_CATALOG_56"] = "Одежда";
$MESS["WZD_OPTION_CATALOG_57"] = "Длина юбки";
$MESS["WZD_OPTION_CATALOG_58"] = "Застежка";
$MESS["WZD_OPTION_CATALOG_59"] = "Сезон";
$MESS["WZD_OPTION_CATALOG_60"] = "Стиль";
$MESS["WZD_OPTION_CATALOG_61"] = "Тип юбки";
$MESS["WZD_OPTION_CATALOG_62"] = "Косметика";
$MESS["WZD_OPTION_CATALOG_63"] = "Применение";
$MESS["WZD_OPTION_CATALOG_64"] = "Тип";
$MESS["WZD_OPTION_CATALOG_65"] = "Обувь";
$MESS["WZD_OPTION_CATALOG_66"] = "Высота каблука";
$MESS["WZD_OPTION_CATALOG_67"] = "Анонс";
$MESS["WZD_OPTION_CATALOG_68"] = "Картинка для анонса";
$MESS["WZD_OPTION_CATALOG_69"] = "Описание для анонса";
$MESS["WZD_OPTION_CATALOG_70"] = "Подробно";
$MESS["WZD_OPTION_CATALOG_71"] = "Детальная картинка";
$MESS["WZD_OPTION_CATALOG_72"] = "*Детальное описание";
$MESS["WZD_OPTION_CATALOG_73"] = "Торговые предложения";
$MESS["WZD_OPTION_CATALOG_74"] = "Торговые предложения";
$MESS["WZD_OPTION_CATALOG_75"] = "Дополнительные материалы";
$MESS["WZD_OPTION_CATALOG_76"] = "Документы";
$MESS["WZD_OPTION_CATALOG_77"] = "Видео";
$MESS["WZD_OPTION_CATALOG_78"] = "Разделы";
$MESS["WZD_OPTION_CATALOG_79"] = "*Разделы";
$MESS["WZD_OPTION_CATALOG_80"] = "SEO";
$MESS["WZD_OPTION_CATALOG_81"] = "Шаблон META TITLE";
$MESS["WZD_OPTION_CATALOG_82"] = "Шаблон META KEYWORDS";
$MESS["WZD_OPTION_CATALOG_83"] = "Шаблон META DESCRIPTION";
$MESS["WZD_OPTION_CATALOG_84"] = "Заголовок элемента";
$MESS["WZD_OPTION_CATALOG_85"] = "Настройки для картинок анонса элементов";
$MESS["WZD_OPTION_CATALOG_86"] = "Шаблон ALT";
$MESS["WZD_OPTION_CATALOG_87"] = "Шаблон TITLE";
$MESS["WZD_OPTION_CATALOG_88"] = "Шаблон имени файла";
$MESS["WZD_OPTION_CATALOG_89"] = "Настройки для детальных картинок элементов";
$MESS["WZD_OPTION_CATALOG_90"] = "Шаблон ALT";
$MESS["WZD_OPTION_CATALOG_91"] = "Шаблон TITLE";
$MESS["WZD_OPTION_CATALOG_92"] = "Шаблон имени файла";
$MESS["WZD_OPTION_CATALOG_93"] = "Дополнительно";
$MESS["WZD_OPTION_CATALOG_94"] = "Теги";