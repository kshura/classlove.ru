<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>

<form name="top_search" action="<?=$arResult["FORM_ACTION"]?>">
	<? $APPLICATION->IncludeComponent(
		"bitrix:search.suggest.input",
		"bit_suggest_input",
		array(
			"NAME" => "q",
			"VALUE" => "",
			"INPUT_SIZE" => 15,
			"DROPDOWN_SIZE" => 10,
			'BORDER' => $arParams['BORDER']
		),
		$component, array("HIDE_ICONS" => "Y")
	); ?>
	<button class="btn_search" type="submit" name="s"><i class="fa fa-search"></i></button>
</form>

