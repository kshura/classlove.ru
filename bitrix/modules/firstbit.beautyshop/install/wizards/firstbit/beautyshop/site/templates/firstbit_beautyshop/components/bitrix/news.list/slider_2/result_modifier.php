<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if (!empty($arResult['ITEMS'])) {
	foreach ($arResult['ITEMS'] as $key => $arItem) {
		if($arItem['PROPERTIES']['SLIDER_PICTURE_BIG']['VALUE']) {
			$arSlider = CFile::GetFileArray($arItem['PROPERTIES']['SLIDER_PICTURE_BIG']['VALUE']);
			$arResult['ITEMS'][$key]['PROPERTIES']['SLIDER_PICTURE_BIG']['VALUE'] = $arSlider;
		}
	}
}
