<?
use Bitrix\Main\Type\Collection;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
$firstBit = new CFirstbitBeautyshop(SITE_ID);

$arResult['TEMPLATE_VIEW'] = $firstBit->options['BRANDS_VIEW'];

if (!empty($arResult['ITEMS'])) {
	if($arResult['TEMPLATE_VIEW'] == 'IMAGES') {
		foreach ($arResult['ITEMS'] as $arItemIndex => $arItem) {
			$_tmpFile = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width' => 130, 'height' => 100), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$arResult['ITEMS'][$arItemIndex]['PREVIEW_PICTURE'] = $_tmpFile['src'];
		}
	}
	if($arResult['TEMPLATE_VIEW'] == 'LIST') {
		$letters = array();
		$brands = array();
		foreach($arResult['ITEMS'] as $arItemIndex => $arItem) {
			$arItemFirstLetter = mb_strtoupper(mb_substr(trim($arItem['NAME']), 0, 1));

			$_tmpFile = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width' => 130, 'height' => 100), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$arItem['PREVIEW_PICTURE'] = $_tmpFile['src'];

			if(preg_match("/[0-9]/", $arItemFirstLetter)) {
				$arItemFirstLetter = 'NUMERIC';
			}
			elseif(preg_match("/[^a-zA-Z]/i", $arItemFirstLetter)) {
				$arItemFirstLetter = 'NON_LATIN';
			}

			if(!in_array($arItemFirstLetter, $letters)) {
				$letters[] = $arItemFirstLetter;
				$brands[] = array("LETTER" => $arItemFirstLetter, "ITEMS" => array($arItem));
			} else {
				foreach($brands as $index=>$brand) {
					if($brand["LETTER"] == $arItemFirstLetter) {
						$brands[$index]['ITEMS'][] = $arItem;
					}
				}
			}
			unset($arResult['ITEMS'][$arItemIndex]);
		}
		$arResult['ITEMS'] = $brands;
	}
}
//echo '<pre>';
//print_r($arResult);
//echo '</pre>';
