<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Список профилей");
$APPLICATION->SetPageProperty('title', "Список профилей");
?>
<?$APPLICATION->IncludeComponent(
	"firstbit:sale.personal.profile",
	".default",
	array(
		"SEF_MODE" => "Y",
		"PER_PAGE" => "20",
		"USE_AJAX_LOCATIONS" => "Y",
		"SET_TITLE" => "Y",
		"SEF_FOLDER" => "#SITE_DIR#personal/profile/profiles/",
		"COMPONENT_TEMPLATE" => ".default",
		"ALLOW_PROPERTIES" => array(),
		"LIST_PROPERTIES" => array(),
		"SEF_URL_TEMPLATES" => Array(
			"list" => "",
			"edit" => "edit/#ID#/"
		),
		"VARIABLE_ALIASES" => array(
			"list" => Array(),
			"add" => Array(),
			"edit" => Array(
				"ID" => "ID"
			),
		)
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>