<section class="row grey about">
	<div class="section_inner">
		<div class="container_16">
			<div class="grid_10 grid_9_sm grid_16_xs">
				<div class="section_line_head">
					<h1 class="section_title">О компании</h1>
				</div>
				<div class="about_content_wrapper">
					<p class="">Представляем Вашему вниманию новые коллекции мебели гостиных, спаоен и детских
						комнат. Мы практикуем модульный принцип построения мебели. Комбинируя различные
						элементы,</p>
					<p class="">Вы можете по своему вкусу создать оригинальную мебельную композицию, оптимально
						соответствующую Вашим потребностям. Кроме, дизайнерами компании рабрабатываются
						индивидуальные проекты на основе ваших решений</p>
					<a href="#SITE_DIR#about/" class="link_more">Подробнее<i class="fa fa-angle-right"></i></a>
				</div>
			</div>
			<div class="grid_6 grid_7_sm hidden_xs">
				<div class="about_image_wrapper">
					<img src="<?= SITE_TEMPLATE_PATH ?>/img/mainimg.png">
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>