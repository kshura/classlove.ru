<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (CModule::IncludeModule('sale'))
{
	$statList = array('0' => GetMessage('SPO_HISTORIC_STATUSES_NONE'));
	$dbStat = CSaleStatus::GetList(array('sort' => 'asc'), array('LID' => LANGUAGE_ID), false, false, array('ID', 'NAME'));
	while ($item = $dbStat->Fetch())
		$statList[$item['ID']] = $item['NAME'];

	$arTemplateParameters['HISTORIC_STATUSES'] = array(
		"NAME" => GetMessage("SPO_HISTORIC_STATUSES"),
		"TYPE" => "LIST",
		"VALUES" => $statList,
		"MULTIPLE" => "Y",
		"DEFAULT" => "F",
		"PARENT" => "ADDITIONAL_SETTINGS",
	);
}
?>