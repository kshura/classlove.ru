<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if (!empty($arResult['ITEMS'])) {
	foreach ($arResult['ITEMS'] as $key => $arItem) {
		if($arItem['PROPERTIES']['BANNER_PICTURE_MEDIUM']['VALUE']) {
			$arBanner = CFile::GetFileArray($arItem['PROPERTIES']['BANNER_PICTURE_MEDIUM']['VALUE']);
			$arResult['ITEMS'][$key]['PROPERTIES']['BANNER_PICTURE_MEDIUM']['VALUE'] = $arBanner;
		}
	}
}
