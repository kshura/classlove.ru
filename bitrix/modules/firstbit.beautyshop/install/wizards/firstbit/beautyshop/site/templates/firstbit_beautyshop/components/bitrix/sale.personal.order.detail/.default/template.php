<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
//Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
CJSCore::Init(array('clipboard'));


?>

<?if(!empty($arResult['ERRORS']['FATAL'])):?>

	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>

	<?$component = $this->__component;?>
	<?if($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])):?>
		<?$APPLICATION->AuthForm('', false, false, 'N', false);?>
	<?endif?>

<?else:?>
	<? $ID = $arResult['ID']; ?>
	<?if(!empty($arResult['ERRORS']['NONFATAL'])):?>

		<?foreach($arResult['ERRORS']['NONFATAL'] as $error):?>
			<?=ShowError($error)?>
		<?endforeach?>

	<?endif?>
	<a class="link_back" href="<?=$arResult["URL_TO_LIST"]?>"><i class="fa fa-angle-left "></i><?=GetMessage("SPOD_CUR_ORDERS")?></a>

	<?
	$APPLICATION->SetPageProperty("showLeftMenu", "N");
	$title = GetMessage('SPOD_ORDER').' '.GetMessage('SPOD_NUM_SIGN').' '.$arResult["ACCOUNT_NUMBER"].' '.GetMessage("SPOD_FROM").' '.$arResult["DATE_INSERT_FORMATED"];
	$APPLICATION->SetTitle($title);
	$APPLICATION->SetPageProperty('title', $title);
	?>

	<div class="order_info_inner cart">
		<div class="order_info_wrap">
			<h2><?=GetMessage('SPOD_GENERAL')?></h2>
			<ul class="order_info_list">
				<li>
					<strong><?=GetMessage('SPOD_ORDER_DATE')?>:</strong>
					<span><?=$arResult["DATE_INSERT_FORMATED"]?></span>
				</li>
				<li>
					<strong><?=GetMessage('SPOD_ORDER_NUMBER')?>:</strong>
					<span><?=$arResult["ACCOUNT_NUMBER"]?></span>
				</li>
				<?foreach ($arResult['PAYMENT'] as $payment):?>
					<li>
						<strong><?=GetMessage('SPOD_ORDER_PAYMENT_TITLE')?>:</strong>
						<span>
							<?if(intval($payment["PAY_SYSTEM_ID"])):?>
								<?if ($payment['PAY_SYSTEM']):?>
									<?=$payment["PAY_SYSTEM"]["NAME"].' ('.$payment['PRICE_FORMATED'].')'?>
								<?else:?>
									<?=$payment["PAY_SYSTEM_NAME"].' ('.$payment['PRICE_FORMATED'].')';?>
								<?endif;?>
							<?else:?>
								<?=GetMessage("SPOD_NONE")?>
							<?endif?>
						</span>
					</li>
				<?endforeach?>
				<?foreach ($arResult['SHIPMENT'] as $shipment):?>
					<li>
						<strong><?=GetMessage("SPOD_ORDER_DELIVERY")?>:</strong>
						<span>
							<?if (intval($shipment["DELIVERY_ID"])):?>
								<?=$shipment["DELIVERY"]["NAME"]?>
							<?else:?>
								<?=GetMessage("SPOD_NONE")?>
							<?endif?>
						</span>
					</li>
				<?endforeach;?>
				<?if (isset($arResult["ORDER_PROPS"])):?>
					<?foreach($arResult["ORDER_PROPS"] as $prop):?>
						<li>
							<strong><?=$prop['NAME']?>:</strong>

						<?if($prop["TYPE"] == "CHECKBOX"):?>
							<?=GetMessage('SPOD_'.($prop["VALUE"] == "Y" ? 'YES' : 'NO'))?>
						<?else:?>
							<span><?=$prop["VALUE"]?></span>
						<?endif?>
						</li>
					<?endforeach?>
				<?endif;?>
				<li>
					<strong><?=GetMessage('SPOD_ORDER_USER_COMMENT')?>:</strong>
					<span><?=$arResult["USER_DESCRIPTION"]?></span>
				</li>
			</ul>
			<div class="order_status_wrap">
				<div class="delivery delivery_compl"><?=$arResult["STATUS"]["NAME"]?></div>
			</div>
		</div>
		<div class="order_btn_wrap grey">
			<h2><?= GetMessage('SPOD_ORDER_PAYMENT') ?></h2>
			<div class="row">
				<div class="sale-order-detail-payment-options-inner-container">
					<div class="row">
						<div class="sale-order-detail-payment-options-info">
							<div class="row">
								<div class="sale-order-detail-payment-options-info-image"></div>
								<div class="sale-order-detail-payment-options-info-container">
									<div class="sale-order-detail-payment-options-info-order-number">
										<?= GetMessage('SPOD_SUB_ORDER_TITLE', array(
											"#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
											"#DATE_ORDER_CREATE#"=> $arResult["DATE_INSERT_FORMATED"]
										))?>
										<?
										if ($arResult['CANCELED'] !== 'Y')
										{
											echo $arResult["STATUS"]["NAME"];
										}
										else
										{
											echo GetMessage('SPOD_ORDER_CANCELED');
										}
										?>
									</div>
									<div class="sale-order-detail-payment-options-info-total-price">
										<?=GetMessage('SPOD_ORDER_PRICE_FULL')?>:
										<span><?=$arResult["PRICE_FORMATED"]?></span>
									</div>
								</div>
							</div>
						</div><!--sale-order-detail-payment-options-info-->
					</div>
					<div class="row">
						<div class="sale-order-detail-payment-options-methods-container">
							<?
							foreach ($arResult['PAYMENT'] as $payment)
							{
								?>
								<div class="row payment-options-methods-row">
									<div class="sale-order-detail-payment-options-methods">
										<div class="row sale-order-detail-payment-options-methods-information-block">
											<div class="sale-order-detail-payment-options-methods-image-container">
													<span class="sale-order-detail-payment-options-methods-image-element"
														  style="background-image: url('<?=htmlspecialcharsbx($payment['PAY_SYSTEM']["SRC_LOGOTIP"])?>');"></span>
											</div>
											<div class="sale-order-detail-payment-options-methods-info">
												<div class="sale-order-detail-payment-options-methods-info-title">
													<div class="sale-order-detail-methods-title">
														<?
														$paymentData[$payment['ACCOUNT_NUMBER']] = array(
															"payment" => $payment['ACCOUNT_NUMBER'],
															"order" => $arResult['ACCOUNT_NUMBER']
														);
														$paymentSubTitle = GetMessage('SPOD_TPL_BILL')." ".GetMessage('SPOD_NUM_SIGN').$payment['ACCOUNT_NUMBER'];
														if(isset($payment['DATE_BILL']))
														{
															$paymentSubTitle .= " ".GetMessage('SPOD_FROM')." ".$payment['DATE_BILL']->format($arParams['ACTIVE_DATE_FORMAT']);
														}
														$paymentSubTitle .=",";
														echo htmlspecialcharsbx($paymentSubTitle);
														?>
														<span class="sale-order-list-payment-title-element"><?=$payment['PAY_SYSTEM_NAME']?></span>
														<?
														if ($payment['PAID'] === 'Y')
														{
															?>
															<span class="sale-order-detail-payment-options-methods-info-title-status-success">
																	<?=GetMessage('SPOD_PAYMENT_PAID')?></span>
															<?
														}
														else
														{
															?>
															<span class="sale-order-detail-payment-options-methods-info-title-status-alert">
																	<?=GetMessage('SPOD_PAYMENT_UNPAID')?></span>
															<?
														}
														?>
													</div>
												</div>
												<div class="sale-order-detail-payment-options-methods-info-total-price">
													<span class="sale-order-detail-sum-name"><?= GetMessage('SPOD_ORDER_PRICE_BILL')?>:</span>
													<span class="sale-order-detail-sum-number"><?=$payment['PRICE_FORMATED']?></span>
												</div>
												<?
												if ($payment['PAID'] !== 'Y' && $arResult['CANCELED'] !== 'Y')
												{
													?>
													<a href="#" id="<?=$payment['ACCOUNT_NUMBER']?>" class="sale-order-detail-payment-options-methods-info-change-link"><?=GetMessage('SPOD_CHANGE_PAYMENT_TYPE')?></a>
													<?
												}
												?>
											</div>
											<?
											if ($payment['PAY_SYSTEM']["IS_CASH"] !== "Y")
											{
												?>
												<div class="col-md-2 col-sm-12 col-xs-12 sale-order-detail-payment-options-methods-button-container">
													<?
													if ($payment['PAY_SYSTEM']['PSA_NEW_WINDOW'] === 'Y')
													{
														?>
														<a class="btn_round btn_color sale-order-detail-payment-options-methods-button-element-new-window"
														   target="_blank"
														   href="<?=htmlspecialcharsbx($payment['PAY_SYSTEM']['PSA_ACTION_FILE'])?>">
															<?= GetMessage('SPOD_ORDER_PAY') ?>
														</a>
														<?
													}
													else
													{
														if ($payment["PAID"] === "Y" || $arResult["CANCELED"] === "Y")
														{
															?>
															<a class="btn_round btn_disabled btn_hover_color sale-order-detail-payment-options-methods-button-element"><?= GetMessage('SPOD_ORDER_PAY') ?></a>
															<?
														}
														else
														{
															?>
															<a class="btn_round btn_color sale-order-detail-payment-options-methods-button-element"><?= GetMessage('SPOD_ORDER_PAY') ?></a>
															<?
														}
													}
													?>
												</div>
												<?
											}
											?>
											<div class="sale-order-detail-payment-inner-row-template col-md-offset-3 col-sm-offset-5 col-md-5 col-sm-10 col-xs-12">
												<a class="sale-order-list-cancel-payment link_back">
													<i class="fa fa-angle-left "></i><?=GetMessage('SPOD_CANCEL_PAYMENT')?>
												</a>
											</div>
										</div>
										<?
										if ($payment["PAID"] !== "Y"
											&& $payment['PAY_SYSTEM']["IS_CASH"] !== "Y"
											&& $payment['PAY_SYSTEM']['PSA_NEW_WINDOW'] !== 'Y'
											&& $arResult['CANCELED'] !== 'Y')
										{
											?>
											<div class="row sale-order-detail-payment-options-methods-template col-md-12 col-sm-12 col-xs-12">
														<span class="sale-paysystem-close active-button">
															<span class="sale-paysystem-close-item sale-order-payment-cancel"></span><!--sale-paysystem-close-item-->
														</span><!--sale-paysystem-close-->
												<?=$payment['BUFFERED_OUTPUT']?>
												<!--<a class="sale-order-payment-cancel">--><?//= GetMessage('SPOD_CANCEL_PAY') ?><!--</a>-->
											</div>
											<?
										}
										?>
									</div>
								</div>
								<?
							}
							?>
						</div>
					</div>
				</div>
			</div>
			<?if($arResult["CANCELED"] == "Y" || $arResult["CAN_CANCEL"] == "Y"):?>
				<?if($arResult["CAN_CANCEL"] == "Y"):?>
					<a href="<?=$arResult["URL_TO_CANCEL"]?>" class="btn_round btn_disabled btn_hover_color order_cancel"><?=GetMessage("SPOD_ORDER_CANCEL")?></a>
				<?endif?>
			<?endif?>
			<a href="<?=$arResult["URL_TO_COPY"]?>" class="btn_round btn_border btn_hover_color order_refresh"><i class="fa fa-refresh"></i> <?=GetMessage('SPOD_REPEAT_ORDER')?></a>
		</div>

		<div class="cart_form_inner">
			<ul class="cart_list">
				<?if (isset($arResult["BASKET"])):?>
					<?
					$i = 0;
					foreach($arResult["BASKET"] as $prod):?>
						<?$hasLink = !empty($prod["DETAIL_PAGE_URL"]);?>
						<li class="cart_item">
							<div class="cart_column1">
								<div class="cart_item_title"><?=GetMessage('SPOD_NAME')?></div>
								<div class="cart_item_info">
									<div class="cart_img">
										<?if($hasLink):?><a href="<?=$prod["DETAIL_PAGE_URL"]?>" target=""><?endif?>
											<?if($prod['PICTURE']['SRC']):?>
												<img src="<?=$prod['PICTURE']['SRC']?>" width="<?=$prod['PICTURE']['WIDTH']?>" height="<?=$prod['PICTURE']['HEIGHT']?>" alt="<?=$prod['NAME']?>" />
											<?endif?>
											<?if($hasLink):?></a><?endif?>
									</div>
									<div class="cart_descr">
										<?if($hasLink):?><a href="<?=$prod["DETAIL_PAGE_URL"]?>" target=""><?endif?>
											<h2><?=$prod['NAME']?></h2>
										<?if($hasLink):?></a><?endif?>
									</div>
								</div>
							</div>
							<div class="cart_column_group">
								<div class="cart_column3">
									<div class="cart_item_title"><?=GetMessage('SPOD_PRICE')?></div>
									<div class="cart_item_info"><?=CurrencyFormat($prod['PRICE'], $prod["CURRENCY"])?></div>
								</div>
								<? if($arResult['HAS_DISCOUNT']) { ?>
									<div class="cart_column4">
										<div class="cart_item_title"><?=GetMessage('SPOD_DISCOUNT')?></div>
										<div class="cart_item_info"><? if($prod["DISCOUNT_PRICE_PERCENT_FORMATED"]) { ?><?=$prod["DISCOUNT_PRICE_PERCENT_FORMATED"]?><? } else { ?>0%<? } ?></div>
									</div>
								<? } ?>
								<div class="cart_column5">
									<div class="cart_item_title"><?=GetMessage('SPOD_QUANTITY')?></div>
									<div class="cart_item_info"><?=$prod["QUANTITY"]?></div>
								</div>
								<div class="cart_column6">
									<div class="cart_item_title"><?=GetMessage('SPOD_ORDER_PRICE')?></div>
									<div class="cart_item_info"><?=CurrencyFormat($prod['PRICE']*$prod["QUANTITY"], $prod["CURRENCY"])?></div>
								</div>
							</div>
						</li>
						<?$i++;?>
					<?endforeach?>
				<?endif;?>
			</ul>
			<div class="cart_summary">
				<div class="cart_summary_right">
					<div class="total_wrapper">
						<span><?=GetMessage('SPOD_SUMMARY')?>:</span>
						<span><?=CurrencyFormat($arResult["PRICE"], $arResult["CURRENCY"])?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?
	$javascriptParams = array(
		"url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
		"templateFolder" => CUtil::JSEscape($templateFolder),
		"paymentList" => $paymentData
	);
	$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
	?>
	<script>
		BX.Sale.PersonalOrderComponent.PersonalOrderDetail.init(<?=$javascriptParams?>);
	</script>

<?endif?>