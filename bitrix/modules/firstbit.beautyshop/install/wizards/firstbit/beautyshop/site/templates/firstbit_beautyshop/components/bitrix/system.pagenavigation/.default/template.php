<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->SetFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>
<div class="pagination_block">
	<ul>
<?

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

if($arResult["bDescPageNumbering"] === true) {
	$bFirst = true;

	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]) {
		if ($arResult["bSavePage"]) {
			?>
			<li class="nav_page pull_left hidden_xs"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><i class="fa fa-angle-double-left"></i><?= GetMessage("title_nav_prev") ?></a></li>
			<?
		} else {
			if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"] + 1)) {
				?>
				<li class="nav_page pull_left hidden_xs"><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><i class="fa fa-angle-double-left"></i><?= GetMessage("title_nav_prev") ?></a></li>
				<?
			} else {
				?>
				<li class="nav_page pull_left hidden_xs"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><i class="fa fa-angle-double-left"></i><?= GetMessage("title_nav_prev") ?></a></li>
				<?
			}
		}

		if ($arResult["nStartPage"] < $arResult["NavPageCount"]) {
			$bFirst = false;
			if ($arResult["bSavePage"]) {
				?>
				<li><a class="modern-page-first" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>">1</a></li>
				<?
			} else {
				?>
				<li><a class="modern-page-first" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a></li>
				<?
			}
			if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)) {
				?>
				<li class="dot_more"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= intVal($arResult["nStartPage"] + ($arResult["NavPageCount"] - $arResult["nStartPage"]) / 2) ?>"><i class="fa fa-ellipsis-h"></i></a></li>
				<?
			}
		}
	}
	do {
		$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;

		if ($arResult["nStartPage"] == $arResult["NavPageNomer"]) {
			?>
			<li class="active"><span class="<?= ($bFirst ? "modern-page-first " : "") ?>"><?= $NavRecordGroupPrint ?></span></li>
			<?
		} elseif ($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false) {
			?>
			<li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="<?= ($bFirst ? "modern-page-first" : "") ?>"><?= $NavRecordGroupPrint ?></a></li>
			<?
		} else {
			?>
			<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>" class="<?= ($bFirst ? "modern-page-first" : "") ?>"><?= $NavRecordGroupPrint ?></a></li>
			<?
		}

		$arResult["nStartPage"]--;
		$bFirst = false;
	} while ($arResult["nStartPage"] >= $arResult["nEndPage"]);
	?>
	<?
	if ($arResult["NavPageNomer"] > 1) {
		if ($arResult["nEndPage"] > 1) {
			if ($arResult["nEndPage"] > 2) {
				?>
				<li class="dot_more"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nEndPage"] / 2) ?>"><i class="fa fa-ellipsis-h"></i></a></li>
				<?
			}
			?>
			<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"><?= $arResult["NavPageCount"] ?></a></li>
			<?
		}

		?>
		<li class="nav_page pull_right hidden_xs"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><?= GetMessage("title_nav_next") ?><i class="fa fa-angle-double-right"></i></a></li>
		<?
	}

} else {
	$bFirst = true;

	if ($arResult["NavPageNomer"] > 1) {
		if ($arResult["bSavePage"]) {
			?>
			<li class="nav_page pull_right hidden_xs"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><i class="fa fa-angle-double-left"></i><?= GetMessage("title_nav_prev") ?></a></li>
			<?
		} else {
			if ($arResult["NavPageNomer"] > 2) {
				?>
				<li class="nav_page pull_left hidden_xs"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><i class="fa fa-angle-double-left"></i><?= GetMessage("title_nav_prev") ?></a></li>
				<?
			} else {
				?>
				<li class="nav_page pull_left hidden_xs"><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><i class="fa fa-angle-double-left"></i><?= GetMessage("title_nav_prev") ?></a></li>
				<?
			}

		}

		if ($arResult["nStartPage"] > 1) {
			$bFirst = false;
			if ($arResult["bSavePage"]) {
				?>
				<li><a class="modern-page-first" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">1</a></li>
				<?
			} else {
				?>
				<li><a class="modern-page-first" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a></li>
				<?
			}
			if ($arResult["nStartPage"] > 2) {
				?>
				<li class="dot_more"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nStartPage"] / 2) ?>"><i class="fa fa-ellipsis-h"></i></a></li>
				<?
			}
		}
	}

	do {
		if ($arResult["nStartPage"] == $arResult["NavPageNomer"]) {
			?>
			<li class="active"><span class="<?= ($bFirst ? "modern-page-first " : "") ?>"><?= $arResult["nStartPage"] ?></span></li>
			<?
		} elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false) {
			?>
			<li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="<?= ($bFirst ? "modern-page-first" : "") ?>"><?= $arResult["nStartPage"] ?></a></li>
			<?
		} else {
			?>
			<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>" class="<?= ($bFirst ? "modern-page-first" : "") ?>"><?= $arResult["nStartPage"] ?></a></li>
			<?
		}
		$arResult["nStartPage"]++;
		$bFirst = false;
	} while ($arResult["nStartPage"] <= $arResult["nEndPage"]);

	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]) {
		if ($arResult["nEndPage"] < $arResult["NavPageCount"]) {
			if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)) {
				?>
				<li class="dot_more"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nEndPage"] + ($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2) ?>"><i class="fa fa-ellipsis-h"></i></a></li>
				<?
			}
			?>
			<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a></li>
			<?
		}
		?>
		<li class="nav_page pull_right hidden_xs"><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><?= GetMessage("title_nav_next") ?><i class="fa fa-angle-double-right"></i></a></li>
		<?
	}
}
?>
	</ul>
</div>