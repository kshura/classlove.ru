<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>
<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "bottom_panel_basket", array(
	"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
	"COLUMNS_LIST" => array(
		0 => "NAME",
		1 => "DISCOUNT",
		2 => "PRICE",
		3 => "QUANTITY",
		4 => "SUM"
	),
	"AJAX_MODE" => "Y",
	"AUTO_CALCULATION" => "Y",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "Y",
	"PATH_TO_ORDER" => "#SITE_DIR#personal/order/make/",
	"PATH_TO_BASKET" => "#SITE_DIR#personal/cart/",
	"PATH_TO_CATALOG" => "#SITE_DIR#catalog/",
	"HIDE_COUPON" => "N",
	"QUANTITY_FLOAT" => "N",
	"PRICE_VAT_SHOW_VALUE" => "Y",
	"TEMPLATE_THEME" => "site",
	"SET_TITLE" => "Y",
	"AJAX_OPTION_ADDITIONAL" => "",
	"OFFERS_PROPS" => array(
		0 => "SIZES_SHOES",
		1 => "SIZES_CLOTHES",
		2 => "COLOR_REF",
	),
),
	false
);?>