$(document).ready(function () {

	$('[data-location-handler]').change(function () {
		var dataLocTarget = $('[data-location-target=' + $(this).attr('data-location-handler') + ']');

		var checkLocationByZip = $.ajax({
			url: '/local/templates/magazine/components/bitrix/main.register/.default/ajax.php',
			data: {'action': 'checkLocationByZip', 'location': $(this).val()},
			method: 'POST',
			dataType: 'json'
		});

		checkLocationByZip.done(function (response) {
			if (response.status == 'OK') {
				var updateLocationByZip = $.ajax({
					url: '/local/templates/magazine/components/bitrix/main.register/.default/ajax.php',
					data: {'action': 'updateLocationByZip', 'location': response.data.CITY_ID},
					method: 'POST',
					dataType: 'html'
				});
				updateLocationByZip.done(function (object) {
					dataLocTarget.children('.bx-slst').replaceWith(object);
				});
			}
		});
		checkLocationByZip.fail(function (response) {
			alert(response);
		});
	});

});

