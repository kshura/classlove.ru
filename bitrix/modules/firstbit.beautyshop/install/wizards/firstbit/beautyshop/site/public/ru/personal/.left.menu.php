<?
$aMenuLinks = Array(
	Array(
		"Личные данные",
		"#SITE_DIR#personal/profile/",
		Array(),
		Array("ICON" => "user"),
		""
	),
	Array(
		"Мои заказы",
		"#SITE_DIR#personal/order/",
		Array(),
		Array("ICON" => "shopping-cart"),
		""
	),
	Array(
		"Сервисы",
		"#SITE_DIR#personal/services/",
		Array(),
		Array("ICON" => "cogs"),
		""
	),
	Array(
		"Акции",
		"#SITE_DIR#personal/special/",
		Array(),
		Array("ICON" => "gift"),
		""
	)
);
?>