<?
$MESS["WZD_OPTION_REF_edit1_TAB_TITLE"]="Цвет";
$MESS["WZD_OPTION_REF_NAME"]="*Название";
$MESS["WZD_OPTION_REF_CODE"]="*Символьный код";
$MESS["WZD_OPTION_REF_PREVIEW_PICTURE"]="Картинка";
$MESS["WZD_OPTION_REF_SORT"]="Сортировка";

$MESS["UF_NAME"] = "Название";
$MESS["UF_FILE"] = "Файл";
$MESS["UF_LINK"] = "Ссылка";
$MESS["UF_SORT"] = "Сортировка";
$MESS["UF_DEF"] = "По умолчанию";
$MESS["UF_XML_ID"] = "XML_ID";
$MESS["UF_DESCRIPTION"] = "Описание";
$MESS["UF_FULL_DESCRIPTION"] = "Полное описание";
$MESS["UF_EXTERNAL_CODE"] = "Внешний код";

$MESS["WZD_REF_COLOR_BLUE"] = "Синий";
$MESS["WZD_REF_COLOR_RED"] = "Красный";
$MESS["WZD_REF_COLOR_GREEN"] = "Зеленый";
$MESS["WZD_REF_COLOR_WHITE"] = "Белый";
$MESS["WZD_REF_COLOR_BLACK"] = "Черный";
$MESS["WZD_REF_COLOR_PINK"] = "Розовый";
$MESS["WZD_REF_COLOR_AZURE"] = "Голубой";
$MESS["WZD_REF_COLOR_GRAY"] = "Cерый";
$MESS["WZD_REF_COLOR_ROSE_GOLD"] = "Розовое золото";
$MESS["WZD_REF_COLOR_YELLOW"] = "Желтый";
$MESS["WZD_REF_COLOR_LEMON_CREAM"] = "Лимонно-кремовый";
$MESS["WZD_REF_COLOR_PEACH"] = "Персиковый";
$MESS["WZD_REF_COLOR_FUCHSIA"] = "Фуксия";
$MESS["WZD_REF_COLOR_GOLD"] = "Золото";
$MESS["WZD_REF_COLOR_SILVER"] = "Серебрянный";
$MESS["WZD_REF_COLOR_VENGE"] = "Венге";
$MESS["WZD_REF_COLOR_LISTV_WHITE"] = "Лиственница белая";
$MESS["WZD_REF_COLOR_NO"] = "Прозрачный";
$MESS["WZD_REF_COLOR_BLACK_ONYX"] = "Черный оникс";
$MESS["WZD_REF_COLOR_LIGHT_GREEN"] = "Салатовый";
$MESS["WZD_REF_COLOR_ORANGE"] = "Оранжевый";
$MESS["WZD_REF_COLOR_BORDO"] = "Бордовый";
$MESS["WZD_REF_COLOR_BRAUN"] = "Коричневый";
$MESS["WZD_REF_COLOR_PURPLE"] = "Фиолетовый";
$MESS["WZD_REF_COLOR_BEG"] = "Бежевый";
$MESS["WZD_REF_COLOR_01"] = "01";
$MESS["WZD_REF_COLOR_02"] = "02";
$MESS["WZD_REF_COLOR_03"] = "03";

$MESS["WZD_REF_ADVANTAGE_FREE_SHIPPING"] = "Бесплатная доставка";
$MESS["WZD_REF_ADVANTAGE_BEST_PRICE"] = "Гарантия качества";
$MESS["WZD_REF_ADVANTAGE_CUSTOMER_CHOICE"] = "Лучшая цена";
$MESS["WZD_REF_ADVANTAGE_ECO"] = "Эко - материалы";
$MESS["WZD_REF_ADVANTAGE_KREDIT"] = "Кредит он-лайн";
$MESS["WZD_REF_ADVANTAGE_HIT"] = "Выбор посетителей";
?>