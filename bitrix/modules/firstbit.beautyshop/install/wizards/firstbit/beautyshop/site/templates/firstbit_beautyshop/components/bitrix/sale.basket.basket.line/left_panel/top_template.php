<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
	<div class="cart_wrapper">
		<a href="<?=$arParams['PATH_TO_BASKET']?>">
			<i class="fa fa-shopping-cart"></i>
			<span class="cart_title"><?=GetMessage('TSB1_CART')?>:</span>
			<span class="cart_item_count"><?=$arResult['NUM_PRODUCTS']?> <span class=""><?=GetMessage('TSB1_MEASURE')?></span></span>
			<span class="cart_price_total"><?=$arResult['TOTAL_PRICE']?></span>
		</a>
	</div>
