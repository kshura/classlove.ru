<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
?>
<nav class="top_menu <?=$arParams['TYPE']?> show_lg">
	<ul class="menu_flex <?=$arParams['ITEMS_SHOW']?>">
		<? foreach ($arResult as $arMenuLink) { ?><li<?=$arMenuLink['SELECTED'] == 'Y' ? ' class="active" ' : ''?>><a href="<?=$arMenuLink['LINK']?>" class="not_underline"><?=$arMenuLink['TEXT']?></a></li><? } ?>
	</ul>
</nav>