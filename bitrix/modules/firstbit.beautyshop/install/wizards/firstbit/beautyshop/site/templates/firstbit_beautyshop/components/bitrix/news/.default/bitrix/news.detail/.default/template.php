<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
IncludeTemplateLangFile(__FILE__);
$this->setFrameMode(true);
$APPLICATION->SetPageProperty('title', $arResult["NAME"]);
$APPLICATION->SetTitle($arResult["NAME"]);
?>
<?$this->SetViewTarget('pageTitle_subTitle');?>
	<div class="article_title_detail">
	<?if($arResult['DISPLAY_ACTIVE_FROM']):?>
		<div class="article_time"><?=$arResult['DISPLAY_ACTIVE_FROM']?></div>
	<?endif?>
	<?if($arResult['PROPERTIES']["ISSALE"]["VALUE"]):?>
		<div class="article_icon"><i class="i-sale-txt"><?=GetMessage("SALE")?></i></div>
	<?endif?>
	</div>
<?$this->EndViewTarget();?>
<div class="article_detail_inner">
	<? if($arResult["DETAIL_PICTURE"]["SRC"]) {?>
		<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="" height="" title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>">
	<? } ?>
	<div class="article_detail">
		<?=$arResult["DETAIL_TEXT"]?>
	</div>
	<div class="share_block">
		<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
		<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
		<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir" data-counter=""></div>
	</div>
	<div>
		<a class="link_back" href="<?=$arResult["LIST_PAGE_URL"]?>"><i class="fa fa-angle-left "></i><?=GetMessage("TOBACK")?></a>
	</div>
</div>
