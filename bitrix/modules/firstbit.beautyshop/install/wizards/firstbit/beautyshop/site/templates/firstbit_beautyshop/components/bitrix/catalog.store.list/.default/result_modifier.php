<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();

foreach ($arResult['STORES'] as $storeIndex => $arStore) {
	if(mb_strlen($arStore['TITLE']) > 0) {
		$title = explode(" (", $arStore['TITLE']);
		$arResult['STORES'][$storeIndex]['TITLE'] = $title[0];
		$arResult['STORES'][$storeIndex]['ADDRESS'] = str_replace(")","",$title[1]);
	}
	$arResult['STORES'][$storeIndex]['USER_FIELDS'] = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ("CAT_STORE", $arStore['ID']);
	$arResult['STORES'][$storeIndex]['PHONE'] = CFirstbitBeautyshop::parsePhone($arStore['PHONE']);
}