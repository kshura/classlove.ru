<?
use Bitrix\Main\Type\Collection;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if (!empty($arResult['ITEMS'])) {
	foreach ($arResult['ITEMS'] as $key => $arItem) {
		if($arItem['PROPERTIES']['VERTICAL_PICTURE']['VALUE']) {
			$arVerticalPicture = CFile::GetFileArray($arItem['PROPERTIES']['VERTICAL_PICTURE']['VALUE']);
			$arResult['ITEMS'][$key]['PROPERTIES']['VERTICAL_PICTURE']['VALUE'] = $arVerticalPicture;
		}
		if($arItem['PROPERTIES']['HORIZONTAL_PICTURE_WIDE']['VALUE']) {
			$arHorizontalPictureWide = CFile::GetFileArray($arItem['PROPERTIES']['HORIZONTAL_PICTURE_WIDE']['VALUE']);
			$arResult['ITEMS'][$key]['PROPERTIES']['HORIZONTAL_PICTURE_WIDE']['VALUE'] = $arHorizontalPictureWide;
		}
		if($arItem['PROPERTIES']['HORIZONTAL_PICTURE_NARROW']['VALUE']) {
			$arHorizontalPictureNarrow = CFile::GetFileArray($arItem['PROPERTIES']['HORIZONTAL_PICTURE_NARROW']['VALUE']);
			$arResult['ITEMS'][$key]['PROPERTIES']['HORIZONTAL_PICTURE_NARROW']['VALUE'] = $arHorizontalPictureNarrow;
		}
	}
}