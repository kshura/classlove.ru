<?php
global $MESS;
$langPath = str_replace("\\", "/", __FILE__);
$langPath = substr($langPath, 0, strlen($langPath) - strlen("/inatall/index.php"));
include(GetLangFileName($langPath."/lang/", "/install/index.php"));

class firstbit_beautyshop extends CModule {

	var $PARTNER_CODE = "firstbit";
	var $MODULE_CODE = "beautyshop";
	var $MODULE_ID = "firstbit.beautyshop";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";
	var $PARTNER_NAME;
	var $PARTNER_URI;

	function firstbit_beautyshop() {
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->PARTNER_NAME = GetMessage("PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("PARTNER_URI");
		$this->MODULE_NAME = GetMessage("WIZARD_TITLE");
		$this->MODULE_DESCRIPTION = GetMessage("WIZARD_DESCRIPTION");
	}

	function InstallDB() {
		return true;
	}
	function UnInstallDB() {
		return true;
	}

	function InstallEvents() {
		RegisterModuleDependences("catalog", "OnPriceAdd", "firstbit.beautyshop", "CFirstbitBeautyshop", "onSetMinPrice");
		RegisterModuleDependences("catalog", "OnPriceUpdate", "firstbit.beautyshop", "CFirstbitBeautyshop", "onSetMinPrice");
		RegisterModuleDependences("main", "OnAfterUserRegister", "firstbit.beautyshop", "CFirstbitBeautyshop", "onUserRegister");
		RegisterModuleDependences("main", "OnUserConsentProviderList", "firstbit.beautyshop", "\\Firstbit\\Beautyshop\\UserConsent", "onProviderList");
		return true;
	}
	function UnInstallEvents() {
		UnRegisterModuleDependences("catalog", "OnPriceAdd", "firstbit.beautyshop", "CFirstbitBeautyshop", "onSetMinPrice");
		UnRegisterModuleDependences("catalog", "OnPriceUpdate", "firstbit.beautyshop", "CFirstbitBeautyshop", "onSetMinPrice");
		UnRegisterModuleDependences("main", "OnAfterUserRegister", "firstbit.beautyshop", "CFirstbitBeautyshop", "onUserRegister");
		UnRegisterModuleDependences("main", "OnUserConsentProviderList", "firstbit.beautyshop", "\\Firstbit\\Beautyshop\\UserConsent", "onProviderList");
		return true;
	}

	function InstallFiles() {
		CopyDirFiles(__DIR__.'/admin/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin', true);
		CopyDirFiles(__DIR__.'/themes/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/themes', true, true);
		CopyDirFiles(__DIR__.'/components/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/components', true, true);
		CopyDirFiles(__DIR__.'/wizards/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards', true, true);
		return true;
	}
	function UnInstallFiles() {
		DeleteDirFiles(__DIR__.'/admin/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin');
		DeleteDirFiles(__DIR__.'/themes/.default/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/themes/.default');
		DeleteDirFilesEx($_SERVER['DOCUMENT_ROOT'].'/bitrix/themes/.default/icons/'.$this->MODULE_ID.'/');
		DeleteDirFilesEx('/bitrix/wizards/'.$this->PARTNER_CODE.'/'.$this->MODULE_CODE.'/');
		return true;
	}

	function DoInstall() {
		global $APPLICATION;
		if (!IsModuleInstalled("firstbit.beautyshop")) {
			RegisterModule("firstbit.beautyshop");
			$this->InstallDB();
			$this->InstallEvents();
			$this->InstallFiles();
		}
	}
	function DoUninstall(){
		if (IsModuleInstalled("firstbit.beautyshop")) {
			$this->UnInstallDB();
			$this->UnInstallEvents();
			$this->UnInstallFiles();
			UnRegisterModule("firstbit.beautyshop");
		}
	}
}