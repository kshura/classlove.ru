<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$isAjax = ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["ajax_action"]) && $_POST["ajax_action"] == "Y");

if ($isAjax) {
	$APPLICATION->RestartBuffer();
}
?>
<div class="row compare" id="bx_catalog_compare_block">
	<?
	if(!empty($arResult['ITEMS']) && is_array($arResult['ITEMS'])) {
		?>
		<div class="row">
			<div class="top_menu_line"></div>
			<div class="container_16">
				<div class="grid_16">
					<div class="common_tabs">
						<ul class="btn-panel">
							<li class="btn-panel-item <? echo(!$arResult["DIFFERENT"] ? 'ui-state-active' : ''); ?>">
								<a class="" href="<? echo $arResult['COMPARE_URL_TEMPLATE'] . 'DIFFERENT=N'; ?>"
								 rel="nofollow"><?= GetMessage("CATALOG_ALL_CHARACTERISTICS") ?></a>
							</li>
							<li class="btn-panel-item <? echo($arResult["DIFFERENT"] ? 'ui-state-active' : ''); ?>">
								<a class="" href="<? echo $arResult['COMPARE_URL_TEMPLATE'] . 'DIFFERENT=Y'; ?>"
								 rel="nofollow"><?= GetMessage("CATALOG_ONLY_DIFFERENT") ?></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="row grey">
			<div class="container_12 section_inner">
				<div class="grid_12 section_line_head">
					<div class="owl_nav small dark" id="owl_compare_nav"></div>
				</div>
				<div class="clear"></div>
				<div class="grid_12 grid_12_sm">
					<div class="compare_left_block hidden_xs ">
						<div class="compare_left_block_inner">
							<span><? echo GetMessage('CATALOG_SHOWN_CHARACTERISTICS'); ?></span>
							<span
								class="compare_count"><span><?= count($arResult['ITEMS']) ?></span><? echo GetMessage('CATALOG_SHOWN_CHARACTERISTICS_MEASURE'); ?></span>
						</div>
					</div>
					<div class="compare_right_block" id="compare_carousel" onload="foo()">
						<?
						foreach ($arResult['ITEMS'] as $arItem) {
							?>
							<div class="product_item" data-idproduct="item1" >
								<div class="close_btn">
									<a href="<?=$APPLICATION->GetCurPage(false)?>" data-compare-id="<? echo $arItem['ID']; ?>"
									 onmousedown="compareHandler(this);"
									 title="<? echo GetMessage('CATALOG_REMOVE_PRODUCT'); ?>"><i
											class="fa fa-times"></i></a>
									<script type="text/javascript">
										compareCheck(<?=$arItem['ID']?>);
									</script>
								</div>
								<a class="product_image" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
									<div style="background-image: url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>);"></div>
								</a>
								<div class="product_description">
									<div class="product_title">
										<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
											<h3><?= ($arItem['OFFER_FIELDS']['NAME'] ? $arItem['OFFER_FIELDS']['NAME'] : $arItem['NAME']) ?></h3>
										</a>
									</div>
									<div class="buy_wrap">
										<div class="price_wrap">
											<span class="current_price">
												<?
												if (!empty($arItem['MIN_PRICE']) && $arItem['MIN_PRICE']['DISCOUNT_VALUE'] > 0) {
													echo $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
													if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) {
														?> <span
															class="old_price"><? echo $arItem['MIN_PRICE']['PRINT_VALUE']; ?></span><?
													}
												} else {
													$notAvailableMessage = ('' != $arParams['MESS_NULL_PRICE'] ? $arParams['MESS_NULL_PRICE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NULL_PRICE'));
													echo $notAvailableMessage;
												}
												?>
											</span>
										</div>
									</div>
								</div>
							</div>
							<?
						}
						?>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="row">
			<div class="container_16">
				<div class="grid_16 grid_16_sm">
					<div class="table_container">
						<div class="compare_table">
							<?
							if (!empty($arResult["SHOW_FIELDS"])) {
								foreach ($arResult["SHOW_FIELDS"] as $code => $arProp) {
									$showRow = true;
									if (!isset($arResult['FIELDS_REQUIRED'][$code]) || $arResult['DIFFERENT']) {
										$arCompare = array();
										foreach ($arResult["ITEMS"] as &$arElement) {
											$arPropertyValue = $arElement["FIELDS"][$code];
											if (is_array($arPropertyValue)) {
												sort($arPropertyValue);
												$arPropertyValue = implode(" / ", $arPropertyValue);
											}
											$arCompare[] = $arPropertyValue;
										}
										unset($arElement);
										$showRow = (count(array_unique($arCompare)) > 1);
									}
									if ($showRow) {
										switch ($code) {
											case "NAME":
											case "PREVIEW_PICTURE":
											case "DETAIL_PICTURE":
												break;
											default:
												?>
												<div class="compare_table_row">
													<div class="compare_table_cell"><?= GetMessage("IBLOCK_FIELD_" . $code) ?></div>
													<?
													foreach ($arResult["ITEMS"] as &$arElement) {
														$strValue = (is_array($arElement["FIELDS"][$code]) ? implode("/ ", $arElement["FIELDS"][$code]) : $arElement["FIELDS"][$code]);
														?>
														<div class="compare_table_cell <?=(mb_strlen($strValue) > 0 ? '' : 'empty')?>"><?=(mb_strlen($strValue) > 0 ? $strValue : '&mdash;')?></div>
														<?
													}
													unset($arElement, $strValue);
													?>
												</div>
												<?
												break;
										}
									}
								}
							}
							if (!empty($arResult["SHOW_OFFER_FIELDS"])) {
								foreach ($arResult["SHOW_OFFER_FIELDS"] as $code => $arProp) {
									$showRow = true;
									if ($arResult['DIFFERENT']) {
										$arCompare = array();
										foreach ($arResult["ITEMS"] as &$arElement) {
											$Value = $arElement["OFFER_FIELDS"][$code];
											if (is_array($Value)) {
												sort($Value);
												$Value = implode(" / ", $Value);
											}
											$arCompare[] = $Value;
										}
										unset($arElement);
										$showRow = (count(array_unique($arCompare)) > 1);
									}
									if ($showRow) {
										switch ($code) {
											case "NAME":
											case "PREVIEW_PICTURE":
											case "DETAIL_PICTURE":
												break;
											default:
												?>
												<div class="compare_table_row">
													<div class="compare_table_cell"><?= GetMessage("IBLOCK_OFFER_FIELD_" . $code) ?></div>
													<? foreach ($arResult["ITEMS"] as &$arElement) {
														$strValue = (is_array($arElement["OFFER_FIELDS"][$code]) ? implode("/ ", $arElement["OFFER_FIELDS"][$code]) : $arElement["OFFER_FIELDS"][$code]);
														?>
														<div class="compare_table_cell <?=(mb_strlen($strValue) > 0 ? '' : 'empty')?>"><?=(mb_strlen($strValue) > 0 ? $strValue : '&mdash;')?></div>
														<?
													}
													unset($arElement, $strValue);
													?>
												</div>
												<?
												break;
										}
									}
								}
							}
							if (!empty($arResult["SHOW_PROPERTIES"])) {
								foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty) {
									$showRow = true;
									if ($arResult['DIFFERENT']) {
										$arCompare = array();
										foreach ($arResult["ITEMS"] as &$arElement) {
											$arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];
											if (is_array($arPropertyValue)) {
												sort($arPropertyValue);
												$arPropertyValue = implode(" / ", $arPropertyValue);
											}
											$arCompare[] = $arPropertyValue;
										}
										unset($arElement);
										$showRow = (count(array_unique($arCompare)) > 1);
									}
									if ($showRow) {
										switch ($code) {
											case "MORE_PHOTO":
												break;
											default:
												?>
												<div class="compare_table_row">
													<div class="compare_table_cell"><?= $arProperty["NAME"] ?></div>
													<? foreach ($arResult["ITEMS"] as &$arElement) {
														$strValue = (is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) ? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) : $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]);
														?>
														<div class="compare_table_cell <?=(mb_strlen($strValue) > 0 ? '' : 'empty')?>"><?=(mb_strlen($strValue) > 0 ? $strValue : '&mdash;')?></div>
														<?
													}
													unset($arElement, $strValue);
													?>
												</div>
												<?
												break;
										}
									}
								}
							}
							if (!empty($arResult["SHOW_OFFER_PROPERTIES"])) {
								foreach ($arResult["SHOW_OFFER_PROPERTIES"] as $code => $arProperty) {
									$showRow = true;
									if ($arResult['DIFFERENT']) {
										$arCompare = array();
										foreach ($arResult["ITEMS"] as &$arElement) {
											$arPropertyValue = $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["VALUE"];
											if (is_array($arPropertyValue)) {
												sort($arPropertyValue);
												$arPropertyValue = implode(" / ", $arPropertyValue);
											}
											$arCompare[] = $arPropertyValue;
										}
										unset($arElement);
										$showRow = (count(array_unique($arCompare)) > 1);
									}
									if ($showRow) {
										switch ($code) {
											case "MORE_PHOTO":
												break;
											default:
												?>
												<div class="compare_table_row">
													<div class="compare_table_cell"><?= $arProperty["NAME"] ?></div>
													<? foreach ($arResult["ITEMS"] as &$arElement) {
														$strValue = (is_array($arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) ? implode("/ ", $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) : $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]);
														?>
														<div class="compare_table_cell <?=(mb_strlen($strValue) > 0 ? '' : 'empty')?>"><?=(mb_strlen($strValue) > 0 ? $strValue : '&mdash;')?></div>
														<?
													}
													unset($arElement, $strValue);
													?>
												</div>
												<?
												break;
										}
									}
								}
							}
							?>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<script type="text/javascript">
			$(document).ready(function() {
				var owl_compare = $("#compare_carousel");
				owl_compare.on('changed.owl.carousel',function(property){
					var startItem = property.item.index + 1,
						countItems = property.item.count,
						lastItem;

					if (countItems > 0) {
						if (property.page.size >= property.item.count) {
							lastItem = property.item.count;
						}
						else {
							lastItem = (property.page.size + startItem) - 1;
						}

						$('.compare_table_row').each(function (rowIndex) {
							var rowCompare = this;
							$(rowCompare).find('.compare_table_cell').each(function (cellIndex) {
								if (cellIndex > 0) {
									$(this).css('display', 'none');
								}
								if ((cellIndex+1) > startItem && (cellIndex) <= lastItem) {
									$(this).css('display', 'table-cell');
								}
								else {
									if (cellIndex != 0) {
										$(this).css('display', 'none');
									}
								}
							});

						});
					}
				});
				owl_compare.owlCarousel({
					responsive: {
						0: {
							items: 2,
						},
						719: {
							items: 3,
						},
						959: {
							items: 4,
						}
					},
					loop: false,
					nav: true,
					dots: false,
					smartSpeed: 500,
					navContainer: '#owl_compare_nav',
					navText: ["<i class=\"fa fa-angle-left\"></i>","<i class=\"fa fa-angle-right\"></i>"],
					afterInit: initSlider(owl_compare),
				});
			});

		</script>
		<script type="text/javascript">
			var CatalogCompareObj = new BX.Iblock.Catalog.CompareClass("bx_catalog_compare_block");
		</script>
		<?
	} else {
		?>
		<div class="container_16">
			<div class="page_empty_message active">
				<h3 class="page_empty_message_title"><?=($arParams['MESS_COMPARE_EMPTY_TITLE'] ? $arParams['MESS_COMPARE_EMPTY_TITLE'] : GetMessage('MESS_COMPARE_EMPTY_TITLE'))?></h3>
				<p class="page_empty_message_text"><?=($arParams['MESS_COMPARE_EMPTY_TEXT'] ? $arParams['MESS_COMPARE_EMPTY_TEXT'] : GetMessage('MESS_COMPARE_EMPTY_TEXT', array('#CATALOG_URL#' => $arParams['PATH_TO_CATALOG'])))?></p>
			</div>
		</div>
		<?
	}
	?>
</div>
