BX.namespace("BX.Iblock.Catalog");

BX.Iblock.Catalog.CompareClass = (function()
{
	var CompareClass = function(wrapObjId)
	{
		this.wrapObjId = wrapObjId;
	};

	CompareClass.prototype.MakeAjaxAction = function(url)
	{
		BX.showWait(BX(this.wrapObjId));
		BX.ajax.post(
			url,
			{
				ajax_action: 'Y'
			},
			BX.proxy(function(result)
			{
				BX.closeWait();
				BX(this.wrapObjId).innerHTML = result;
			}, this)
		);
	};

	return CompareClass;
})();
var owl_compare_bottom_panel = $('#tabs-compare .carousel_wrap > ul');
owl_compare_bottom_panel.owlCarousel({
	items: 4,
	loop: false,
	nav: true,
	dots: false,
	smartSpeed: 500,
	navContainer: '#owl_compare_bottom_nav',
	navText: ["<i class=\"fa fa-chevron-left\"></i>","<i class=\"fa fa-chevron-right\"></i>"],
	afterInit: initSlider(owl_compare_bottom_panel),
});