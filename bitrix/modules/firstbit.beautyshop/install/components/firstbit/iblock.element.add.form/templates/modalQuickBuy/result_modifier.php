<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if (strlen($arResult["MESSAGE"]) > 0) {
	$arResult['MESSAGE'] = $arParams['SUCCESS_MESSAGE'] ? $arParams['SUCCESS_MESSAGE'] : GetMessage('IBLOCK_FORM_SUCCESS');
}

if (CModule::IncludeModule("catalog")) {
	if(isset($arParams['PRODUCT_ID']) && $arParams['PRODUCT_ID']>0) {
		$rsProduct = CCatalogSku::GetProductInfo($arParams['PRODUCT_ID']);
		if(is_array($rsProduct)) {
			$productID = $rsProduct['ID'];
			$offerID = $arParams['PRODUCT_ID'];
		} else {
			$productID = $arParams['PRODUCT_ID'];
		}
		unset($rsProduct);
		$rsProduct = CIBlockElement::GetByID($productID);
		if($arProduct = $rsProduct->GetNext()) {
			$arResult['PRODUCT_ID'] = $productID;
			$arResult['PRODUCT_NAME'] = $arProduct['NAME'];
		}
		if($offerID) {
			$rsOffer = CIBlockElement::GetByID($productID);
			if($arOffer = $rsOffer->GetNext()) {
				$arResult['OFFER_ID'] = $offerID;
				$arResult['OFFER_NAME'] = $arOffer['NAME'];
			}
		}
	}
}