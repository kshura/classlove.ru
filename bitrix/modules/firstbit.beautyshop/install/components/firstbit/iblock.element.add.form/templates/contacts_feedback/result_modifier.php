<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if (strlen($arResult["MESSAGE"]) > 0) {
	$arResult['MESSAGE'] = $arParams['SUCCESS_MESSAGE'] ? $arParams['SUCCESS_MESSAGE'] : GetMessage('IBLOCK_FORM_SUCCESS');
}