<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(false);
$firstBit = new CFirstbitBeautyshop();
if (strlen($arResult["MESSAGE"]) > 0) {?>
	<span class="form-result">
		<?echo $arResult["MESSAGE"];?>
	</span>
	<script type="text/javascript">
		modalArrange();
	</script>
	<?
} else {
	?>
	<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" class="form form_modal">
		<?= bitrix_sessid_post() ?>
		<?
		$elementName = GetMessage('IBLOCK_ELEMENT_NAME', array("#DATE#" => date('d.m.Y')));
		?>
		<input type="hidden" name="PROPERTY[NAME][]" value="<?= $elementName ?>"/>
		<?
		if($USER->IsAuthorized()) {
			$useridValue = $USER->GetID();
		} else {
			$useridValue = '';
		}
		?>
		<input type="hidden" name="PROPERTY[USER_ID][]" value="<?= $useridValue ?>"/>
		<input type="hidden" name="PROPERTY[USER_IP][]" value="<?= $firstBit->getRealIP() ?>"/>
		<input type="hidden" name="PROPERTY[FEEDBACK_TYPE][]" value="<?= $arParams['FEEDBACK_TYPE'] ?>"/>
		<ul class="form_fields">
			<li>
				<label><?= GetMessage("IBLOCK_FORM_FIELD_USER_NAME") ?></label>
				<?
				if($USER->IsAuthorized()) {
					$usernameValue = $USER->GetFullName();
				} else {
					$usernameValue = '';
				}
				?>
				<input type="text" name="PROPERTY[USER_NAME][]" placeholder="<?= GetMessage("IBLOCK_FORM_FIELD_USER_NAME") ?>" value="<?= $usernameValue ?>" required="required"/><i
					class="fa fa-check-circle not-active"></i>
			</li>
			<li>
				<label><?= GetMessage("IBLOCK_FORM_FIELD_USER_EMAIL") ?></label>
				<?
				if($USER->IsAuthorized()) {
					$emailValue = $USER->GetEmail();
				} else {
					$emailValue = '';
				}
				?>
				<input type="text" name="PROPERTY[USER_EMAIL][]" data-mask="email" placeholder="<?= GetMessage("IBLOCK_FORM_FIELD_USER_EMAIL") ?>" value="<?= $emailValue ?>" required="required"/><i class="fa fa-check-circle not-active"></i>
			</li>
			<li>
				<label><?= GetMessage('IBLOCK_FORM_FIELD_MESSAGE') ?></label>
				<textarea name="PROPERTY[MESSAGE][]" required="required"></textarea><i
					class="fa fa-check-circle not-active"></i>
			</li>
			<?
			if ($firstBit->options['CONSENT_FORM'] === 'Y' && $firstBit->options['AGREEMENT_ID'] > 0) {
				?>
				<li>
					<input type="hidden" name="agreement_id" value="<?= $firstBit->options['AGREEMENT_ID']?>" />
					<input type="checkbox" required name="agreement_consent" id="legal" class="legal" value="Y"><label for="legal"><a href="<?= $firstBit->options['AGREEMENT_PAGE_LINK']?>" target="_blank" rel="nofollow"><?= $firstBit->options['AGREEMENT_LABEL_FORM_TEXT']?></a></label>
				</li>
				<?
			}
			?>
		</ul>
		<div class="form_btn_wrapper btn_wrapper">
			<input type="submit" class="btn_round btn_color" name="iblock_submit" value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
		</div>
	</form>
	<script type="text/javascript">
		$(document).ready(function () {
			modalInitJS();
		});
	</script>
	<?
}
?>