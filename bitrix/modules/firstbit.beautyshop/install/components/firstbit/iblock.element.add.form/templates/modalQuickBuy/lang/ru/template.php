<?
$MESS["IBLOCK_FORM_SUBMIT"] = "Отправить";
$MESS["IBLOCK_FORM_SUCCESS"] = "Спасибо, ваша заявка успешно отправлена. Наш менеджер свяжется с вами в самое ближайшее время.";
$MESS["IBLOCK_ELEMENT_NAME"] = "Заявка от #DATE# на товар #PRODUCT_NAME# (#OFFER_NAME#)";
$MESS["IBLOCK_FORM_FIELD_USER_NAME"] = "Ваше имя";
$MESS["IBLOCK_FORM_FIELD_USER_PHONE"] = "Телефон";