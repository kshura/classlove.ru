<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?
$this->setFrameMode(false);
?>
	<div class="demo_request_form <?if (strlen($arResult["MESSAGE"]) > 0) {?>open<?}?>">
	<div class="demo_request_form_label"><?=GetMessage('IBLOCK_FORM_DEMO_REQUEST_LABEL')?></div>
	<div class="demo_request_form_body grey">
		<div class="demo_request_form_title"><?=GetMessage('IBLOCK_FORM_DEMO_REQUEST_TITLE')?></div>
<?
$firstBit = new CFirstbitBeautyshop();
if (strlen($arResult["MESSAGE"]) > 0) {
?>
	<div class="form form_modal">
		<span class="form-result">
			<?echo $arResult["MESSAGE"];?>
		</span>
		<div class="form_btn_wrapper btn_wrapper">
			<span class="btn_round btn_color demo_request_form_close"><?= GetMessage("IBLOCK_FORM_CLOSE") ?></span>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function () {
			$('.demo_request_form_close').click(function () {
				$('.demo_request_form').hide();
			});
		});
	</script>
<?
} else {
	?>
	<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" class="form form_modal">
		<?= bitrix_sessid_post() ?>
		<?
		$elementName = GetMessage('IBLOCK_ELEMENT_NAME', array("#DATE#" => date('d.m.Y')));
		?>
		<input type="hidden" name="PROPERTY[NAME][]" value="<?= $elementName ?>"/>
		<?
		if($USER->IsAuthorized()) {
			$useridValue = $USER->GetID();
		} else {
			$useridValue = '';
		}
		?>
		<ul class="form_fields">
			<li>
				<label><?= GetMessage("IBLOCK_FORM_FIELD_USER_NAME") ?></label>
				<input type="text" name="PROPERTY[USER_NAME][]" placeholder="<?= GetMessage("IBLOCK_FORM_FIELD_USER_NAME") ?>" value="" required="required"/><i
					class="fa fa-check-circle not-active"></i>
			</li>
			<li>
				<label><?= GetMessage("IBLOCK_FORM_FIELD_USER_EMAIL") ?></label>
				<?
				if($USER->IsAuthorized()) {
					$emailValue = $USER->GetEmail();
				} else {
					$emailValue = '';
				}
				?>
				<input type="text" name="PROPERTY[USER_EMAIL][]" data-mask="email" placeholder="<?= GetMessage("IBLOCK_FORM_FIELD_USER_EMAIL") ?>" value="<?= $emailValue ?>" required="required"/><i class="fa fa-check-circle not-active"></i>
			</li>
			<li>
				<label><?= GetMessage("IBLOCK_FORM_FIELD_USER_PHONE") ?></label>
				<input type="text" name="PROPERTY[USER_PHONE][]" data-mask="phone" placeholder="<?= GetMessage("IBLOCK_FORM_FIELD_USER_PHONE") ?>" value="" required="required"/><i class="fa fa-check-circle not-active"></i>
			</li>
			<?
			if ($firstBit->options['CONSENT_FORM'] === 'Y' && $firstBit->options['AGREEMENT_ID'] > 0) {
				?>
				<li>
					<input type="hidden" name="agreement_id" value="<?= $firstBit->options['AGREEMENT_ID']?>" />
					<input type="checkbox" required name="agreement_consent" id="legal" class="legal" value="Y"><label for="legal"><a href="<?= $firstBit->options['AGREEMENT_PAGE_LINK']?>" target="_blank" rel="nofollow"><?= $firstBit->options['AGREEMENT_LABEL_FORM_TEXT']?></a></label>
				</li>
				<?
			}
			?>
		</ul>
		<div class="form_btn_wrapper btn_wrapper">
			<input type="submit" class="btn_round btn_color" name="iblock_submit" value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
		</div>
	</form>
	<script type="text/javascript">
		$(document).ready(function () {
			$('.demo_request_form_label').click(function () {
				if($('.demo_request_form').hasClass('open')) {
					$('.demo_request_form').removeClass('open');
				} else {
					$('.demo_request_form').addClass('open');
				}
			});
		});
	</script>
	<?
}
?>
	</div>
</div>
