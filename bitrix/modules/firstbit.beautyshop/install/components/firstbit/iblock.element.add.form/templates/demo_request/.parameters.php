<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters["SUCCESS_MESSAGE"] = array(
	"NAME" => GetMessage("T_IBLOCK_SUCCESS_MESSAGE"),
	"DEFAULT" => "",
	"TYPE" => "STRING",
	"MULTIPLE" => "N",
	"COLS" => 25,
);

?>