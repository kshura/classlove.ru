<?
$MESS["IBLOCK_FORM_SUBMIT"] = "Отправить";
$MESS["IBLOCK_FORM_SUCCESS"] = "Ваше заявка на звонок принята. Мы перезвоним вам в самое ближайшее время.";
$MESS["IBLOCK_ELEMENT_NAME"] = "Запрос обратного звонка от #DATE#";
$MESS["IBLOCK_FORM_FIELD_USER_NAME"] = "Ваше имя";
$MESS["IBLOCK_FORM_FIELD_USER_PHONE"] = "Телефон";
$MESS["IBLOCK_FORM_FIELD_MESSAGE"] = "Комментарий";