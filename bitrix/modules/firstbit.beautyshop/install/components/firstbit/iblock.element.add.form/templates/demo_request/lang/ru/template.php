<?
$MESS["IBLOCK_FORM_SUBMIT"] = "Запросить доступ";
$MESS["IBLOCK_FORM_CLOSE"] = "Закрыть";
$MESS["IBLOCK_FORM_SUCCESS"] = "Ваше запрос принят. Мы подготовим для вас демо-версию и отправим доступы на указанную электронную почту в ближайшее время.";
$MESS["IBLOCK_ELEMENT_NAME"] = "Запрос демо-версии от #DATE#";
$MESS["IBLOCK_FORM_FIELD_USER_NAME"] = "Имя";
$MESS["IBLOCK_FORM_FIELD_USER_EMAIL"] = "E-mail";
$MESS["IBLOCK_FORM_FIELD_USER_PHONE"] = "Телефон";
$MESS["IBLOCK_FORM_DEMO_REQUEST_TITLE"] = "Получить доступ в админку";
$MESS["IBLOCK_FORM_DEMO_REQUEST_LABEL"] = "Доступ в админку";