<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$frame = $this->createFrame()->begin();
$APPLICATION->IncludeComponent(
	"firstbit:sale.personal.profile.edit",
	"",
	array(
		"PATH_TO_LIST" => $arResult["PATH_TO_LIST"],
		"PATH_TO_EDIT" => $arResult["PATH_TO_EDIT"],
		"SET_TITLE" =>$arParams["SET_TITLE"],
		"USE_AJAX_LOCATIONS" => $arParams['USE_AJAX_LOCATIONS'],
		"ID" => $arResult["VARIABLES"]["ID"],
	),
	$component
);
?>
