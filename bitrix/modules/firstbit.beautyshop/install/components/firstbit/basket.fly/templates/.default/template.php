<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<div id="fly_cart" class="show_lg">
	<div class="fly_cart_inner">
		<ul class="fly_cart_label">
			<li><a href="#fly-tabs-1" class="label_cart sc fly-tabs-1"><i class="fa cart"></i></a></li>
		</ul>
		<div class="fly_cart_container grey2">
			<div class="fly_cart_preloader"></div>
			<div class="fly_tabs" id="fly-tabs-1">
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>