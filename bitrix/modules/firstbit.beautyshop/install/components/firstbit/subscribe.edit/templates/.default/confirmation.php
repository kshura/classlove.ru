<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="<?=$arResult["FORM_ACTION"]?>" method="get" class="form form_general">
	<div class="form_fields_section">
		<h2><?echo GetMessage("subscr_title_confirm")?></h2>
	</div>
	<ul class="form_fields">
		<li>
			<label><?echo GetMessage("subscr_conf_code")?></label>
			<input type="text" name="CONFIRM_CODE" value="<?echo $arResult["REQUEST"]["CONFIRM_CODE"];?>" />
			<i class="fa fa-check-circle not-active"></i>
		</li>
		<li class="form_text">
			<?echo GetMessage("subscr_conf_note1")?> <a title="<?echo GetMessage("adm_send_code")?>" href="<?echo $arResult["FORM_ACTION"]?>?ID=<?echo $arResult["ID"]?>&amp;action=sendcode&amp;<?echo bitrix_sessid_get()?>"><?echo GetMessage("subscr_conf_note2")?></a>.
		</li>
		<li class="form_text">
			<?echo GetMessage("subscr_conf_date")?> <?echo $arResult["SUBSCRIPTION"]["DATE_CONFIRM"];?>
		</li>
	</ul>
	<div class="form_btn_wrapper btn_wrapper">
		<input type="submit" class="btn_round btn_color" name="confirm" value="<?echo GetMessage("subscr_conf_button")?>" />
	</div>
	<input type="hidden" name="ID" value="<?echo $arResult["ID"];?>" />
	<?echo bitrix_sessid_post();?>
</form>
<br />
