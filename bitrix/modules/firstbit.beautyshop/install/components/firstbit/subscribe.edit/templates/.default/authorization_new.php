<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<form action="<?=$arResult["FORM_ACTION"]?>" method="post" class="form form_general">
	<?echo bitrix_sessid_post();?>
	<div class="form_fields_section">
		<h2><?echo GetMessage("adm_auth_exist")?></h2>
	</div>
	<ul class="form_fields">
		<li class="form_text">
			<?if($arResult["ALLOW_ANONYMOUS"]=="Y"):?>
				<?echo GetMessage("subscr_auth_note")?>
			<?else:?>
				<?echo GetMessage("adm_must_auth")?>
			<?endif;?>
		</li>
		<li>
			<label><?echo GetMessage("adm_auth_login")?></label>
			<input type="text" name="LOGIN" value="<?echo $arResult["REQUEST"]["LOGIN"]?>" />
			<i class="fa fa-check-circle not-active"></i>
		</li>
		<li>
			<label><?echo GetMessage("adm_auth_pass")?></label>
			<input type="password" name="PASSWORD" value="<?echo $arResult["REQUEST"]["PASSWORD"]?>" />
			<i class="fa fa-check-circle not-active"></i>
		</li>
	</ul>
	<div class="form_btn_wrapper btn_wrapper">
		<input type="submit" class="btn_round btn_color" name="Save" value="<?echo GetMessage("adm_auth_butt")?>" />
	</div>

	<?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
		<input type="hidden" name="RUB_ID[]" value="<?=$itemValue["ID"]?>">
	<?endforeach;?>
	<input type="hidden" name="PostAction" value="<?echo ($arResult["ID"]>0? "Update":"Add")?>" />
	<input type="hidden" name="ID" value="<?echo $arResult["SUBSCRIPTION"]["ID"];?>" />
	<?if($_REQUEST["register"] == "YES"):?>
		<input type="hidden" name="register" value="YES" />
	<?endif;?>
	<?if($_REQUEST["authorize"]=="YES"):?>
		<input type="hidden" name="authorize" value="YES" />
	<?endif;?>
	</form>
<br />
<?if($arResult["ALLOW_REGISTER"]=="Y"):
	?>
	<form action="<?=$arResult["FORM_ACTION"]?>" method="post" class="form form_general">
		<?echo bitrix_sessid_post();?>
		<div class="form_fields_section">
			<h2><?echo GetMessage("adm_reg_new")?></h2>
		</div>
		<ul class="form_fields">
			<li class="form_text">
				<?if($arResult["ALLOW_ANONYMOUS"]=="Y"):?>
					<?echo GetMessage("subscr_auth_note")?>
				<?else:?>
					<?echo GetMessage("adm_must_auth")?>
				<?endif;?>
			</li>
			<li>
				<label><?echo GetMessage("adm_reg_login")?></label>
				<input type="text" name="NEW_LOGIN" value="<?echo $arResult["REQUEST"]["NEW_LOGIN"]?>" />
				<i class="fa fa-check-circle not-active"></i>
			</li>
			<li>
				<label><?echo GetMessage("adm_reg_pass")?></label>
				<input type="password" name="NEW_PASSWORD" value="<?echo $arResult["REQUEST"]["NEW_PASSWORD"]?>" />
				<i class="fa fa-check-circle not-active"></i>
			</li>
			<li>
				<label><?echo GetMessage("adm_reg_pass_conf")?></label>
				<input type="password" name="CONFIRM_PASSWORD" value="<?echo $arResult["REQUEST"]["CONFIRM_PASSWORD"]?>" />
				<i class="fa fa-check-circle not-active"></i>
			</li>
			<li>
				<label><?echo GetMessage("subscr_email")?></label>
				<input type="text" name="EMAIL" value="<?=$arResult["SUBSCRIPTION"]["EMAIL"]!=""?$arResult["SUBSCRIPTION"]["EMAIL"]:$arResult["REQUEST"]["EMAIL"];?>" />
				<i class="fa fa-check-circle not-active"></i>
			</li>
			<?
			if (COption::GetOptionString("main", "captcha_registration", "N") == "Y"):
				$capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();
				?>
				<li>
					<label><?echo GetMessage("subscr_CAPTCHA_REGF_TITLE")?></label>
					<div>
						<input type="hidden" name="captcha_sid" value="<?= htmlspecialcharsbx($capCode) ?>" />
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($capCode) ?>" width="180" height="40" alt="CAPTCHA" />
					</div>
				</li>
				<li>
					<label><?echo GetMessage("subscr_CAPTCHA_REGF_PROMT")?></label>
					<input type="text" name="captcha_word" value="" />
					<i class="fa fa-check-circle not-active"></i>
				</li>
			<?endif;?>
		</ul>
		<div class="form_btn_wrapper btn_wrapper">
			<input type="submit" class="btn_round btn_color" name="Save" value="<?echo GetMessage("adm_reg_butt")?>" />
		</div>
		<?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
			<input type="hidden" name="RUB_ID[]" value="<?=$itemValue["ID"]?>">
		<?endforeach;?>
		<input type="hidden" name="PostAction" value="<?echo ($arResult["ID"]>0? "Update":"Add")?>" />
		<input type="hidden" name="ID" value="<?echo $arResult["SUBSCRIPTION"]["ID"];?>" />
		<?if($_REQUEST["register"] == "YES"):?>
			<input type="hidden" name="register" value="YES" />
		<?endif;?>
		<?if($_REQUEST["authorize"]=="YES"):?>
			<input type="hidden" name="authorize" value="YES" />
		<?endif;?>
	</form>
	<br />
<?endif;?>
