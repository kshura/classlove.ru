<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
global $APPLICATION;
$path = $this->GetFolder();

ob_start();
?>
<div class="content_wrapper content<?=$arResult['FOOTER_TYPE']?>">
<?
$APPLICATION->AddViewContent('content_wrapper_type', ob_get_contents());
ob_end_clean();

?>
<footer class="footer1">
	<div class="row dark">
		<div class="container_16 relative">
			<div class="grid_4 grid_6_sm grid_8_xs">
				<div class="first_footer_line">
					<div class="logo_wrapper">
						<a href="<?=SITE_DIR?>" class="not_underline"><img src="<?=$arResult['LOGO']['SRC']?>"></a>
					</div>
				</div>
				<div class="second_footer_line hidden_xs">
					<div class="address_wrapper">
						<? if ($arResult['COMPANY_ADDRESS']) { ?>
							<?=$arResult['COMPANY_ADDRESS']?>
						<? } ?>
					</div>
					<div class="copyright">
						&copy; <?=date("Y")?> <?=$arResult['COMPANY_NAME']?>
					</div>
				</div>
			</div>
			<div class="grid_12 grid_10_sm grid_8_xs relative">
				<div class="first_footer_line show_lg">
					<? $APPLICATION->IncludeComponent("bitrix:menu", "footer_menu", Array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "",
						"DELAY" => "N",
						"MAX_LEVEL" => "1",
						"MENU_CACHE_GET_VARS" => array(""),
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"ROOT_MENU_TYPE" => "bottom",
						"USE_EXT" => "N"
					), $component);?>
				</div>
				<div class="second_footer_line">
					<div class="subscribe_wrapper grid_10_sm hidden_xs">
						<?$APPLICATION->IncludeComponent(
	"firstbit:subscribe.edit",
	"footer_subscribe",
	array(
		"AJAX_MODE" => "Y",
		"SHOW_HIDDEN" => "N",
		"ALLOW_ANONYMOUS" => "Y",
		"SHOW_AUTH_LINKS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"SET_TITLE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"RUBRIC_ID" => $arParams['SUBSCRIBE_RUBRIC_ID'],
		"COMPONENT_TEMPLATE" => "footer_subscribe",
		"AJAX_OPTION_ADDITIONAL" => "footer"
	),
	false
); ?>
					</div>
					<div class="contacts_wrapper hidden_xs">
						<span class="footer_block_title"><?=GetMessage('FOOTER_CONTACTS')?></span>
						<p>
							<? if ($arResult['SITE_PHONE']) { ?>
								<?=GetMessage('FOOTER_PHONE')?> <a class="phone" href="tel:<?=$arResult['~SITE_PHONE']?>"><?=$arResult['SITE_PHONE']?></a><br/>
							<? } ?>
							<? if ($arResult['SITE_EMAIL']) { ?>
								<?=GetMessage('FOOTER_EMAIL')?> <a class="email" href="mailto:<?=$arResult['SITE_EMAIL']?>"><?=$arResult['SITE_EMAIL']?></a>
							<? } ?>
						</p>
					</div>
					<div class="callback_wrapper hidden_xs">
						<a href="#" class="btn_round btn_color btn_square btn_narrow" onclick="modalFeedback(this);return false;" title="<?=GetMessage('BIT_FEEDBACK_LINK')?>"><?=GetMessage('BIT_FEEDBACK_LINK')?></a>
					</div>
					<div class="social_wrapper">
						<?$APPLICATION->IncludeComponent("bitrix:news.list","footer_socserv",Array(
							"IBLOCK_TYPE" => "promotional",
							"IBLOCK_ID" => $arParams['SOCIAL_SERVICES_IBLOCK_ID'],
							"NEWS_COUNT" => "20",
							"SORT_BY1" => "SORT",
							"SORT_ORDER1" => "ASC",
							"FILTER_NAME" => "",
							"FIELD_CODE" => Array("ID","NAME","CODE"),
							"PROPERTY_CODE" => Array("LINK"),
							"SET_TITLE" => "N",
							"SET_BROWSER_TITLE" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_LAST_MODIFIED" => "N",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "N",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "3600",
							"CACHE_FILTER" => "Y",
							"CACHE_GROUPS" => "Y",
							)
						);?>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</footer>