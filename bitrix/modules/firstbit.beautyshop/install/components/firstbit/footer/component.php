<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
$firstBit = new CFirstbitBeautyshop(SITE_ID);

CJSCore::Init();

if (!is_array($arParams)) {
	$arParams = array();
}

$arResult['FOOTER_TYPE'] = $firstBit->options['FOOTER_TYPE'];
$arResult['COMPANY_NAME'] = $firstBit->options['COMPANY_NAME'];
$arResult['LOGO'] = ($firstBit->options['LOGO_ALT'] ? $firstBit->options['LOGO_ALT'] : $firstBit->options['LOGO']);
if($arResult['LOGO']>0) {
	if($arLogo = CFile::GetFileArray($arResult['LOGO'])) {
		$arResult['LOGO'] = $arLogo;
		if(!file_exists($rootLogo = $_SERVER['DOCUMENT_ROOT'] . $arResult['LOGO']['SRC'])) {
			unset($arResult['LOGO']);
		}
	}
} else {
	unset($arResult['LOGO']);
}

$arResult['SITE_PHONE'] = $firstBit->options['SITE_PHONE_HIDE'] != 'Y' ? $firstBit->options['SITE_PHONE'] : '';
$arResult['~SITE_PHONE'] = preg_replace("/[^0-9]+/", '', $arResult['SITE_PHONE']);
$arResult['SITE_EMAIL'] = $firstBit->options['SITE_EMAIL_HIDE'] != 'Y' ? $firstBit->options['SITE_EMAIL'] : '';
$arResult['COMPANY_ADDRESS'] = $firstBit->options['COMPANY_ADDRESS'] != 'Y' ? $firstBit->options['COMPANY_ADDRESS'] : '';

$this->IncludeComponentTemplate('footer_'.$arResult['FOOTER_TYPE']);