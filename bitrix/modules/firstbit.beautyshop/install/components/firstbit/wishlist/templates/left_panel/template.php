<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
?>
<div class="wishlist_wrapper" id="wishlist_left_panel">
	<a href="<? echo $arParams['PATH_TO_WISHLIST']?>">
		<i class="fa fa-heart custom_color_hover"></i>
		<span class="wishlist_title"><?=GetMessage('WISHLIST_TITLE')?></span>
		<span class="wishlist_item_count"><span data-wishlist-count="">0</span> <?=GetMessage('WISHLIST_MEASURE')?></span>
		<?$frame = $this->createFrame()->begin();?>
		<script type="text/javascript">
			wishlistSetCount();
		</script>
		<? $frame->end(); ?>
	</a>
</div>
