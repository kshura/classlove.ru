<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<div class="wishlist_wrapper <?=$arParams['ADDITIONAL_CLASS']?>" id="wishlist_header">
	<a href="<? echo $arParams['PATH_TO_WISHLIST']?>"> <i class="fa fa-heart"></i> <span data-wishlist-count="">0</span> </a>
	<?$frame = $this->createFrame()->begin();?>
	<script type="text/javascript">
		wishlistSetCount();
	</script>
	<? $frame->end(); ?>
</div>