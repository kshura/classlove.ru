<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

CJSCore::Init();

if($_POST['settings_reset']) {
	unset($_SESSION['SETTINGS']);
}

if(!CModule::IncludeModule("firstbit.beautyshop")) die();
$firstBit = new CFirstbitBeautyshop(SITE_ID, FIRSTBIT_DEMO);

if(!is_array($arParams)) $arParams = array();

if(!is_array($arResult)) $arResult = array();

$arMainPageBlocks = array('SLIDER', 'BANNERS', 'HITS', 'NEWPRODUCT', 'ADVANTAGES', 'NEWS', 'BRANDS', 'COMPANY', 'FEEDBACK', 'STORES');

if($_POST['settings_panel']) {
	$arOptionsValue = array();
	if($_POST['OPTION']['SLIDER_VIEW']) {
		$arOptionsValue['SLIDER_VIEW'] = $_POST['OPTION']['SLIDER_VIEW'];
	}
	if($_POST['OPTION']['BANNERS_VIEW']) {
		$arOptionsValue['BANNERS_VIEW'] = $_POST['OPTION']['BANNERS_VIEW'];
	}
	if($_POST['OPTION']['HEADER_TYPE']) {
		$arOptionsValue['HEADER_TYPE'] = $_POST['OPTION']['HEADER_TYPE'];
	}
	if($_POST['OPTION']['FOOTER_TYPE']) {
		$arOptionsValue['FOOTER_TYPE'] = $_POST['OPTION']['FOOTER_TYPE'];
	}
	if($_POST['OPTION']['MENU_MIDDLE_TYPE']) {
		$arOptionsValue['MENU_MIDDLE_TYPE'] = $_POST['OPTION']['MENU_MIDDLE_TYPE'];
	}
	if($_POST['OPTION']['USE_FLYING_BASKET']) {
		$arOptionsValue['USE_FLYING_BASKET'] = $_POST['OPTION']['USE_FLYING_BASKET'];
	}
	$arOptionsValue['USE_FLYING_MENU'] = ($_POST['OPTION']['USE_FLYING_MENU'] ? "Y" : "N");

	if($_POST['OPTION']['USE_BOTTOM_PANEL']) {
		$arOptionsValue['USE_BOTTOM_PANEL'] = $_POST['OPTION']['USE_BOTTOM_PANEL'];
	}

	if($_POST['OPTION']['BRANDS_VIEW']) {
		$arOptionsValue['BRANDS_VIEW'] = $_POST['OPTION']['BRANDS_VIEW'];
	}

	if($_POST['OPTION']['MAIN_PAGE_ORDER']['HIDE'] && is_array($_POST['OPTION']['MAIN_PAGE_ORDER']['HIDE'])) {
		$arMainPageOrder = 1;
		foreach ($_POST['OPTION']['MAIN_PAGE_ORDER']['HIDE'] as $arBlockName => $arBlockHide) {
			$arOptionsValue['MAIN_PAGE_ORDER_'.$arBlockName] = $arMainPageOrder;
			$arOptionsValue['MAIN_PAGE_ORDER_'.$arBlockName.'_HIDE'] = $arBlockHide;
			$arMainPageOrder++;
		}
		unset($arMainPageOrder);
	}
	if($_POST['OPTION']['COLOR_MAIN'] || $_POST['OPTION']['COLOR_ADDITIONAL']) {
		$arOptionsValue['COLOR_MAIN'] = $_POST['OPTION']['COLOR_MAIN'];
		$arOptionsValue['COLOR_ADDITIONAL'] = $_POST['OPTION']['COLOR_ADDITIONAL'];
		$lessFile = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/templates/' . SITE_TEMPLATE_ID . '/styles.less';
		if($firstBit->demo) {
			$css_hash = md5($arOptionsValue['COLOR_MAIN'] . $arOptionsValue['COLOR_ADDITIONAL']);
			$cssFile = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/templates/' . SITE_TEMPLATE_ID . '/styles_' . $css_hash . '.css';
		} else {
			$cssFile = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/templates/' . SITE_TEMPLATE_ID . '/styles.css';
		}
		$firstBit->generateCSS($arOptionsValue['COLOR_MAIN'], $arOptionsValue['COLOR_ADDITIONAL'], $lessFile, $cssFile);
	}
	$firstBit->setOptions($arOptionsValue);
}

if($firstBit->demo) {
	$cssFile = SITE_TEMPLATE_PATH . '/styles_'.md5($firstBit->options['COLOR_MAIN'] . $firstBit->options['COLOR_ADDITIONAL']).'.css';
	if(file_exists($_SERVER['DOCUMENT_ROOT'].$cssFile)) {
		$APPLICATION->AddHeadString('<link type="text/css" href="'.$cssFile.'" rel="stylesheet" />');
	}
}

$arResult['SLIDER_VIEW'] = $firstBit->options['SLIDER_VIEW'];
$arResult['BANNERS_VIEW'] = $firstBit->options['BANNERS_VIEW'];
$arResult['HEADER_TYPE'] = $firstBit->options['HEADER_TYPE'];
$arResult['FOOTER_TYPE'] = $firstBit->options['FOOTER_TYPE'];
$arResult['MENU_MIDDLE_TYPE'] = $firstBit->options['MENU_MIDDLE_TYPE'];
$arResult['USE_FLYING_MENU'] = $firstBit->options['USE_FLYING_MENU'];
$arResult['USE_FLYING_BASKET'] = $firstBit->options['USE_FLYING_BASKET'];
$arResult['USE_BOTTOM_PANEL'] = $firstBit->options['USE_BOTTOM_PANEL'];
$arResult['BRANDS_VIEW'] = $firstBit->options['BRANDS_VIEW'];
$arResult['COLOR_MAIN'] = $firstBit->options['COLOR_MAIN'];
$arResult['COLOR_ADDITIONAL'] = $firstBit->options['COLOR_ADDITIONAL'];

$arMainPageOrder = array();
$arMainPageShow = array();
foreach($arMainPageBlocks as $arBlock) {
	$arMainPageOrder[$arBlock] = $firstBit->options['MAIN_PAGE_ORDER_'.$arBlock];
	$arMainPageShow[$arBlock] = $firstBit->options['MAIN_PAGE_ORDER_'.$arBlock.'_HIDE'];
}
natsort($arMainPageOrder);
$arMainPageOrderEmpty = array();
foreach($arMainPageOrder as $arBlock=>$arOrder) {
	if(!$arOrder) {
		$arMainPageOrderEmpty[$arBlock] = '';
		unset($arMainPageOrder[$arBlock]);
	}
}
$arResult['MAIN_PAGE_ORDER'] = $arMainPageOrder + $arMainPageOrderEmpty;
$arResult['MAIN_PAGE_HIDE'] = $arMainPageShow;

$this->IncludeComponentTemplate();
