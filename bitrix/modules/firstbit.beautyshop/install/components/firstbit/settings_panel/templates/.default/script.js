$('#sortable_blocks_settings').sortable({
	placeholder: "ui-state-highlight"
});
$('#sortable_blocks_settings').disableSelection();

$('.slider_settings a.filters_block').click(function() {
	$(this).siblings().removeClass('active');
	$(this).addClass('active');
	$('#slider_view').val($(this).attr("data-firstbit-option"));
});

$('.banners_settings select').change(function () {
	var bannersImgType = $(this).val();
	$('.banners_img').removeClass().addClass('banners_img banners_variant' + bannersImgType);
});

$('#sortable_blocks_settings').ready(function () {
	for (var i = 0; i < $('#sortable_blocks_settings').children('li').length; i++) {
		$('#sortable_blocks_settings li').children('.sort_num').eq(i).text(i + 1);
	}
});
$('#sortable_blocks_settings input[type=checkbox]').change(function() {
	if($(this).prop( "checked") == true) {
		$(this).parent().find('input[type=hidden]').val('N');
	}
	if($(this).prop( "checked") == false) {
		$(this).parent().find('input[type=hidden]').val('Y');
	}
});

$('#colorpicker_main').minicolors({
	format: 'rgb',
	position: 'bottom left',
	change: function(hex, opacity) {
		var log;
		try {
			log = hex ? hex : 'transparent';
			if( opacity ) log += ', ' + opacity;
			$('#colorpicker_sample').css('background-color',log);
		} catch(e) {}
	},
});
$('#colorpicker_additional').minicolors({
	format: 'rgb',
	position: 'bottom left',
	change: function(hex, opacity) {
		var log;
		try {
			log = hex ? hex : 'transparent';
			if( opacity ) log += ', ' + opacity;
			$('#colorpicker_sample').css('color',log);
		} catch(e) {}
	},
});
$('.color_settings a.color').click(function () {
	$(this).parent('.settings_item').find('a.color').removeClass('select');
	$(this).addClass('select');
	$('#colorpicker_main').minicolors('value', $(this).find('div.color_block').attr('data-color-rgb'));
})
$('.other_color_settings a.color').click(function () {
	$(this).parent('.settings_item').find('a.color').removeClass('select');
	$(this).addClass('select');
})

$('#settings_header_type').change(function() {
	if($('#settings_header_type').val() == 'header4' || $('#settings_header_type').val() == 'header6') {
		$('#settings_flying_menu').hide();
	} else {
		$('#settings_flying_menu').show();
	}
});


$('.settings_wrapper select').select2({
	minimumResultsForSearch: Infinity
});