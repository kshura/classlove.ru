<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<div class="enter_wrapper" id="sale-basket-basket-line-container">
	<? $frame = $this->createFrame()->begin(); ?>
		<? if ($arResult['AUTH']) { ?>
			<span class="login">
				<a href="<?=$arParams['PATH_TO_PERSONAL']?>"><?=$arResult['USER_NAME']?></a>
			</span>
			<i class="fa vertical_split"></i>
			<span class="registration">
				<a href="<?=$arParams['PATH_TO_LOGOUT']?>"><?=GetMessage('BIT_LOGOUT')?></a>
			</span>
		<? } else {?>
			<span class="login">
				<i class="fa fa-lock"></i>
				<a href="<?=$arParams['PATH_TO_LOGIN']?>" data-auth="" title="<?=GetMessage('BIT_LOGIN_FULL')?>"><?=GetMessage('BIT_LOGIN')?></a>
			</span>
			<i class="fa vertical_split"></i>
			<span class="registration">
				<a href="<?=$arParams['PATH_TO_REGISTER']?>"><?=GetMessage('BIT_REGISTER')?></a>
			</span>
		<? }?>
	<? $frame->beginStub(); ?>
	...
	<? $frame->end(); ?>
</div>
