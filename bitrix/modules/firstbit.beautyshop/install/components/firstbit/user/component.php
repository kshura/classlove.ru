<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

CModule::IncludeModule('main');
global $USER;

$arParams['USER'] = $USER->GetID();

if ($arResult['AUTH'] = $USER->IsAuthorized()) {
	$arResult['USER_NAME'] = (($arResult['USER_NAME'] = $USER->GetFullName()) ? $arResult['USER_NAME'] : $USER->GetEmail());
}

$this->IncludeComponentTemplate();
