<section class="row feedback">
	<div class="feedback_inner">
		<div class="container_16">
			<div class="grid_8 grid_16_sm">
				<?
				$APPLICATION->IncludeComponent(
					"firstbit:iblock.element.add.form",
					"main_page_feedback",
					array(
						"SEF_MODE" => "N",
						"IBLOCK_TYPE" => "firstbit_beautyshop_feedback",
						"IBLOCK_ID" => $arParams['FEEDBACK_IBLOCK_ID'],
						"PROPERTY_CODES" => array("NAME","USER_ID","USER_NAME","USER_EMAIL","MESSAGE","USER_IP"),
						"PROPERTY_CODES_REQUIRED" => array("NAME","USER_NAME","USER_EMAIL","MESSAGE"),
						"GROUPS" => array(
							0 => "2",
						),
						"STATUS_NEW" => "N",
						"STATUS" => "ANY",
						"LIST_URL" => "",
						"ELEMENT_ASSOC" => "PROPERTY_ID",
						"ELEMENT_ASSOC_PROPERTY" => "",
						"MAX_USER_ENTRIES" => "100000",
						"MAX_LEVELS" => "100000",
						"LEVEL_LAST" => "Y",
						"USE_CAPTCHA" => "N",
						"USER_MESSAGE_EDIT" => "",
						"USER_MESSAGE_ADD" => "",
						"DEFAULT_INPUT_SIZE" => "30",
						"RESIZE_IMAGES" => "Y",
						"MAX_FILE_SIZE" => "0",
						"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
						"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
						"CUSTOM_TITLE_NAME" => "",
						"CUSTOM_TITLE_TAGS" => "",
						"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
						"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
						"CUSTOM_TITLE_IBLOCK_SECTION" => "",
						"CUSTOM_TITLE_PREVIEW_TEXT" => "",
						"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
						"CUSTOM_TITLE_DETAIL_TEXT" => "",
						"CUSTOM_TITLE_DETAIL_PICTURE" => "",
						"SEF_FOLDER" => "/",
						"AJAX_MODE" => "Y",
						"AJAX_OPTION_SHADOW" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"SET_TITLE" => "N",
						"COMPONENT_TEMPLATE" => "main_page_feedback"
					),
					false
				);?>
			</div>
			<div class="grid_8 grid_16_sm">
				<?$APPLICATION->IncludeComponent(
					"firstbit:subscribe.edit",
					"main_page_subscribe",
					array(
						"AJAX_MODE" => "Y",
						"SHOW_HIDDEN" => "N",
						"ALLOW_ANONYMOUS" => "Y",
						"SHOW_AUTH_LINKS" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "3600",
						"SET_TITLE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"RUBRIC_ID" => $arParams['SUBSCRIBE_RUBRIC_ID'],
						"COMPONENT_TEMPLATE" => "main_page_subscribe",
						"AJAX_OPTION_ADDITIONAL" => "main_page"
					),
					false
				); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</section>