<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->SetFrameMode(true);
foreach ($arResult['BLOCKS'] as $arBlock=>$arBlockSort) {
	include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/block_".strtolower($arBlock).".php");
}
?>
