<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if(!CModule::IncludeModule("firstbit.beautyshop") || !CModule::IncludeModule('iblock'))
	die();
$firstBit = new CFirstbitBeautyshop(SITE_ID);

CJSCore::Init();

if (!is_array($arParams)) {
	$arParams = array();
}

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arResult['SITE_PHONE'] = $firstBit->options['SITE_PHONE_HIDE'] != 'Y' ? $firstBit->options['SITE_PHONE'] : '';
$arResult['~SITE_PHONE'] = preg_replace("/[^0-9]+/", '', $arResult['SITE_PHONE']);
$arResult['SITE_EMAIL'] = $firstBit->options['SITE_EMAIL_HIDE'] != 'Y' ? $firstBit->options['SITE_EMAIL'] : '';

$obCache = new CPHPCache;
if ($obCache->InitCache($arParams['CACHE_TIME'], md5(serialize(array($arParams, $arResult))), '/header/')) {
	$arCache = $obCache->GetVars();
	$arResult = $arCache['arResult'];
}
elseif ($obCache->StartDataCache()) {

	$arResult['HEADER_TYPE'] = $firstBit->options['HEADER_TYPE'];
	$arResult['MENU_MIDDLE_TYPE'] = $firstBit->options['MENU_MIDDLE_TYPE'];
	$arResult['TOP_MENU_ITEMS'] = $firstBit->options['TOP_MENU_ITEMS'];
	$arResult['MIDDLE_MENU_ITEMS'] = $firstBit->options['MIDDLE_MENU_ITEMS'];
	$arResult['MIDDLE_MENU_LIMIT'] = $firstBit->options['MIDDLE_MENU_LIMIT'];
	$arResult['USE_FLYING_BASKET'] = $firstBit->options['USE_FLYING_BASKET'];
	$arResult['USE_FLYING_MENU'] = $firstBit->options['USE_FLYING_MENU'];
	$arResult['PATH_TO_BRANDS'] = $arParams['PATH_TO_BRANDS'];

	$arResult['LOGO'] = $firstBit->options['LOGO'];
	if($arResult['LOGO'] > 0) {
		if($arLogo = CFile::GetFileArray($arResult['LOGO'])) {
			$arResult['LOGO'] = $arLogo;
			if(!file_exists($rootLogo = $_SERVER['DOCUMENT_ROOT'] . $arResult['LOGO']['SRC'])) {
				unset($arResult['LOGO']);
			}
		}
	} else {
		unset($arResult['LOGO']);
	}

	$obSection = new CIBlockSection();
	$obElement = new CIBlockElement();
	$obFile = new CFile();
	$arFilter = array(
		'ACTIVE' => 'Y',
		'GLOBAL_ACTIVE' => 'Y',
		'IBLOCK_ID' => $arParams['CATALOG_IBLOCK_ID'],
	);

	$dbSection = $obSection->GetList(
		Array(
			"LEFT_MARGIN" => "ASC",
			'SORT' => 'ASC'
		),
		$arFilter,
		false
	);

	$arResult['CATALOG_SECTION'] = array(
		'DEPTH_LEVEL' => array(),
		'BRANDS' => array(),
	);

	while ($arSection = $dbSection->GetNext()) {
		if ($arResult['MENU_MIDDLE_TYPE'] == '3') {
			// get image data for menu type 3
			if (isset($arSection['PICTURE'])) {
				$arSection['PICTURE'] = CFile::GetFileArray($arSection['PICTURE']);
			}
		}
		// get general sections list
		$arResult['CATALOG_SECTION']['SECTIONS'][$arSection['ID']] = $arSection;
	}

	// link section url to section id
	if($arResult['MENU_MIDDLE_TYPE'] == '2' || $arResult['MENU_MIDDLE_TYPE'] == '3') {
		foreach ($arResult['CATALOG_SECTION']['SECTIONS'] as $arSection) {
			$arResult['CATALOG_SECTION']['URL_TO_ID'][$arSection['SECTION_PAGE_URL']] = $arSection['ID'];
		}
	}

	if($arResult['MENU_MIDDLE_TYPE'] == '2') {

		// get brands grouped by sections from elements
		$dbAllItems = $obElement->GetList(
			array(
				'PROPERTY_BRAND_REF.NAME' => 'ASC',
				'IBLOCK_SECTION_ID' => 'ASC',
				'PROPERTY_BRAND_REF.CODE' => 'ASC',
			),
			$arFilter = array(
				'ACTIVE' => 'Y',
				'GLOBAL_ACTIVE' => 'Y',
				'IBLOCK_ID' => $arParams['CATALOG_IBLOCK_ID'],
				'!PROPERTY_BRAND_REF' => false,
				'INCLUDE_SUBSECTIONS' => 'Y',
			),
			array(
				"PROPERTY_BRAND_REF",
				"IBLOCK_SECTION_ID",
			),
			false
		);

		// append brands from last sections
		while ($arBrand = $dbAllItems->GetNext()) {
			if(!is_array($arResult['CATALOG_SECTION']['APPEND'][$arBrand['IBLOCK_SECTION_ID']]) || !in_array($arBrand['PROPERTY_BRAND_REF_VALUE'], $arResult['CATALOG_SECTION']['APPEND'][$arBrand['IBLOCK_SECTION_ID']]) && array_key_exists($arBrand['IBLOCK_SECTION_ID'], $arResult['CATALOG_SECTION']['SECTIONS'])) {
				$arResult['CATALOG_SECTION']['APPEND'][$arBrand['IBLOCK_SECTION_ID']][] = array('ITEM_ID' => $arBrand['PROPERTY_BRAND_REF_VALUE'], 'NAME' => $arBrand['PROPERTY_BRAND_REF_NAME'], 'DETAIL_PAGE_URL' => $arParams['PATH_TO_BRANDS'].$arBrand['PROPERTY_BRAND_REF_CODE'].'/', 'CNT' => $arBrand['CNT']);
			}
		}

		// create array with sections grouped by depth level
		$arDepthLevel = array();
		foreach ($arResult['CATALOG_SECTION']['SECTIONS'] as $arSection) {
			$arDepthLevel[$arSection['DEPTH_LEVEL']][$arSection['ID']] = array(
				'ID' => $arSection['ID'],
				'IBLOCK_SECTION_ID' => $arSection['IBLOCK_SECTION_ID'],
				'DEPTH_LEVEL' => $arSection['DEPTH_LEVEL'],
			);
		}
		$arResult['CATALOG_SECTION']['DEPTH_LEVEL'] = $arDepthLevel;
		krsort($arDepthLevel);

		// append brands to sections (except last)
		foreach($arDepthLevel as $depthLevel => $arSections) {
			foreach($arSections as $arSection) {
				if(!intval($arSection['IBLOCK_SECTION_ID'])) $arSection['IBLOCK_SECTION_ID'] = 0;
				if(array_key_exists($arSection['ID'], $arResult['CATALOG_SECTION']['APPEND'])) {
					foreach($arResult['CATALOG_SECTION']['APPEND'][$arSection['ID']] as $arBrand) {
						$arResult['CATALOG_SECTION']['APPEND'][$arSection['IBLOCK_SECTION_ID']][] = $arBrand;
					}
				}
			}
		}

		// delete duplicated brands and count general quantity
		foreach($arResult['CATALOG_SECTION']['APPEND'] as $arSectionIndex => $arSection) {
			$sectionBrands = array();
			foreach ($arSection as $arBrandIndex => $arBrand) {
				if(array_key_exists($arBrand['ITEM_ID'], $sectionBrands)) {
					$arResult['CATALOG_SECTION']['APPEND'][$arSectionIndex][$sectionBrands[$arBrand['ITEM_ID']]]['CNT'] = $arResult['CATALOG_SECTION']['APPEND'][$arSectionIndex][$sectionBrands[$arBrand['ITEM_ID']]]['CNT'] + $arBrand['CNT'];
					unset($arResult['CATALOG_SECTION']['APPEND'][$arSectionIndex][$arBrandIndex]);
				} else {
					$sectionBrands[$arBrand['ITEM_ID']] = $arBrandIndex;
				}
			}
		}

		// sorting sections brands by quantity
		foreach ($arResult['CATALOG_SECTION']['APPEND'] as $sectionID => $sectionAppend) {
			$brand_cnt = array();
			$brand_name = array();
			foreach ($sectionAppend as $key => $field) {
				$brand_cnt[$key] = $field['CNT'];
				$brand_name[$key] = strtolower($field['NAME']);
			}
			array_multisort($brand_cnt, SORT_DESC, $brand_name, SORT_ASC, SORT_NATURAL, $sectionAppend);
			$arResult['CATALOG_SECTION']['APPEND'][$sectionID] = $sectionAppend;
		}
	}

	$obCache->EndDataCache(array(
		'arResult' => $arResult,
	));
}
$this->IncludeComponentTemplate('header_'.$arResult['HEADER_TYPE']);
