<?
$MESS ['P_ID'] = "Код";
$MESS ['P_NAME'] = "Название";
$MESS ['P_PERSON_TYPE'] = "Тип плательщика";
$MESS ['P_DATE_UPDATE'] = "Дата обновления";
$MESS ['SALE_ACTION'] = "Действия";
$MESS ['SALE_DETAIL'] = "Изменить";
$MESS ['SALE_DELETE'] = "Удалить";
$MESS ['SALE_DETAIL_DESCR'] = "Изменить профиль";
$MESS ['SALE_DELETE_DESCR'] = "Удалить профиль";
$MESS ['SALE_ADD'] = "Добавить профиль";
$MESS["STPPL_DELETE_CONFIRM"] = "Вы уверены, что хотите удалить этот профиль?";
$MESS["PROFILE_LIST_INFO"] = "Здесь находится список всех ваших профилей покупателя.<br>Вы можете добавить новый профиль здесь или при оформлении заказа."
?>
