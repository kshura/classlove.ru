<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<p><?=GetMessage('PROFILE_LIST_INFO')?></p>
	<ul class="profile_list">
		<?
		foreach ($arResult['PROFILES'] as $arProfile) {
			?>
			<li>
				<div class="profile_list_title">
					<a href="<?=$arProfile["URL_TO_EDIT"]?>"><strong><?=$arProfile['NAME']?></strong> (<?=$arProfile['PERSON_TYPE']['NAME']?>)</a>
					<a title="<?= GetMessage("SALE_DELETE_DESCR") ?>" href="javascript:if(confirm('<?= GetMessage("STPPL_DELETE_CONFIRM") ?>')) window.location='<?=$arProfile["URL_TO_DELETE"]?>'" class="edit-bar-item"><i class="fa fa-times"></i></a>
					<a title="<?= GetMessage("SALE_DETAIL_DESCR") ?>" href="<?=$arProfile["URL_TO_EDIT"]?>" class="edit-bar-item hidden_xs"><i class="fa fa-edit"></i></a>
				</div>
				<?
				foreach ($arProfile['PROPERTIES'] as $arProperty) {
					?>
					<div class="profile_list_props">
						<span><?=$arProperty['NAME']?>:</span>
						<span><?=$arProperty['VALUE']?></span>
					</div>
					<?
				}
				?>
			</li>
			<?
		}
		?>
	</ul>
	<div class="btn_wrapper">
		<a href="<?=$arResult['URL_TO_ADD']?>" class="btn_round btn_color"><?=GetMessage('SALE_ADD')?></a>
	</div>

<?if(strlen($arResult["NAV_STRING"]) > 0):?>
	<p><?=$arResult["NAV_STRING"]?></p>
<?endif?>