<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");
$APPLICATION->SetPageProperty('title', "Страница не найдена");
$APPLICATION->SetPageProperty("showLeftMenu", "N");

?>
	<div class="row page404 page grey">
		<div class="container_16">
			<div class="grid_10 grid_9_sm grid_16_xs margin_xs_10">
				<div class="page404_inner">
					<p>К сожалению, страница. которую Вы запросили, не была найдена (ошибка 404). Вы можете перейти на главную страницу или воспользоваться каталогом товаров. Если эта ошибку будет повторяться, обратитесь, пожалуйста, в службу поддержки.</p>
					<a href="/" class="link_more">Главная страница<i class="fa fa-angle-right"></i></a><br/>
					<a href="/catalog/" class="link_more">Каталог товаров<i class="fa fa-angle-right"></i></a>
				</div>
			</div>
			<div class="grid_6 grid_7_sm hidden_xs">
				<div class="page404_inner">
					<div class="page404_infographics">
						<img src="/img/404.png">
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<section class="row feedback">
		<div class="feedback_inner">
			<div class="container_16">
				<div class="grid_8 grid_16_sm">
					<?
					$APPLICATION->IncludeComponent(
						"firstbit:iblock.element.add.form",
						"main_page_feedback",
						array(
							"SEF_MODE" => "N",
							"IBLOCK_TYPE" => "firstbit_beautyshop_feedback",
							"IBLOCK_ID" => "13",
							"PROPERTY_CODES" => array("NAME","USER_ID","USER_NAME","FEEDBACK_TYPE","USER_EMAIL","MESSAGE","USER_IP"),
							"PROPERTY_CODES_REQUIRED" => array("NAME","USER_NAME","USER_EMAIL","MESSAGE"),
							"GROUPS" => array(
								0 => "2",
							),
							"STATUS_NEW" => "N",
							"STATUS" => "ANY",
							"LIST_URL" => "",
							"ELEMENT_ASSOC" => "PROPERTY_ID",
							"ELEMENT_ASSOC_PROPERTY" => "",
							"MAX_USER_ENTRIES" => "100000",
							"MAX_LEVELS" => "100000",
							"LEVEL_LAST" => "Y",
							"USE_CAPTCHA" => "N",
							"USER_MESSAGE_EDIT" => "",
							"USER_MESSAGE_ADD" => "",
							"DEFAULT_INPUT_SIZE" => "30",
							"RESIZE_IMAGES" => "Y",
							"MAX_FILE_SIZE" => "0",
							"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
							"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
							"CUSTOM_TITLE_NAME" => "",
							"CUSTOM_TITLE_TAGS" => "",
							"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
							"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
							"CUSTOM_TITLE_IBLOCK_SECTION" => "",
							"CUSTOM_TITLE_PREVIEW_TEXT" => "",
							"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
							"CUSTOM_TITLE_DETAIL_TEXT" => "",
							"CUSTOM_TITLE_DETAIL_PICTURE" => "",
							"SEF_FOLDER" => "/",
							"AJAX_MODE" => "Y",
							"AJAX_OPTION_SHADOW" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"SET_TITLE" => "N",
							"COMPONENT_TEMPLATE" => "main_page_feedback"
						),
						false
					);?>
				</div>
				<div class="grid_8 grid_16_sm">
					<?$APPLICATION->IncludeComponent(
						"firstbit:subscribe.edit",
						"main_page_subscribe",
						array(
							"AJAX_MODE" => "Y",
							"SHOW_HIDDEN" => "N",
							"ALLOW_ANONYMOUS" => "Y",
							"SHOW_AUTH_LINKS" => "N",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "3600",
							"SET_TITLE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"RUBRIC_ID" => "5",
							"COMPONENT_TEMPLATE" => "main_page_subscribe",
							"AJAX_OPTION_ADDITIONAL" => "main_page"
						),
						false
					); ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>