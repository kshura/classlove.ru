<?
define('BIT_MODULE_ID', 'firstbit.mag');
?>

<!-- боковая панель с настройками начало -->
<div class="settings_wrapper">
	<div class="settings_label"><i class="fa fa-cog"></i></div>
	<form method="POST" name="settings" action="">
		<div class="settings_body">

			<div class="column1_2">
				<div class="column1">
					<!-- выбор тематики -->
					<!--
					<div class="settings_inner">
						<div class="settings_title"><span>Тематика</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner active">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="themes" value="">
								<option value="cloth">Одежда</option>
								<option value="elecro">Электроника</option>
								<option value="stroy">Строительство</option>
							</select>
						</div>
					</div> -->

					<!-- выбор цветовой схемы -->
					<div class="settings_inner color_settings">
						<div class="settings_title"><span>Цветовая схема</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<input type="hidden" name="color_themes" value="">
							<a href="#" class="color">
								<div class="color_block" style="background: #e3212a;" title="красный"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: #ee7518;" title="оранжевый"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: #fdc41d;" title="жёлтый"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: #59af30;" title="зелёный"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: #3ea2ee;" title="голубой"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: #2969a9;" title="синий"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: #65262f;" title="коричневый"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: #872163;" title="фиолетовый"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: #bb0f5b;" title="сиреневый"></div>
							</a>
						</div>
					</div>

					<!-- выбор цвета ручную -->
					<div class="settings_inner">
						<div class="settings_title"><span>Выбор цвета вручную</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<input type="hidden" name="manually_color_themes" value="">
							<div class="colorPicker">
								<div id="colorpickerHolder"></div>
								<input type="text" class="colorSelect" value="Выбранный цвет">
								<a href="#" class="btn-setcolor">Выбрать</a>
							</div>
						</div>
					</div>

					<!-- дополнительный цвет -->
					<div class="settings_inner other_color_settings">
						<div class="settings_title"><span>Дополнительный цвет</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<input type="hidden" name="additional_color_themes" value="">
							<a href="#" class="color select">
								<div class="color_block" style="background: #49515b;" title="тёмно серый"></div>
							</a>
							<a href="#" class="color">
								<div class="color_block" style="background: #d6d6d6;" title="светло серый"></div>
							</a>
						</div>
					</div>
				</div>
				<div class="column2">
					<!-- выбор шапки -->
					<div class="settings_inner">
						<div class="settings_title"><span>Шапка</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="header" class="select_nofilter">
								<option value="place1">Расположение №1</option>
								<option value="place2">Расположение №2</option>
								<option value="place3">Расположение №3</option>
							</select>
							<div class="checkbox_wrapper">
								<input type="checkbox" name="flying_menu_on" id="flying_menu_on">
								<label for="flying_menu_on">Плавающее меню</label>
							</div>

						</div>
					</div>

					<!-- выбор типа выпадающего меню -->
					<div class="settings_inner">
						<div class="settings_title"><span>Тип выпадающего меню</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="menu_types" class="select_nofilter">
								<option value="varian1">Вариант №1</option>
								<option value="varian2">Вариант №2</option>
								<option value="varian3">Вариант №3</option>
							</select>
						</div>
					</div>

					<!-- выбор корзины -->
					<div class="settings_inner hidden">
						<div class="settings_title"><span>Корзина</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="header_types" class="select_nofilter">
								<option value="varian1">В шапке</option>
								<option value="varian2">Плавющая</option>
							</select>
						</div>
					</div>

					<!-- выбор нижней плашки -->
					<div class="settings_inner hidden">
						<div class="settings_title"><span>Нижняя</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="bottom_panel_types" class="select_nofilter">
								<option value="varian1">Широкая</option>
								<option value="varian2">Узкая</option>
								<option value="varian3">Нет</option>
							</select>
						</div>
					</div>

					<!-- выбор шрифтов -->
					<div class="settings_inner hidden">
						<div class="settings_title"><span>Шрифты</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="fonts_types" class="select_nofilter">
								<option value="varian1">Roboto</option>
								<option value="varian2">Open Sans</option>
								<option value="varian3">Arial</option>
							</select>
						</div>
					</div>

					<!-- выбор фильтров -->
					<div class="settings_inner filters_settings hidden">
						<div class="settings_title"><span>Фильтры</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<input type="hidden" name="filters_themes" value="">
							<a href="#" class="filters_block active">
								<div class="select_block" title="фильтры слева">
									<div class="select_block_inner">
										<div class="blue_left"></div>
									</div>
								</div>
							</a>
							<a href="#" class="filters_block">
								<div class="select_block" title="фильтры справа">
									<div class="select_block_inner">
										<div class="blue_top"></div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<!-- выбор SKU -->
					<div class="settings_inner hidden">
						<div class="settings_title"><span>SKU</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="sku_types" class="select_nofilter">
								<option value="varian1">С закруглением</option>
								<option value="varian2">Без закругелния</option>
							</select>
						</div>
					</div>

					<!-- выбор базовой валюты -->
					<div class="settings_inner hidden">
						<div class="settings_title"><span>Базовая валюта</span>
							<div class="help_wrapper">
								<i class="fa fa-question-circle"></i>
								<div class="help_inner">
									<div class="help_angle"></div>
									Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
									пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
								</div>
							</div>
						</div>
						<div class="settings_item">
							<select name="currency_types" class="select_nofilter">
								<option value="varian1">Рубли</option>
								<option value="varian2">Доллары</option>
								<option value="varian3">Евро</option>
							</select>
						</div>
					</div>
				</div>
			</div>

			<div class="column3">
				<!-- выбор расположения баннеров -->
				<div class="settings_inner banners_settings">
					<div class="settings_title"><span>Расположение баннеров</span>
						<div class="help_wrapper">
							<i class="fa fa-question-circle"></i>
							<div class="help_inner">
								<div class="help_angle"></div>
								Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
								пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
							</div>
						</div>
					</div>
					<div class="settings_item">
						<?
						$arResult['SLIDER_VIEW'] = COption::GetOptionString(BIT_MODULE_ID, 'SLIDER_VIEW');
						?>
						<input type="hidden" name="SLIDER_VIEW" value="<?=$arResult['SLIDER_VIEW']?>">
						<a href="#" class="filters_block <?=($arResult['SLIDER_VIEW']=='wide' ? 'active' : '')?>" data-firstbit-option="wide">
							<div class="select_block" title="Сверху широкий">
								<div class="select_block_inner">
									<div class="blue_top"></div>
								</div>
							</div>
						</a>
						<a href="#" class="filters_block <?=($arResult['SLIDER_VIEW']=='short' ? 'active' : '')?>" data-firstbit-option="short">
							<div class="select_block" title="Сверху узкий">
								<div class="select_block_inner">
									<div class="blue_top_short"></div>
								</div>
							</div>
						</a>
						<a href="#" class="filters_block <?=($arResult['SLIDER_VIEW']=='four_banners' ? 'active' : '')?>" data-firstbit-option="four_banners">
							<div class="select_block" title="Слайдер и баннеры">
								<div class="select_block_inner">
									<div class="blue_top_short">
										<div class="blue_top1"></div>
										<div class="blue_top1">
											<div class="blue_top1"></div>
											<div class="blue_top1"></div>
										</div>
									</div>
								</div>
							</div>
						</a>
						<a href="#" class="filters_block <?=($arResult['SLIDER_VIEW']=='two_banners' ? 'active' : '')?>" data-firstbit-option="two_banners">
							<div class="select_block" title="Баннеры">
								<div class="select_block_inner">
									<div class="blue_top_short">
										<div class="blue_top2"></div>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="checkbox_wrapper">
						<input type="checkbox" name="banners_header" id="banners_header" checked>
						<label for="banners_header">Включить баннеры</label>
					</div>
				</div>
				<!-- выбор расположения акций -->
				<div class="settings_inner actions_settings">
					<div class="settings_title"><span>Акции</span>
						<div class="help_wrapper">
							<i class="fa fa-question-circle"></i>
							<div class="help_inner">
								<div class="help_angle"></div>
								Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
								пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
							</div>
						</div>
					</div>
					<div class="settings_item">
						<div class="settings_item">
							<select name="actions_types" class="select_nofilter">
								<option value="variant1">2 большие 2 маленькие</option>
								<option value="variant2">3 большие</option>
								<option value="variant3">1 большая 4 маленькие</option>
								<option value="variant4">2 большие 2 маленькие</option>
								<option value="variant5">2 маленькие 1 большая</option>
								<option value="variant6">2 большие 3 маленькие</option>
							</select>
							<div class="selected_actions_types">
								<div class="select_actions_inner">
									<div class="actions_img actions_variant1"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- выбор порядка элементов -->
				<div class="settings_inner">
					<div class="settings_title"><span>Элементы страницы</span>
						<div class="help_wrapper">
							<i class="fa fa-question-circle"></i>
							<div class="help_inner">
								<div class="help_angle"></div>
								Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
								пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
							</div>
						</div>
					</div>
					<div class="settings_item">
						<ul id="sortable_blocks_settings">
							<li class="ui-state-default">
								<span class="sort_num">1</span>
								<input type="checkbox" name="offers" id="offers_themes1" checked>
								<label for="offers_themes1">Предложения</label>
								<i class="fa fa-sort"></i>
							</li>
							<li class="ui-state-default">
								<span class="sort_num">2</span>
								<input type="checkbox" name="brands" id="brands_themes2" checked>
								<label for="brands_themes2">Бренды</label>
								<select name="brands_types" class="select_nofilter">
									<option value="varian1">Текстом</option>
									<option value="varian2">Карусель 1</option>
									<option value="varian3">Карусель 2</option>
									<option value="varian4">Карусель 3</option>
								</select>
								<i class="fa fa-sort"></i>
							</li>
							<li class="ui-state-default">
								<span class="sort_num">3</span>
								<input type="checkbox" name="services_themes" id="services_themes3" checked>
								<label for="services_themes3">Услуги</label>
								<i class="fa fa-sort"></i>
							</li>
							<li class="ui-state-default">
								<span class="sort_num">4</span>
								<input type="checkbox" name="news_themes" id="news_themes4" checked>
								<label for="news_themes4">Новости</label>
								<i class="fa fa-sort"></i>
							</li>
							<li class="ui-state-default">
								<span class="sort_num">5</span>
								<input type="checkbox" name="about_themes" id="about_themes5" checked>
								<label for="about_themes5">О компании</label>
								<i class="fa fa-sort"></i>
							</li>
							<li class="ui-state-default">
								<span class="sort_num">6</span>
								<input type="checkbox" name="shops_themes" id="shops_themes6" checked>
								<label for="shops_themes6">Магазины</label>
								<i class="fa fa-sort"></i>
							</li>
						</ul>
					</div>
				</div>

				<script>
					$('#sortable_blocks_settings li').mouseenter(function () {
						for (var i = 0; i < $('#sortable_blocks_settings').children('li').length; i++) {
							$('#sortable_blocks_settings li').children('.sort_num').eq(i).text(i + 1);
						}
					});
				</script>

				<!-- выбор подвала -->
				<div class="settings_inner footer_settings hidden">
					<div class="settings_title"><span>Подвал</span>
						<div class="help_wrapper">
							<i class="fa fa-question-circle"></i>
							<div class="help_inner">
								<div class="help_angle"></div>
								Тут какая то подсказка для пользователей. Много текста, которые описывают что это за
								пункт. Подсказка на полупрозрачном фоне. Всплывающая. Со сноской слева.
							</div>
						</div>
					</div>
					<div class="settings_item">
						<div class="settings_item">
							<select name="footer_types" class="select_nofilter">
								<option value="varian1">Стандартный подвал</option>
								<option value="varian2">Не стандартный подвал</option>
							</select>
							<div class="checkbox_wrapper">
								<input type="checkbox" name="subscribe_on" id="subscribe_on" checked>
								<label for="subscribe_on">Подписка</label>
							</div>
							<div class="checkbox_wrapper">
								<input type="checkbox" name="social_on" id="social_on" checked>
								<label for="social_on">Социальные сети</label>
							</div>
							<div class="checkbox_wrapper">
								<input type="checkbox" name="menu_on" id="menu_on" checked>
								<label for="menu_on">Разделы</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="settings_footer_wrapper">
			<div class="">
				<input type="reset" class="btn-reset" value="Сбросить все настройки">
			</div>
			<div class="">
				<input type="submit" class="btn-set" value="Применить настройки">
			</div>
		</div>
	</form>
</div>
<!-- боковая панель с настройками конец -->
