<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty('title', "Управление подпиской");
$APPLICATION->SetTitle("Управление подпиской");
$GLOBALS['personal_comments'] = array("PROPERTY_USER_ID" => CUser::GetID());
?>
<?$APPLICATION->IncludeComponent("firstbit:subscribe.edit","",Array(
	"AJAX_MODE" => "Y",
	"SHOW_HIDDEN" => "Y",
	"ALLOW_ANONYMOUS" => "Y",
	"SHOW_AUTH_LINKS" => "Y",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "3600",
	"SET_TITLE" => "Y",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"AJAX_OPTION_ADDITIONAL" => "personal"
)
);?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>