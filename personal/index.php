<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("showLeftMenu", "N");
$APPLICATION->SetPageProperty('title', "Персональный раздел");
$APPLICATION->SetTitle("Персональный раздел");
?><? $APPLICATION->IncludeComponent("bitrix:menu", "personal", Array(
	"ROOT_MENU_TYPE" => "left",
	"CHILD_MENU_TYPE" => "left_submenu",
	"MAX_LEVEL" => "2",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "Y",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => ""
)); ?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>