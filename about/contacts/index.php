<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty('title', "Контактная информация");
$APPLICATION->SetTitle("Контактная информация");

if(!CModule::IncludeModule("firstbit.beautyshop"))
	die();
$firstBit = new CFirstbitBeautyshop();
?>
	<div class="page_container">
		<?
		if($firstBit->options['COMPANY_NAME_HIDE'] != "Y") {
			?>
			<h2 class="company_name"><?= $firstBit->options['COMPANY_NAME'] ?></h2>
			<?
		}
		?>
		<?
		if($firstBit->options['COMPANY_ADDRESS_HIDE'] != "Y") {
			?>
			<div class="left_border">
				<b>Наш адрес</b>
				<span><?= $firstBit->options['COMPANY_ADDRESS'] ?></span>
			</div>
			<?
			}
		?>
		<?
		if($firstBit->options['COMPANY_PHONE_HIDE'] != "Y") {
			?>
			<div class="left_border">
				<b>Телефон</b>
				<span><?= $firstBit->options['COMPANY_PHONE'] ?></span>
			</div>
			<?
		}
		?>
		<?
		if($firstBit->options['COMPANY_EMAIL_HIDE'] != "Y") {
			?>
			<div class="left_border">
				<b>E-mail</b>
				<a href="mailto:<?= $firstBit->options['COMPANY_EMAIL'] ?>" class="email_link"><?= $firstBit->options['COMPANY_EMAIL'] ?></a>
			</div>
			<?
		}
		?>
		<?
		if($firstBit->options['COMPANY_COORDINATES_HIDE'] != "Y") {
			?>
			<div class="map_contacts">
				<div id="ya_map_container" style="width: 100%; height: 330px; margin-bottom: 40px;"></div>
			</div>
			<script>
				var myMap;
				function init() {
					var myMap = new ymaps.Map('ya_map_container', {
						center: [<?=$firstBit->options['COMPANY_COORDINATES']?>],
						zoom: 15,
						behaviors: ['default', 'scrollZoom'],
						controls: ['zoomControl']
					}),
					myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
						hintContent: 'Тут находится наша компания',
						balloonContent: '<b>Наш адрес</b><br><span><?=$firstBit->options['COMPANY_ADDRESS']?></span><br><b>Телефон</b><br><?= $firstBit->options['COMPANY_PHONE'] ?>'
					}, {
					});
					myMap.geoObjects.add(myPlacemark);
				};
			</script>
			<script src="https://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU&onload=init"></script>
			<?
		}
		?>
		<?$APPLICATION->IncludeComponent("firstbit:iblock.element.add.form","contacts_feedback",Array(
				"SEF_MODE" => "N",
				"IBLOCK_TYPE" => "firstbit_beautyshop_feedback",
				"IBLOCK_ID" => "13",
				"PROPERTY_CODES" => array("NAME","USER_ID","USER_NAME","FEEDBACK_TYPE","USER_EMAIL","MESSAGE","USER_IP"),
				"PROPERTY_CODES_REQUIRED" => array("NAME","USER_NAME","USER_EMAIL","MESSAGE"),
				"GROUPS" => array("2"),
				"STATUS_NEW" => "2",
				"STATUS" => array("2"),
				"LIST_URL" => "",
				"ELEMENT_ASSOC" => "PROPERTY_ID",
				"ELEMENT_ASSOC_PROPERTY" => "",
				"MAX_USER_ENTRIES" => "100000",
				"MAX_LEVELS" => "100000",
				"LEVEL_LAST" => "Y",
				"USE_CAPTCHA" => "N",
				"USER_MESSAGE_EDIT" => "",
				"USER_MESSAGE_ADD" => "",
				"DEFAULT_INPUT_SIZE" => "30",
				"RESIZE_IMAGES" => "Y",
				"MAX_FILE_SIZE" => "0",
				"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
				"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
				"CUSTOM_TITLE_NAME" => "",
				"CUSTOM_TITLE_TAGS" => "",
				"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
				"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
				"CUSTOM_TITLE_IBLOCK_SECTION" => "",
				"CUSTOM_TITLE_PREVIEW_TEXT" => "",
				"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
				"CUSTOM_TITLE_DETAIL_TEXT" => "",
				"CUSTOM_TITLE_DETAIL_PICTURE" => "",
				"SEF_FOLDER" => "/",
				"AJAX_MODE" => "Y",
				"AJAX_OPTION_SHADOW" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"VARIABLE_ALIASES" => Array(),
				"FEEDBACK_TYPE" => "#FEEDBACK_TYPE_PROPERTY_ID#"
			)
		);?>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>