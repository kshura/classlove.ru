<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>
<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "fly_basket", array(
	"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
	"COLUMNS_LIST" => array(
		0 => "NAME",
		1 => "DISCOUNT",
		2 => "PRICE",
		3 => "QUANTITY",
		6 => "DELETE",
	),
	"AJAX_MODE" => "Y",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "Y",
	"PATH_TO_ORDER" => "/personal/order/make/",
	"HIDE_COUPON" => "N",
	"QUANTITY_FLOAT" => "N",
	"PRICE_VAT_SHOW_VALUE" => "Y",
	"TEMPLATE_THEME" => "site",
	"SET_TITLE" => "Y",
	"AJAX_OPTION_ADDITIONAL" => "",
	"OFFERS_PROPS" => array(
		0 => "SIZES_SHOES",
		1 => "SIZES_CLOTHES",
		2 => "COLOR_REF",
	),
),
	false
);?>