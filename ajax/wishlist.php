<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>
<?
switch($_POST['action']) {
	case 'update':
		try {
			if (!isset($_POST['productID']))
				throw new Exception('ERROR PRODUCT ID');

			$productID = $_POST['productID'];

			if($USER->IsAuthorized()){
				if(!$arWishlist = CUserOptions::GetOption("custom", "wishlist")) {
					$wishlistAction = 'add';
					CUserOptions::SetOption("custom", "wishlist", array($_POST['productID']));
				} else {
					$wishlistAction = 'add';
					foreach($arWishlist as $wishlistIndex=>$wishlistID) {
						if($wishlistID > 0) {
							if ($wishlistID == $productID) {
								$wishlistAction = 'delete';
								unset($arWishlist[$wishlistIndex]);
								break;
							}
						} else {
							unset($arWishlist[$wishlistIndex]);
						}
					}
					if($wishlistAction == 'add') {
						$arWishlist[] = $productID;
					}
					CUserOptions::SetOption("custom", "wishlist", $arWishlist);
				}
			} else {
				if(!$arWishlist = $APPLICATION->get_cookie("WISHLIST")) {
					$arWishlist[] = $productID;
					$APPLICATION->set_cookie("WISHLIST", serialize($arWishlist), time()+60*60*24*30*12*2, "/");
					$wishlistAction = 'add';
				} else {
					$wishlistAction = 'add';
					$arWishlistTmp = unserialize($arWishlist);
					foreach($arWishlistTmp as $wishlistIndex=>$wishlistID) {
						if($wishlistID > 0) {
							if ($wishlistID == $productID) {
								$wishlistAction = 'delete';
								unset($arWishlistTmp[$wishlistIndex]);
								break;
							}
						} else {
							unset($arWishlistTmp[$wishlistIndex]);
						}
					}
					if($wishlistAction == 'add') {
						$arWishlistTmp[] = $productID;
					}
					$APPLICATION->set_cookie("WISHLIST", serialize($arWishlistTmp), time()+60*60*24*30*12*2, "/");
				}
			}
			$mes = $wishlistAction;
			if(!$mes) $mes = true;
			echo json_encode(array('status'=>true, 'mes'=>$mes));
		} catch(Exception $e) {
			echo json_encode(array('status'=>false, 'mes'=>'ERROR: '.$e->getMessage()));
		}
		break;
	case 'count':
		try {
			if($USER->IsAuthorized()){
				if(!$arWishlist = CUserOptions::GetOption("custom", "wishlist")) {
					$mes = 0;
				} else {
					$mes = count($arWishlist);
				}
			} else {
				if(!$arWishlist = $APPLICATION->get_cookie("WISHLIST")) {
					$mes = 0;
				} else {
					$mes = count(unserialize($arWishlist));
				}
			}
			if(!$mes && $mes!=0) $mes = 'true';
			echo json_encode(array('status'=>true, 'mes'=>$mes));
		} catch(Exception $e) {
			echo json_encode(array('status'=>false, 'mes'=>'ERROR: '.$e->getMessage()));
		}
		break;
}