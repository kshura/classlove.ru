<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php'); ?>
<?
$APPLICATION->SetTitle('Главная');
?>
<?$APPLICATION->IncludeComponent('firstbit:mainpage',
	'',
	array(
		"MAINPAGE_BLOCKS" => array('SLIDER', 'BANNERS', 'HITS', 'NEWPRODUCT', 'BRANDS', 'ADVANTAGES', 'NEWS', 'COMPANY', 'FEEDBACK', 'STORES'),
		"SLIDER_IBLOCK_ID" => "9",
		"BANNERS_IBLOCK_ID" => "10",
		"ADVANTAGES_IBLOCK_ID" => "11",
		"NEWS_IBLOCK_ID" => "5",
		"BRANDS_IBLOCK_ID" => "8",
		"FEEDBACK_IBLOCK_ID" => "13",
		"SALE_IBLOCK_ID" => "17",
		"CATALOG_IBLOCK_ID" => "15",
		"SITE_DIR" => "/",
		"PATH_TO_CATALOG" => "/catalog/",
		"PATH_TO_COMPARE" => "/catalog/compare/",
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_NEWS" => "/news/",
		"PATH_TO_BRANDS" => "/brands/",
		"PATH_TO_COMPANY" => "/about/company/",
		"PATH_TO_STORES" => "/stores/",
		"FILTER_HITS" => array("!PROPERTY_SALELEADER_VALUE" => false),
		"FILTER_NEWPRODUCT" => array("!PROPERTY_NEWPRODUCT_VALUE" => false),
		"SUBSCRIBE_RUBRIC_ID" => "5"
	),
	false,
	array('HIDE_ICONS' => 'Y'));
?>
<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>
