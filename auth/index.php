<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
$APPLICATION->SetPageProperty('title', "Авторизация");
$APPLICATION->SetPageProperty("showLeftMenu", "N");
?><?
if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0)
	LocalRedirect($backurl);

?>
<p>Вы зарегистрированы и успешно авторизовались.</p>

<p>Используйте административную панель в верхней части экрана для быстрого доступа к функциям управления структурой и информационным наполнением сайта. Набор кнопок верхней панели отличается для различных разделов сайта. Так отдельные наборы действий предусмотрены для управления статическим содержимым страниц, динамическими публикациями (новостями, каталогом, фотогалереей) и т.п.</p>

<p><a href="/" class="link_more">Вернуться на главную страницу<i class="fa fa-angle-right"></i></a></p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
