<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Магазины");
$APPLICATION->SetPageProperty('title', "Магазины");
$APPLICATION->SetPageProperty('showLeftMenu', "N");
?>
<?$APPLICATION->IncludeComponent("bitrix:catalog.store", "", Array(
	"PHONE" => "Y",
	"SCHEDULE" => "Y",
	"PATH_TO_ELEMENT" => "/store/#store_id#/",
	"MAP_TYPE" => "0",
	"SET_TITLE" => "Y",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"COMPONENT_TEMPLATE" => ".default",
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "/stores/",
	"TITLE" => "Список магазинов",
	"SEF_URL_TEMPLATES" => array(
		"liststores" => "index.php",
		"element" => "#store_id#/",
	)
),
false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>